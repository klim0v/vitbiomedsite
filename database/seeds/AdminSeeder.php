<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datetime = date('Y-m-d H:i:s');

        $admins[] = [
            'group_id' => 1,
            'email'    => 'admin@vitbiomed.ru',
            'password' => bcrypt('848451'),
            'created_at' => $datetime,
            'updated_at' => $datetime
        ];

        \Illuminate\Support\Facades\DB::table('users')
            ->insert($admins);
    }
}
