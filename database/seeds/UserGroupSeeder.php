<?php

use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_groups[] = ['name' => 'Администратор'];
        $user_groups[] = ['name' => 'Модератор'];
        $user_groups[] = ['name' => 'Пользователь'];

        \Illuminate\Support\Facades\DB::table('user_groups')
            ->insert($user_groups);
    }
}
