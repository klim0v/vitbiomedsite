<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserGroupSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(ShopSeeder::class);
//        $this->call(CouponSeeder::class);
    }
}
