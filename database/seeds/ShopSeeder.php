<?php

use App\Model\Coupon;
use App\Model\Shop\Course\Course;
use App\Model\Shop\Product\Component;
use App\Model\Shop\Product\Product;
use App\Model\Shop\Product\Variation;
use App\Model\Slider;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     * @throws Exception
     */
    public function run()
    {
        Coupon::create([
            'code'  => 'ABC123',
            'type'  => 'fixed',
            'value' => 100,
        ]);
    
        Coupon::create([
            'code'  => 'DEF456',
            'type'  => 'percent',
            'value' => 50,
        ]);
        
        $courseCategories = [
            [
                'id'   => 1,
                'name' => 'Профилактика заболеваний',
            ],
            [
                'id'   => 2,
                'name' => 'Очищение и оздоровление',
            ],
        ];
    
        \Illuminate\Support\Facades\DB::table('course_categories')
            ->insert($courseCategories);
    
        $productCategories = [
            [
                'id'   => 1,
                'name' => 'Культуральные',
            ],
            [
                'id'   => 2,
                'name' => 'Кисломолочные',
            ],
            [
                'id'   => 3,
                'name' => 'Таблетки и капсулы',
            ],
            [
                'id'   => 4,
                'name' => 'Споровые',
            ],
        ];
    
        \Illuminate\Support\Facades\DB::table('product_categories')
            ->insert($productCategories);
    
        $ageCategories = [
            [
                'id'   => 1,
                'key'  => 'kids',
                'name' => 'Детям',
            ],
            [
                'id'   => 2,
                'key'  => 'adult',
                'name' => 'Взрослым',
            ],
        ];
    
        \Illuminate\Support\Facades\DB::table('age_categories')
            ->insert($ageCategories);
    
        /** @var Collection|Component[] $coms */
        $coms = factory(Component::class, 10)->create();
        /** @var Collection|Product[] $prods */
        $prods = factory(Product::class, 10)->create();
    
        $vars = [];
        $prods->each(function ($prod, $i) use ($coms, &$vars) {
            /** @var Collection|Variation[] $models */
            $models = factory(Variation::class, random_int(1, 3))->make();
            $vars[$i] = $models;
            /** @var Product $prod */
            $prod->productVariations()->saveMany($models);
            $prod->productComponents()->attach($coms->random(4));
        });
        /** @var Collection|Course[] $courses */
        $courses = factory(Course::class, 10)->create();
        $courses->each(function ($course) use ($vars) {
            /** @var Variation[] $randVars */
            $randKeys = array_rand($vars, random_int(2, 6));
            /** @var Collection|Variation[] $var */
            /** @var Course $course */
    
            foreach ($randKeys as $key) {
                $course->productVariations()->attach([$vars[$key]->random()->id => ['count' => random_int(1, 5)]]);
            }
        });
        /** @var Course $course */
        foreach ($courses as $course) {
            $course->ageCategories()->attach(random_int(0, 1) ? 2 : [1, 2]);
        }
    
        foreach ($courses->random(2) as $course) {
            factory(Slider::class, 1)->create([
                'link' => route('course.index', $course->slug),
            ]);
        }
    
        factory(\App\User::class)->create(['email' => bcrypt('test@vitbiomed.ru')]);
    }
}
