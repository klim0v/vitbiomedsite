<?php

use App\Model\Shop\Course\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2),
        'keyword' => $faker->sentence(random_int(5,10)),
        'subtitle' => $faker->sentence(random_int(5,10)),
        'annotation' => $faker->sentence(random_int(5,10)),
        'image' => $faker->imageUrl(640, 480),
        'thumb' =>$faker->imageUrl(320, 240),
        'description' => $faker->sentence(random_int(5,10)),
        'composition' => $faker->sentence(random_int(5,10)),
        'duration' => $faker->sentence(random_int(5,10)),
        'for_whom' => $faker->sentence(random_int(5,10)),
        'features' => $faker->sentence(random_int(5,10)),
        'price' => random_int(99,9999),
        'category_id' => random_int(1,2),

        'title' => $faker->sentence(3),
        'heading' => $faker->sentence(3),
        'meta_title' => $faker->sentence(3),
        'meta_description' => $faker->sentence(10),
        'slug' => $faker->slug
    ];
});
