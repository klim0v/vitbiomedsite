<?php

use App\Model\Shop\Product\Component;
use Faker\Generator as Faker;

$factory->define(Component::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(1),
        'icon' => $faker->imageUrl(50, 50),
    ];
});
