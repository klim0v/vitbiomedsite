<?php

use App\Model\Shop\Product\Variation;
use Faker\Generator as Faker;

$factory->define(Variation::class, function () {
    $variants = [60 => 290, 150 => 715, 300 => 1415];
    $variant = array_rand($variants);
    return [
        'volume' => $variant + random_int(-10, 50),
        'price' => $variants[$variant] + random_int(-50, 50),
    ];
});
