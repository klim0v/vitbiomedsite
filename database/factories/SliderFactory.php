<?php

use App\Model\Slider;
use Faker\Generator as Faker;

$factory->define(Slider::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(2),
        'subtitle' => null,
        'description' => $faker->sentence(random_int(5,10)),
        'image' => $faker->imageUrl(1246, 255),
        'link' => $faker->url,
    ];
});
