<?php

use App\Model\Shop\Product\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2),
        'word' => strtoupper($faker->randomLetter),
        'keyword' => $faker->word,
        'subtitle' => $faker->sentence(random_int(5,10)),
        'annotation' => $faker->sentence(random_int(5,10)),
        'description' => $faker->sentence(random_int(5,10)),
        'composition' => $faker->sentence(random_int(5,10)),
        'effects_on_the_body' => $faker->sentence(random_int(5,10)),
        'indications_for_use' => $faker->sentence(random_int(5,10)),
        'how_to_use' => $faker->sentence(random_int(5,10)),
        'storage_conditions' => $faker->sentence(random_int(5,10)),
        'image' => $faker->imageUrl(640, 480),
        'thumb' => $faker->imageUrl(320, 240),
        'product_category_id' => random_int(1, 4),


        'title' => $faker->sentence(3),
        'heading' => $faker->sentence(3),
        'meta_title' => $faker->sentence(3),
        'meta_description' => $faker->sentence(10),
        'slug' => $faker->slug
    ];
});
