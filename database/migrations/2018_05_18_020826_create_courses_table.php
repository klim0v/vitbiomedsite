<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('keyword')->nullable();
            $table->string('subtitle');
            $table->string('image')->nullable();
            $table->string('thumb')->nullable();
            $table->text('annotation');
            $table->text('description');
            $table->text('composition');
            $table->text('for_whom')->nullable();
            $table->text('features')->nullable();
            $table->text('why_probiotics')->nullable();
            $table->text('duration');
            $table->unsignedInteger('price');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')
                ->on('course_categories')->onDelete('cascade');
            $table->timestamps();

            $table->string('title');
            $table->string('heading');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('slug')->unique();
            $table->string('og_title')->nullable();
            $table->string('og_type')->nullable();
            $table->text('og_description')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
