<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('heading');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('slug')->unique()->index();
            $table->text('annotation')->nullable();
            $table->text('text')->nullable();
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('image_author')->nullable();
            $table->boolean('is_favorite')->default(0);
            $table->boolean('is_hidden')->default(0);
            $table->integer('publication_type_id')->unsigned();
            $table->foreign('publication_type_id')->references('id')->on('publication_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
