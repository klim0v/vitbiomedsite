<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataToBillsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->unsignedInteger('type_delivery');
            $table->unsignedInteger('type_payment');
            $table->text('comment')->nullable();
            $table->text('note')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->string('customer_phone');
            $table->string('customer_name');
            $table->string('delivery_cost')->default(0);
            $table->string('delivery_index')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('delivery_date')->nullable();
            $table->string('delivery_time')->nullable();
            $table->unsignedInteger('current_status');
        });
    }
    
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->dropColumn([
                'type_delivery',
                'type_payment',
                'comment',
                'note',
                'cancel_reason',
                'customer_phone',
                'customer_name',
                'delivery_cost',
                'delivery_index',
                'delivery_address',
                'delivery_date',
                'delivery_time',
                'current_status',
            ]);
        });
    }
}
