<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('word');
            $table->string('keyword')->nullable();
            $table->string('subtitle');
            $table->string('image')->nullable();
            $table->string('thumb')->nullable();
            $table->text('annotation');
            $table->text('description');
            $table->text('composition');
            $table->text('effects_on_the_body')->nullable();
            $table->text('indications_for_use')->nullable();
            $table->text('how_to_use')->nullable();
            $table->text('storage_conditions')->nullable();
            $table->unsignedInteger('product_category_id');
            $table->foreign('product_category_id')->references('id')
                ->on('product_categories')->onDelete('cascade');
            $table->timestamps();


            $table->string('title');
            $table->string('heading');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('slug')->unique();
            $table->string('og_title')->nullable();
            $table->string('og_type')->nullable();
            $table->text('og_description')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
