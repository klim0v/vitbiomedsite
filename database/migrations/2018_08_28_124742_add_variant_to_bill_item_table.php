<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVariantToBillItemTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('bill_item', function (Blueprint $table) {
            $table->unsignedInteger('variant_id');
        });
    }
    
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('bill_item', function (Blueprint $table) {
            $table->dropColumn('variant_id');
        });
    }
}
