<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->integer('doctor_id')->unsigned()->nullable();
            $table->tinyInteger('rating')->unsigned();  //рейтинг - от 1 до 5
            $table->string('facebook')->nullable();     //ссылка на фейсбук
            $table->string('thumbnail')->nullable();    //миниатюра
            $table->text('text');
            $table->boolean('is_hidden')->default(0);
            $table->timestamps();

            $table->index('doctor_id');
            $table->foreign('doctor_id')
                ->references('id')
                ->on('doctors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
