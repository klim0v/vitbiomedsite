<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');                //полное имя врача
            $table->string('speciality');               //специальность
            $table->string('work_experience');          //стаж работы
            $table->text('description');                //описание
            $table->text('experience');                 //опыт работы
            $table->string('video')->nullable();                    //youtube ссылка
            $table->string('cover')->nullable();                    //фоновое изображение на странице врача
            $table->string('thumbnail')->nullable();                //миниатюра
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
