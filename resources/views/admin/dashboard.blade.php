<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Заявки</h1>

            <table class="uk-table uk-table-middle uk-table-divider">
                <thead>
                <tr>
                    <th class="uk-width-expand">ID</th>
                    <th class="uk-width-expand">Описание заявки</th>
                    <th class="uk-table-shrink"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>
                            {{ $order->full_name }} хочет записатся
                            @if ($order->doctor) к врачу "{{ $order->doctor->full_name }}"
                            @elseif ($order->service) на услугу "{{ $order->service->name }}"
                            @else на приём. @endif
                            <br>
                            Контактные данные: <a href="tel:{{ $order->phone }}">{{ $order->phone }}</a>,
                            <a href="{{ $order->email }}">{{ $order->email }}</a>
                        </td>
                        <td id="button_id_{{ $order->id }}">
                            @if ($order->processed)
                            <button class="uk-button uk-button-link" type="button" disabled>Готово</button>
                            @else
                            <a class="uk-button uk-button-link processe" attr-id="{{ $order->id }}">Завершить</a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">Заказов в базе нет</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            {{ $orders->links('vendor.pagination.default') }}
        </section>
    </div>
</div>

<script>
    $('.processe').on('click', function () {
        var id = $(this).attr('attr-id');
        console.log(id);
        $.post('/admin/order/processe/' + id, null, function (data) {
            if (data.status == 'success') {
                $('#button_id_' + id).html('<button class="uk-button uk-button-link" type="button" disabled>Готово</button>');

                UIkit.notification({
                    message: "<span uk-icon='icon: check'></span> заявка обработана",
                    status: 'success',
                    pos: 'top-center',
                    timeout: 5000
                });
            } else {
                UIkit.notification({
                    message: "<span uk-icon='icon: warning'></span> не удалось обработать заявку",
                    status: 'danger',
                    pos: 'top-center',
                    timeout: 5000
                });
            }
        });
    });
</script>
</body>
</html>