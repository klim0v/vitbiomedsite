<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
    <script src="/js/ckeditor/ckeditor.js"></script>
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Публикация</h1>

            <a class="uk-button uk-button-default" href="{{ route('admin.publications') }}">Вернуться назад</a>

            <div class="uk-overflow-auto" style="margin-top: 60px;">
                <form method="post" enctype="multipart/form-data"
                      action="{{ route('admin.publications.save', ['id' => $publication ? $publication->id : null]) }}"
                      class="uk-form-horizontal uk-margin-large">
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-check">Скрытый</label>
                        <div class="uk-form-controls">
                            <input class="uk-checkbox"
                                   type="checkbox" name="is_hidden" id="form-check" value="1"
                                   @if( old('is_hidden', $publication ? $publication->is_hidden : '')) checked @endif
                            >
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Название</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('title')) uk-form-danger @endif"
                                   type="text" name="title" placeholder=""
                                   value="{{ old('title', $publication ? $publication->title : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Заголовок H1</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('heading')) uk-form-danger @endif"
                                   type="text" name="heading" placeholder=""
                                   value="{{ old('heading', $publication ? $publication->heading : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Ссылка</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('slug')) uk-form-danger @endif"
                                   type="text" name="slug" placeholder=""
                                   value="{{ old('slug', $publication ? $publication->slug : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Рубрика</label>
                        <div class="uk-form-controls">
                            <select class="uk-input @if ($errors->has('publication_type_id')) uk-form-danger @endif"
                                    name="publication_type_id" class="form-control">
                                @foreach ($publicationTypes as $publicationType)
                                    <option value="{{ $publicationType->id }}"
                                            @if( $publicationType->id == old('publication_type_id', $publication ? $publication->publication_type_id : '')) selected @endif >
                                        {{ $publicationType->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-check">Популярная</label>
                        <div class="uk-form-controls">
                            <input class="uk-checkbox"
                                   type="checkbox" name="is_favorite" id="form-check" value="1"
                                   @if( old('is_favorite', $publication ? $publication->is_favorite : '')) checked @endif
                            >
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Текст</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('text')) uk-form-danger @endif"
                                      rows="5" name="text" id="editor_1">
                            {{ old('text', $publication ? $publication->text : '') }}
                            </textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Аннотация</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('annotation')) uk-form-danger @endif"
                                      rows="5" name="annotation" id="editor_2">
                            {{ old('annotation', $publication ? $publication->annotation : '') }}
                            </textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Meta title</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('meta_title')) uk-form-danger @endif"
                                   type="text" name="meta_title" placeholder=""
                                   value="{{ old('meta_title', $publication ? $publication->meta_title : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Meta description</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('meta_description')) uk-form-danger @endif"
                                   type="text" name="meta_description" placeholder=""
                                   value="{{ old('meta_description', $publication ? $publication->meta_description : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">og:title</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('og_title')) uk-form-danger @endif"
                                   type="text" name="og_title" placeholder=""
                                   value="{{ old('og_title', $publication ? $publication->og_title : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">og:type</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('og_type')) uk-form-danger @endif"
                                   type="text" name="og_type" placeholder="article"
                                   value="{{ old('og_type', $publication ? $publication->og_type : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">og:description</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('og_description')) uk-form-danger @endif"
                                   type="text" name="og_description" placeholder=""
                                   value="{{ old('og_description', $publication ? $publication->og_description : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">og:url</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('og_url')) uk-form-danger @endif"
                                   type="text" name="og_url" placeholder="og_url"
                                   value="{{ old('og_url', $publication ? $publication->og_url : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">og:image</label>
                        <div class="uk-form-controls">
                            @if ($publication && !empty($publication->image))
                                <div class="uk-cover-container">
                                    <img src="/storage/{{ $publication->image }}"
                                         style="max-height: 160px; max-width: 160px;" alt="">
                                </div>
                            @endif
                            <div uk-form-custom="target: true">
                                <input name="og_image" type="file">
                                <input class="uk-input uk-form-width-medium"
                                       type="text" placeholder="Выбрать фото..." disabled>
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Картинка</label>
                        <div class="uk-form-controls">
                            @if ($publication && !empty($publication->image))
                                <div class="uk-cover-container">
                                    <img src="/storage/{{ $publication->image }}"
                                         style="max-height: 160px; max-width: 160px;" alt="">
                                </div>
                                <br>
                                <input class="uk-checkbox"
                                       type="checkbox" name="delete_image" id="form-check" value="1">
                                <br>
                                <label class="uk-form-label" for="delete_image">Удалить изображение</label>
                                <br>
                                <br>
                            @endif
                            <div uk-form-custom="target: true">
                                <input name="image" type="file">
                                <input class="uk-input uk-form-width-medium"
                                       type="text" placeholder="Выбрать фото..." disabled>
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Автор фотографии</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('image_author')) uk-form-danger @endif"
                                   type="text" name="image_author" placeholder=""
                                   value="{{ old('image_author', $publication ? $publication->image_author : '') }}">
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary" type="submit"
                            style="margin-top: 20px; margin-bottom: 60px;">Сохранить
                    </button>
                </form>
            </div>
        </section>
    </div>
</div>

<script>
    CKEDITOR.replace('editor_1');
    CKEDITOR.replace('editor_2');
</script>
</body>
</html>