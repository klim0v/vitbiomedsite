<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Публикации</h1>

            <a class="uk-button uk-button-primary" href="{{ route('admin.publications.edit') }}">Добавить публикацию</a>

            <div class="uk-overflow-auto">
                <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                    <thead>
                    <tr>
                        <th class="uk-table-shrink"></th>
                        <th class="uk-table-shrink">Фото</th>
                        <th class="uk-table-expand">Название</th>
                        <th class="uk-table-expand">Рубрика</th>
                        <th class="uk-table-expand">Ссылка</th>
                        <th class="uk-table-shrink uk-text-nowrap uk-text-center">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($publications as $publication)
                    <tr>
                        <td>{{ $publication->id }}</td>
                        <td><img class="uk-preserve-width uk-border-circle"
                                 @if(!empty($publication->thumbnail))
                                    src="/storage/{{ $publication->thumbnail }}"
                                 @endif width="40" alt="">
                        </td>
                        <td class="uk-table-link">
                            {{ $publication->title }}
                        </td>
                        <td class="uk-table-link">
                            {{ $publication->publicationType->title }}
                        </td>
                        <td class="uk-table-link">
                            <a class="uk-link-reset"
                               href="{{ route('publication.index', ['type' => $publication->publicationType->slug, 'publication' =>$publication->slug]) }}">{{ $publication->slug }}</a>
                        </td>
                        <td class="uk-text-nowrap">
                            <a class="uk-button uk-button-link uk-display-block uk-width-1-1"
                               href="{{ route('admin.publications.edit', ['id' => $publication->id]) }}">Изменить</a>
                            <a class="uk-button uk-button-link uk-display-block uk-width-1-1 uk-text-danger"
                               href="{{ route('admin.publications.delete', ['id' => $publication->id]) }}">Удалить</a>
                            <a class="uk-button uk-button-link uk-display-block uk-width-1-1 uk-text-warning"
                               href="{{ route('admin.publications.hide', ['id' => $publication->id]) }}">
                                @if($publication->is_hidden)
                                    Показать
                                @else
                                    Скрыть
                                @endif
                            </a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4">Данных в базе нет</td>
                    </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            {{ $publications->links('vendor.pagination.default') }}
        </section>
    </div>
</div>

</body>
</html>