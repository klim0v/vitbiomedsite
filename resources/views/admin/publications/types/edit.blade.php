<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
    <script src="/js/ckeditor/ckeditor.js"></script>

</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Рубрика</h1>

            <a class="uk-button uk-button-default" href="{{ route('admin.publication.types') }}">Вернуться назад</a>

            <div class="uk-overflow-auto" style="margin-top: 60px;">
                <form method="post" enctype="multipart/form-data"
                      action="{{ route('admin.publication.types.save', ['id' => $publicationType ? $publicationType->id : null]) }}"
                      class="uk-form-horizontal uk-margin-large">
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Название</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('title')) uk-form-danger @endif"
                                   type="text" name="title" placeholder=""
                                   value="{{ old('title', $publicationType ? $publicationType->title : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Заголовок H1</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('heading')) uk-form-danger @endif"
                                   type="text" name="heading" placeholder=""
                                   value="{{ old('heading', $publicationType ? $publicationType->heading : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Ссылка</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('slug')) uk-form-danger @endif"
                                   type="text" name="slug" placeholder=""
                                   value="{{ old('slug', $publicationType ? $publicationType->slug : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Текст</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('text')) uk-form-danger @endif"
                                      rows="5" name="text" placeholder="" id="editor">
                                {{ old('text', $publicationType ? $publicationType->text : '') }}
                            </textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Meta title</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('meta_title')) uk-form-danger @endif"
                                   type="text" name="meta_title" placeholder=""
                                   value="{{ old('meta_title', $publicationType ? $publicationType->meta_title : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Meta description</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('meta_description')) uk-form-danger @endif"
                                   type="text" name="meta_description" placeholder=""
                                   value="{{ old('meta_description', $publicationType ? $publicationType->meta_description : '') }}">
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary" type="submit" style="margin-top: 20px; margin-bottom: 60px;">Сохранить</button>
                </form>
            </div>
        </section>
    </div>
</div>

<script>
    CKEDITOR.replace( 'editor' );
</script>
</body>
</html>