<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Услуги</h1>

            <a class="uk-button uk-button-primary" href="{{ route('admin.services.edit') }}">Добавить услугу</a>

            <table class="uk-table uk-table-middle uk-table-divider">
                <thead>
                <tr>
                    <th class="uk-width-expand">ID</th>
                    <th class="uk-width-shrink">Название</th>
                    <th class="uk-width-shrink">Цена</th>
                    <th class="uk-table-shrink">Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse($services as $service)
                <tr>
                    <td>{{ $service->id }}</td>
                    <td>
                        {{ $service->name }}
                    </td>
                    <td>
                        {{ $service->price }}₽
                    </td>
                    <td>
                        <a href="{{ route('admin.services.edit', ['id' => $service->id]) }}" class="uk-button uk-button-link uk-display-block uk-width-1-1" type="button">Изменить</a>
                        <a href="{{ route('admin.services.delete', ['id' => $service->id]) }}" class="uk-button uk-button-link uk-display-block uk-width-1-1 uk-text-danger" type="button">Удалить</a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">Данных в базе нет</td>
                </tr>
                @endforelse
                </tbody>
            </table>

            {{ $services->links('vendor.pagination.default') }}
        </section>
    </div>
</div>

</body>
</html>