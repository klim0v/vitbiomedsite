<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
    <div class="uk-grid-match uk-child-width-1-5@m" uk-grid>
        <div></div>
        <div></div>
        <div>
            <div style="margin-top: 120px;"></div>
            <form action="" method="post">
                {{ csrf_field() }}
                <div class="uk-margin">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input" value="{{ old('email') }}" name="email" type="email" required placeholder="admin@vitbiomed.ru">
                    </div>
                </div>

                <div class="uk-margin">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: lock"></span>
                        <input class="uk-input" value="{{ old('password') }}" name="password" type="password" required placeholder="******">
                    </div>
                </div>

                <input class="uk-button uk-button-primary" type="submit"/>
            </form>
        </div>
    </div>

    @if ($errors->has('*'))
        <script>
            UIkit.notification({
                message: "<span uk-icon='icon: warning'></span> Не правильно введен логин или пароль",
                status: 'danger',
                pos: 'top-center',
                timeout: 5000
            });
        </script>
    @endif
</body>
</html>