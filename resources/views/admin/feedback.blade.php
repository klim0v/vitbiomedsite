<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Отзыв</h1>

            <a class="uk-button uk-button-default">Вернуться назад</a>

            <div class="uk-overflow-auto" style="margin-top: 60px;">
                <form class="uk-form-horizontal uk-margin-large" enctype="multipart/form-data" action="{{ route('admin.feedbacks.save', ['id' => $feedback ? $feedback->id : null]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Полное Имя клиента</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('full_name')) uk-form-danger @endif" type="text" name="full_name" placeholder="Иванов Иван Иванович" value="{{ old('full_name', $feedback ? $feedback->full_name : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Ссылка на facebook</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('facebook')) uk-form-danger @endif" type="text" name="facebook" placeholder="https://www.facebook.com/profile.php?id=1" value="{{ old('facebook', $feedback ? $feedback->facebook : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Оценка</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('rating')) uk-form-danger @endif" type="number" min="1" name="rating" max="5" placeholder="5" value="{{ old('rating', $feedback ? $feedback->rating : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Миниатюра клиента</label>
                        <div class="uk-form-controls">
                            @if ($feedback && !empty($feedback->thumbnail))
                                <div class="uk-cover-container">
                                    <img src="/storage/{{ $feedback->thumbnail }}" style="max-height: 160px; max-width: 160px;" alt="">
                                </div>
                            @endif
                            <div uk-form-custom="target: true">
                                <input name="thumbnail" type="file">
                                <input class="uk-input uk-form-width-medium" type="text" placeholder="Выбрать фото..." disabled>
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Текст отзыва</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('text')) uk-form-danger @endif" rows="5" name="text" placeholder="Были на приеме вчера. Все понравилось.">{{ old('text', $feedback ? $feedback->text : '') }}</textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Отзыв для врача:</label>
                        <div class="uk-form-controls">
                            <select class="uk-select @if ($errors->has('doctor_id')) uk-form-danger @endif" name="doctor_id">
                                <option value="">Без привязки к врачу</option>
                                @foreach($doctors as $doctor)
                                    <option value="{{ $doctor->id }}" @if (old('doctor_id', $feedback ? $feedback->doctor_id : '') == $doctor->id) selected @endif>{{ $doctor->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary" type="submit" style="margin-top: 20px; margin-bottom: 60px;">Сохранить</button>
                </form>
            </div>
        </section>
    </div>
</div>

</body>
</html>