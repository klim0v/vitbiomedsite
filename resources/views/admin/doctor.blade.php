<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Врач</h1>

            <a class="uk-button uk-button-default" href="{{ route('admin.doctors') }}">Вернуться назад</a>

            <div class="uk-overflow-auto" style="margin-top: 60px;">
                <form class="uk-form-horizontal uk-margin-large" enctype="multipart/form-data" action="{{ route('admin.doctors.save', ['id' => $doctor ? $doctor->id : null]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Полное Имя</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('full_name')) uk-form-danger @endif" type="text" name="full_name" placeholder="Иванов Ивано Иванович" value="{{ old('full_name', $doctor ? $doctor->full_name : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Специальность</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('speciality')) uk-form-danger @endif" type="text" name="speciality" placeholder="Терапевт, Кардиолог-ревматолог." value="{{ old('speciality', $doctor ? $doctor->speciality : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Стаж</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('work_experience')) uk-form-danger @endif" type="text" name="work_experience" placeholder="20 лет" value="{{ old('work_experience', $doctor ? $doctor->work_experience : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Описание</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('description')) uk-form-danger @endif" rows="5" name="description" placeholder="Основатель медицинского центра “Витбиомед”, автор уникальной методики лечения дисбатериоза. Основатель медицинского центра “Витбиомед”, автор уникальной методики лечения дисбатериоза. Основатель медицинского центра “Витбиомед”, автор уникальной методики лечения дисбатериоза.">{{ old('description', $doctor ? $doctor->description : '') }}</textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Опыт работы</label>
                        <div class="uk-form-controls">
                            <textarea class="uk-textarea @if ($errors->has('experience')) uk-form-danger @endif" rows="5" name="experience" placeholder="В 2001 году занимал должность врача кардиореанимации РКБ№2.
                                С 2002 года работал врачом кардиореанимации НИИСП им. Н.В. Склифосовского.">{{ old('experience', $doctor ? $doctor->experience : '') }}</textarea>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Видео (ссылка на youtube)</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('video')) uk-form-danger @endif" type="text" name="video" placeholder="https://www.youtube.com/watch?v=123456" value="{{ old('video', $doctor ? $doctor->video : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Обложка врача</label>
                        <div class="uk-form-controls">
                            @if ($doctor && !empty($doctor->cover))
                            <div id="cover" class="uk-cover-container">
                                <img src="/storage/{{ $doctor->cover }}" style="max-height: 160px; max-width: 160px;" alt="">
                            </div>
                            <a href="#" onclick="deleteCoverImage(this); return false;">Удалить</a>
                            @endif
                            <div uk-form-custom="target: true">
                                <input name="cover" type="file">
                                <input class="uk-input uk-form-width-medium" type="text" placeholder="Выбрать фото..." disabled>
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Миниатюра врача</label>
                        <div class="uk-form-controls">
                            @if ($doctor && !empty($doctor->thumbnail))
                            <div id="thumb" class="uk-cover-container">
                                <img src="/storage/{{ $doctor->thumbnail }}" style="max-height: 160px; max-width: 160px;" alt="">
                            </div>
                            <a href="#" onclick="deleteThumbImage(this); return false;">Удалить</a>
                            @endif
                            <div uk-form-custom="target: true">
                                <input name="thumbnail" type="file">
                                <input class="uk-input uk-form-width-medium" type="text" placeholder="Выбрать фото..." disabled>
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Время работы</label>
                        <div class="uk-form-controls">
                            @if ($doctor)
                                @forelse($doctor->schedule as $time)
                                    <input class="uk-input uk-margin-small-bottom" type="text" name="working_time[{{ $loop->iteration }}]" placeholder="вт, чт: 12:00 – 20:00" value="{{ $time->row }}">
                                @empty
                                    <input class="uk-input uk-margin-small-bottom" type="text" name="working_time[1]" placeholder="вт, чт: 12:00 – 20:00">
                                @endforelse
                                <a id="add_working_time" class="uk-button uk-button-text" href="#" attr-i="{{ $doctor->schedule->count() ? $doctor->schedule->count() + 1 : 2 }}">Добавить еще поле</a>
                            @else
                                <input class="uk-input uk-margin-small-bottom" type="text" name="working_time[1]" placeholder="вт, чт: 12:00 – 20:00">
                                <a id="add_working_time" class="uk-button uk-button-text" href="#" attr-i="2">Добавить еще поле</a>
                            @endif
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary" type="submit" style="margin-top: 20px; margin-bottom: 60px;">Сохранить</button>
                </form>
            </div>
        </section>
    </div>
</div>

<script>
    $('#add_working_time').on('click', function (e) {
        e.preventDefault();
        $(this).before('<input class="uk-input uk-margin-small-bottom" type="text" name="working_time['+parseInt($(this).attr('attr-i'))+']" placeholder="вт, чт: 12:00 – 20:00">');
        $(this).attr('attr-i', parseInt($(this).attr('attr-i')) + 1);
    });

    function deleteCoverImage(button) {
        $.get("{{ route('admin.doctors.cover.delete', ['id' => $doctor ? $doctor->id : null ]) }}", {}, function () {
            $(button).remove();
            $('#cover').remove();
        });
    }

    function deleteThumbImage(button) {
        $.get("{{ route('admin.doctors.thumb.delete', ['id' => $doctor ? $doctor->id : null ]) }}", {}, function () {
            $(button).remove();
            $('#thumb').remove();
        });
    }


</script>

</body>
</html>
