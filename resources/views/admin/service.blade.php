<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Услуга</h1>

            <a class="uk-button uk-button-default" href="{{ route('admin.services') }}">Вернуться назад</a>

            <div class="uk-overflow-auto" style="margin-top: 60px;">
                <form class="uk-form-horizontal uk-margin-large" action="{{ route('admin.services.save', ['id' => $service ? $service->id : null]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Название</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('name')) uk-form-danger @endif" type="text" name="name"  placeholder="Приём терапевта амбулаторный" value="{{ old('name', $service ? $service->name : '') }}">
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-text">Категория</label>
                        <div class="uk-form-controls">
                            <select class="uk-select @if ($errors->has('category_id')) uk-form-danger @endif" name="category_id">
                                <option value="">Выберите категорию...</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if (old('category_id', $service ? $service->category_id : '') == $category->id) selected @endif>{{ $category->name }}</option>
                                @endforeach
                                <option id="create_category" value="" @if (old('new_category', false) === true or $errors->has('category_id')) selected @endif>Создать новую категорию</option>
                            </select>
                            <input id="new_category" class="uk-input uk-margin-small-bottom uk-margin-small-top" style="@if (old('new_category', false) === false) display: none; @endif" type="text" name="new_category" placeholder="Педиатрия" value="{{ old('new_category') }}">
                        </div>
                    </div>

                        <label class="uk-form-label" for="form-horizontal-text">Цена</label>
                        <div class="uk-form-controls">
                            <input class="uk-input @if ($errors->has('price')) uk-form-danger @endif" type="number" name="price" placeholder="1300" value="{{ old('price', $service ? $service->price : '') }}">
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary" type="submit" style="margin-top: 20px; margin-bottom: 60px;">Сохранить</button>
                </form>
            </div>
        </section>
    </div>
</div>

<script>
    $('select[name="category_id"]').on('change', function () {
        if ($('#create_category', this).is(':selected')) {
            $('#new_category').show();
        } else {
            $('#new_category').val('');
            $('#new_category').hide();
        }

        $(this).removeClass('uk-form-danger');
    });

    $('input').on('input', function () {
        $(this).removeClass('uk-form-danger');
    });
</script>

</body>
</html>