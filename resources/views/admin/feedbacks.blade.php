<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration Panel</title>

    @include('include.scripts')
</head>
<body>
<div class="uk-grid-match uk-child-width-1-4@m" uk-grid>
    <div></div>
    <div class="uk-width-1-2">
        @include('include.navigation')

        <section>

            <h1 style="margin-top: 60px; margin-bottom: 40px;">Отзывы</h1>

            <a class="uk-button uk-button-primary" href="{{ route('admin.feedbacks.edit') }}">Добавить отзыв</a>

            <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                <thead>
                <tr>
                    <th class="uk-table-shrink"></th>
                    <th class="uk-table-shrink">Фото</th>
                    <th class="uk-table-small">Имя</th>
                    <th class="uk-width-shrink">Отзыв</th>
                    <th class="uk-table-shrink uk-text-nowrap uk-text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse($feedbacks as $feedback)
                <tr>
                    <td>{{ $feedback->id }}</td>
                    <td><img class="uk-preserve-width uk-border-circle" @if (!empty($feedback->thumbnail)) src="/storage/{{ $feedback->thumbnail }}" @endif width="40" alt=""></td>
                    <td class="uk-table-link">
                        <a class="uk-link-reset" href="">{{ $feedback->full_name }}</a>
                    </td>
                    <td class="uk-text-break">{{ str_limit($feedback->text, 100) }}</td>
                    <td class="uk-text-nowrap">
                        <a class="uk-button uk-button-link uk-display-block uk-width-1-1" href="{{ route('admin.feedbacks.edit', ['id' => $feedback->id]) }}">Изменить</a>
                        <a class="uk-button uk-button-link uk-display-block uk-width-1-1 uk-text-danger" href="{{ route('admin.feedbacks.delete', ['id' => $feedback->id]) }}">Удалить</a>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="5">Данных в базе нет</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            {{ $feedbacks->links('vendor.pagination.default') }}
        </section>
    </div>
</div>

</body>
</html>