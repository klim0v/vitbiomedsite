<nav class="uk-navbar-container" uk-navbar>

    <div class="uk-navbar-left">

        <ul class="uk-navbar-nav">
            <li @if (\Illuminate\Support\Facades\Request::is('admin/dashboard')) class="uk-active" @endif><a href="{{ route('admin.dashboard') }}">Рабочий стол</a></li>
            <li >
                <a href="#">Справочники</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li @if (\Illuminate\Support\Facades\Request::is('admin/doctors*')) class="uk-active" @endif><a href="{{ route('admin.doctors') }}">Врачи</a></li>
                        <li @if (\Illuminate\Support\Facades\Request::is('admin/services*')) class="uk-active" @endif><a href="{{ route('admin.services') }}">Услуги</a></li>
                        <li @if (\Illuminate\Support\Facades\Request::is('admin/feedbacks*')) class="uk-active" @endif><a href="{{ route('admin.feedbacks') }}">Отзывы</a></li>
                    </ul>
                </div>
            </li>
            <li @if (\Illuminate\Support\Facades\Request::is('admin/publication*')) class="uk-active" @endif>
                <a href="#">Публикации</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li @if (\Illuminate\Support\Facades\Request::is('admin/publication-types*')) class="uk-active" @endif><a href="{{ route('admin.publication.types') }}">Рубрики</a></li>
                        <li @if (\Illuminate\Support\Facades\Request::is('admin/publications*')) class="uk-active" @endif><a href="{{ route('admin.publications') }}">Все публикации</a></li>
                    </ul>
                </div>
            </li>
        </ul>

    </div>

    <div class="uk-navbar-right">

        <ul class="uk-navbar-nav">
            <li><a href="{{ route('admin.logout') }}">Выйти</a></li>
        </ul>

    </div>

</nav>