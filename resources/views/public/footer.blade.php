<section class="b-footer">
    <div class="container">

        <a href="mailto:info@vitbiomed.ru" class="b-footer__question-btn">
            <span>Задайте вопрос нашим специалистам</span>
        </a>

        <div class="b-footer__wrap-contats">

            <div class="b-footer__main-links">
                <div>
                    <span>Компания</span>
                    <ul>
                        <li>
                            <a href="#">Производство</a>
                        </li>
                        <li>
                            <a href="#">Медицинский центр</a>
                        </li>
                        <li>
                            <a href="#">Пресс-кит</a>
                        </li>
                        <li>
                            <a href="#">О нас говорят</a>
                        </li>
                        <li>
                            <a href="#">Контакты</a>
                        </li>
                    </ul>
                </div>

                <div>
                    <span>Информация</span>
                    <ul>
                        <li>
                            <a href="#">Цены</a>
                        </li>
                        <li>
                            <a href="#">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="#">Подписка</a>
                        </li>
                        <li>
                            <a href="#">Публичная оферта</a>
                        </li>
                        <li>
                            <a href="#">Вопросы</a>
                        </li>
                    </ul>
                </div>

                <div>
                    <span>Пробиотики</span>
                    <ul>
                        <li>
                            <a href="#">Купить пробиотики</a>
                        </li>
                        <li>
                            <a href="#">Купить курс</a>
                        </li>
                        <li>
                            <a href="#">Рассчет курса</a>
                        </li>
                        <li>
                            <a href="#">Партнерам</a>
                        </li>
                    </ul>
                </div>
                <div class="b-footer__main-links__desktop_hide">
                    <span>Контакты</span>
                    <ul>
                        <li>
                            <a href="#">Купить пробиотики</a>
                        </li>
                        <li>
                            <a href="#">Купить курс</a>
                        </li>
                        <li>
                            <a href="#">Рассчет курса</a>
                        </li>
                        <li>
                            <a href="#">Партнерам</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="b-footer__contacts-info">
                <div class="b-footer__social-links">
                    <ul>
                        <li>
                            <a href="#" class="b-footer__main-links__icon1">
                                <span>Блог</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-footer__main-links__icon2">
                                <span>Facebook</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-footer__main-links__icon3">
                                <span>Vkontakte</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-footer__main-links__icon4">
                                <span>Google+</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-footer__main-links__icon5">
                                <span>Instagram</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-footer__main-links__icon6">
                                <span>Youtube</span>
                            </a>
                        </li>
                </div>

                <div class="b-footer__col-contacts">
                    <a href="#order-form" rel="modal:open">Записаться на прием</a>
                    <div>
                        <a href="mailto:info@vitbiomed.ru">info@vitbiomed.ru</a>
                        <span>
							<span><a href="tel:+74991641380">+7 (499) 164-13-80</a>;</span>
							<span><a href="tel:+74991648831">+7 (499) 164-88-31</a></span>
						</span>
                        <a href="https://yandex.ru/maps/213/moscow/?ll=37.789862%2C55.799825&z=16&mode=whatshere&whatshere%5Bpoint%5D=37.788274%2C55.800302&whatshere%5Bzoom%5D=16" target="_blank">Москва, ул. 5-я Парковая, д. 46</a>
                    </div>
                </div>
            </div>

        </div>

        <div class="b-footer__copyright">
            <span>Витбиомед © 2015 </span>
        </div>

        <div class="b-footer__fullversion">
            <a href="#">Загрузить полную версию сайта</a>
        </div>

    </div>
</section>

<div id="order-form" style="display:none; padding: 40px; width: 460px; -webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
    <form>
        <div class="l-feedback-form__input" style="margin-bottom: 20px; width: 100%;">
            <input id="order-form-full_name" type="text" name="full_name" class="b-form-input" oninput="$(this).css('border-color','');" placeholder="Фамилия Имя Отчество">
        </div>
        <div class="l-feedback-form__input" style="margin-bottom: 20px; width: 100%;">
            <input id="order-form-phone" type="text" name="phone" class="b-form-input phone-mask" oninput="$(this).css('border-color','');" placeholder="Контактный телефон">
        </div>
        <div class="l-feedback-form__input" style="margin-bottom: 20px; width: 100%;">
            <input id="order-form-email" type="text" name="email" class="b-form-input" oninput="$(this).css('border-color','');" placeholder="Электронная почта">
        </div>
        <input id="order_doctor_id" type="hidden" name="doctor_id">
        <input id="order_service_id" type="hidden" name="service_id">
        <a id="submit-order-form" href="#order-result" class="b-header__add-bottom-btn__inner" style="background: #689f38;">
            <span>Записаться <span class="hide-max-mobile">на прием</span></span>
        </a>
    </form>
</div>

<div id="order-result" style="display:none; padding: 40px; width: 460px; -webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
    <p style="text-align: center; font-size: 24px;">Благодарим за запись!</p>
    <p style="text-align: center; font-size: 18px;">Мы свяжемся с вами в ближайшее время.</p>
</div>

<div id="feedback-result" style="display:none; padding: 40px; width: 460px; -webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
    <p style="text-align: center; font-size: 24px;">Благодарим за обратную связь!</p>
    <p style="text-align: center; font-size: 18px;">Ваш отзыв появится на сайте сразу после модерации.</p>
</div>

<script>
    $('.phone-mask').mask('+79999999999');

    $('#submit-order-form').on('click', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $.post('/order/make', $(form).serialize(), function (data) {
            if (data.status == 'success') {
                $('#order-form-full_name').css('border-color','');
                $('#order-form-email').css('border-color', '');
                $('#order-form-phone').css('border-color', '');

                $('#order-result').modal();
            } else {
                console.log(data.errors);
                if (data.errors.full_name != undefined) {
                    $('#order-form-full_name').css('border-color','#f0506e');
                } else {
                    $('#order-form-full_name').css('border-color','');
                }
                if (data.errors.email != undefined) {
                    $('#order-form-email').css('border-color', '#f0506e');
                } else {
                    $('#order-form-email').css('border-color', '');
                }
                if (data.errors.phone != undefined) {
                    $('#order-form-phone').css('border-color', '#f0506e');
                } else {
                    $('#order-form-phone').css('border-color', '');
                }
            }
        })
    });

    $('#submit-feedback-form').on('click', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $.post('/feedback/make', $(form).serialize(), function (data) {
            if (data.status == 'success') {
                $('#order-form-full_name').css('border-color','');
                $('#order-form-email').css('border-color', '');
                $('#order-form-phone').css('border-color', '');

                $('#order-result').modal();
            } else {
                console.log(data.errors);
                if (data.errors.full_name != undefined) {
                    $('#order-form-full_name').css('border-color','#f0506e');
                } else {
                    $('#order-form-full_name').css('border-color','');
                }
                if (data.errors.email != undefined) {
                    $('#order-form-email').css('border-color', '#f0506e');
                } else {
                    $('#order-form-email').css('border-color', '');
                }
                if (data.errors.phone != undefined) {
                    $('#order-form-phone').css('border-color', '#f0506e');
                } else {
                    $('#order-form-phone').css('border-color', '');
                }
            }
        })
    });
</script>