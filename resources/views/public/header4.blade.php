<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title></title>

    @include('public.header_includes')

</head>
<body>

<section class="b-header b-header--white b-header--smooth-redisign">
    <div class="container">

        @include('public.header_menu')

    </div>
</section>


