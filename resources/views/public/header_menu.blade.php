<div class="b-header__wrap">
    <div class="b-header__wrap-menu">
        <span class="b-header__wrap-menu__icon"></span>
        <div class="b-hide-menu">

            <ul class="b-hide-menu__list">
                <li>
                    <span>Клиника</span>
                    <ul>
                        <li><a href="/clinics">О клинике</a></li>
                        <li><a href="/treatment-methods">Методы лечения</a></li>
                        <li><a href="/feedbacks">Отзывы</a></li>
                        <li><a href="/services">Услуги</a></li>
                        <li><a href="/doctors">Врачи</a></li>
                    </ul>
                </li>
                <li>
                    <span><a href="http://store.vitbiomed.ru/">Пробиотики</a></span>
                </li>
                <li>
                    <span><a href="http://store.vitbiomed.ru/courses/">Курсы</a></span>
                </li>
                <li>
                    <span><a href="http://research.vitbiomed.ru/">Исследования</a></span>
                </li>
                <li>
                    <span><a href="/blog">Блог</a></span>
                </li>
            </ul>

            <ul class="b-hide-menu__socials">
                <li><a href="#" class="b-hide-menu__socials-link-1"></a></li>
                <li><a href="#" class="b-hide-menu__socials-link-2"></a></li>
                <li><a href="#" class="b-hide-menu__socials-link-3"></a></li>
                <li><a href="#" class="b-hide-menu__socials-link-4"></a></li>
                <li><a href="#" class="b-hide-menu__socials-link-5"></a></li>
                <li><a href="#" class="b-hide-menu__socials-link-6"></a></li>
            </ul>

            <div class="b-hide-menu__caller">
                <a href="tel:+74991641380"></a>
            </div>

        </div>
    </div>
    <div class="b-header__wrap-logotype">
        <a href="/" class="b-header__logotype"></a>
    </div>
    <div class="b-header__wrap-contacts">
        <div class="b-header__phone">
            <span>+7 (499) 164-13-80</span>
        </div>
        <div class="b-header__socials-wrap">
            <a href="#" class="b-header__socials-inner b-header__socials-inner--telegram"></a>
            <a href="#" class="b-header__socials-inner b-header__socials-inner--msg"></a>
        </div>
        <div class="b-header__logout">
            <a href="#" class="b-header__logout-inner"></a>
        </div>
    </div>
</div>