<link rel="stylesheet" type="text/css" href="/resources/fonts/fonts.css">
<link rel="stylesheet" type="text/css" href="/lib/slick-1.6.0/slick.css">
<link rel="stylesheet" type="text/css" href="/lib/slick-1.6.0/slick-theme.css">
<link rel="stylesheet" type="text/css" href="/lib/magnific-popup/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="/lib/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.modal.min.css">

<script src="/lib/js/jquery-1.11.3.min.js"></script>
<script src="/lib/slick-1.6.0/slick.min.js"></script>
<script src="/lib/magnific-popup/magnific-popup.js"></script>
<script src="/lib/js/record_calendar.js"></script>
<script src="/lib/js/script.js"></script>
<script src="/js/jquery.modal.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>