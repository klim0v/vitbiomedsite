@include('public.header3')


<section class="l-big-title">
    <div class="container">
        <div class="b-big-title b-big-title__center">
            <span>Заголовок для раздела отзывы</span>
        </div>
    </div>
</section>

{{--<section class="l-list-video">--}}
    {{--<div class="container">--}}

        {{--<div class="b-list-video">--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<a href="https://www.youtube.com/watch?v=Fy7FzXLin7o" class="b-list-video__item popup-youtube" style="background:url(http://img.youtube.com/vi/Fy7FzXLin7o/maxresdefault.jpg)">--}}
                    {{--<span class="b-list-video__play"></span>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
{{--</section>--}}

<section class="l-feedback-title">
    <div class="container">
        <div class="b-big-title b-big-title__center">
            <span>Отзывы</span>
        </div>
    </div>
</section>


<section class="l-feedback-list">
    @foreach($feedbacks as $feedback)
        <div id="feedback{{ $feedback->id }}" class="l-feedback-list__item {{--l-feedback-list__item--active--}}">
            <div class="container">

                <div class="b-about-clinic-feedback__box">

                    <div class="b-about-clinic-feedback__head">
                        <div class="b-about-clinic-feedback__avatar" @if($feedback->thumbnail) style="background-image: url(/storage/{{ $feedback->thumbnail }}); background-size: cover;" @endif></div>
                        <div class="b-about-clinic-feedback__info">
                            <div class="b-about-clinic-feedback__name">
                                <a href="{{ $feedback->facebook }}" class="b-about-clinic-feedback__name-social"></a>
                                <span class="b-about-clinic-feedback__name-text">{{ $feedback->full_name }}</span>
                            </div>
                            <div class="b-about-clinic-feedback__stars">
                                @for ($i = 0; $i < $feedback->rating; $i++)
                                    <span class="active"></span>
                                @endfor
                                @for ($i = 0; $i < 5 - $feedback->rating; $i++)
                                    <span></span>
                                @endfor
                            </div>
                        </div>
                    </div>

                    <div class="b-about-clinic-feedback__foot">
                        <p class="b-about-clinic-feedback__text">
                            {{ $feedback->text }}
                        </p>

                        {{--<div class="l-feedback-list__responses">--}}
                            {{--<div class="l-feedback-list__responses-title">Ответы</div>--}}

                            {{--<div class="l-feedback-list__response">--}}
                                {{--<a href="#" class="l-feedback-list__response-name">Сидорова Тамара Ивановна – врач-уролог</a>--}}
                                {{--<p class="l-feedback-list__response-text">--}}
                                    {{--Добрый день! Для педиатра может быть и субъективно, но в ортопедии и рентгенологии существуют четкие  критерии, по которым оцениваются снимки тазобедренных суставов. На представленной рентгенограмме тазобедренные суставы сформированы удовлетворительно. С уважением.--}}
                                {{--</p>--}}
                            {{--</div>--}}

                            {{--<div class="l-feedback-list__response">--}}
                                {{--<a href="#" class="l-feedback-list__response-name">Сидорова Тамара Ивановна – врач-уролог</a>--}}
                                {{--<p class="l-feedback-list__response-text">--}}
                                    {{--Добрый день! Для педиатра может быть и субъективно, но в ортопедии и рентгенологии существуют четкие  критерии, по которым оцениваются снимки тазобедренных суставов. На представленной рентгенограмме тазобедренные суставы сформированы удовлетворительно. С уважением.--}}
                                {{--</p>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        {{--<a class="b-about-clinic-feedback__link l-feedback-list__link-response" attr-n-responses="2">Смотреть ответы(2)</a>--}}
                    </div>

                </div>

            </div>
        </div>
    @endforeach
</section>


<section class="l-feedback-form">
    <div class="container">

        <div class="b-big-title b-big-title--less l-feedback-form__title">
            <span>Оставьте отзыв</span>
        </div>

        <form class="l-feedback-form__fields">

            <div class="l-feedback-form__inputs">

                <div class="l-feedback-form__input">
                    <input type="text" class="b-form-input" placeholder="Имя">
                </div>

                <div class="l-feedback-form__input">
                    <input type="text" class="b-form-input" placeholder="Фамилия">
                </div>

                <div class="l-feedback-form__input">
                    <input type="text" class="b-form-input" placeholder="Соц.сеть">
                </div>

            </div>

            <div class="l-feedback-form__textarea">
                <textarea class="b-form-textarea" placeholder="Ваш отзыв"></textarea>
            </div>

            <div class="l-feedback-form__foot">
                <div class="l-feedback-form__leave-rating">

                    <div class="b-leave-rating">
                        <input type="hidden" value="5" class="b-leave-rating__input">
                        <div class="b-leave-rating__title">Дайте оценку</div>
                        <div class="b-leave-rating__box">
                            <div class="b-leave-rating__box-item"></div>
                            <div class="b-leave-rating__box-item"></div>
                            <div class="b-leave-rating__box-item"></div>
                            <div class="b-leave-rating__box-item"></div>
                            <div class="b-leave-rating__box-item"></div>
                        </div>
                    </div>

                </div>
                <div class="l-feedback-form__submit-btn">

                    <button class="b-form-submit">ОТПРАВИТЬ</button>

                </div>
            </div>


        </form>

    </div>
</section>

@include('public.footer')