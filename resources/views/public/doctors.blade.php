@include('public.header3')


<section class="l-big-title">
    <div class="container">
        <div class="b-big-title b-big-title__center">
            <span>Наши врачи</span>
        </div>
    </div>
</section>

<section class="l-list-doctors">
    <div class="container">
        @foreach($doctors as $doctor)
        <div class="b-doctor-mini">
            <div class="b-doctor-mini__about-wrap">

                <div class="b-doctor-mini__dossier">
                    <div class="b-doctor-mini__photo" style="background: url(/storage/{{ $doctor->thumbnail }})"></div>
                    <div class="b-doctor-mini__info">
                        <div class="b-doctor-mini__name"><a href="{{ route('doctor', ['id' => $doctor->id]) }}">{{ $doctor->full_name }}</a></div>
                        <p class="b-doctor-mini__biography">
                            {!! strip_tags(nl2br($doctor->description)) !!}
                        </p>
                        <div class="b-doctor-mini__features">
                            <div class="b-doctor-mini__feature">
                                <span class="b-doctor-mini__attribute">Специальность</span>
                                <span class="b-doctor-mini__attribute-value">{{ $doctor->speciality }}</span>
                            </div>
                            <div class="b-doctor-mini__feature">
                                <span class="b-doctor-mini__attribute">Стаж</span>
                                <span class="b-doctor-mini__attribute-value">{{ $doctor->work_experience }}</span>
                            </div>
                        </div>
                        <div class="b-doctor-mini__link-more is-mobile-none">
                            <a href="{{ route('doctor', ['id' => $doctor->id]) }}" class="">Подробнее</a>
                        </div>
                    </div>
                </div>
                <div class="b-doctor-mini__timetable">
                    <div class="b-doctor-mini__timetable-title">Расписание</div>
                    <div class="b-doctor-mini__timetable-info">
                        @if (!empty($doctor->schedule))
                            <span>
                                {!! $doctor->schedule->implode('row', '</span><span>')  !!}
                            </span>
                        @endif
                    </div>
                </div>

            <!-- Для мобильных -->
                <a href="#order-form" rel="modal:open"><div class="b-doctor-mini__mobile-record-btn js-mobile-record-doctor" onclick="$('#order_doctor_id').val({{ $doctor->id }})">Записаться</div></a>
                <a href="#order-form" rel="modal:open"><div class="b-doctor-mini__trigger" onclick="$('#order_doctor_id').val({{ $doctor->id }})"></div></a>
            </div>
            <!-- Для десктопа -->

        </div>
        @endforeach
    </div>
</section>

@include('public.footer')