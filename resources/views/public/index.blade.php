@include('public.header')

<section class="b-main-slider">
    <div class="b-video-bg">
        <video loop muted autoplay poster="video/video.jpg" class="b-video-bg__video">
            <source src="/video/video.ogv" type='video/ogg; codecs="theora, vorbis"'>
            <source src="/video/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
            <source src="/video/video.webm" type='video/webm; codecs="vp8, vorbis"'>
        </video>
    </div>
    <div class="container">
        <div class="b-main-slider__wrap-slider">
            <div>
                <div class="b-main-slider__inner-slider">
                    <p class="b-main-slider__text-main">
							<span class="b-main-slider__text-main__head">
								<span class="weight_900">Витбиомед</span>
							</span>
                        <span class="b-main-slider__text-main__hyphen">
								–
							</span>
                        <span class="b-main-slider__text-main__first-word">
								первый
							</span>
                        в России  специализированный медицинский центр по лечению дисбактериоза пробиотиками собственного производства
                    </p>
                    <p class="b-main-slider__text-secondary">
                        Уже 15 лет мы помогаем быть здоровыми и жить полноценной
                        жизнью  пациентам с нарушениями микрофлоры и ослабленным иммунитетом различных степеней сложности.
                    </p>
                    <a href="{{ route('clinics') }}" class="b-main-slider__btn btn-bg-none btn-with-next-white">
                        <span>Подробнее</span>
                    </a>
                </div>
            </div>
            <div>
                <div class="b-main-slider__inner-slider">
                    <p class="b-main-slider__text-main">
                        <span class="weight_900">Наши «Умные пробиотики»</span> – это добавки для здоровья, которые содержат живые полезные бактерии, бережно сохраненные в их естественном виде.
                    </p>
                    <p class="b-main-slider__text-secondary">
                        Пробиотики помогают нормализовать микрофлору, омолодить организм и восстановить его естественные природные ресурсы.
                    </p>
                    <a href="http://store.vitbiomed.ru/" class="b-main-slider__btn btn-bg-none btn-with-next-white">
                        <span>Подробнее</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    main_slider = {}
    main_slider.el = document.querySelector(".b-main-slider");
    main_slider.h = main_slider.el.clientHeight;
    main_slider.w = main_slider.el.clientWidth;

    main_slider.rel_wh = main_slider.h/main_slider.w;

    b_video = {}
    b_video.el = document.querySelector(".b-video-bg video");
    b_video.h = b_video.el.clientHeight;
    b_video.w = b_video.el.clientWidth;

    b_video.rel_wh = b_video.h/b_video.w;


    if(main_slider.rel_wh > b_video.rel_wh){
        //высота видео 100%

        b_video.el.style.height = "100%";
        b_video.el.style.width = "initial";

        b_video.w = b_video.w/b_video.h*main_slider.h;
        b_video.el.style.left = -(b_video.w-main_slider.w)/2+"px";

    }else{
        //ширина видео 100%
        b_video.el.style.height = "initial";
        b_video.el.style.width = "100%";

        b_video.h = b_video.h/b_video.w*main_slider.w;
        b_video.el.style.top = -(b_video.h-main_slider.h)/2+"px";
    }


</script>

<section class="b-additionaly-menu">
    <div class="container">
        <ul class="b-additionaly-menu__wrap">
            <li class="b-additionaly-menu__item">
                <a href="{{ route('clinics') }}" class="b-additionaly-menu__link">
                    <span class="b-additionaly-menu__icon b-additionaly-menu__icon--i1"></span>
                    <span class="b-additionaly-menu__title">Клиника</span>
                </a>
            </li>
            <li class="b-additionaly-menu__item">
                <a href="http://store.vitbiomed.ru/" class="b-additionaly-menu__link">
                    <span class="b-additionaly-menu__icon b-additionaly-menu__icon--i2"></span>
                    <span class="b-additionaly-menu__title">Пробиотики</span>
                </a>
            </li>
            <li class="b-additionaly-menu__item">
                <a href="http://store.vitbiomed.ru/courses/" class="b-additionaly-menu__link">
                    <span class="b-additionaly-menu__icon b-additionaly-menu__icon--i3"></span>
                    <span class="b-additionaly-menu__title">Курсы</span>
                </a>
            </li>
            <li class="b-additionaly-menu__item">
                <a href="http://research.vitbiomed.ru" class="b-additionaly-menu__link">
                    <span class="b-additionaly-menu__icon b-additionaly-menu__icon--i4"></span>
                    <span class="b-additionaly-menu__title">Исследования</span>
                </a>
            </li>
            <li class="b-additionaly-menu__item">
                <a href="{{ route('publication.all.index') }}" class="b-additionaly-menu__link">
                    <span class="b-additionaly-menu__icon b-additionaly-menu__icon--i5"></span>
                    <span class="b-additionaly-menu__title">Блог</span>
                </a>
            </li>
        </ul>
    </div>
</section>

@include('public.footer')