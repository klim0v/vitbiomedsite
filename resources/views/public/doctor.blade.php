@include('public.header4')

<div class="b-doctor-face" style="background-image:url(/storage/{{ $doctor->cover }});">
    <!-- 	Изображение для десктопа.
                Сначала изоображение подгоняется под блок,
                затем центруется и смещается на
                определенное количество пикселей вправо.
                Для разных экранов смещение разное	-->
    <div class="container b-doctor-face__container">
        <div class="b-doctor-face__face-mobile" style="background-image:url(/storage/{{ $doctor->cover }});"></div>
        <!-- Изображение для телефона -->
        <div class="b-doctor-face__name">
            {{ $doctor->full_name }}
        </div>
        <div class="b-doctor-face__timetable">
            <div class="b-doctor-face__timetable-title">Расписание</div>
            <div class="b-doctor-face__timetable-value">
            @if (!empty($doctor->schedule))
                <span>
                    {!! $doctor->schedule->implode('row', '</span><span>') !!}
                </span>
            @endif
            </div>
        </div>
        <a href="#order-form" rel="modal:open" onclick="$('#order_doctor_id').val({{ $doctor->id }})"><div class="b-doctor-face__btn">
            Записаться на прием
        </div></a>
    </div>
</div>

<div class="l-doctor-page-dosier">
    <div class="container">

        <div class="b-columns">
            <div>
                <p class="b-paragraph b-mobile-head">
                    <span class="b-text-bold">Общая информация:</span>
                </p>
                <p class="b-paragraph">
                    {!! nl2br($doctor->description) !!}
                </p>
                <p class="b-paragraph">
                    <span class="b-text-bold">Специальность:</span> {{ $doctor->speciality }}<br>
                    <span class="b-text-bold">Стаж:</span> {{ $doctor->work_experience }}
                </p>
            </div>
            <div>
                <p class="b-paragraph  b-mobile-head">
                    <span class="b-text-bold">Образование и опыт работы:</span>
                </p>
                <p class="b-paragraph">
                    {!! nl2br($doctor->experience) !!}
                </p>

                <p>
                <div class="b-mini-timetable b-mini-timetable--inline">
                    <div class="b-mini-timetable__title">Расписание</div>
                    <div class="b-mini-timetable__value">
                    @if (!empty($doctor->schedule))
                        <span>
                            {!! $doctor->schedule->implode('row', '</span><span>') !!}
                        </span>
                    @endif
                    </div>
                </div>
                </p>
            </div>
        </div>

    </div>
</div>

@if (!empty($embed_id))
<div class="l-doctor-video">
    <div class="container">
        <div class="b-video-full-width">
            <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/{{ $embed_id }}" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>
@endif

@if ($doctor->feedback->count())
<section class="b-about-clinic-feedback l-our-doctor-feedback">
    <div class="container">
        <div class="b-about-clinic-feedback__main_title">Отзывы</div>
        <div class="b-about-clinic-feedback__wrap">

            @foreach($doctor->feedback as $feedback)
                <div class="b-about-clinic-feedback__inner">
                    <div class="b-about-clinic-feedback__box">

                        <div class="b-about-clinic-feedback__head">
                            <div class="b-about-clinic-feedback__avatar"></div>
                            <div class="b-about-clinic-feedback__info">
                                <div class="b-about-clinic-feedback__name">
                                    @if (!empty($feedback->facebook))<a href="{{ $feedback->facebook }}" class="b-about-clinic-feedback__name-social"></a> @endif
                                    <span class="b-about-clinic-feedback__name-text">{{ $feedback->full_name }}</span>
                                </div>
                                <div class="b-about-clinic-feedback__stars">
                                    @for ($i = 0; $i < $feedback->rating; $i++)
                                        <span class="active"></span>
                                    @endfor
                                    @for ($i = 0; $i < 5 - $feedback->rating; $i++)
                                        <span></span>
                                    @endfor
                                </div>
                            </div>
                        </div>

                        <div class="b-about-clinic-feedback__foot">
                            <p class="b-about-clinic-feedback__text">
                                {{ str_limit($feedback->text, 70) }}
                            </p>
                            <a href="#" class="b-about-clinic-feedback__link">Подробнее...</a>
                        </div>

                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
@endif

@if ($doctors->count())
<section class="b-about-clinc-our-doctors l-our-doctor-other-doctors">
    <div class="container">
        <div class="b-about-clinc-our-doctors__main-title">
            Другие врачи
        </div>
        <div class="b-about-clinc-our-doctors__wrap">
            @foreach($doctors as $doctor)
                <div class="b-about-clinc-our-doctors__inner">

                    <div class="b-about-clinc-our-doctors__avatar">
                        <div class="b-about-clinc-our-doctors__avatar-image" style="background: url(/storage/{{ $doctor->thumbnail }})"></div>
                        <a href="#" class="b-about-clinc-our-doctors__avata-feedback">Отзывов: {{ $doctor->feedback->count() }}</a>
                    </div>

                    <div class="b-about-clinc-our-doctors__text">
                        <div class="b-about-clinc-our-doctors__name">
                            <a href="{{ route('doctor', ['id' => $doctor->id]) }}">{{ $doctor->full_name }}</a>
                        </div>
                        <div class="b-about-clinc-our-doctors__text-p">
                            {!! $doctor->description !!}
                        </div>
                        <div class="b-about-clinc-our-doctors__text-p">
                            {{ $doctor->speciality }}<br>
                            Стаж: {{ $doctor->work_experience }}
                        </div>
                    </div>

                    <div class="b-about-clinc-our-doctors__btn-wrap">
                        <a class="b-about-clinc-our-doctors__btn" href="#order-form" rel="modal:open">
                            <span>Записаться</span>
                        </a>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@include('public.footer')