@if (count($publications))
    <div class="inner_articles">
        @include('public.blog.publication_strips', ['publications' => $publications])
    </div>
    <noscript>{{ $publications->render() }}</noscript>
    <script type="text/javascript">
        function showMore(elem) {

            var url=$(elem).data("href");
            var parent = $(elem).parent();
            $('#preloader').fadeIn('fast');
            $.ajax({
                url:url,
                success:function (data) {
                    $('#show_more_publications').remove();
                    $(parent).append(data);
                    $('#preloader').fadeOut('fast');

                },
                error:function (data, status) {
                    alert("Произошла ошибка загрузки: " + data + status);

                },
                complete:function () {
                    try {}
                    finally {
                    }
                }
            });
            return false;
        }
    </script>
@else
    <h2>Статей не найдено.</h2>
@endif
