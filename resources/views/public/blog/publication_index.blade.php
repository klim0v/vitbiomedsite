<!DOCTYPE html>
<html lang="ru" data-ng-app="app">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{{$publication->meta_title}}</title>
    @if ($publication->og_title)
        <meta property="og:title" content="{{$publication->og_title}}"/>
    @endif
    @if ($publication->og_description)
        <meta property="og:description" content="{{$publication->og_description}}"/>
    @endif
    @if ($publication->og_image)
        <meta property="og:image" content="{{URL::to('/storage/'.$publication->og_image)}}">
    @endif
    <meta property="og:type" content="{{$publication->og_type ?? 'article'}}"/>
    @if ($publication->og_url)
        <meta property="og:url" content="{{$publication->og_url}}"/>
    @endif
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content="Keywords"/>
    <meta name="description" content="Description"/>
    <link href="/old/bitrix/js/main/core/css/core.css?14392234113963" type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/js/main/core/css/core_popup.css?148665183732860" type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news/blog/bitrix/news.detail/.default/style.css?1439223420155"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/blog_slider/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/similar_blog_articles/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/blog_comments/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/menu/standart/style.css?1439223420581" type="text/css"
          rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/blog_popular/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/components/bitrix/socserv.auth.form/templates/flat/style.css?14845676842847" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/menu/menusi/style.css?1482946469581" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/sender.subscribe/template1/style.css?14829457374640"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/form.result.new/popup/style.css?1439223420666"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/template_styles.css?1498751288196458" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <script type="text/javascript">if (!window.BX) window.BX = {
            message: function (mess) {
                if (typeof mess == 'object') for (var i in mess) BX.message[i] = mess[i];
                return true;
            }
        };</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'JS_CORE_LOADING': 'Загрузка...',
            'JS_CORE_NO_DATA': '- Нет данных -',
            'JS_CORE_WINDOW_CLOSE': 'Закрыть',
            'JS_CORE_WINDOW_EXPAND': 'Развернуть',
            'JS_CORE_WINDOW_NARROW': 'Свернуть в окно',
            'JS_CORE_WINDOW_SAVE': 'Сохранить',
            'JS_CORE_WINDOW_CANCEL': 'Отменить',
            'JS_CORE_WINDOW_CONTINUE': 'Продолжить',
            'JS_CORE_H': 'ч',
            'JS_CORE_M': 'м',
            'JS_CORE_S': 'с',
            'JSADM_AI_HIDE_EXTRA': 'Скрыть лишние',
            'JSADM_AI_ALL_NOTIF': 'Показать все',
            'JSADM_AUTH_REQ': 'Требуется авторизация!',
            'JS_CORE_WINDOW_AUTH': 'Войти',
            'JS_CORE_IMAGE_FULL': 'Полный размер'
        });</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'LANGUAGE_ID': 'ru',
            'FORMAT_DATE': 'DD.MM.YYYY',
            'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
            'COOKIE_PREFIX': 'BITRIX_SM',
            'SERVER_TZ_OFFSET': '10800',
            'SITE_ID': 's1',
            'SITE_DIR': '/',
            'USER_ID': '',
            'SERVER_TIME': '1508931752',
            'USER_TZ_OFFSET': '0',
            'USER_TZ_AUTO': 'Y',
            'bitrix_sessid': 'c4adbd43495e03b07e0204c044ae2ef9'
        });</script>


    <script type="text/javascript" src="/old/bitrix/js/main/core/core.js?1486651837116711"></script>
    <script type="text/javascript" src="/old/bitrix/js/main/core/core_popup.js?148665183741651"></script>
    <script type="text/javascript" src="/old/bitrix/js/main/core/core_ajax.js?148665183735622"></script>

    <meta name="description" content="{{$publication->meta_description}}"/>

    <link href="/old/bitrix/templates/vit/css/swiper.min.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/preloader.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/magnific-popup.css" rel="stylesheet">

    <link href="/old/bitrix/templates/vit/css/new_styles.css" rel="stylesheet">

    <link href="/old/static/css/font.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Exo+2:700,300italic,200,900italic,500italic,800,300,600italic,500,800italic,900,100italic,200italic,600,700italic,400,100,400italic&amp;subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>

    <link rel="stylesheet" href="/old/dist/bundle.css?rev=1ede78ce969cf752c75bdda5642606e0">
    <link rel="stylesheet" href="/old/dist/magnific-popup.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


    <link href="/old/bitrix/templates/vit/css/magnific-popup.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>

</head>
<body data-spy="scroll" data-target="#nav" class="likearts-content">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4&appId=273276476164317";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<div class="md-overlay"></div>
<!--//auth-modal-->

<!-- MODAL WINDOW LK reg -->
<div id="auth-modal-reg" class="md-modal uk-modal">
    <div class="uk-modal-content md-content">


        <div class="md-top">
            <div class="md-close" onclick="$('.md-overlay').click();">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7">
                    <polygon
                            points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/>
                </svg>
            </div>
            <div class="md-title">Регистрация</div>
            <div class="lk-social-media">
                <span>через</span>
                <ul class="lk-social-media__list">
                    <script type="text/javascript">
                        function BxSocServPopup(id) {
                            var content = BX("bx_socserv_form_" + id);
                            if (content) {
                                var popup = BX.PopupWindowManager.create("socServPopup" + id, BX("bx_socserv_icon_" + id), {
                                    autoHide: true,
                                    closeByEsc: true,
                                    angle: {offset: 24},
                                    content: content,
                                    offsetTop: 3
                                });

                                popup.show();

                                var input = BX.findChild(content, {
                                    'tag': 'input',
                                    'attribute': {'type': 'text'}
                                }, true);
                                if (input) {
                                    input.focus();
                                }

                                var button = BX.findChild(content, {
                                    'tag': 'input',
                                    'attribute': {'type': 'submit'}
                                }, true);
                                if (button) {
                                    button.className = 'btn btn-primary';
                                }
                            }
                        }
                    </script>

                    <div class="bx-authform-social">
                        <ul>
                            <li>
                                <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon"
                                   href="javascript:void(0)"
                                   onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1801245380125777&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fblog%2Fissledovanie-probiotiki-pomogayut-pokhudet%2F%3Fauth_service_id%3DFacebook%26check_key%3D3b0be073e2a78a112595e5fd1aac1f81%26SECTION_CODE_PATH%3D%252F%26blog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%3D%26backurl%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253FSECTION_CODE_PATH%253D%25252F%2526amp%253Bblog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D&amp;scope=email,publish_actions,user_friends&amp;display=popup', 680, 600)"
                                   title="Facebook"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon"
                                   href="javascript:void(0)"
                                   onclick="BX.util.popup('https://oauth.yandex.ru/authorize?response_type=code&amp;client_id=88cb897161164506923f302388cd2ca5&amp;display=popup&amp;state=site_id%3Ds1%26backurl%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253Fcheck_key%253D3b0be073e2a78a112595e5fd1aac1f81%2526SECTION_CODE_PATH%253D%25252F%2526blog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D%26mode%3Dopener%26redirect_url%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253FSECTION_CODE_PATH%253D%25252F%2526amp%253Bblog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D', 680, 600)"
                                   title="Яндекс"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon"
                                   href="javascript:void(0)"
                                   onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=5825158&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fblog%2Fissledovanie-probiotiki-pomogayut-pokhudet%2F%3Fauth_service_id%3DVKontakte&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds1%26backurl%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253Fcheck_key%253D3b0be073e2a78a112595e5fd1aac1f81%2526SECTION_CODE_PATH%253D%25252F%2526blog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D%26redirect_url%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253FSECTION_CODE_PATH%253D%25252F%2526amp%253Bblog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D', 660, 425)"
                                   title="ВКонтакте"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon"
                                   href="javascript:void(0)"
                                   onclick="BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=674779939050-7ejv3833b2g3vivpkir9ekio10id5d8j.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGooglePlusOAuth%26site_id%3Ds1%26backurl%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253Fcheck_key%253D3b0be073e2a78a112595e5fd1aac1f81%2526SECTION_CODE_PATH%253D%25252F%2526blog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D%26mode%3Dopener%26redirect_url%3D%252Fblog%252Fissledovanie-probiotiki-pomogayut-pokhudet%252F%253FSECTION_CODE_PATH%253D%25252F%2526amp%253Bblog%25252Fissledovanie-probiotiki-pomogayut-pokhudet%25252F%253D', 580, 400)"
                                   title="Google+"></a>
                            </li>
                        </ul>
                    </div>
                </ul>
                <span>или</span>
            </div>
        </div>
        <div class="md-form">
            <form method="POST"
                  action="/blog/issledovanie-probiotiki-pomogayut-pokhudet/?SECTION_CODE_PATH=%2F&amp;blog%2Fissledovanie-probiotiki-pomogayut-pokhudet%2F="
                  name="regform" class="uk-form uk-form-stacked">
                <input type="hidden" name="TYPE" value="REGISTRATION"/>
                <input type="hidden" name="register_submit_button" value="Y"/>
                <input type="hidden" class="api-mf-antibot" value="" name="ANTIBOT[NAME]">
                <input type="hidden" name="backurl" value="" class="backurl">

                <div class="uk-form-row">
                    <label class="uk-form-label">Имя:<span class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[NAME]"
                               value="">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Фамилия:<span class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[LAST_NAME]"
                               value="">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Адрес e-mail:<span class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[EMAIL]"
                               value="">
                    </div>
                </div>


                <div class="uk-form-row">
                    <button type="submit"
                            name="register_submit_button"
                            class="btn" data-md-ink-ripple="#fff"
                            value="Регистрация">Регистрация
                    </button>
                </div>
                <div class="uk-form-row">
                    <noindex>
                        <a href="/blog/issledovanie-probiotiki-pomogayut-pokhudet/?login=yes"
                           rel="nofollow"
                           class="ajax-link">Авторизация</a>
                    </noindex>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL WINDOW OVERLAY -->
<div class="md-overlay"></div>
<!--//reg-modal-->


<div class="header-container">
    <div class="container">
        <header class="header">
            <div class="flex-container">
                <div class="flex-item">
                    <a href="/" class="logo-link">
                        <div class="logo"></div>
                    </a>
                </div>
                <div class="flex-item">
                    <div class="menu-desktop">


                        <ul class="menu-desktop-list">

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru"
                                                                   class="menu-desktop-list__link">Клиника</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru"
                                                                   class="menu-desktop-list__link">Пробиотики</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru/courses/"
                                                                   class="menu-desktop-list__link">Курсы</a></li>

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/reviews/"
                                                                   class="menu-desktop-list__link">Отзывы</a></li>

                            <li class="menu-desktop-list__item"><a href="{{ route('publication.all.index') }}"
                                                                   class="menu-desktop-list__link">Блог</a></li>


                            <li class="menu-desktop-list__item">
                                <span class="phone">+7 (499) 164-13-80</span>
                            </li>
                        </ul>
                    </div>
                    <div class="menu-mobile-container">
                        <div class="menu-mobile" data-ng-controller="MobileMenuController as mobileMenu">
                            <div id="menu-mobile-btn" data-hm-tap="mobileMenu.controlMenu($event)"
                                 class="menu-mobile__btn">
                                <i class="material-icons menu-mobile__icon">view_headline</i>
                            </div>
                            <div id="menu-mobile-body" class="menu-mobile__body display-hide">
                                <ul class="menu-mobile-list">
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Компания</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://vitbiomed.ru/proizvodstvo/"
                                               class="menu-mobile-submenu__link">Производство</a>
                                            <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                               class="menu-mobile-submenu__link">Медицинский центр</a>
                                            <a href="http://vitbiomed.ru/reviews/" class="menu-mobile-submenu__link ">О
                                                нас говорят</a>
                                            <a href="http://vitbiomed.ru/nashi-kontakty/"
                                               class="menu-mobile-submenu__link">Контакты</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Информация</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="#" class="menu-mobile-submenu__link ">Публичная оферта</a>
                                            <a href="http://store.vitbiomed.ru/faq/" class="menu-mobile-submenu__link">Вопросы</a>
                                            <a data-uk-modal="" href="#auth-modal"
                                               class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                               data-modal="auth-modal">Вход</a>
                                            <a data-uk-modal="" href="#auth-modal-reg"
                                               class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                               data-modal="auth-modal-reg">Регистрация</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Пробиотики</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://store.vitbiomed.ru/dostavka/"
                                               class="menu-mobile-submenu__link">Доставка и оплата</a>
                                            <a href="http://store.vitbiomed.ru/" class="menu-mobile-submenu__link">Купить
                                                пробиотики</a>
                                            <a href="http://store.vitbiomed.ru/courses/"
                                               class="menu-mobile-submenu__link">Купить курс</a>
                                            <a href="http://vitbiomed.ru/partnyeram/" class="menu-mobile-submenu__link">Партнерам</a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="social-block">
                                    <a href="https://www.facebook.com/vitbiomed/">
                                        <div class="social-item social-item--facebook"></div>
                                    </a>
                                    <a href="https://www.instagram.com/vitbiomed/">
                                        <div class="social-item social-item--instagram"></div>
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ">
                                        <div class="social-item social-item--youtube"></div>
                                    </a>
                                    <a href="https://plus.google.com/+vitbiomed">
                                        <div class="social-item social-item--google"></div>
                                    </a>
                                    <a href="https://vk.com/vitbiomed ">
                                        <div class="social-item social-item--vk"></div>
                                    </a>
                                </div>
                                <div class="phone-block">
                                    <a href="tel:+74991641380">
                                        <div class="phone-btn">
                                            <i class="material-icons phone-btn__icon">call</i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </header>
    </div>
</div>

<!-- /header -->


<section class="blog article">

    <div class="blog-slider">
        <div id="comp_f48382b38f42f8e8656ad9bc5fe49650">
            <div id="slider" class="carousel slide" data-ride="carousel">
                <!-- slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <!-- slide back -->
                        <img

                                border="0"
                                src="/old/upload/iblock/6a3/6a35174469d6c8f5932d8f4339e27dde.jpg"
                                width="1920"
                                height="320"


                        />
                        <div class="slide-content">
                            <h3>Кто живет внутри нас?</h3>
                            <a href="/blog/vsye-o-disbakterioze/kto-zhivet-vnutri-nas/">ЧИТАТЬ СТАТЬЮ »</a>
                        </div>
                    </div>


                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                    <i class="slide-left"></i>
                </a>
                <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                    <i class="slide-right"></i>
                </a>
            </div>


        </div>          <!-- search -->
        <div class="search">
            <div class="container">
                <div class="form">
                    <form action="/blog/" method="GET">
                        <input type="text" name="search"
                               placeholder="Для поиска по статьям, введите интересующий Вас запрос" value="">
                        <button>НАЙТИ<span class="hidden-xs"> СТАТЬИ</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <!-- articles here -->
                <div class="blog-article">
                    <h1 class="title">{{$publication->heading}}</h1>
                    <div class="date">
                        <span class="date-data">{{$publication->created_at_formatted}}</span>
                        <span class="date-tags">{{$publication->publicationType->title}}</span>
                    </div>
                    <figure>
                        @if ($publication->image)
                            <img src="/storage/{{$publication->image}}" alt="{{$publication->title}}">
                            @if ($publication->author)
                                <figcaption>Фото: {{$publication->author ?? $publication->title}}</figcaption> @endif
                        @endif
                    </figure>
                    {!! $publication->text !!}
                    <span class="like">
    Понравилась статья? Поделитесь с друзьями!
  </span>
                    <div class="social-share">
                        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="https://yastatic.net/share2/share.js"></script>
                        <div class="ya-share2"
                             data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div>
                    </div>
                </div>
                @if($otherPublications)
                    <div id="comp_8a03a269a47bb8f1efe66acd2f042d00">
                        <div class="similar-articles">
                            <p class="title">ДРУГИЕ СТАТЬИ НА ЭТУ ТЕМУ:</p>
                            @foreach($otherPublications as $otherPublication)
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 ">
                                        <div class="similar-item">
                                            @if($otherPublication->thumbnail)
                                                <a href="{{ route('publication.index', ['type' => $otherPublication->publicationType->slug, 'publication' => $otherPublication->slug]) }}">
                                                    <img src="/storage/{{$otherPublication->thumbnail}}" alt=""
                                                         class="img-responsive">
                                                </a>
                                            @endif
                                            <a class="title"
                                               href="{{ route('publication.index', ['type' => $otherPublication->publicationType->slug, 'publication' => $otherPublication->slug]) }}">
                                                {{$otherPublication->title}}
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </div>

                    </div>
                @endif


                <div class="add-comment">
                    <button>
                        ДОБАВИТЬ КОММЕНТАРИЙ
                    </button>

                    <form action="/bitrix/templates/vit/ajax/add_comment.php" method="POST" id="blog_add_comment">
                        <input type="hidden" name="OBJECT" required value="809"/>
                        <input type="text" name="USER_NAME" required placeholder="Представьтесь, пожалуйста:">
                        <input type="text" name="PHONE_OR_EMAIL" required placeholder="Ваш e-mail:">
                        <textarea name="review-text" required placeholder="Ваш комментарий:"></textarea>
                        <button>ОПУБЛИКОВАТЬ КОММЕНТАРИЙ</button>
                    </form>
                </div>
                <div id="comp_346d681017429adca736d7089b46a7c9">
                    <div class="comments">
  <span class="title">
    ОПУБЛИКОВАННЫЕ КОММЕНТАРИИ (<span>0</span>)
  </span>

                    </div>


                </div>
            </div>
            <div class="col-sm-3 hidden-xs">
                @include('public.blog.rubric_menu')
            </div>
        </div>
    </div>
</section>


<br>


<div class="footer-container">
    <div class="container">
        <footer class="footer">
            <div class="footer__content--desktop">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-label">
                            <a href="mailto:info@vitbiomed.ru" class="white-label__link">Задайте вопрос нашим
                                специалистам</a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-sm-12-override">
                        <div class="footer-menu--desktop">
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Компания</div>
                                <div class="footer-menu-links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu-links__item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/" class="footer-menu-links__item">Медицинский
                                        центр</a>
                                    <a href="http://vitbiomed.ru/uslugi/" class="footer-menu-links item">Услуги</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu-links__item">О нас
                                        говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu-links__item">Контакты</a>
                                    <a href="http://vitbiomed.ru/nezavisimaya-otsenka-kachestva-okazaniya-uslug/"
                                       class="footer-menu-links__item">Независимая оценка качества<br> оказания
                                        услуг</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Информация</div>
                                <div class="footer-menu-links">
                                    <a href="{{ route('publication.all.index') }}"
                                       class="footer-menu-links__item">Блог</a>
                                    <a href="#" class="footer-menu-links__item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu-links__item">Вопросы</a>
                                    <a href="http://vitbiomed.ru/normativnie-documenti/"
                                       class="footer-menu-links__item">Нормативные документы</a>
                                    <a data-uk-modal="" href="#auth-modal"
                                       class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                       data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg"
                                       class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                       data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Пробиотики</div>
                                <div class="footer-menu-links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu-links__item">Доставка
                                        и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu-links__item">Купить
                                        пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu-links__item">Купить
                                        курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/"
                                       class="footer-menu-links__item">Партнерам</a>
                                </div>
                            </div>
                            <div class="footer-menu__column footer-menu__column--mobile">
                                <div class="footer-menu__title">Мы в соц сетях</div>
                                <div class="footer-menu-links">
                                    <a href="https://www.facebook.com/vitbiomed/"
                                       class="footer-menu-links__item footer-social-link footer-social-link--facebook">Facebook</a>
                                    <a href="https://vk.com/vitbiomed "
                                       class="footer-menu-links__item footer-social-link footer-social-link--vk">Vkontakte</a>
                                    <a href="https://plus.google.com/+vitbiomed"
                                       class="footer-menu-links__item footer-social-link footer-social-link--google">Google+</a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                       class="footer-menu-links__item footer-social-link footer-social-link--youtube">Youtube</a>
                                    <a href="https://www.instagram.com/vitbiomed/"
                                       class="footer-menu-links__item footer-social-link footer-social-link--instagram">Instagram</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tablet-visible">
                        <div class="footer-social footer-social--tablet">
                            <a href="https://www.facebook.com/vitbiomed/"
                               class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed"
                               class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                               class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/"
                               class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-offset-1 col-md-3 col-sm-12-override">
                        <a href="#openModal" class="btn-link">
                            <div class="buy-btn">Подписаться</div>
                        </a>

                        <div class="contacts">
                            <div class="contacts__item contacts__item--phone">
                                Онлайн-магазин:<br>
                                +7 (962) 938-26-40<br>
                                order@vitbiomed.ru<br>
                            </div>
                            <div class="contacts__item contacts__item--phone">
                                Клиника: <br>
                                +7 (499) 164-13-80<br>
                                info@vitbiomed.ru<br>
                                Москва, ул. 5-я Парковая, д.46
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">Витбиомед &copy; 2016</div>
                    </div>
                </div>
            </div>
            <div class="footer__content--phone">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="white-label white-label--phone">
                            <a href="#" class="white-label__link">Задайте вопрос нашим специалистам</a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-menu--phone">
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Компания</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu--phone__link-item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                       class="footer-menu--phone__link-item">Медицинский центр</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu--phone__link-item">О нас
                                        говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu--phone__link-item">Контакты</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Информация</div>
                                <div class="footer-menu--phone__links">
                                    <a href="#openModal" class="footer-menu--phone__link-item btn-link">Подписка</a>
                                    <a href="#" class="footer-menu--phone__link-item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu--phone__link-item">Вопросы</a>
                                    <a data-uk-modal="" href="#auth-modal"
                                       class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                       data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg"
                                       class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                       data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Пробиотики</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu--phone__link-item">Доставка
                                        и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu--phone__link-item">Купить
                                        пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu--phone__link-item">Купить
                                        курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu--phone__link-item">Партнерам</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-social footer-social--phone">
                            <a href="https://www.facebook.com/vitbiomed/"
                               class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed"
                               class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                               class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/"
                               class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>


<!-- Modals -->

<div id="preloader">
    <div class="table">
        <div class="cell">
            <div class="windows8">
                <div class="wBall" id="wBall_1">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_2">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_3">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_4">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_5">
                    <div class="wInnerBall">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Test modal -->


<!-- callback-modal modal -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>


<script src="/old/bower_components/jquery/dist/jquery.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="/old/bitrix/templates/vit/js/bootstrap.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.form-mask.js"></script>

<script src="/old/bitrix/templates/vit/js/retina.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.stellar.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.magnific-popup.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.dotdotdot.min.js"></script>
<script src="/old/bitrix/templates/vit/js/main.js"></script>

<script src="/old/resources/js/common.js"></script>

<script src="/old/bower_components/angular/angular.min.js"></script>
<script src="/old/bower_components/lodash/dist/lodash.min.js"></script>
<script src="/old/bower_components/hammerjs/hammer.min.js"></script>
<script src="/old/bower_components/AngularHammer/angular.hammer.min.js"></script>
<script src="/old/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/old/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/old/bower_components/angular-material/angular-material.min.js"></script>
<script src="/old/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="/old/dist/bundle.js"></script>
<script src="/old/bitrix/templates/vit/js/auth.js"></script>
<script src="/old/bitrix/templates/vit/uikit/uikit.min.js"></script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true,
                    ecommerce: "dataLayer"
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter-->


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>