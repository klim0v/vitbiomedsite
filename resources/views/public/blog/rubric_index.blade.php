<!DOCTYPE html>
<html lang="ru" data-ng-app="app">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{{ $rubric->meta_title }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content="Keywords"/>
    <meta name="description" content="{{ $rubric->meta_description }}"/>
    <link href="/old/bitrix/js/main/core/css/core.css?14392234113963" type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/js/main/core/css/core_popup.css?148665183732860" type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/blog_slider/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news/blog/bitrix/news.list/.default/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/menu/standart/style.css?1439223420581" type="text/css"
          rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/news.list/blog_popular/style.css?1439223420150"
          type="text/css" rel="stylesheet"/>
    <link href="/old/bitrix/components/bitrix/socserv.auth.form/templates/flat/style.css?14845676842847" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/menu/menusi/style.css?1482946469581" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/sender.subscribe/template1/style.css?14829457374640"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/components/bitrix/form.result.new/popup/style.css?1439223420666"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/bitrix/templates/vit/template_styles.css?1498751288196458" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <script type="text/javascript">if (!window.BX) window.BX = {
            message: function (mess) {
                if (typeof mess == 'object') for (var i in mess) BX.message[i] = mess[i];
                return true;
            }
        };</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'JS_CORE_LOADING': 'Загрузка...',
            'JS_CORE_NO_DATA': '- Нет данных -',
            'JS_CORE_WINDOW_CLOSE': 'Закрыть',
            'JS_CORE_WINDOW_EXPAND': 'Развернуть',
            'JS_CORE_WINDOW_NARROW': 'Свернуть в окно',
            'JS_CORE_WINDOW_SAVE': 'Сохранить',
            'JS_CORE_WINDOW_CANCEL': 'Отменить',
            'JS_CORE_WINDOW_CONTINUE': 'Продолжить',
            'JS_CORE_H': 'ч',
            'JS_CORE_M': 'м',
            'JS_CORE_S': 'с',
            'JSADM_AI_HIDE_EXTRA': 'Скрыть лишние',
            'JSADM_AI_ALL_NOTIF': 'Показать все',
            'JSADM_AUTH_REQ': 'Требуется авторизация!',
            'JS_CORE_WINDOW_AUTH': 'Войти',
            'JS_CORE_IMAGE_FULL': 'Полный размер'
        });</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'LANGUAGE_ID': 'ru',
            'FORMAT_DATE': 'DD.MM.YYYY',
            'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
            'COOKIE_PREFIX': 'BITRIX_SM',
            'SERVER_TZ_OFFSET': '10800',
            'SITE_ID': 's1',
            'SITE_DIR': '/',
            'USER_ID': '',
            'SERVER_TIME': '1508931921',
            'USER_TZ_OFFSET': '0',
            'USER_TZ_AUTO': 'Y',
            'bitrix_sessid': '5f2b7e6cbd106ff0f2d8cdfbe6cff148'
        });</script>


    <script type="text/javascript" src="/old/bitrix/js/main/core/core.js?1486651837116711"></script>
    <script type="text/javascript" src="/old/bitrix/js/main/core/core_popup.js?148665183741651"></script>
    <script type="text/javascript" src="/old/bitrix/js/main/core/core_ajax.js?148665183735622"></script>



    <script type="text/javascript">var _ba = _ba || [];
        _ba.push(["aid", "645478648abbf1d479a423fd862edac4"]);
        _ba.push(["host", "vitbiomed.ru"]);
        (function () {
            var ba = document.createElement("script");
            ba.type = "text/javascript";
            ba.async = true;
            ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(ba, s);
        })();</script>


    <meta name="keywords" content="Keywords"/>
    <meta name="description" content="Description"/>

    <link href="/old/bitrix/templates/vit/css/swiper.min.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/preloader.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/magnific-popup.css" rel="stylesheet">

    <link href="/old/bitrix/templates/vit/css/new_styles.css" rel="stylesheet">

    <link href="/old/static/css/font.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Exo+2:700,300italic,200,900italic,500italic,800,300,600italic,500,800italic,900,100italic,200italic,600,700italic,400,100,400italic&amp;subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="icon" href="old/favicon.ico" type="image/x-icon"/>

    <link rel="stylesheet" href="/old/dist/bundle.css?rev=1ede78ce969cf752c75bdda5642606e0">
    <link rel="stylesheet" href="/old/dist/magnific-popup.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


    <link href="/old/bitrix/templates/vit/css/magnific-popup.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>

</head>
<body data-spy="scroll" data-target="#nav" class="likearts-content">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4&appId=273276476164317";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<!-- MODAL WINDOW LK AUTH -->

<div class="md-overlay"></div>
<!--//auth-modal-->

<!-- MODAL WINDOW LK reg -->

<!-- MODAL WINDOW OVERLAY -->
<div class="md-overlay"></div>
<!--//reg-modal-->


<div class="header-container">
    <div class="container">
        <header class="header">
            <div class="flex-container">
                <div class="flex-item">
                    <a href="/" class="logo-link">
                        <div class="logo"></div>
                    </a>
                </div>
                <div class="flex-item">
                    <div class="menu-desktop">


                        <ul class="menu-desktop-list">

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru"
                                                                   class="menu-desktop-list__link">Клиника</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru"
                                                                   class="menu-desktop-list__link">Пробиотики</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru/courses/"
                                                                   class="menu-desktop-list__link">Курсы</a></li>

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/reviews/"
                                                                   class="menu-desktop-list__link">Отзывы</a></li>

                            <li class="menu-desktop-list__item"><a href="{{ route('publication.all.index') }}"
                                                                   class="menu-desktop-list__link">Блог</a></li>


                            <li class="menu-desktop-list__item">
                                <span class="phone">+7 (499) 164-13-80</span>
                            </li>
                        </ul>
                    </div>
                    <div class="menu-mobile-container">
                        <div class="menu-mobile" data-ng-controller="MobileMenuController as mobileMenu">
                            <div id="menu-mobile-btn" data-hm-tap="mobileMenu.controlMenu($event)"
                                 class="menu-mobile__btn">
                                <i class="material-icons menu-mobile__icon">view_headline</i>
                            </div>
                            <div id="menu-mobile-body" class="menu-mobile__body display-hide">
                                <ul class="menu-mobile-list">
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Компания</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://vitbiomed.ru/proizvodstvo/"
                                               class="menu-mobile-submenu__link">Производство</a>
                                            <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                               class="menu-mobile-submenu__link">Медицинский центр</a>
                                            <a href="http://vitbiomed.ru/reviews/" class="menu-mobile-submenu__link ">О
                                                нас говорят</a>
                                            <a href="http://vitbiomed.ru/nashi-kontakty/"
                                               class="menu-mobile-submenu__link">Контакты</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Информация</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="#" class="menu-mobile-submenu__link ">Публичная оферта</a>
                                            <a href="http://store.vitbiomed.ru/faq/" class="menu-mobile-submenu__link">Вопросы</a>
                                            <a data-uk-modal="" href="#auth-modal"
                                               class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                               data-modal="auth-modal">Вход</a>
                                            <a data-uk-modal="" href="#auth-modal-reg"
                                               class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                               data-modal="auth-modal-reg">Регистрация</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Пробиотики</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://store.vitbiomed.ru/dostavka/"
                                               class="menu-mobile-submenu__link">Доставка и оплата</a>
                                            <a href="http://store.vitbiomed.ru/" class="menu-mobile-submenu__link">Купить
                                                пробиотики</a>
                                            <a href="http://store.vitbiomed.ru/courses/"
                                               class="menu-mobile-submenu__link">Купить курс</a>
                                            <a href="http://vitbiomed.ru/partnyeram/" class="menu-mobile-submenu__link">Партнерам</a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="social-block">
                                    <a href="https://www.facebook.com/vitbiomed/">
                                        <div class="social-item social-item--facebook"></div>
                                    </a>
                                    <a href="https://www.instagram.com/vitbiomed/">
                                        <div class="social-item social-item--instagram"></div>
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ">
                                        <div class="social-item social-item--youtube"></div>
                                    </a>
                                    <a href="https://plus.google.com/+vitbiomed">
                                        <div class="social-item social-item--google"></div>
                                    </a>
                                    <a href="https://vk.com/vitbiomed ">
                                        <div class="social-item social-item--vk"></div>
                                    </a>
                                </div>
                                <div class="phone-block">
                                    <a href="tel:+74991641380">
                                        <div class="phone-btn">
                                            <i class="material-icons phone-btn__icon">call</i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </header>
    </div>
</div>

<!-- /header -->


<section class="blog">
    <div class="blog-slider">
        <div id="comp_448527dffd6fae81a7741d6e64ec3e68">
            <div id="slider" class="carousel slide" data-ride="carousel">
                <!-- slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <!-- slide back -->
                        <img

                                border="0"
                                src="/old/upload/iblock/6a3/6a35174469d6c8f5932d8f4339e27dde.jpg"
                                width="1920"
                                height="320"


                        />
                        <div class="slide-content">
                            <h3>Кто живет внутри нас?</h3>
                            <a href="/blog/vsye-o-disbakterioze/kto-zhivet-vnutri-nas/">ЧИТАТЬ СТАТЬЮ »</a>
                        </div>
                    </div>


                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                    <i class="slide-left"></i>
                </a>
                <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                    <i class="slide-right"></i>
                </a>
            </div>


        </div>          <!-- search -->
        <div class="search">
            <div class="container">
                <div class="form">
                    <form action="/blog/" method="GET">
                        <input type="text" name="search"
                               placeholder="Для поиска по статьям, введите интересующий Вас запрос" value="">
                        <button>НАЙТИ<span class="hidden-xs"> СТАТЬИ</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-sm-9 articles-prew-1920">

                <div class="articles-prews">
                    @include('public.blog.publication_list', ['publications' => $publications])
                </div>


            </div>
            <div class="col-sm-3 hidden-xs articles-menu-1920">
                @include('public.blog.rubric_menu')
            </div>
        </div>
    </div>
</section>

<br>


<div class="footer-container">
    <div class="container">
        <footer class="footer">
            <div class="footer__content--desktop">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-label">
                            <a href="mailto:info@vitbiomed.ru" class="white-label__link">Задайте вопрос нашим
                                специалистам</a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-sm-12-override">
                        <div class="footer-menu--desktop">
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Компания</div>
                                <div class="footer-menu-links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu-links__item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/" class="footer-menu-links__item">Медицинский
                                        центр</a>
                                    <a href="http://vitbiomed.ru/uslugi/" class="footer-menu-links item">Услуги</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu-links__item">О нас
                                        говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu-links__item">Контакты</a>
                                    <a href="http://vitbiomed.ru/nezavisimaya-otsenka-kachestva-okazaniya-uslug/"
                                       class="footer-menu-links__item">Независимая оценка качества<br> оказания
                                        услуг</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Информация</div>
                                <div class="footer-menu-links">
                                    <a href="{{ route('publication.all.index') }}" class="footer-menu-links__item">Блог</a>
                                    <a href="#" class="footer-menu-links__item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu-links__item">Вопросы</a>
                                    <a href="http://vitbiomed.ru/normativnie-documenti/"
                                       class="footer-menu-links__item">Нормативные документы</a>
                                    <a data-uk-modal="" href="#auth-modal"
                                       class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                       data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg"
                                       class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                       data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Пробиотики</div>
                                <div class="footer-menu-links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu-links__item">Доставка
                                        и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu-links__item">Купить
                                        пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu-links__item">Купить
                                        курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/"
                                       class="footer-menu-links__item">Партнерам</a>
                                </div>
                            </div>
                            <div class="footer-menu__column footer-menu__column--mobile">
                                <div class="footer-menu__title">Мы в соц сетях</div>
                                <div class="footer-menu-links">
                                    <a href="https://www.facebook.com/vitbiomed/"
                                       class="footer-menu-links__item footer-social-link footer-social-link--facebook">Facebook</a>
                                    <a href="https://vk.com/vitbiomed "
                                       class="footer-menu-links__item footer-social-link footer-social-link--vk">Vkontakte</a>
                                    <a href="https://plus.google.com/+vitbiomed"
                                       class="footer-menu-links__item footer-social-link footer-social-link--google">Google+</a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                       class="footer-menu-links__item footer-social-link footer-social-link--youtube">Youtube</a>
                                    <a href="https://www.instagram.com/vitbiomed/"
                                       class="footer-menu-links__item footer-social-link footer-social-link--instagram">Instagram</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tablet-visible">
                        <div class="footer-social footer-social--tablet">
                            <a href="https://www.facebook.com/vitbiomed/"
                               class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed"
                               class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                               class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/"
                               class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-offset-1 col-md-3 col-sm-12-override">
                        <a href="#openModal" class="btn-link">
                            <div class="buy-btn">Подписаться</div>
                        </a>

                        <div class="contacts">
                            <div class="contacts__item contacts__item--phone">
                                Онлайн-магазин:<br>
                                +7 (962) 938-26-40<br>
                                order@vitbiomed.ru<br>
                            </div>
                            <div class="contacts__item contacts__item--phone">
                                Клиника: <br>
                                +7 (499) 164-13-80<br>
                                info@vitbiomed.ru<br>
                                Москва, ул. 5-я Парковая, д.46
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">Витбиомед &copy; 2016</div>
                    </div>
                </div>
            </div>
            <div class="footer__content--phone">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="white-label white-label--phone">
                            <a href="#" class="white-label__link">Задайте вопрос нашим специалистам</a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-menu--phone">
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Компания</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu--phone__link-item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                       class="footer-menu--phone__link-item">Медицинский центр</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu--phone__link-item">О нас
                                        говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu--phone__link-item">Контакты</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Информация</div>
                                <div class="footer-menu--phone__links">
                                    <a href="#openModal" class="footer-menu--phone__link-item btn-link">Подписка</a>
                                    <a href="#" class="footer-menu--phone__link-item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu--phone__link-item">Вопросы</a>
                                    <a data-uk-modal="" href="#auth-modal"
                                       class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                       data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg"
                                       class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                       data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Пробиотики</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu--phone__link-item">Доставка
                                        и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu--phone__link-item">Купить
                                        пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu--phone__link-item">Купить
                                        курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu--phone__link-item">Партнерам</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-social footer-social--phone">
                            <a href="https://www.facebook.com/vitbiomed/"
                               class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed"
                               class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                               class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/"
                               class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>


<!-- Modals -->

<!-- ask modal -->

<div id="preloader">
    <div class="table">
        <div class="cell">
            <div class="windows8">
                <div class="wBall" id="wBall_1">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_2">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_3">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_4">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_5">
                    <div class="wInnerBall">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Test modal -->


<!-- callback-modal modal -->

<!-- fastbuy-modal modal -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>


<script src="/old/bower_components/jquery/dist/jquery.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="/old/bitrix/templates/vit/js/bootstrap.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.form-mask.js"></script>

<script src="/old/bitrix/templates/vit/js/retina.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.stellar.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.magnific-popup.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.dotdotdot.min.js"></script>
<script src="/old/bitrix/templates/vit/js/main.js"></script>

<script src="/old/resources/js/common.js"></script>

<script src="/old/bower_components/angular/angular.min.js"></script>
<script src="/old/bower_components/lodash/dist/lodash.min.js"></script>
<script src="/old/bower_components/hammerjs/hammer.min.js"></script>
<script src="/old/bower_components/AngularHammer/angular.hammer.min.js"></script>
<script src="/old/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/old/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/old/bower_components/angular-material/angular-material.min.js"></script>
<script src="/old/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="/old/dist/bundle.js"></script>
<script src="/old/bitrix/templates/vit/uikit/uikit.min.js"></script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true,
                    ecommerce: "dataLayer"
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter-->


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>