<!-- menu here -->
<div class="blog-menu">
    <span class="title">НАШИ РУБРИКИ</span>
    <ul>

        @foreach($rubrics as $rubric)
        <li>
            <a href="{{route('publication.type.index', [ 'type' => $rubric->slug ])}}">{{$rubric->title}}</a>
        </li>
        @endforeach

    </ul>


</div>
<div class="blog-menu">
    <span class="title">САМОЕ ЧИТАЕМОЕ</span>
    <div id="comp_3c65e1099eecc17c02f9ba7f6ecafdf7">
        <ul>
            @foreach($favorite as $item)
            <li>
                <a href="{{route('publication.index', ['type' => $item->publicationType->slug, 'publication' => $item->slug])}}">{{$item->title}}</a>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<div class="fb-plugin">
    <!-- plugin here -->
    <div class="fb-page" data-href="https://www.facebook.com/vitbiomed" data-small-header="true"
         data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"
         data-show-posts="false">
        <div class="fb-xfbml-parse-ignore">
            <blockquote cite="https://www.facebook.com/vitbiomed"><a
                        href="https://www.facebook.com/vitbiomed">Полезные пробиотики</a></blockquote>
        </div>
    </div>
</div>