@foreach($publications as $publication )
    <div class="article-prew  prew-image-1200" id="bx_651765591_771">
        @if ($publication->thumbnail)
            <img
                    class="hidden-xs"
                    border="0"
                    src="/storage/{{ $publication->thumbnail }}"
                    alt="{{ $publication->title }}"
                    title="{{ $publication->title }}"
            />
        @endif
        <div class="article-content">
            <h2 class="title">{{ $publication->title }}</h2>
            <div class="date">
                <span class="date-data">{{ $publication->created_at_formatted }}</span>
                <span class="date-tags">{{ $publication->publicationType->title }}</span>
            </div>
            @if ($publication->thumbnail)
                <img
                        class="hidden-sm hidden-lg hidden-md"
                        border="0"
                        src="/storage/{{ $publication->thumbnail }}"
                        alt="{{ $publication->title }}"
                        title="{{ $publication->title }}"
                />
            @endif
            <div class="text">
                {!! $publication->annotation !!}
            </div>
            <a href="{{ route('publication.index', [ 'type' => $publication->publicationType->slug, 'publication' => $publication->slug]) }}"
               class="read-more">Читать далее »</a>
        </div>
    </div>
@endforeach
@if($publications->hasMorePages())
    <button class="show-all" id="show_more_publications" data-href="{{$publications->nextPageUrl(). ( isset($query) ? '&search='.$query : '') }}"
            onclick="return showMore(this);">
        ЗАГРУЗИТЬ ЕЩЁ СТАТЬИ
    </button>

@endif

