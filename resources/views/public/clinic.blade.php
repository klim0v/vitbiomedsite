@include('public.header2')

<section class="b-appeal-to-us-text">
    <div class="container">
        <div class="b-appeal-to-us-text__title">
            <span>С чем к нам обращаются</span>
        </div>
        <div class="b-appeal-to-us-text__inner">
            <div>
                <div>
                    <a href="#">Пониженный иммунитет              </a>
                    <a href="#">Утомляемость, нервозность </a>
                    <a href="#">Проблемы с кожей и волосами       </a>
                    <a href="#">Часто болеющие дети               </a>
                </div>
            </div>
            <div>
                <div>
                    <a href="#">Аллергические реакции           </a>
                    <a href="#">Дерматит, экзема, рожа          </a>
                    <a href="#">Нарушение обмена веществ        </a>
                    <a href="#">Последствия приёма антибиотиков </a>
                </div>
            </div>
            <div>
                <div>
                    <a href="#">Расстройство кишечника </a>
                    <a href="#">Заболевания ЖКТ        </a>
                    <a href="#">НЯК, болезнь Крона     </a>
                    <a href="#">Кишечные инфекции      </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="b-about-clinc-our-doctors">
    <div class="container">
        <div class="b-about-clinc-our-doctors__main-title">
            Наши врачи
        </div>
        <div class="b-about-clinc-our-doctors__wrap">
            @foreach($doctors as $doctor)
            <div class="b-about-clinc-our-doctors__inner">

                <div class="b-about-clinc-our-doctors__avatar">
                    <div class="b-about-clinc-our-doctors__avatar-image" @if($doctor->thumbnail) style="background-image: url(/storage/{{ $doctor->thumbnail }}); background-size: cover;" @endif></div>
                    <a href="#" class="b-about-clinc-our-doctors__avata-feedback">Отзывов: {{ $doctor->feedback->count() }}</a>
                </div>

                <div class="b-about-clinc-our-doctors__text">
                    <div class="b-about-clinc-our-doctors__name">
                        {{ $doctor->full_name }}
                    </div>
                    <div class="b-about-clinc-our-doctors__text-p">
                        {!! $doctor->description !!}
                    </div>
                    <div class="b-about-clinc-our-doctors__text-p">
                        {{ $doctor->speciality }}<br>
                        Стаж: {{ $doctor->work_experience }}
                    </div>
                </div>

                <div class="b-about-clinc-our-doctors__btn-wrap">
                    <a class="b-about-clinc-our-doctors__btn" href="#order-form" rel="modal:open">
                        <span>Записаться</span>
                    </a>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="b-about-clinic-add-service">
    <div class="container">
        <div class="b-about-clinic-add-service__head">
            Дополнительные услуги
        </div>
        <div class="b-about-clinic-add-service__wrap">
            <div class="b-about-clinic-add-service__item">
                <div class="b-about-clinic-add-service__icon b-about-clinic-add-service__icon--i1"></div>
                <div class="b-about-clinic-add-service__name">Анализы</div>
            </div>
            <div class="b-about-clinic-add-service__item">
                <div class="b-about-clinic-add-service__icon b-about-clinic-add-service__icon--i2"></div>
                <div class="b-about-clinic-add-service__name">Инъекции, капельницы</div>
            </div>
            <div class="b-about-clinic-add-service__item">
                <div class="b-about-clinic-add-service__icon b-about-clinic-add-service__icon--i3"></div>
                <div class="b-about-clinic-add-service__name">Массаж</div>
            </div>
            <div class="b-about-clinic-add-service__item">
                <div class="b-about-clinic-add-service__icon b-about-clinic-add-service__icon--i4"></div>
                <div class="b-about-clinic-add-service__name">УЗИ, ЭКГ</div>
            </div>
        </div>
    </div>
</section>

<section class="b-about-clinic-feedback">
    <div class="container">
        <div class="b-about-clinic-feedback__main_title">Отзывы</div>
        <div class="b-about-clinic-feedback__wrap">

            @foreach($feedbacks as $feedback)
            <div class="b-about-clinic-feedback__inner">
                <div class="b-about-clinic-feedback__box">

                    <div class="b-about-clinic-feedback__head">
                        <div class="b-about-clinic-feedback__avatar" @if($feedback->thumbnail) style="background-image: url(/storage/{{ $feedback->thumbnail }}); background-size: cover;" @endif></div>
                        <div class="b-about-clinic-feedback__info">
                            <div class="b-about-clinic-feedback__name">
                                @if (!empty($feedback->facebook))<a href="{{ $feedback->facebook }}" class="b-about-clinic-feedback__name-social"></a> @endif
                                <span class="b-about-clinic-feedback__name-text">{{ $feedback->full_name }}</span>
                            </div>
                            <div class="b-about-clinic-feedback__stars">
                                @for ($i = 0; $i < $feedback->rating; $i++)
                                <span class="active"></span>
                                @endfor
                                @for ($i = 0; $i < 5 - $feedback->rating; $i++)
                                    <span></span>
                                @endfor
                            </div>
                        </div>
                    </div>

                    <div class="b-about-clinic-feedback__foot">
                        <p class="b-about-clinic-feedback__text">
                            {{ str_limit($feedback->text, 70) }}
                        </p>
                        <a href="{{ route('feedbacks') }}#feedback{{ $feedback->id }}" class="b-about-clinic-feedback__link">Подробнее...</a>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="b-map">
    <div class="b-map__address">
        <div class="container">
            <span>Адрес: Москва, ул. 5-я Парковая, 46</span>
        </div>

    </div>
    <div class="b-map__map-inner" id="google-map"></div>
    <div class="b-map__bottom_text">
        <div class="container">
            м. Первомайская: 14 мин. пешком (первый вагон из центра, выход налево)<br>
            м. Измайловская: первый вагон из центра, троллейбусы: №23, №51, автобус №34, м/т: №23м, №51м до остановки “Верхняя Первомайская улица” (~6 мин)<br>
            м. Щелковская: последний вагон из центра, тролл. №23, м/т №23м (~6 мин)<br>
            м. Преображенская площадь: первый вагон из центра, автобус №230 или м/т №230м до остановки “5-я Парковая улица” (~14 мин)<br>
            м. Черкизовская: автобус №230, м/т 230м (~8 мин)
        </div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    var map;
    function initialize() {
        var myLatlng = new google.maps.LatLng(55.800413, 37.788275);
        map = new google.maps.Map(document.getElementById('google-map'), {
            zoom: 16,
            center: myLatlng,
            scrollwheel: false,
            draggable: false
        });
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

@include('public.footer')