<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title></title>

    @include('public.header_includes')

</head>
<body>

<section class="b-header b-header--white b-header--with-bottom-btn b-header--with-center-menu">
    <div class="container">

        @include('public.header_menu')

        <div class="b-header__add-bottom-btn">
            <a class="b-header__add-bottom-btn__inner" href="#order-form" rel="modal:open">
                <span>Записаться <span class="hide-max-mobile">на прием</span></span>
            </a>
        </div>

    </div>
</section>

<div class="f-mobile-reverse-order">
    @include('public.header_slider')

    @include('public.header_menu2')
</div>

