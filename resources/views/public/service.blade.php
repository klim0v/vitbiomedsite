@include('public.header3')



<section class="l-big-title">
    <div class="container">
        <div class="b-big-title b-big-title__center">
            <span>Цены на медицинские услуги</span>
        </div>
    </div>
</section>

<section class="l-list-dropdown-lists">

    <div class="l-dropdown-list">
        @foreach($categories as $category)
        <div class="b-dropdown-list @if ($loop->first) b-dropdown-list--open @endif">
            <div class="container">
                <div class="b-dropdown-list__inner js-price-med-service-rec">

                    <div class="b-dropdown-list__inner-services">
                        <div class="b-dropdown-list__head">
                            <div class="b-dropdown-list__head-title">
                                {{ $category->name }}
                            </div>
                            <div class="b-dropdown-list__wrap-icon">
                                <div class="b-dropdown-list__icon-dropdown js-dropownlist-open"></div>
                            </div>
                        </div>

                        <div class="b-dropdown-list__content">

                            <div class="l-dropdown-list-table-price">
                                <div class="b-table-price">
                                    <table class="b-table-price__table">
                                        @foreach($category->service as $service)
                                        <tr class="js-price-med-service-rec__item">
                                            <td>
                                                <div class="b-table-price__desc-text">
														<span class="js-price-med-service-rec__open-btn">
															{{ $service->name }}
														</span>
                                                </div>
                                                <span class="b-table-price__price-sign-up-desktop">
                                                    <a class="js-price-med-service-rec__btn-record" style="color: #689f38;" href="#order-form" rel="modal:open" onclick="$('#order_service_id').val({{ $service->id }})">Записаться</a>
                                            </td>
                                            <td>
                                                <span class="b-table-price__price">
                                                    @if ($service->price)
                                                        {{ $service->price }} руб.
                                                    @else
                                                        бесплатно
                                                    @endif
                                                </span>
                                                <span href="#" class="b-table-price__price-sign-up-mobile">Записаться</span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="b-dropdown-list__inner-record"></div>

                </div>
            </div>
        </div>

        @endforeach

    </div>


</section>

<section class="l-btn-load-pdf">
    <div class="container">
        <div class="b-btn-load-pdf">
            <a href="#"></a>
        </div>
        <div>
</section>

@include('public.footer')