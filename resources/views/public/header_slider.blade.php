<section class="b-top-slider after-header-with-bottom-btn">
    <div class="b-video-bg">
        <video loop muted autoplay poster="video/video.jpg" class="b-video-bg__video">
            <source src="/video/video.ogv" type='video/ogg; codecs="theora, vorbis"'>
            <source src="/video/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
            <source src="/video/video.webm" type='video/webm; codecs="vp8, vorbis"'>
        </video>
    </div>
    <div class="container">
        <div class="b-top-slider__wrap-slider">
            <div>
                <div class="b-top-slider__inner-slider">
                    <div class="b-top-slider__title">Медицинский центр «Витбиомед»</div>
                    <p class="b-top-slider__text">
                        Первый в России специализированный медицинский центр по лечению дисбактериоза, иммунодефицитов и аллергозов у взрослых и детей
                    </p>
                    <a href="{{ route('clinics') }}" class="b-top-slider__btn btn-bg-none btn-with-next-white">
                        <span>Подробнее</span>
                    </a>
                </div>
            </div>
            <div>
                <div class="b-top-slider__inner-slider">
                    <div class="b-top-slider__title">Многолетний опыт работы</div>
                    <p class="b-top-slider__text">
                        Более 15-ти лет отрабатывались оптимальные схемы, дозы и длительность применения пробиотиков, наряду с базисной, общеукрепляющей терапией

                    </p>
                    <a href="{{ route('clinics') }}" class="b-top-slider__btn btn-bg-none btn-with-next-white">
                        <span>Подробнее</span>
                    </a>
                </div>
            </div>
            <div>
                <div class="b-top-slider__inner-slider">
                    <div class="b-top-slider__title">Сотрудничество с микробиологами</div>
                    <p class="b-top-slider__text">
                        Сплоченная работа врачей и микробиологов позволяет регулярно совершенствовать ассортимент и схемы приема иммунобиологических препаратов
                    </p>
                    {{--<a href="#" class="b-top-slider__btn btn-bg-none btn-with-next-white">--}}
                        {{--<span>Подробнее</span>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
    </div>

</section>