<!DOCTYPE html>
<html lang="ru" data-ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/old/store/bitrix/js/main/core/css/core.css?14392234113963" type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/js/main/core/css/core_popup.css?148665183732860" type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/courses/style.css?1478377898697"
          type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/courses/bitrix/catalog.element/.default/style.css?148251124432847"
          type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/components/bitrix/socserv.auth.form/templates/flat/style.css?14845676842847"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/menu/menusi/style.css?1484155620665"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket_colitem/style.css?14792952024229"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket/style.css?14787141634229"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/form/clickCart/bitrix/form.result.new/.default/style.css?1479744509666"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sender.subscribe/template1/style.css?14791328994640"
          type="text/css" data-template-style="true" rel="stylesheet"/>

    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core.js?1486651837116711"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core_popup.js?148665183741651"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core_ajax.js?148665183735622"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/currency/core_currency.js?14779190782402"></script>

    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket_colitem/script.js?14792952025292"></script>
    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket/script.js?14794011705409"></script>
    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/courses/bitrix/catalog.element/.default/script.js?147929744275328"></script>


    <title>{{$course->title}}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/old/store/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/angular-material/angular-material.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/ng-dialog/css/ngDialog.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/ng-dialog/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="/old/store/dist/bundle.css?rev=1ede78ce969cf752c75bdda5642606e0">
    <link rel="stylesheet" href="/old/store/resources/css/style.css">
    <link rel="stylesheet" href="/old/store/resources/css/media.css">
    <link rel="stylesheet" href="/old/store/dist/magnific-popup.css">
    <link rel="stylesheet" href="/old/store/resources/libs/owl-carousel/owl.carousel.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <style> .toggle {
            position: absolute !important;
            transform: rotate(-270deg) translateX(-50%);
            right: 0;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="likearts-content" data-ng-cloak>
<div class="container-fluid">

    <!-- MODAL WINDOW LK AUTH -->
@include('public.shop.auth')
    <!-- MODAL WINDOW OVERLAY -->
    <div class="md-overlay"></div>
    <!--//auth-modal-->

    <!-- MODAL WINDOW LK reg -->
    <div class="md-modal uk-modal" id="auth-modal-reg">
        <div class="md-content uk-modal-content">
            <div class="md-top">
                <div class="md-close" onclick="$('.md-overlay').click();">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7">
                        <polygon
                                points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/>
                    </svg>
                </div>
                <div class="md-title">Регистрация</div>
                <div class="lk-social-media">
                    <span>через</span>
                    <ul class="lk-social-media__list">
                        <script type="text/javascript">
                            function BxSocServPopup(id) {
                                var content = BX("bx_socserv_form_" + id);
                                if (content) {
                                    var popup = BX.PopupWindowManager.create("socServPopup" + id, BX("bx_socserv_icon_" + id), {
                                        autoHide: true,
                                        closeByEsc: true,
                                        angle: {offset: 24},
                                        content: content,
                                        offsetTop: 3
                                    });

                                    popup.show();

                                    var input = BX.findChild(content, {
                                        'tag': 'input',
                                        'attribute': {'type': 'text'}
                                    }, true);
                                    if (input) {
                                        input.focus()
                                    }

                                    var button = BX.findChild(content, {
                                        'tag': 'input',
                                        'attribute': {'type': 'submit'}
                                    }, true);
                                    if (button) {
                                        button.className = 'btn btn-primary'
                                    }
                                }
                            }
                        </script>

                        <div class="bx-authform-social">
                            <ul>
                                <li>
                                    <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1801245380125777&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fcourses%2Fkurs-bereznoe-vosstanovlenie%2F%3Fauth_service_id%3DFacebook%26check_key%3Dd7fb55907579226e83bfdf96d1ea641a%26backurl%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F&amp;scope=email,publish_actions,user_friends&amp;display=popup', 680, 600)"
                                       title="Facebook"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://oauth.yandex.ru/authorize?response_type=code&amp;client_id=88cb897161164506923f302388cd2ca5&amp;display=popup&amp;state=site_id%3Ds2%26backurl%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F%253Fcheck_key%253Dd7fb55907579226e83bfdf96d1ea641a%26mode%3Dopener%26redirect_url%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F', 680, 600)"
                                       title="Яндекс"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=5825158&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fcourses%2Fkurs-bereznoe-vosstanovlenie%2F%3Fauth_service_id%3DVKontakte&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds2%26backurl%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F%253Fcheck_key%253Dd7fb55907579226e83bfdf96d1ea641a%26redirect_url%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F', 660, 425)"
                                       title="ВКонтакте"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=674779939050-7ejv3833b2g3vivpkir9ekio10id5d8j.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGooglePlusOAuth%26site_id%3Ds2%26backurl%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F%253Fcheck_key%253Dd7fb55907579226e83bfdf96d1ea641a%26mode%3Dopener%26redirect_url%3D%252Fcourses%252Fkurs-bereznoe-vosstanovlenie%252F', 580, 400)"
                                       title="Google+"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                    <span>или</span>
                </div>
            </div>
            <div class="md-form">
                <form method="POST" action="/courses/kurs-bereznoe-vosstanovlenie/" name="regform"
                      class="uk-form uk-form-stacked">
                    <input type="hidden" name="TYPE" value="REGISTRATION"/>
                    <input type="hidden" name="register_submit_button" value="Y"/>
                    <input type="hidden" class="api-mf-antibot" value="" name="ANTIBOT[NAME]">
                    <input type="hidden" name="backurl" value="" class="backurl">

                    <div class="uk-form-row">
                        <label class="uk-form-label">Имя:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[NAME]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Фамилия:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[LAST_NAME]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Телефон:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[PERSONAL_PHONE]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Адрес e-mail:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[EMAIL]"
                                   value="">
                        </div>
                    </div>


                    <div class="uk-form-row">
                        <button type="submit"
                                name="register_submit_button"
                                class="btn" data-md-ink-ripple="#fff"
                                value="Регистрация">Регистрация
                        </button>
                    </div>
                    <div class="uk-form-row">
                        <noindex>
                            <a href=""
                               rel="nofollow"
                               class="ajax-link">Авторизация</a>
                        </noindex>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- MODAL WINDOW OVERLAY -->
    <div class="md-overlay"></div>
    <!--//reg-modal-->

    <div class="header-container">
        <div class="container">
            <header class="header">
                <div class="flex-container">
                    <div class="flex-item">
                        <a href="/" class="logo-link">
                            <div class="logo"></div>
                        </a>
                    </div>
                    <div class="flex-item">
                        <div class="menu-desktop">


                            <ul class="menu-desktop-list">

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru"
                                                                       class="menu-desktop-list__link">Клиника</a></li>

                                <li class="menu-desktop-list__item"><a href="/" class="menu-desktop-list__link">Пробиотики</a>
                                </li>

                                <li class="menu-desktop-list__item"><a href="/courses/"
                                                                       class="menu-desktop-list__link menu-desktop-list__link--active">Курсы</a>
                                </li>

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/reviews/"
                                                                       class="menu-desktop-list__link">Отзывы</a></li>

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/blog/"
                                                                       class="menu-desktop-list__link">Блог</a></li>


                                @include('public.shop.menu_user_auth')

                            </ul>
                        </div>
                        <div class="menu-mobile-container">
                            <div class="menu-mobile" data-ng-controller="MobileMenuController as mobileMenu">
                                <div id="menu-mobile-btn" data-hm-tap="mobileMenu.controlMenu($event)"
                                     class="menu-mobile__btn">
                                    <i class="material-icons menu-mobile__icon">view_headline</i>
                                </div>
                                <div id="menu-mobile-body" class="menu-mobile__body display-hide">
                                    <ul class="menu-mobile-list">
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Компания</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="http://vitbiomed.ru/proizvodstvo/"
                                                   class="menu-mobile-submenu__link">Производство</a>
                                                <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                                   class="menu-mobile-submenu__link">Медицинский центр</a>
                                                <a href="http://vitbiomed.ru/reviews/"
                                                   class="menu-mobile-submenu__link ">О нас говорят</a>
                                                <a href="http://vitbiomed.ru/nashi-kontakty/"
                                                   class="menu-mobile-submenu__link">Контакты</a>
                                            </div>
                                        </li>
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Информация</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="#" class="menu-mobile-submenu__link ">Публичная оферта</a>
                                                <a href="http://store.vitbiomed.ru/faq/"
                                                   class="menu-mobile-submenu__link">Вопросы</a>
                                                <a data-uk-modal="" href="#auth-modal"
                                                   class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                                   data-modal="auth-modal">Вход</a>
                                                <a data-uk-modal="" href="#auth-modal-reg"
                                                   class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                                   data-modal="auth-modal-reg">Регистрация</a>
                                            </div>
                                        </li>
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Пробиотики</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="http://store.vitbiomed.ru/dostavka/"
                                                   class="menu-mobile-submenu__link">Доставка и оплата</a>
                                                <a href="http://store.vitbiomed.ru/" class="menu-mobile-submenu__link">Купить
                                                    пробиотики</a>
                                                <a href="http://store.vitbiomed.ru/courses/"
                                                   class="menu-mobile-submenu__link">Купить курс</a>
                                                <a href="http://vitbiomed.ru/partnyeram/"
                                                   class="menu-mobile-submenu__link">Партнерам</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="social-block">
                                        <a href="https://www.facebook.com/vitbiomed/">
                                            <div class="social-item social-item--facebook"></div>
                                        </a>
                                        <a href="https://www.instagram.com/vitbiomed/">
                                            <div class="social-item social-item--instagram"></div>
                                        </a>
                                        <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ">
                                            <div class="social-item social-item--youtube"></div>
                                        </a>
                                        <a href="https://plus.google.com/+vitbiomed">
                                            <div class="social-item social-item--google"></div>
                                        </a>
                                        <a href="https://vk.com/vitbiomed ">
                                            <div class="social-item social-item--vk"></div>
                                        </a>
                                    </div>
                                    <div class="phone-block">
                                        <a href="tel:+74991641380">
                                            <div class="phone-btn">
                                                <i class="material-icons phone-btn__icon">call</i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('public.shop.cart.small.index')
                    </div>
                </div>

                <div id="openClickCart" class="modalbg">
                    <div id="comp_7ad265e72172ab88850d60fc3e597cd6">

                        <form name="oneclickbuy" action="" method="POST"
                              onsubmit="event.preventDefault(); buyoneclick(this);" enctype="multipart/form-data"
                              class="ng-pristine ng-valid">
                            <div class="dialogCart">
                                <div class="modalbhCart">
                                    <a href="#close" title="Закрыть" class="closeCart"></a>
                                    <div class="form">
                                        <div class="inCartW">
                                            <div class="Texho">Быстрый заказ</div>
                                            <input type="text" class="inputtext" name="name" value="" placeholder="Имя">
                                            <input type="text" class="inputtext" name="phone" value=""
                                                   placeholder="Телефон">
                                            <!--	<textarea style="display:none;" name="form_textarea_27" cols="40" rows="5" class="inputtextarea">Товары - -->
                                            <!-- / Итого:--><!--</textarea>-->

                                            <input id="oneclickbuy" style="margin-top: 10px;" type="submit"
                                                   name="web_form_submit" class="btn md-ink-ripple"
                                                   data-md-ink-ripple="#fff" value="Отправить">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>
        </div>
    </div>


    <div class="page">
        <div class="container">
            <div class="page-content course" data-ng-controller="ProductItemPageController as productItemPage">
                <h1>{{$course->title}}</h1>
                <div class="course-wrap course-wrap-1">
                    <div class="course-content">
                        <h2>{{$course->subtitle}}</h2><br>
                        <div class="course-img2">


                            <div class="captextin"><span class="captext"></span></div>

                            <div style="display: inline-block;overflow: hidden; height: 100%; width: 100%;">
                                <div class="bx_item_slider" id="{{$course->id}}_big_slider" style="height: 100%;">


                                    <img id="{{$course->id}}_pict"
                                         src="{{$course->image}}"
                                         alt="{{$course->title}}" title="{{$course->title}}">
                                    <div class="bx_stick average left top" style="display:none;"
                                         id="{{$course->id}}_sticker" title=""></div>

                                    <div class="bx_slider_conteiner" id="{{$course->id}}_slider_cont_447"
                                         style="display: ;">
                                        <div class="bx_slider_scroller_container">
                                            <div class="bx_slide">
                                                <ul style="width: 100%;" id="{{$course->id}}_slider_list_447">
                                                    <li data-value="447_396" style="width: 20%; padding-top: 20%"><span
                                                                class="cnt"><span class="cnt_item"
                                                                                  style="background-image:url('{{$course->image}}');"></span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="bx_slide_left" id="{{$course->id}}_slider_left_447"
                                                 style="display: none;" data-value="447"></div>
                                            <div class="bx_slide_right" id="{{$course->id}}_slider_right_447"
                                                 style="display: none;" data-value="447"></div>
                                        </div>
                                    </div>
                                    <div class="bx_slider_conteiner" id="{{$course->id}}_slider_cont_446"
                                         style="display: none;">
                                        <div class="bx_slider_scroller_container">
                                            <div class="bx_slide">
                                                <ul style="width: 100%;" id="{{$course->id}}_slider_list_446">
                                                    <li data-value="446_396" style="width: 20%; padding-top: 20%"><span
                                                                class="cnt"><span class="cnt_item"
                                                                                  style="background-image:url('{{$course->image}}');"></span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="bx_slide_left" id="{{$course->id}}_slider_left_446"
                                                 style="display: none;" data-value="446"></div>
                                            <div class="bx_slide_right" id="{{$course->id}}_slider_right_446"
                                                 style="display: none;" data-value="446"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="captextin"><span class="captext"></span></div>

                        </div>
                    </div>
                    <div class="course-sidebar">
                        <div class="course-discount hidden-xs"></div>
                        <div class="course-form">
                            <form action="#" method="POST" class="form">
                                <div class="course-form__price">
                                    <div id="{{$course->id}}_price" data-price="{{$course->price}}">{{$course->price}}
                                        руб.
                                    </div>
                                    <span class="price-count-1">цена за 1 шт.</span>
                                </div>
                                <div class="course-form__amount-wrap bx_sive">
                                    <div class="amount">
                                        <div class="bx_item_detail bx_blue" id="{{$course->id}}">
                                            <div class="bx_item_container">

                                                <div id="{{$course->id}}_skudiv">

                                                    <div class="bx_item_detail_size" id="{{$course->id}}_prop_57_cont">


                                                        <div class="bx_size_scroller_container">
                                                            <div class="bx_size">
                                                                <ul id="{{$course->id}}_prop_57_list"
                                                                    data-course="{{$course->id}}"
                                                                    style="width: 100%;margin-left:0%;">
                                                                    <label class="toggler js-toggler-l toggler--is-active">
                                                                        <li data-treevalue="2" data-onevalue="2"
                                                                            style="width: 30%; "
                                                                            onclick="toggleAge(this);">
                                                                            <i title="Взрослым"></i><span class="cnt"
                                                                                                          title="Взрослым">Взрослым</span>
                                                                        </li>
                                                                    </label>

                                                                    @if($course->ageCategories->where('key', 'kids')->first())
                                                                        <label class="toggler js-toggler-r">
                                                                            <li data-treevalue="57_10" data-onevalue="1"
                                                                                style="width: 30%; "
                                                                                onclick="toggleAge(this);">
                                                                                <i title="Детям"></i><span class="cnt"
                                                                                                           title="Детям">Детям</span>
                                                                            </li>
                                                                        </label>


                                                                        <div class="toggle">
                                                                            <input style="display:block;"
                                                                                   type="checkbox"
                                                                                   class="check js-switcher">
                                                                            <b class="a switch"></b>
                                                                        </div>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                            <div class="bx_slide_left" style="display: none;"
                                                                 id="{{$course->id}}_prop_57_left"
                                                                 data-treevalue="57"></div>
                                                            <div class="bx_slide_right" style="display: none;"
                                                                 id="{{$course->id}}_prop_57_right"
                                                                 data-treevalue="57"></div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="course-form__amount-wrap">
                                    <div class="amount__title">Количество</div>
                                    <div class="amount">

                                        <div id="{{$course->id}}_basis_price" style="display:none;"
                                             class="item_section_name_gray">Цена {{$course->price}} руб. за 1 шт
                                        </div>
                                        <div class="item_buttons vam">
													<span class="item_buttons_counter_block">
														<a href="javascript:void(0)"

                                                           data-count="-1"
                                                           onclick="addCount(this)"
                                                           class="amount__button amount__button--minus"
                                                           data-md-ink-ripple="#fff" id="{{$course->price}}_quant_down"><i
                                                                    class="material-icons amount-control__icon">remove</i></a>
														<input id="{{$course->id}}_quantity" type="text"
                                                               class="amount__input" readonly value="1">
														<a href="javascript:void(0)"

                                                           data-count="1"
                                                           onclick="addCount(this)"
                                                           class="amount__button amount__button--plus"
                                                           data-md-ink-ripple="#fff" id="{{$course->price}}_quant_up"><i
                                                                    class="material-icons amount-control__icon">add</i></a>
														<span class="bx_cnt_desc"
                                                              id="{{$course->id}}_quant_measure">шт</span>
													</span>

                                        </div>
                                    </div>
                                </div>

                                <span id="{{$course->id}}_basket_actions" style="display: ;">
									<div class="bx_item_detail bx_blue" id="{{$course->id}}">
									<div class="bx_item_container">
																									<a onclick="addToCart(this)"
                                                                                                       data-id="{{$course->id}}"
                                                                                                       data-variant="2"
                                                                                                       href="javascript:void(0);"
                                                                                                       class="btn md-ink-ripple single_add_to_cb"
                                                                                                       data-md-ink-ripple="#fff"
                                                                                                       id="{{$course->id}}_buy_link"><span></span>В корзину</a>
																				</div></div>
													</span>


                            </form>
                        </div>
                    </div>
                </div>
                <div class="course-wrap course-wrap-2">
                    <div class="course-content">


                        <div class="accordion">
                            <div class="accordion__item">
                                <div class="accordion__linkS">Описание</div>
                                <div class="accordion__contentS">
                                    <div class="accordion__text">
                                        {{$course->description}}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Для кого</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {{$course->for_whom}}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Из чего состоит</div>
                                <div class="accordion__content">

                                    <div class="accordion__text course-nabor-row">

                                        @foreach($course->productVariations as $productVariation)
                                            <div class="course-nabor">
                                                <div class="course-nabor__img">
                                                    <a href="{{ route('product.index', $productVariation->product->slug) }}"><img
                                                                src="{{ $productVariation->product->thumb }}"></a>
                                                </div>
                                                <a href="{{ route('product.index', $productVariation->product->slug) }}">{{ $productVariation->product->name }} {{ $productVariation->volume }}
                                                    мл</a>
                                                <br>{{ $productVariation->pivot->count }} шт.
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Особенности</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {{$course->features}}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Почему пробиотики?</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {{$course->why_probiotics}}</div>
                                </div>
                            </div>
                        </div>
                        <p class="consult">Перед прохождением курса мы рекомендуем проконсультироваться с врачом.</p>
                    </div>

                    <div class="course-sidebar">

                        <div class="faq-sidebar">
                            <div class="icon-text icon-text--course hidden-xs">
                                <i class="icon-text__icon"></i>
                                <span class="icon-text__content">{{$course->duration}}</span>
                            </div>
                            <hr>
                            <div class="faq-sidebar-title">Часто задаваемые вопросы</div>
                            <div class="accordion">


                            </div>
                        </div>
                        <div class="testimonials-sidebar">


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="footer-container">
        <div class="container">
            <footer class="footer">
                <div class="footer__content--desktop">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-label">
                                <a href="mailto:info@vitbiomed.ru" class="white-label__link">Задайте вопрос нашим
                                    специалистам</a>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-sm-12-override">
                            <div class="footer-menu--desktop">
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Компания</div>
                                    <div class="footer-menu-links">
                                        <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu-links__item">Производство</a>
                                        <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                           class="footer-menu-links__item">Медицинский центр</a>
                                        <a href="http://vitbiomed.ru/reviews/" class="footer-menu-links__item">О нас
                                            говорят</a>
                                        <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu-links__item">Контакты</a>
                                        <a href="http://vitbiomed.ru/nezavisimaya-otsenka-kachestva-okazaniya-uslug/"
                                           class="footer-menu-links__item">Независимая оценка качества<br> оказания
                                            услуг</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Информация</div>
                                    <div class="footer-menu-links">
                                        <a href="http://vitbiomed.ru/blog/" class="footer-menu-links__item">Блог</a>
                                        <a href="#" class="footer-menu-links__item">Публичная оферта</a>
                                        <a href="http://store.vitbiomed.ru/faq/"
                                           class="footer-menu-links__item">Вопросы</a>
                                        <a href="http://vitbiomed.ru/normativnie-documenti/"
                                           class="footer-menu-links__item">Нормативные документы</a>
                                        <a data-uk-modal="" href="#auth-modal"
                                           class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                           data-modal="auth-modal">Вход</a>
                                        <a data-uk-modal="" href="#auth-modal-reg"
                                           class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                           data-modal="auth-modal-reg">Регистрация</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Пробиотики</div>
                                    <div class="footer-menu-links">
                                        <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu-links__item">Доставка
                                            и оплата</a>
                                        <a href="http://store.vitbiomed.ru/" class="footer-menu-links__item">Купить
                                            пробиотики</a>
                                        <a href="http://store.vitbiomed.ru/courses/" class="footer-menu-links__item">Купить
                                            курс</a>
                                        <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu-links__item">Партнерам</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column footer-menu__column--mobile">
                                    <div class="footer-menu__title">Мы в соц сетях</div>
                                    <div class="footer-menu-links">
                                        <a href="https://www.facebook.com/vitbiomed/"
                                           class="footer-menu-links__item footer-social-link footer-social-link--facebook">Facebook</a>
                                        <a href="https://vk.com/vitbiomed "
                                           class="footer-menu-links__item footer-social-link footer-social-link--vk">Vkontakte</a>
                                        <a href="https://plus.google.com/+vitbiomed"
                                           class="footer-menu-links__item footer-social-link footer-social-link--google">Google+</a>
                                        <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                           class="footer-menu-links__item footer-social-link footer-social-link--youtube">Youtube</a>
                                        <a href="https://www.instagram.com/vitbiomed/"
                                           class="footer-menu-links__item footer-social-link footer-social-link--instagram">Instagram</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-visible">
                            <div class="footer-social footer-social--tablet">
                                <a href="https://www.facebook.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--facebook"></a>
                                <a href="https://plus.google.com/+vitbiomed"
                                   class="footer-social__item footer-social__item--google"></a>
                                <a href="https://vk.com/vitbiomed "
                                   class="footer-social__item footer-social__item--vk"></a>
                                <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                   class="footer-social__item footer-social__item--youtube"></a>
                                <a href="https://www.instagram.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--instagram"></a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-offset-1 col-md-3 col-sm-12-override">
                            <a href="#openModal" class="btn-link">
                                <div class="buy-btn btn" data-md-ink-ripple="#fff">Подписаться</div>
                            </a>
                            <div id="openModal" class="modalbg">
                                <div class="dialog">
                                    <div class="modalbh">
                                        <a href="#close" title="Close" class="close"></a>
                                        <div class="text_df"><span class="spanx">Полезная рассылка</span>
                                            <p>Советы по поддержанию здоровья Рекомендации по питанию Обновления блога
                                                Видео-консультации наших врачей</p>
                                        </div>
                                        <div class="text_df">
                                            <div class="bx-subscribe2" id="sender-subscribe">
                                                <!--'start_frame_cache_sender-subscribe'-->
                                                <script>
                                                    BX.ready(function () {
                                                        BX.bind(BX("bx_subscribe_btn_sljzMT"), 'click', function () {
                                                            setTimeout(mailSender, 250);
                                                            return false
                                                        })
                                                    });

                                                    function mailSender() {
                                                        setTimeout(function () {
                                                            var btn = BX("bx_subscribe_btn_sljzMT");
                                                            if (btn) {
                                                                var btn_span = btn.querySelector("span");
                                                                var btn_subscribe_width = btn_span.style.width;
                                                                BX.addClass(btn, "send");
                                                                btn_span.outterHTML = "<span><i class='fa fa-check'></i> ГОТОВО</span>";
                                                                if (btn_subscribe_width) {
                                                                    btn.querySelector("span").style["min-width"] = btn_subscribe_width + "px"
                                                                }
                                                            }
                                                        }, 400)
                                                    }
                                                </script>
                                                <form role="form" method="post"
                                                      action="/courses/kurs-bereznoe-vosstanovlenie/"
                                                      onsubmit="BX('bx_subscribe_btn_sljzMT').disabled=true;">
                                                    <input type="hidden" name="sessid" id="sessid_1"
                                                           value="0a42a434ea9985f476b18969ca7ee3d5"/> <input
                                                            type="hidden" name="sender_subscription" value="add">

                                                    <div class="bx-input-group">
                                                        <input class="bx-form-control" type="text"
                                                               name="SENDER_SUBSCRIBE_NAME" value="" title=""
                                                               placeholder="Ваше имя">
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="bx-input-group">
                                                        <input class="bx-form-control" type="email"
                                                               name="SENDER_SUBSCRIBE_EMAIL" value=""
                                                               title="Введите ваш e-mail"
                                                               placeholder="Введите ваш e-mail">
                                                    </div>
                                                    <div class="bx_subscribe_submit_container">
                                                        <button class="sendebtn" id="bx_subscribe_btn_sljzMT">
                                                            ПОДПИСАТЬСЯ
                                                        </button>
                                                    </div>
                                                </form>
                                                <!--'end_frame_cache_sender-subscribe'--></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts">
                                <div class="contacts__item contacts__item--phone">
                                    Онлайн-магазин:<br>
                                    +7 (962) 938-26-40<br>
                                    order@vitbiomed.ru<br>
                                </div>
                                <div class="contacts__item contacts__item--phone">
                                    Клиника: <br>
                                    +7 (499) 164-13-80<br>
                                    info@vitbiomed.ru<br>
                                    Москва, ул. 5-я Парковая, д.46
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">Витбиомед &copy; 2016</div>
                        </div>
                    </div>
                </div>
                <div class="footer__content--phone">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="white-label white-label--phone">
                                <a href="#" class="white-label__link">Задайте вопрос нашим специалистам</a>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="footer-menu--phone">
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Компания</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="http://vitbiomed.ru/proizvodstvo/"
                                           class="footer-menu--phone__link-item">Производство</a>
                                        <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                           class="footer-menu--phone__link-item">Медицинский центр</a>
                                        <a href="http://vitbiomed.ru/reviews/" class="footer-menu--phone__link-item">О
                                            нас говорят</a>
                                        <a href="http://vitbiomed.ru/nashi-kontakty/"
                                           class="footer-menu--phone__link-item">Контакты</a>
                                    </div>
                                </div>
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Информация</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="#openModal" class="footer-menu--phone__link-item btn-link">Подписка</a>
                                        <a href="#" class="footer-menu--phone__link-item">Публичная оферта</a>
                                        <a href="http://store.vitbiomed.ru/faq/" class="footer-menu--phone__link-item">Вопросы</a>
                                        <a data-uk-modal="" href="#auth-modal"
                                           class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                           data-modal="auth-modal">Вход</a>
                                        <a data-uk-modal="" href="#auth-modal-reg"
                                           class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                           data-modal="auth-modal-reg">Регистрация</a>
                                    </div>
                                </div>
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Пробиотики</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="http://store.vitbiomed.ru/dostavka/"
                                           class="footer-menu--phone__link-item">Доставка и оплата</a>
                                        <a href="http://store.vitbiomed.ru/" class="footer-menu--phone__link-item">Купить
                                            пробиотики</a>
                                        <a href="http://store.vitbiomed.ru/courses/"
                                           class="footer-menu--phone__link-item">Купить курс</a>
                                        <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu--phone__link-item">Партнерам</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="footer-social footer-social--phone">
                                <a href="https://www.facebook.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--facebook"></a>
                                <a href="https://plus.google.com/+vitbiomed"
                                   class="footer-social__item footer-social__item--google"></a>
                                <a href="https://vk.com/vitbiomed "
                                   class="footer-social__item footer-social__item--vk"></a>
                                <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                   class="footer-social__item footer-social__item--youtube"></a>
                                <a href="https://www.instagram.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--instagram"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

</div>


<script src="/old/store/bower_components/angular/angular.min.js"></script>
<script src="/old/store/bower_components/lodash/dist/lodash.min.js"></script>
<script src="/old/store/bower_components/hammerjs/hammer.min.js"></script>
<script src="/old/store/bower_components/AngularHammer/angular.hammer.min.js"></script>
<script src="/old/store/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/old/store/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/old/store/bower_components/angular-material/angular-material.min.js"></script>
<script src="/old/store/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="/old/store/dist/bundle.js"></script>
<script src="/old/store/resources/js/common.js"></script>


<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.8/css/suggestions.css" type="text/css" rel="stylesheet">


<!--[if lt IE 10]>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/jquery.suggestions/16.8/js/jquery.suggestions.min.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_YBZqtSQOcYvGzM6Ev7yiTXuhAaDFzzU&amp;signed_in=true&amp;callback=initMap"></script>

<script src="/old/store/bitrix/templates/vitbnew/js/auth.js"></script>
<script src="/old/store/bitrix/templates/vitbnew/uikit/uikit.min.js"></script>
<script src="/old/store/resources/libs/owl-carousel/owl.carousel.min.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true,
                    ecommerce: "dataLayer"
                })
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n)
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false)
        } else {
            f()
        }
    })(document, window, "yandex_metrika_callbacks")
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter-->

<script>
    function addCount(elem) {

        let $1 = $("#{{$course->id}}_quantity");
        var count = Number($1.val()) + Number($(elem).data('count'));
        if (count <= 0) {
            count = 1;
        }
        $1.val(count);
        var price = $("#{{$course->id}}_price").data('price');
        $("#{{$course->id}}_price").text(count * price + " руб.")
    }

    function toggleAge(elem) {
        var ul = $(elem).parent().parent();
        var courseId = ul.data('course');
        var variantId = $(elem).data('onevalue');
        var toCard = $("#" + courseId + "_buy_link");
        toCard.data('variant', variantId)
    }

    function afterChangeCart(data) {
        $("#cart_div").remove();
        $("#cart_main").append('<div id="cart_div">' + data + '</div>');
    }

    function addToCart(elem) {
        var id = $(elem).data('id');
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
            });
        } else {
            console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
        }
        var quantity = $("#" + id + "_quantity").val();
        if (!quantity) {
            quantity = 1;
        }
        $.post('{{ route('cart.change.course') }}',
            {
                id: id,
                variant: $(elem).data('variant'),
                count: quantity
            }
        ).done(function (data) {
            afterChangeCart(data);
        });
    }
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview')

</script>
</body>
</html>