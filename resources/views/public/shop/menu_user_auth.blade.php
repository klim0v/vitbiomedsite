@guest
    <li class="menu-desktop-list__item">
        <a data-uk-modal="" href="#auth-modal"
           class="menu-desktop-list__item-o ajax-link md-trigger" id="auth-link"
           data-modal="auth-modal">Вход</a>
    </li>
    <li class="menu-desktop-list__item">
        <a data-uk-modal="" href="#auth-modal-reg"
           class="menu-desktop-list__item-o ajax-link md-trigger" id="auth-link"
           data-modal="auth-modal-reg">Регистрация</a>
    </li>
@else
    <li class="menu-desktop-list__item">
        <a href="{{route('personal.index')}}" class="menu-desktop-list__item-o">{{Auth::user()->fio}}</a>
    </li>
    <li class="menu-desktop-list__item">
        <a href="javascript:void(0);" class="menu-desktop-list__item-o"
           onclick="$.post('{{route('logout')}}').done(location.reload())">Выход</a>
    </li>
@endguest