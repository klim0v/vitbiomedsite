<script>
    let token = $('meta[name="csrf-token"]').attr('content');
    if (token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }
</script>
<div class="md-modal uk-modal" id="auth-modal">
    <div class="md-content uk-modal-content">
        <div class="md-top">
            <div class="md-close" onclick="$('.md-overlay').click();">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7">
                    <polygon
                            points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/>
                </svg>
            </div>
            <div class="md-title">Вход</div>
            <div class="lk-social-media">
                <span>через</span>
                <ul class="lk-social-media__list">

                    <div class="bx-authform-social">
                        <ul>
                            <li>
                                <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon"
                                   href="javascript:void(0)" title="Facebook"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon"
                                   href="javascript:void(0)" title="Яндекс"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon"
                                   href="javascript:void(0)" title="ВКонтакте"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon"
                                   href="javascript:void(0)" title="Google+"></a>
                            </li>
                        </ul>
                    </div>
                </ul>
                <span>или</span>
            </div>
        </div>
        <div class="md-form">
            <form name="system_auth_form6zOYVN"
                  method="post"
                  onsubmit="
                          $.post('{{route('login')}}', {
                          email: $('#emailAuth').val(),
                          password: $('#passwordAuth').val(),
                          }).done( function(request) {
                          if (request.result === true) {
                          location.reload()
                          }
                          } );"
                  class="">
                <input type="hidden" name="USER_REMEMBER" value="Y">
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="AUTH"/>
                <input type="hidden" name="backurl" value="" class="backurl">
                <div class="uk-form-row">
                    <label class="uk-form-label">Эл. почта</label>

                    <div class="uk-form-controls">
                        <input type="text"
                               id="emailAuth"
                               name="email"
                               maxlength="50"
                               value=""
                               class="uk-form-large uk-width-1-1">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Пароль</label>

                    <div class="uk-form-controls">
                        <div class="uk-form-password uk-width-1-1">
                            <input type="password"
                                   id="passwordAuth"
                                   name="password"
                                   maxlength="50"
                                   class="uk-form-large uk-width-1-1">
                            <a data-uk-form-password='{lblShow: "<i class=\"uk-icon-eye-slash\"></i>", lblHide: "<i class=\"uk-icon-eye\"></i>"}'
                               class="uk-form-password-toggle" href=""><i class='uk-icon-eye-slash'></i></a>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <button type="submit"
                            name="Login"
                            value="Войти"
                            class="btn" data-md-ink-ripple="#fff">Войти
                    </button>
                </div>


                <div class="md-b">
                    <a href=""
                       class="fl-l ajax-link" rel="nofollow">Вспомнить пароль</a>
                    <a href=""
                       class="fl-r ajax-link" rel="nofollow">Зарегистрироваться</a>
                </div>


            </form>

        </div>
    </div>
</div>