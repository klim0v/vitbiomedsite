<!DOCTYPE html>
<html lang="ru" data-ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/old/store/bitrix/js/main/core/css/core.css?14392234113963" type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/js/main/core/css/core_popup.css?148665183732860" type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/story/style.css?1478180037697"
          type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/story/bitrix/catalog.element/.default/style.css?148829022932451"
          type="text/css" rel="stylesheet"/>
    <link href="/old/store/bitrix/components/bitrix/socserv.auth.form/templates/flat/style.css?14845676842847"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/menu/menusi/style.css?1484155620665"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket_colitem/style.css?14792952024229"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket/style.css?14787141634229"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/form/clickCart/bitrix/form.result.new/.default/style.css?1479744509666"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="/old/store/bitrix/templates/vitbnew/components/bitrix/sender.subscribe/template1/style.css?14791328994640"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <script type="text/javascript">
        if (!window.BX) {
            window.BX = {
                message: function (mess) {
                    if (typeof mess == 'object') {
                        for (var i in mess) BX.message[i] = mess[i]
                    }
                    return true
                }
            }
        }</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'JS_CORE_LOADING': 'Загрузка...',
            'JS_CORE_NO_DATA': '- Нет данных -',
            'JS_CORE_WINDOW_CLOSE': 'Закрыть',
            'JS_CORE_WINDOW_EXPAND': 'Развернуть',
            'JS_CORE_WINDOW_NARROW': 'Свернуть в окно',
            'JS_CORE_WINDOW_SAVE': 'Сохранить',
            'JS_CORE_WINDOW_CANCEL': 'Отменить',
            'JS_CORE_WINDOW_CONTINUE': 'Продолжить',
            'JS_CORE_H': 'ч',
            'JS_CORE_M': 'м',
            'JS_CORE_S': 'с',
            'JSADM_AI_HIDE_EXTRA': 'Скрыть лишние',
            'JSADM_AI_ALL_NOTIF': 'Показать все',
            'JSADM_AUTH_REQ': 'Требуется авторизация!',
            'JS_CORE_WINDOW_AUTH': 'Войти',
            'JS_CORE_IMAGE_FULL': 'Полный размер'
        })</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'LANGUAGE_ID': 'ru',
            'FORMAT_DATE': 'MM/DD/YYYY',
            'FORMAT_DATETIME': 'MM/DD/YYYY H:MI:SS T',
            'COOKIE_PREFIX': 'BITRIX_SM',
            'SERVER_TZ_OFFSET': '10800',
            'SITE_ID': 's2',
            'SITE_DIR': '/',
            'USER_ID': '',
            'SERVER_TIME': '1532365915',
            'USER_TZ_OFFSET': '0',
            'USER_TZ_AUTO': 'Y',
            'bitrix_sessid': '51125442bf38c3da0ff1ad05cfb0aac3'
        })</script>


    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core.js?1486651837116711"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core_popup.js?148665183741651"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/main/core/core_ajax.js?148665183735622"></script>
    <script type="text/javascript" src="/old/store/bitrix/js/currency/core_currency.js?14779190782402"></script>
    {{--<script type="text/javascript">
      (function () {
        "use strict"

        var counter = function () {
          var cookie = (function (name) {
            var parts = ("; " + document.cookie).split("; " + name + "=")
            if (parts.length == 2) {
              try {
                return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()))
              }
              catch (e) {
              }
            }
          })("BITRIX_CONVERSION_CONTEXT_s2")

          if (!cookie || cookie.EXPIRE < BX.message("SERVER_TIME")) {
            var request = new XMLHttpRequest()
            request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true)
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            request.send(
              "SITE_ID=" + encodeURIComponent("s2") +
              "&sessid=" + encodeURIComponent(BX.bitrix_sessid()) +
              "&HTTP_REFERER=" + encodeURIComponent(document.referrer)
            )
          }
        }

        if (window.frameRequestStart === true) {
          BX.addCustomEvent("onFrameDataReceived", counter)
        } else {
          BX.ready(counter)
        }
      })()
    </script>--}}


    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket_colitem/script.js?14792952025292"></script>
    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/sale.basket.basket.line/basket/script.js?14794011705409"></script>
    <script type="text/javascript"
            src="/old/store/bitrix/templates/vitbnew/components/bitrix/catalog/story/bitrix/catalog.element/.default/script.js?147929215775328"></script>
    {{--<script type="text/javascript">var _ba = _ba || []
    _ba.push(["aid", "645478648abbf1d479a423fd862edac4"])
    _ba.push(["host", "store.vitbiomed.ru"])
    _ba.push(["ad[ct][item]", "eyJwcm9kdWN0X2lkIjoiOTgiLCJpYmxvY2tfaWQiOjIsInByb2R1Y3RfdGl0bGUiOiJcdTA0MWFcdTA0NDNcdTA0M2JcdTA0NGNcdTA0NDJcdTA0NDNcdTA0NDBcdTA0MzBcdTA0M2JcdTA0NGNcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDExXHUwNDM4XHUwNDNlXHUwNDMxXHUwNDMwXHUwNDNiXHUwNDMwXHUwNDNkXHUwNDQxIFx1MDQxMCIsImNhdGVnb3J5X2lkIjoiMiIsImNhdGVnb3J5Ijp7IjIiOiJcdTA0MzZcdTA0MzhcdTA0MzRcdTA0M2FcdTA0MzhcdTA0MzUgXHUwNDNhXHUwNDQzXHUwNDNiXHUwNDRjXHUwNDQyXHUwNDQzXHUwNDQwXHUwNDMwXHUwNDNiXHUwNDRjXHUwNDNkXHUwNDRiXHUwNDM1In0sInByaWNlIjoyNTAsImN1cnJlbmN5IjoiUlVCIn0="])
    _ba.push(["ad[ct][user_id]", function () {
      return BX.message("USER_ID") ? BX.message("USER_ID") : 0
    }])
    _ba.push(["ad[ct][recommendation]", function () {
      var rcmId = ""
      var cookieValue = BX.getCookie("BITRIX_SM_RCM_PRODUCT_LOG")
      var productId = 98
      var cItems = [], cItem
      if (cookieValue) {
        cItems = cookieValue.split('.')
      }
      var i = cItems.length
      while (i--) {
        cItem = cItems[i].split('-')
        if (cItem[0] == productId) {
          rcmId = cItem[1]
          break
        }
      }
      return rcmId
    }])
    _ba.push(["ad[ct][v]", "2"]);
    (function () {
      var ba = document.createElement("script")
      ba.type = "text/javascript"
      ba.async = true
      ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js"
      var s = document.getElementsByTagName("script")[0]
      s.parentNode.insertBefore(ba, s)
    })()</script>
    <script>new Image().src = 'http://vitbiomed.ru/bitrix/spread.php?s=QklUUklYX1NNX0FCVEVTVF9zMgEBMTU2MzQ2OTkxNQEvAQEBAg%3D%3D&k=536ae24e11ed22f54f0d8dd14af96e92'
    </script>--}}


    <title>{{$product->title}}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/old/store/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/angular-material/angular-material.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/ng-dialog/css/ngDialog.min.css">
    <link rel="stylesheet" href="/old/store/bower_components/ng-dialog/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="/old/store/dist/bundle.css?rev=1ede78ce969cf752c75bdda5642606e0">
    <link rel="stylesheet" href="/old/store/resources/css/style.css">
    <link rel="stylesheet" href="/old/store/resources/css/media.css">
    <link rel="stylesheet" href="/old/store/dist/magnific-popup.css">
    <link rel="stylesheet" href="/old/store/resources/libs/owl-carousel/owl.carousel.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {{--<script type="text/javascript">
      window.onload = initMap

      function buyoneclick (form) {
        $("#oneclickbuy").attr('disabled', true)

        $.post(
          '/ajax/oneclickbuy.php',
          $(form).serialize(),
          function (data) {
            if (data.status == 'error') {
              $("#oneclickbuy").removeAttr('disabled')
              $('.cartErrors').remove()
              var error = '<p class="cartErrors" style="color: red;">' + data.message + '</p>'
              $('.Texho').after(error)
            } else {
              $("#oneclickbuy").removeAttr('disabled')
              alert('Спасибо за заказ! В ближайшее время с вами свяжется наш менеджер')
//						recalcBasketAjax();
              document.location.href = document.location.href.replace('#openClickCart', '#close')
            }
          }
        )
      }
    </script>--}}

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="likearts-content" data-ng-cloak>
<div class="container-fluid">

    <!-- MODAL WINDOW LK AUTH -->
@include('public.shop.auth')
    <!-- MODAL WINDOW OVERLAY -->
    <div class="md-overlay"></div>
    <!--//auth-modal-->

    <!-- MODAL WINDOW LK reg -->
    <div class="md-modal uk-modal" id="auth-modal-reg">
        <div class="md-content uk-modal-content">
            <div class="md-top">
                <div class="md-close" onclick="$('.md-overlay').click();">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7">
                        <polygon
                                points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/>
                    </svg>
                </div>
                <div class="md-title">Регистрация</div>
                <div class="lk-social-media">
                    <span>через</span>
                    <ul class="lk-social-media__list">
                        <script type="text/javascript">
                            function BxSocServPopup(id) {
                                var content = BX("bx_socserv_form_" + id);
                                if (content) {
                                    var popup = BX.PopupWindowManager.create("socServPopup" + id, BX("bx_socserv_icon_" + id), {
                                        autoHide: true,
                                        closeByEsc: true,
                                        angle: {offset: 24},
                                        content: content,
                                        offsetTop: 3
                                    });

                                    popup.show();

                                    var input = BX.findChild(content, {
                                        'tag': 'input',
                                        'attribute': {'type': 'text'}
                                    }, true);
                                    if (input) {
                                        input.focus()
                                    }

                                    var button = BX.findChild(content, {
                                        'tag': 'input',
                                        'attribute': {'type': 'submit'}
                                    }, true);
                                    if (button) {
                                        button.className = 'btn btn-primary'
                                    }
                                }
                            }
                        </script>

                        <div class="bx-authform-social">
                            <ul>
                                <li>
                                    <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1801245380125777&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fbiobalans-a-kulturalnyj%2F%3Fauth_service_id%3DFacebook%26check_key%3D8a56b44ab237721e0e040bef67dede03%26backurl%3D%252Fbiobalans-a-kulturalnyj%252F&amp;scope=email,publish_actions,user_friends&amp;display=popup', 680, 600)"
                                       title="Facebook"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://oauth.yandex.ru/authorize?response_type=code&amp;client_id=88cb897161164506923f302388cd2ca5&amp;display=popup&amp;state=site_id%3Ds2%26backurl%3D%252Fbiobalans-a-kulturalnyj%252F%253Fcheck_key%253D8a56b44ab237721e0e040bef67dede03%26mode%3Dopener%26redirect_url%3D%252Fbiobalans-a-kulturalnyj%252F', 680, 600)"
                                       title="Яндекс"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=5825158&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fbiobalans-a-kulturalnyj%2F%3Fauth_service_id%3DVKontakte&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds2%26backurl%3D%252Fbiobalans-a-kulturalnyj%252F%253Fcheck_key%253D8a56b44ab237721e0e040bef67dede03%26redirect_url%3D%252Fbiobalans-a-kulturalnyj%252F', 660, 425)"
                                       title="ВКонтакте"></a>
                                </li>
                                <li>
                                    <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon"
                                       href="javascript:void(0)"
                                       onclick="BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=674779939050-7ejv3833b2g3vivpkir9ekio10id5d8j.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fstore.vitbiomed.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGooglePlusOAuth%26site_id%3Ds2%26backurl%3D%252Fbiobalans-a-kulturalnyj%252F%253Fcheck_key%253D8a56b44ab237721e0e040bef67dede03%26mode%3Dopener%26redirect_url%3D%252Fbiobalans-a-kulturalnyj%252F', 580, 400)"
                                       title="Google+"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                    <span>или</span>
                </div>
            </div>
            <div class="md-form">
                <form method="POST" action="" name="regform" class="uk-form uk-form-stacked">
                    <input type="hidden" name="TYPE" value="REGISTRATION"/>
                    <input type="hidden" name="register_submit_button" value="Y"/>
                    <input type="hidden" class="api-mf-antibot" value="" name="ANTIBOT[NAME]">
                    <input type="hidden" name="backurl" value="" class="backurl">

                    <div class="uk-form-row">
                        <label class="uk-form-label">Имя:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[NAME]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Фамилия:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[LAST_NAME]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Телефон:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[PERSONAL_PHONE]"
                                   value="">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Адрес e-mail:<span class="asterisk">*</span></label>
                        <div class="uk-form-controls">
                            <input size="30"
                                   class="uk-width-1-1 uk-form-large"
                                   type="text"
                                   name="REGISTER[EMAIL]"
                                   value="">
                        </div>
                    </div>


                    <div class="uk-form-row">
                        <button type="submit"
                                name="register_submit_button"
                                class="btn" data-md-ink-ripple="#fff"
                                value="Регистрация">Регистрация
                        </button>
                    </div>
                    <div class="uk-form-row">
                        <noindex>
                            <a href=""
                               rel="nofollow"
                               class="ajax-link">Авторизация</a>
                        </noindex>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- MODAL WINDOW OVERLAY -->
    <div class="md-overlay"></div>
    <!--//reg-modal-->

    <div class="header-container">
        <div class="container">
            <header class="header">
                <div class="flex-container">
                    <div class="flex-item">
                        <a href="/old/store/" class="logo-link">
                            <div class="logo"></div>
                        </a>
                    </div>
                    <div class="flex-item">
                        <div class="menu-desktop">


                            <ul class="menu-desktop-list">

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru"
                                                                       class="menu-desktop-list__link">Клиника</a></li>

                                <li class="menu-desktop-list__item"><a href="/"
                                                                       class="menu-desktop-list__link menu-desktop-list__link--active">Пробиотики</a>
                                </li>

                                <li class="menu-desktop-list__item"><a href="/courses/" class="menu-desktop-list__link">Курсы</a>
                                </li>

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/reviews/"
                                                                       class="menu-desktop-list__link">Отзывы</a></li>

                                <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/blog/"
                                                                       class="menu-desktop-list__link">Блог</a></li>


                                @include('public.shop.menu_user_auth')

                            </ul>
                        </div>
                        <div class="menu-mobile-container">
                            <div class="menu-mobile" data-ng-controller="MobileMenuController as mobileMenu">
                                <div id="menu-mobile-btn" data-hm-tap="mobileMenu.controlMenu($event)"
                                     class="menu-mobile__btn">
                                    <i class="material-icons menu-mobile__icon">view_headline</i>
                                </div>
                                <div id="menu-mobile-body" class="menu-mobile__body display-hide">
                                    <ul class="menu-mobile-list">
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Компания</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="http://vitbiomed.ru/proizvodstvo/"
                                                   class="menu-mobile-submenu__link">Производство</a>
                                                <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                                   class="menu-mobile-submenu__link">Медицинский центр</a>
                                                <a href="http://vitbiomed.ru/reviews/"
                                                   class="menu-mobile-submenu__link ">О нас говорят</a>
                                                <a href="http://vitbiomed.ru/nashi-kontakty/"
                                                   class="menu-mobile-submenu__link">Контакты</a>
                                            </div>
                                        </li>
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Информация</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="#" class="menu-mobile-submenu__link ">Публичная оферта</a>
                                                <a href="http://store.vitbiomed.ru/faq/"
                                                   class="menu-mobile-submenu__link">Вопросы</a>
                                                <a data-uk-modal="" href="#auth-modal"
                                                   class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                                   data-modal="auth-modal">Вход</a>
                                                <a data-uk-modal="" href="#auth-modal-reg"
                                                   class="menu-mobile-submenu__link md-trigger" id="auth-link"
                                                   data-modal="auth-modal-reg">Регистрация</a>
                                            </div>
                                        </li>
                                        <li class="menu-mobile-list__item">
                                            <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Пробиотики</a>
                                            <div class="menu-mobile-submenu">
                                                <a href="http://store.vitbiomed.ru/dostavka/"
                                                   class="menu-mobile-submenu__link">Доставка и оплата</a>
                                                <a href="http://store.vitbiomed.ru/" class="menu-mobile-submenu__link">Купить
                                                    пробиотики</a>
                                                <a href="http://store.vitbiomed.ru/courses/"
                                                   class="menu-mobile-submenu__link">Купить курс</a>
                                                <a href="http://vitbiomed.ru/partnyeram/"
                                                   class="menu-mobile-submenu__link">Партнерам</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="social-block">
                                        <a href="https://www.facebook.com/vitbiomed/">
                                            <div class="social-item social-item--facebook"></div>
                                        </a>
                                        <a href="https://www.instagram.com/vitbiomed/">
                                            <div class="social-item social-item--instagram"></div>
                                        </a>
                                        <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ">
                                            <div class="social-item social-item--youtube"></div>
                                        </a>
                                        <a href="https://plus.google.com/+vitbiomed">
                                            <div class="social-item social-item--google"></div>
                                        </a>
                                        <a href="https://vk.com/vitbiomed ">
                                            <div class="social-item social-item--vk"></div>
                                        </a>
                                    </div>
                                    <div class="phone-block">
                                        <a href="tel:+74991641380">
                                            <div class="phone-btn">
                                                <i class="material-icons phone-btn__icon">call</i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @include('public.shop.cart.small.index')
                    </div>
                </div>

                <div id="openClickCart" class="modalbg">
                    <div id="comp_7ad265e72172ab88850d60fc3e597cd6">

                        <form name="oneclickbuy" action="" method="POST"
                              onsubmit="event.preventDefault(); buyoneclick(this);" enctype="multipart/form-data"
                              class="ng-pristine ng-valid">
                            <div class="dialogCart">
                                <div class="modalbhCart">
                                    <a href="#close" title="Закрыть" class="closeCart"></a>
                                    <div class="form">
                                        <div class="inCartW">
                                            <div class="Texho">Быстрый заказ</div>
                                            <input type="text" class="inputtext" name="name" value="" placeholder="Имя">
                                            <input type="text" class="inputtext" name="phone" value=""
                                                   placeholder="Телефон">
                                            <!--	<textarea style="display:none;" name="form_textarea_27" cols="40" rows="5" class="inputtextarea">Товары - -->
                                            <!-- / Итого:--><!--</textarea>-->

                                            <input id="oneclickbuy" style="margin-top: 10px;" type="submit"
                                                   name="web_form_submit" class="btn md-ink-ripple"
                                                   data-md-ink-ripple="#fff" value="Отправить">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>
        </div>
    </div>


    <div class="page">
        <div class="container">
            <div class="page-content course" data-ng-controller="ProductItemPageController as productItemPage">
                <h1>{{$product->subtitle}}</h1>
                <div class="course-wrap course-wrap-1">
                    <div class="course-content">
                        <h2>{{$product->annotation}}<br></h2>
                        <div class="course-img">


                            <div class="captextin"><span class="captext">{{$product->keyword}}</span></div>

                            <div style="display: inline-block;overflow: hidden; height: 100%; width: 100%;">
                                <div class="bx_item_slider" id="bx_117848907_98_big_slider" style="height: 100%;">


                                    <img id="bx_117848907_98_pict" src="{{$product->image}}"
                                         alt="Культуральный Биобаланс А" title="{{$product->title}}">
                                    <div class="bx_stick average left top" style="display:none;"
                                         id="bx_117848907_98_sticker" title="{{$product->title}}"></div>

                                    <div class="bx_slider_conteiner" id="bx_117848907_98_slider_cont_443"
                                         style="display: block;">
                                        <div class="bx_slider_scroller_container">
                                            <div class="bx_slide">
                                                <ul style="width: 100%;" id="bx_117848907_98_slider_list_443">
                                                    <li data-value="443_204" style="width: 20%; padding-top: 20%"><span
                                                                class="cnt"><span class="cnt_item"
                                                                                  style="background-image:url('{{$product->image}}');"></span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="bx_slide_left" id="bx_117848907_98_slider_left_443"
                                                 style="display: none;" data-value="443"></div>
                                            <div class="bx_slide_right" id="bx_117848907_98_slider_right_443"
                                                 style="display: none;" data-value="443"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="course-sidebar">
                        <div class="course-discount hidden-xs">Вы экономите 10% на покупке препарата вместе с курсом
                        </div>
                        <div class="course-form">
                            <form action="#" method="POST" class="form">
                                <div class="course-form__price">
                                    <div id="product_price">{{$product->productVariations->first()->price}} руб.
                                    </div>
                                    <span class="price-count-1">цена за 1 шт.</span>
                                    <div class="sum-block_discount">Скидка 10% при покупке курсом</div>
                                </div>
                                <div class="course-form__amount-wrap bx_sive">
                                    <div class="amount__title">Выберите обьем</div>
                                    <div class="amount">
                                        <div class="bx_item_detail bx_blue" id="bx_117848907_98">
                                            <div class="bx_item_container">

                                                <div id="bx_117848907_98_skudiv">

                                                    <div class="bx_item_detail_size" id="bx_117848907_98_prop_51_cont">

                                                        <div class="bx_size_scroller_container">
                                                            <div class="bx_size">
                                                                <ul id="bx_117848907_98_prop_51_list"
                                                                    style="width: 100%;margin-left:0%;">
                                                                    @foreach($product->productVariations as $variation)
                                                                        <li
                                                                                data-price="{{$variation->price}}"
                                                                                data-onevalue="{{$variation->id}}"
                                                                            style="width: 20%;"
                                                                            class="@if($loop->first) bx_active @endif">
                                                                            <i title="{{$variation->volume}}"></i>
                                                                            <span onclick="changeVariant(this)"
                                                                                  class="cnt"
                                                                                  title="{{$variation->volume}}">{{$variation->volume}}</span>
                                                                        </li>
                                                                    @endforeach
                                                                    <li data-treevalue="51_0" data-onevalue="0"
                                                                        style="width: 20%; display: none;"
                                                                        class="bx_missing">
                                                                    {{--<li data-treevalue="51_0" data-onevalue="0"
                                                                        style="width: 20%; display: none;">
                                                                        <i title="-"></i><span class="cnt"
                                                                                               title="-">-</span></li>--}}
                                                                </ul>
                                                                {{--<script></script>--}}
                                                            </div>
                                                            <div class="bx_slide_left" style="display: none;"
                                                                 id="bx_117848907_98_prop_51_left"
                                                                 data-treevalue="51"></div>
                                                            <div class="bx_slide_right" style="display: none;"
                                                                 id="bx_117848907_98_prop_51_right"
                                                                 data-treevalue="51"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="course-form__amount-wrap">
                                    <div class="amount__title">Количество</div>
                                    <div class="amount">

                                        <div id="bx_117848907_98_basis_price" style="display:none;"
                                             class="item_section_name_gray">
                                            Цена {{$product->productVariations->first()->price}} руб. за 1 шт
                                        </div>
                                        <div class="item_buttons vam">
													<span class="item_buttons_counter_block">
														<a
                                                                {{--data-hm-tap="productCard.addToCart($event)"--}}
                                                                href="javascript:void(0)"
                                                                data-count="-1"
                                                                class="amount__button amount__button--minus"
                                                                data-md-ink-ripple="#fff"
                                                                id="quant_down" onclick="addCount(this)"><i
                                                                    class="material-icons amount-control__icon">remove</i></a>
														<input id="product_quantity" type="text"
                                                               class="amount__input" readonly value="1">
														<a href="javascript:void(0)"
                                                           onclick="addCount(this)"
                                                           data-count="1"
                                                           class="amount__button amount__button--plus"
                                                           data-md-ink-ripple="#fff" id="quant_up"><i
                                                                    class="material-icons amount-control__icon">add</i></a>
														<span class="bx_cnt_desc"
                                                              id="bx_117848907_98_quant_measure">шт</span>
													</span>

                                        </div>
                                    </div>
                                </div>

                                <span id="bx_117848907_98_basket_actions" style="display: block;">
									<div class="bx_item_detail bx_blue" id="bx_117848907_98">
									<div class="bx_item_container">
                                            <a onclick="addToCart(this);"
                                               {{--data-hm-tap="productItemPage.addToCart($event)"--}}
                                               href="javascript:void(0);"
                                               class="btn md-ink-ripple single_add_to_cb"
                                               data-md-ink-ripple="#fff"
                                               data-variant="{{$product->productVariations->first()->id}}"
                                               data-id="{{$product->id}}"
                                               id="add_to_cart_link_{{$product->id}}">
                                                <span></span>В корзину</a>

                                    </div>
                                    </div>
													</span>

                            </form>
                        </div>
                        <div class="icon-text icon-text--course hidden-xs">
                        </div>
                    </div>
                </div>
                <div class="course-wrap course-wrap-2">
                    <div class="course-content">
                        <div class="accordion__item">
                            <div class="accordion__linkS">Описание</div>
                            <div class="accordion__contentS">
                                <div class="accordion__text">
                                    {!! $product->description !!}
                                </div>
                            </div>
                        </div>
                        <div class="accordion">
                            <div class="accordion__item">
                                <div class="accordion__link">Воздействие на организм</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {!! $product->effects_on_the_body !!}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Показания к применению</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {!! $product->indications_for_use !!}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Как принимать</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {!! $product->how_to_use !!}
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Состав</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {!! $product->composition !!}
                                    </div>
                                </div>

                            </div>
                            <div class="accordion__item">
                                <div class="accordion__link">Условия хранения</div>
                                <div class="accordion__content">
                                    <div class="accordion__text">
                                        {!! $product->storage_conditions !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="course-sidebar">
                        <div class="faq-sidebar">
                            <div class="faq-sidebar-title">Сертификаты</div>

                            <div class="certificate-block__description">
                                <p>
                                <p>
                                </p>
                                <div>
                                    Препарат запатентован с участием ведущих микробиологов РФ имеет свидетельство о
                                    регистрации <br>
                                    <br>
                                    БИОЛОГИЧЕСКИ АКТИВНАЯ ДОБАВКА. НЕ ЯВЛЯЕТСЯ ЛЕКАРСТВЕННЫМ ПРЕПАРАТОМ
                                </div>
                                <p>
                                </p>
                            </div>


                            <div class="certificate-items">
                                <div data-hm-tap="productItemPage.clickToOpen('certificate-0')"
                                     class="certificate-items__item">
                                    <img class="img-responsive"
                                         src='/images/certificate0.png'
                                         alt="certificate">
                                    <script type="text/ng-template" id="certificate-0">
                                        <img class="img-responsive"
                                             src='/images/certificate0.png'
                                             alt="certificate">
                                    </script>
                                </div>
                                <div data-hm-tap="productItemPage.clickToOpen('certificate-1')"
                                     class="certificate-items__item">
                                    <img class="img-responsive"
                                         src='/images/certificate1.png'
                                         alt="certificate">
                                    <script type="text/ng-template" id="certificate-1">
                                        <img class="img-responsive"
                                             src='/images/certificate1.png'
                                             alt="certificate">
                                    </script>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="footer-container">
        <div class="container">
            <footer class="footer">
                <div class="footer__content--desktop">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-label">
                                <a href="mailto:info@vitbiomed.ru" class="white-label__link">Задайте вопрос нашим
                                    специалистам</a>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-sm-12-override">
                            <div class="footer-menu--desktop">
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Компания</div>
                                    <div class="footer-menu-links">
                                        <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu-links__item">Производство</a>
                                        <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                           class="footer-menu-links__item">Медицинский центр</a>
                                        <a href="http://vitbiomed.ru/reviews/" class="footer-menu-links__item">О нас
                                            говорят</a>
                                        <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu-links__item">Контакты</a>
                                        <a href="http://vitbiomed.ru/nezavisimaya-otsenka-kachestva-okazaniya-uslug/"
                                           class="footer-menu-links__item">Независимая оценка качества<br> оказания
                                            услуг</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Информация</div>
                                    <div class="footer-menu-links">
                                        <a href="http://vitbiomed.ru/blog/" class="footer-menu-links__item">Блог</a>
                                        <a href="#" class="footer-menu-links__item">Публичная оферта</a>
                                        <a href="http://store.vitbiomed.ru/faq/"
                                           class="footer-menu-links__item">Вопросы</a>
                                        <a href="http://vitbiomed.ru/normativnie-documenti/"
                                           class="footer-menu-links__item">Нормативные документы</a>
                                        <a data-uk-modal="" href="#auth-modal"
                                           class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                           data-modal="auth-modal">Вход</a>
                                        <a data-uk-modal="" href="#auth-modal-reg"
                                           class="footer-menu-links__item md-trigger md-trigger" id="auth-link"
                                           data-modal="auth-modal-reg">Регистрация</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column">
                                    <div class="footer-menu__title">Пробиотики</div>
                                    <div class="footer-menu-links">
                                        <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu-links__item">Доставка
                                            и оплата</a>
                                        <a href="http://store.vitbiomed.ru/" class="footer-menu-links__item">Купить
                                            пробиотики</a>
                                        <a href="http://store.vitbiomed.ru/courses/" class="footer-menu-links__item">Купить
                                            курс</a>
                                        <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu-links__item">Партнерам</a>
                                    </div>
                                </div>
                                <div class="footer-menu__column footer-menu__column--mobile">
                                    <div class="footer-menu__title">Мы в соц сетях</div>
                                    <div class="footer-menu-links">
                                        <a href="https://www.facebook.com/vitbiomed/"
                                           class="footer-menu-links__item footer-social-link footer-social-link--facebook">Facebook</a>
                                        <a href="https://vk.com/vitbiomed "
                                           class="footer-menu-links__item footer-social-link footer-social-link--vk">Vkontakte</a>
                                        <a href="https://plus.google.com/+vitbiomed"
                                           class="footer-menu-links__item footer-social-link footer-social-link--google">Google+</a>
                                        <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                           class="footer-menu-links__item footer-social-link footer-social-link--youtube">Youtube</a>
                                        <a href="https://www.instagram.com/vitbiomed/"
                                           class="footer-menu-links__item footer-social-link footer-social-link--instagram">Instagram</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-visible">
                            <div class="footer-social footer-social--tablet">
                                <a href="https://www.facebook.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--facebook"></a>
                                <a href="https://plus.google.com/+vitbiomed"
                                   class="footer-social__item footer-social__item--google"></a>
                                <a href="https://vk.com/vitbiomed "
                                   class="footer-social__item footer-social__item--vk"></a>
                                <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                   class="footer-social__item footer-social__item--youtube"></a>
                                <a href="https://www.instagram.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--instagram"></a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-offset-1 col-md-3 col-sm-12-override">
                            <a href="#openModal" class="btn-link">
                                <div class="buy-btn btn" data-md-ink-ripple="#fff">Подписаться</div>
                            </a>
                            <div id="openModal" class="modalbg">
                                <div class="dialog">
                                    <div class="modalbh">
                                        <a href="#close" title="Close" class="close"></a>
                                        <div class="text_df"><span class="spanx">Полезная рассылка</span>
                                            <p>Советы по поддержанию здоровья Рекомендации по питанию Обновления блога
                                                Видео-консультации наших врачей</p>
                                        </div>
                                        <div class="text_df">
                                            <div class="bx-subscribe2" id="sender-subscribe">
                                                <!--'start_frame_cache_sender-subscribe'-->
                                                <script>
                                                    BX.ready(function () {
                                                        BX.bind(BX("bx_subscribe_btn_sljzMT"), 'click', function () {
                                                            setTimeout(mailSender, 250);
                                                            return false
                                                        })
                                                    });

                                                    function mailSender() {
                                                        setTimeout(function () {
                                                            var btn = BX("bx_subscribe_btn_sljzMT");
                                                            if (btn) {
                                                                var btn_span = btn.querySelector("span");
                                                                var btn_subscribe_width = btn_span.style.width;
                                                                BX.addClass(btn, "send");
                                                                btn_span.outterHTML = "<span><i class='fa fa-check'></i> ГОТОВО</span>";
                                                                if (btn_subscribe_width) {
                                                                    btn.querySelector("span").style["min-width"] = btn_subscribe_width + "px"
                                                                }
                                                            }
                                                        }, 400)
                                                    }
                                                </script>
                                                <form role="form" method="post" action="/biobalans-a-kulturalnyj/"
                                                      onsubmit="BX('bx_subscribe_btn_sljzMT').disabled=true;">
                                                    <input type="hidden" name="sessid" id="sessid_1"
                                                           value="51125442bf38c3da0ff1ad05cfb0aac3"/> <input
                                                            type="hidden" name="sender_subscription" value="add">

                                                    <div class="bx-input-group">
                                                        <input class="bx-form-control" type="text"
                                                               name="SENDER_SUBSCRIBE_NAME" value="" title=""
                                                               placeholder="Ваше имя">
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="bx-input-group">
                                                        <input class="bx-form-control" type="email"
                                                               name="SENDER_SUBSCRIBE_EMAIL" value=""
                                                               title="Введите ваш e-mail"
                                                               placeholder="Введите ваш e-mail">
                                                    </div>
                                                    <div class="bx_subscribe_submit_container">
                                                        <button class="sendebtn" id="bx_subscribe_btn_sljzMT">
                                                            ПОДПИСАТЬСЯ
                                                        </button>
                                                    </div>
                                                </form>
                                                <!--'end_frame_cache_sender-subscribe'--></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts">
                                <div class="contacts__item contacts__item--phone">
                                    Онлайн-магазин:<br>
                                    +7 (962) 938-26-40<br>
                                    order@vitbiomed.ru<br>
                                </div>
                                <div class="contacts__item contacts__item--phone">
                                    Клиника: <br>
                                    +7 (499) 164-13-80<br>
                                    info@vitbiomed.ru<br>
                                    Москва, ул. 5-я Парковая, д.46
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">Витбиомед &copy; 2016</div>
                        </div>
                    </div>
                </div>
                <div class="footer__content--phone">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="white-label white-label--phone">
                                <a href="#" class="white-label__link">Задайте вопрос нашим специалистам</a>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="footer-menu--phone">
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Компания</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="http://vitbiomed.ru/proizvodstvo/"
                                           class="footer-menu--phone__link-item">Производство</a>
                                        <a href="http://vitbiomed.ru/meditsinskiy-tsentr/"
                                           class="footer-menu--phone__link-item">Медицинский центр</a>
                                        <a href="http://vitbiomed.ru/reviews/" class="footer-menu--phone__link-item">О
                                            нас говорят</a>
                                        <a href="http://vitbiomed.ru/nashi-kontakty/"
                                           class="footer-menu--phone__link-item">Контакты</a>
                                    </div>
                                </div>
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Информация</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="#openModal" class="footer-menu--phone__link-item btn-link">Подписка</a>
                                        <a href="#" class="footer-menu--phone__link-item">Публичная оферта</a>
                                        <a href="http://store.vitbiomed.ru/faq/" class="footer-menu--phone__link-item">Вопросы</a>
                                        <a data-uk-modal="" href="#auth-modal"
                                           class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                           data-modal="auth-modal">Вход</a>
                                        <a data-uk-modal="" href="#auth-modal-reg"
                                           class="footer-menu--phone__link-item md-trigger" id="auth-link"
                                           data-modal="auth-modal-reg">Регистрация</a>
                                    </div>
                                </div>
                                <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                    <div class="footer-menu--phone__title">Пробиотики</div>
                                    <div class="footer-menu--phone__links">
                                        <a href="http://store.vitbiomed.ru/dostavka/"
                                           class="footer-menu--phone__link-item">Доставка и оплата</a>
                                        <a href="http://store.vitbiomed.ru/" class="footer-menu--phone__link-item">Купить
                                            пробиотики</a>
                                        <a href="http://store.vitbiomed.ru/courses/"
                                           class="footer-menu--phone__link-item">Купить курс</a>
                                        <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu--phone__link-item">Партнерам</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="footer-social footer-social--phone">
                                <a href="https://www.facebook.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--facebook"></a>
                                <a href="https://plus.google.com/+vitbiomed"
                                   class="footer-social__item footer-social__item--google"></a>
                                <a href="https://vk.com/vitbiomed "
                                   class="footer-social__item footer-social__item--vk"></a>
                                <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"
                                   class="footer-social__item footer-social__item--youtube"></a>
                                <a href="https://www.instagram.com/vitbiomed/"
                                   class="footer-social__item footer-social__item--instagram"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

</div>


<script src="/old/store/bower_components/angular/angular.min.js"></script>
<script src="/old/store/bower_components/lodash/dist/lodash.min.js"></script>
<script src="/old/store/bower_components/hammerjs/hammer.min.js"></script>
<script src="/old/store/bower_components/AngularHammer/angular.hammer.min.js"></script>
<script src="/old/store/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/old/store/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/old/store/bower_components/angular-material/angular-material.min.js"></script>
<script src="/old/store/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="/old/store/dist/bundle.js"></script>
<script src="/old/store/resources/js/common.js"></script>


<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.8/css/suggestions.css" type="text/css" rel="stylesheet">


<!--[if lt IE 10]>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/jquery.suggestions/16.8/js/jquery.suggestions.min.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_YBZqtSQOcYvGzM6Ev7yiTXuhAaDFzzU&amp;signed_in=true&amp;callback=initMap"></script>

<script src="/old/store/bitrix/templates/vitbnew/js/auth.js"></script>
<script src="/old/store/bitrix/templates/vitbnew/uikit/uikit.min.js"></script>
<script src="/old/store/bitrix/templates/vitbnew/uikit/form-password.min.js"></script>
<script src="/old/store/resources/libs/owl-carousel/owl.carousel.min.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id: 31548593,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true,
                    ecommerce: "dataLayer"
                })
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n)
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false)
        } else {
            f()
        }
    })(document, window, "yandex_metrika_callbacks")
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter-->


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview')

</script>

<script>
    function addCount(elem) {

        var count = Number($("#product_quantity").val()) + Number($(elem).data('count'));
        if (count <= 0) {
            count = 1;
        }
        $("#product_quantity").val(count);
        var price = $("li.bx_active").data('price');
        $("#product_price").text(count * price + " руб.")
    }
    function changeVariant(elem) {
        $("#product_quantity").val(1);
        var li = $(elem).parent();
        var ul = li.parent();
        var price = li.data('price');
        $("#product_price").text(price + " руб.");
        ul.find("li.bx_active").removeClass("bx_active");
        li.addClass("bx_active");
        var varId = li.data('onevalue');
        var $addToCartLink = $("#add_to_cart_link_{{$product->id}}");
        $addToCartLink.data('variant', varId);
    }

    function afterChangeCart(data) {
        $("#cart_div").remove();
        $("#cart_main").append('<div id="cart_div">' + data + '</div>');
    }
    function addToCart(elem) {
        var id = $(elem).data('id');
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
            });
        } else {
            console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
        }
        var quantity = $("#" + id + "_quantity").val();
        if (!quantity) {
            quantity = 1;
        }
        $.post('{{ route('cart.change.product') }}',
            {
                id: id,
                variant: $(elem).data('variant'),
                count: quantity
            }
        ).done(function (data) {
            afterChangeCart(data)
        });
    }
</script>


</body>
</html>