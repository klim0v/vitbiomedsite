@extends('public.shop.layout')

@section('title', 'Оформление заказа')

@section('page')
    <div class="page">
        <div class="container">
            <div class="page-content">
                <div class="order">


                    <a name="order_form"></a>

                    <div id="order_form_div" class="order-checkout">
                        <NOSCRIPT>
                            <div class="errortext">Для оформления заказа необходимо включить JavaScript. По-видимому,
                                JavaScript либо не поддерживается браузером, либо отключен. Измените настройки браузера
                                и затем <a href="">повторите попытку</a>.
                            </div>
                        </NOSCRIPT>


                        <div class="bx_order_make">

                            <form action="/personal/order/make/" class="form" method="POST" name="ORDER_FORM"
                                  id="ORDER_FORM" enctype="multipart/form-data">
                                <div class="tabs js-tabs">
                                    <div class="tabs__content">
                                        <input type="hidden" name="sessid" id="sessid_1"
                                               value="1110138accb63afdabc2f45d3490729c"/>
                                        <div id="order_form_content">
                                            <div class="tab__content active">
                                                <h1>Оплата и доставка</h1>


                                                <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="0"/>
                                                <div class="order-information tabs js-tabs">
                                                    <ul class="tabs__caption maps-js">

                                                        <li class="tab__link label-radio active">
                                                            <label for="ID_DELIVERY_ID_2"
                                                                   style="font-weight: 400;cursor: pointer;">
                                                                <div class="radio active">
                                                                    <div style="display:none;">
                                                                        <input type="radio"
                                                                               id="ID_DELIVERY_ID_2"
                                                                               name="DELIVERY_ID"
                                                                               value="2" checked onclick="submitForm();"
                                                                        />
                                                                    </div>

                                                                </div>


                                                                Доставка
                                                                <div class="bx_description">


                                                                </div>
                                                            </label>
                                                            <table class="delivery_extra_services">
                                                            </table>

                                                        </li>

                                                        <li class="tab__link label-radio ">
                                                            <label for="ID_DELIVERY_ID_3"
                                                                   style="font-weight: 400;cursor: pointer;">
                                                                <div class="radio ">
                                                                    <div style="display:none;">
                                                                        <input type="radio"
                                                                               id="ID_DELIVERY_ID_3"
                                                                               name="DELIVERY_ID"
                                                                               value="3" onclick="submitForm();"
                                                                        />
                                                                    </div>

                                                                </div>


                                                                Самовывоз
                                                                <div class="bx_description">


                                                                </div>
                                                            </label>

                                                        </li>
                                                    </ul>
                                                </div>
                                                <span style="display:none;">
		<input type="text" name="PERSON_TYPE" value="1"/>
		<input type="text" name="PERSON_TYPE_OLD" value="1"/>
		</span>

                                                <div class="overflow-hidden active">
                                                    <div class="order-left">


                                                        <input type="hidden" name="showProps" id="showProps" value="N"/>

                                                        <div id="sale_order_props">
                                                            <div class="2222">
                                                                <div class="no_disp" data-property-id-row="12">
                                                                    <input type="text" placeholder="Фамилия"
                                                                           maxlength="250" size=""
                                                                           value="{{ optional($user)->last_name}}"
                                                                           name="last_name" id="ORDER_LAST_NAME"/>
                                                                </div>

                                                                <div class="no_disp" data-property-id-row="12">
                                                                    <input type="text" placeholder="Имя"
                                                                           maxlength="250" size=""
                                                                           value="{{ optional($user)->first_name}}"
                                                                           name="first_name" id="ORDER_FIRST_NAME"/>
                                                                </div>

                                                                <div class="no_disp" data-property-id-row="12">
                                                                    <input type="text" placeholder="Отчество"
                                                                           maxlength="250" size=""
                                                                           value="{{ optional($user)->second_name}}"
                                                                           name="second_name" id="ORDER_SECOND_NAME"/>
                                                                </div>


                                                                <div class="no_disp" data-property-id-row="13">
                                                                    <input type="text" placeholder="Email"
                                                                           maxlength="250" size=""
                                                                           value="{{ optional($user)->email }}"
                                                                           name="email" id="ORDER_PROP_13"/>
                                                                </div>


                                                                <div class="no_disp" data-property-id-row="14">
                                                                    <input type="text" placeholder="Телефон"
                                                                           maxlength="250" size=""
                                                                           value="{{ optional($user)->phone }}"
                                                                           name="phone" id="ORDER_PROP_14"/>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="2222">
                                                            <div class="no_disp" data-property-id-row="15">
                                                                <input type="text" placeholder="Город" maxlength="250"
                                                                       size="" value="" name="address[city]"
                                                                       id="ORDER_PROP_15"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="16">
                                                                <input type="text" placeholder="Улица" maxlength="250"
                                                                       size="" value="" name="address[street]"
                                                                       id="ORDER_PROP_16"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="17">
                                                                <input type="text" placeholder="Дом" maxlength="250"
                                                                       size="" value="" name="address[home]"
                                                                       id="ORDER_PROP_17"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="18">
                                                                <input type="text" placeholder="Корпус" maxlength="250"
                                                                       size="" value="" name="address[block]"
                                                                       id="ORDER_PROP_18"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="19">
                                                                <input type="text" placeholder="Квартира"
                                                                       maxlength="250" size="" value=""
                                                                       name="address[flat]" id="ORDER_PROP_19"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="20">
                                                                <textarea placeholder="Комментарий" rows="4" cols="40"
                                                                          name="ORDER_PROP_20"
                                                                          id="ORDER_PROP_20"></textarea>

                                                            </div>

                                                            <div class="no_disp" data-property-id-row="22">
                                                                <input type="text" placeholder="Выбрать время"
                                                                       maxlength="250" size="" value=""
                                                                       name="ORDER_PROP_22" id="ORDER_PROP_22"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="21">
                                                                <input type="text" placeholder="Выбрать дату"
                                                                       maxlength="250" size="" value=""
                                                                       name="ORDER_PROP_21" id="ORDER_PROP_21"/>
                                                            </div>

                                                            <div class="no_disp" data-property-id-row="25">

                                                                <div class="select-date">
                                                                    <div class="select-date__top">
                                                                        <div class="fl-l"><span class="js-drop-date">Выбрать дату</span>
                                                                        </div>
                                                                        <div class="fl-r"><span class="js-drop-time">Выбрать время</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="select-date__middle">
                                                                        <div class="selected-date fl-l">
                                                                            <input type="hidden" value="24 Августа 2018"
                                                                                   name="ORDER_PROP_21" id="ik-date">
                                                                            <span class="ik-date-title">24 Августа 2018</span>
                                                                        </div>
                                                                        <div class="selected-time fl-r">
                                                                            <input type="hidden"
                                                                                   value="с 12.00 до 14.00"
                                                                                   name="ORDER_PROP_22" id="ik-time">
                                                                            <span class="ik-time-title">с 12.00 до 14.00</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="select-date__content">
                                                                        <div class="select-date__ik-date">
                                                                            <div class="ik-calendar">
                                                                                <div class="ik-calendar__slider"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="select-date__ik-time">
                                                                            <div class="ik-time__item active">с 12.00 до
                                                                                14.00
                                                                            </div>
                                                                            <div class="ik-time__item">с 14.00 до
                                                                                17.00
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                </label>


                                                            </div>


                                                            <script>

                                                                (window.top.BX || BX).saleOrderAjax.addPropertyDesc({
                                                                    'id': '25',
                                                                    'attributes': {
                                                                        'type': 'DATE',
                                                                        'valueSource': 'form'
                                                                    }
                                                                });

                                                            </script>


                                                        </div>
                                                    </div>
                                                    <div class="order-right hidden-sm hidden-xs">
                                                        <div class="maps-group">
                                                            <div class="maps-content">
                                                                <div id="maps-group" class="map"></div>
                                                            </div>
                                                        </div>
                                                        <div class="notice notice-warning">Пробиотики требуют соблюдение
                                                            температурного режима 4±2, обязательно храните их в
                                                            холодильнике.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="select-payment">
                                                    <script type="text/javascript">
                                                        function changePaySystem(param) {
                                                            if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
                                                            {
                                                                if (param == 'account') {
                                                                    if (BX("PAY_CURRENT_ACCOUNT")) {
                                                                        BX("PAY_CURRENT_ACCOUNT").checked = true;
                                                                        BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                                                                        BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

                                                                        // deselect all other
                                                                        var el = document.getElementsByName("PAY_SYSTEM_ID");
                                                                        for (var i = 0; i < el.length; i++)
                                                                            el[i].checked = false;
                                                                    }
                                                                }
                                                                else {
                                                                    BX("PAY_CURRENT_ACCOUNT").checked = false;
                                                                    BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                                                                    BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                                                }
                                                            }
                                                            else if (BX("account_only") && BX("account_only").value == 'N') {
                                                                if (param == 'account') {
                                                                    if (BX("PAY_CURRENT_ACCOUNT")) {
                                                                        BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

                                                                        if (BX("PAY_CURRENT_ACCOUNT").checked) {
                                                                            BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                                                                            BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                                                        }
                                                                        else {
                                                                            BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                                                                            BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            submitForm();
                                                        }
                                                    </script>
                                                    <div class="select-payment-title">Способы оплаты</div>

                                                    <div class="label-radio icon-1">


                                                        <div class="radio ">

                                                            <input type="radio"
                                                                   class="radio__input"
                                                                   id="ID_PAY_SYSTEM_ID_2"
                                                                   name="PAY_SYSTEM_ID"
                                                                   value="2"
                                                                   onclick="changePaySystem();"/>
                                                        </div>
                                                        <label for="ID_PAY_SYSTEM_ID_2"
                                                               onclick="BX('ID_PAY_SYSTEM_ID_2').checked=true;changePaySystem();">
<span class="label-radio-text">

Наличными курьеру
							</span></label>


                                                    </div>
                                                    <div class="label-radio icon-2">


                                                        <div class="radio active">

                                                            <input type="radio"
                                                                   class="radio__input"
                                                                   id="ID_PAY_SYSTEM_ID_5"
                                                                   name="PAY_SYSTEM_ID"
                                                                   value="5"
                                                                   checked="checked" onclick="changePaySystem();"/>
                                                        </div>
                                                        <label for="ID_PAY_SYSTEM_ID_5"
                                                               onclick="BX('ID_PAY_SYSTEM_ID_5').checked=true;changePaySystem();">
<span class="label-radio-text">

Банковской картой
							</span></label>


                                                    </div>

                                                </div>
                                            </div>


                                            <div class="tab__content">
                                                <h1>Ваш заказ</h1>

                                                <div class="order ordersu">
                                                    <div class="order-info-wrap">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="order-block order-info-products">
                                                                    <ol>
                                                                        <li>Культуральный Биобаланс Б - 7 шт.</li>
                                                                        <li>Курс «Стоп-аллерген&quot; - 1 шт.</li>
                                                                    </ol>
                                                                    <div class="order-info__total">Стоимость заказа – 5
                                                                        740 руб.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 text-right text-left-sm-xs">
                                                                <div class="order-block order-info-user">
                                                                    <p><b>Комментарии к заказу:</b></p>
                                                                    <textarea placeholder="Ваш комментарий"
                                                                              name="ORDER_DESCRIPTION"
                                                                              id="ORDER_DESCRIPTION"
                                                                              style="max-width:100%;min-height:120px"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="col-md-6 text-right2 text-left-sm-xs">
                                                            <div class="order-block text-right2 text-left-sm-xs">
                                                                <div class="order-info__total order-info__total--grean">
                                                                    Итого к оплате – 5 740 руб.
                                                                </div>

                                                                <a class="btn" data-md-ink-ripple="#fff"
                                                                   href="javascript:void();"
                                                                   onclick="submitForm('Y'); return false;"
                                                                   id="ORDER_CONFIRM_BUTTON" class="checkout">Оформить
                                                                    заказ</a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <input type="hidden" name="" value="">
                                                </div>


                                            </div>

                                        </div>


                                        <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                                        <input type="hidden" name="profile_change" id="profile_change" value="N">
                                        <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                                        <input type="hidden" name="json" value="Y">


                                        <ul class="korz tabs__caption tab-panel__controls" style="display:nne;">
                                            <li id="cl23" class="clikd1 tab__link active" data-md-ink-ripple="#fff">
                                                Назад
                                            </li>
                                            <li class="clikd2 tab__link"
                                                onclick="$('html, body').animate({scrollTop:0}, 'fast');"
                                                data-md-ink-ripple="#fff">Продолжить
                                            </li>
                                        </ul>


                                    </div>
                                </div>
                            </form>

                            <div style="display:none;">
                                <div id="delivery_info_"><a href="javascript:void(0)"
                                                            onClick="deliveryCalcProceed({'STEP':'1','DELIVERY_ID':'','DELIVERY':'','PROFILE':'','WEIGHT':'0','PRICE':'0','LOCATION':'0','LOCATION_ZIP':'','CURRENCY':'','INPUT_NAME':'','TEMP':'','ITEMS':'','EXTRA_PARAMS_CALLBACK':'','ORDER_DATA':[]})">Рассчитать
                                        стоимость</a></div>
                                <div id="wait_container_" style="display: none;"></div>
                            </div>
                        </div>
                    </div>


                    <div style="display: none">


                        <div id="sls-26315" class="bx-slst">


                            <input type="hidden" name="LOCATION" value="" class="bx-ui-slst-target"/>

                            <div class="bx-ui-slst-pool">
                            </div>

                            <div data-bx-ui-id="slst-error">
                            </div>

                            <script type="text/html" data-template-id="bx-ui-slst-selector-scope">

                                <div class="dropdown-block bx-ui-slst-input-block">
                                    <span class="dropdown-icon"></span>
                                    <input type="text" name="" value="" autocomplete="off" class="dropdown-field"/>
                                    <div class="bx-ui-combobox-container"
                                         style="margin: 0px; padding: 0px; border: none; position: relative;">
                                        <input type="text" value="" autocomplete="off" class="bx-ui-combobox-fake"
                                               placeholder="Выберите местоположение ..."/>
                                    </div>
                                    <div class="dropdown-fade2white"></div>
                                    <div class="bx-ui-combobox-loader" data-bx-ui-id="combobox-loader"></div>
                                    <div class="bx-ui-combobox-toggle" title="Открыть / закрыть"
                                         data-bx-ui-id="combobox-toggle"></div>

                                    <div class="bx-ui-combobox-dropdown" data-bx-ui-id="combobox-dropdown">

                                        <div data-bx-ui-id="pager-pane">
                                        </div>
                                    </div>
                                </div>

                            </script>

                            <div class="bx-ui-slst-loader"></div>
                        </div>

                        <script type="text/javascript">

                            if (!window.BX && top.BX)
                                window.BX = top.BX;


                            new BX.Sale.component.location.selector.steps({
                                'scope': 'sls-26315',
                                'source': '/bitrix/components/bitrix/sale.location.selector.steps/get.php',
                                'query': {
                                    'FILTER': {'SITE_ID': ''},
                                    'BEHAVIOUR': {'SEARCH_BY_PRIMARY': '0', 'LANGUAGE_ID': 'ru'}
                                },
                                'selectedItem': 0,
                                'knownBundles': {'a': []},
                                'provideLinkBy': 'id',
                                'messages': {
                                    'notSelected': 'Выберите местоположение ...',
                                    'error': 'К сожалению, произошла внутренняя ошибка',
                                    'nothingFound': 'Не удалось обнаружить местоположение',
                                    'clearSelection': '--- Отменить выбор'
                                },
                                'callback': '',
                                'useSpawn': false,
                                'initializeByGlobalEvent': '',
                                'globalEventScope': '',
                                'rootNodeValue': 0,
                                'showDefault': false,
                                'bundlesIncomplete': {'a': true, '0': true},
                                'autoSelectWhenSingle': true,
                                'types': '',
                                'disableKeyboardInput': false,
                                'dontShowNextChoice': false
                            });


                        </script>


                        <div id="sls-42500" class="bx-sls ">


                            <div class="dropdown-block bx-ui-sls-input-block">

                                <span class="dropdown-icon"></span>
                                <input type="text" autocomplete="off" name="LOCATION" value="" class="dropdown-field"
                                       placeholder="Введите название ..."/>

                                <div class="dropdown-fade2white"></div>
                                <div class="bx-ui-sls-loader"></div>
                                <div class="bx-ui-sls-clear" title="Отменить выбор"></div>
                                <div class="bx-ui-sls-pane"></div>

                            </div>

                            <script type="text/html" data-template-id="bx-ui-sls-error">
                                <div class="bx-ui-sls-error">
                                    <div></div>
                                    {{--{{message}}--}}
                                </div>
                            </script>

                            <script type="text/html" data-template-id="bx-ui-sls-dropdown-item">
                                <div class="dropdown-item bx-ui-sls-variant">
                                    <span class="dropdown-item-text">
                                        {{--{{display_wrapped}}--}}
                                    </span>
                                </div>
                            </script>

                            <div class="bx-ui-sls-error-message">
                            </div>

                        </div>

                        <script>

                            if (!window.BX && top.BX)
                                window.BX = top.BX;


                            new BX.Sale.component.location.selector.search({
                                'scope': 'sls-42500',
                                'source': '/bitrix/components/bitrix/sale.location.selector.search/get.php',
                                'query': {
                                    'FILTER': {'EXCLUDE_ID': 0, 'SITE_ID': ''},
                                    'BEHAVIOUR': {'SEARCH_BY_PRIMARY': '0', 'LANGUAGE_ID': 'ru'}
                                },
                                'selectedItem': false,
                                'knownItems': [],
                                'provideLinkBy': 'id',
                                'messages': {
                                    'nothingFound': 'К сожалению, ничего не найдено',
                                    'error': 'К сожалению, произошла внутренняя ошибка'
                                },
                                'callback': '',
                                'useSpawn': false,
                                'initializeByGlobalEvent': '',
                                'globalEventScope': '',
                                'pathNames': [],
                                'types': ''
                            });


                        </script>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
