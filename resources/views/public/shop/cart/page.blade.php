@extends('public.shop.layout')


@section('title', 'Корзина')

@section('page')
    <div class="page">
        <div class="container">

            <div id="warning_message">
            </div>
            <div class="page-content">
                <h1>Корзина
                    <div class="basket-count">{{\count($productCollection->elements)}}</div>
                </h1>
                <a href="#" onclick="$.post('{{route('cart.clear')}}').done(function (data) {
                        afterChangeCart(data);
                        });
                        $('#basket_items').remove();
                        $('#basket_items').remove();
                        $('#allSum_FORMATED').text('0  руб.');
                        "
                   class="clear-basket hover-1 hidden-xs">Очистить корзину</a>
                <div class="banner-free hidden-xs"><img src="/resources/img/icon-1.png" alt="img">Бесплатная доставка от
                    5000 рублей
                </div>
                <div class="basket">
                    <form name="basket_form" class="form ng-pristine ng-valid" method="post"
                          {{--action="{{route('delivery.index')}}"--}}
                          id="basket_form">
                        @csrf
                        <div id="basket_form_container">
                            <div class="bx_ordercart bx_blue">


                                <div class="row basket-products-head">
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-center">Описание товара</div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 font-size-0">
                                        <div class="basket-products-head__item hidden-1024">Цена</div>
                                        <div class="basket-products-head__item"></div>
                                        <div class="basket-products-head__item hidden-xs">Стоимость</div>
                                    </div>
                                </div>


                                <table class="basket-products" id="basket_items">
                                    <tbody>
                                    <tr>
                                        <td class="item" colspan="2" id="col_NAME">

                                        </td>
                                        <td class="custom" id="col_DISCOUNT">

                                        </td>
                                        <td class="price" id="col_PRICE">

                                        </td>
                                        <td class="custom" id="col_QUANTITY">

                                        </td>
                                        <td class="custom" id="col_SUM">

                                        </td>


                                    </tr>
                                    @foreach($productCollection->elements as $product)
                                        <tr class="basket-product row"
                                            id="1{{$product->type}}{{$product->id}}variant{{$product->variantId}}">
                                            <td style="width: 100%; display: block;">


                                                <a href="#"
                                                   class="basket-product__title show-480">{{$product->title}}</a>
                                                <a class="backet-product__delete show-480"
                                                   href="#"
                                                   data-id="{{$product->id}}"
                                                   data-variant="{{$product->variantId}}"
                                                   data-quantity="{{$product->count}}"
                                                   data-type="{{$product->type}}"
                                                   onclick="remove(this)"></a>
                                                <div class="col-md-6 col-sm-6 col-xs-8 col-xsl-6 padding-0 basket-product-left">
                                                    <a href="{{$product->type === 'course'
                                            ? route('course.index', $product->slug)
                                            : route('product.index', $product->slug)}}"
                                                       class="basket-product__img">
                                                        <img src="{{$product->img}}"
                                                             alt="img">
                                                    </a>
                                                    <div class="backet-product__content">
                                                        <a href="{{$product->type === 'course'
                                                 ? route('course.index', $product->slug)
                                                  : route('product.index', $product->slug) }}"
                                                           class="basket-product__title hover-1">{{ $product->title}}</a>
                                                        <div class="basket-product__desk">
                                                            @if ($product->type === 'course')
                                                                Тип курса: &nbsp;
                                                                <span>{{ $product->variantValue}}</span>
                                                            @else
                                                                Объем: &nbsp;<span>{{ $product->variantValue}}</span>
                                                            @endif
                                                            <br>
                                                        </div>
                                                        <a class="backet-product__delete hover-1"
                                                           data-id="{{$product->id}}"
                                                           data-variant="{{$product->variantId}}"
                                                           {{--data-quantity="{{$product->count}}"--}}
                                                           data-quantity="99999"
                                                           data-type="{{$product->type}}"
                                                           onclick="remove(this);"
                                                           href="#">Удалить</a>
                                                    </div>
                                                </div>


                                                <div class="col-md-6 col-sm-6 col-xs-4 col-xsl-6 padding-0">
                                                    <div class="backet-product__info">
                                                        <div class="backet-product__info-item hidden-1024">
                                                            <div class="backet-product__price">
                                                                <div id="current_price_{{$product->type . $product->id}}variant{{$product->variantId}}">
                                                                    {{ $product->price}} руб.
                                                                </div>
                                                                <div id="old_price_{{$product->type . $product->id}}variant{{$product->variantId}}">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="backet-product__info-item">

                                                            <div class="amount-wrap">

                                                                <div class="amount__title">Количество</div>
                                                                <div class="amount">
																		<span id="basket_quantity_control">
																				<a class="amount__button amount__button--minus md-ink-ripple"
                                                                                   data-md-ink-ripple="#fff"
                                                                                   href="javascript:void(0);"
                                                                                   data-product="{{ $product->id}}"
                                                                                   data-variant="{{ $product->variantId}}"
                                                                                   data-count="-1"
                                                                                   data-price="{{ $product->price}}"
                                                                                   data-type="{{ $product->type}}"
                                                                                   onclick="addCount(this)"><i
                                                                                            class="material-icons amount-control__icon">remove</i></a>
																				<input id="QUANTITY_INPUT_{{$product->type . $product->id}}variant{{$product->variantId}}"
                                                                                       name="QUANTITY_INPUT_{{$product->type . $product->id}}variant{{$product->variantId}}"
                                                                                       max="0" class="amount__input"
                                                                                       step="1"
                                                                                       value="{{ $product->count}}"
                                                                                       readonly="" type="text">
																				<a class="amount__button amount__button--plus md-ink-ripple"
                                                                                   data-md-ink-ripple="#fff"
                                                                                   href="javascript:void(0);"
                                                                                   data-product="{{ $product->id}}"
                                                                                   data-variant="{{ $product->variantId}}"
                                                                                   data-count="1"
                                                                                   data-price="{{ $product->price}}"
                                                                                   data-type="{{ $product->type}}"
                                                                                   onclick="addCount(this)"><i
                                                                                            class="material-icons amount-control__icon">add</i></a>

																		</span>
                                                                </div>
                                                            </div>
                                                            {{--<input id="QUANTITY_{{$product->type . $product->id}}variant{{$product->variantId}}" name="QUANTITY_{{$product->type . $product->id}}variant{{$product->variantId}}"
                                                                   value="{{ $product->count}}"
                                                                   type="hidden">--}}

                                                        </div>
                                                        <div class="backet-product__info-item backet-product__info-total">
                                                            <div class="backet-product__total">


                                                                <div id="sum_{{$product->type . $product->id}}variant{{$product->variantId}}">
                                                                    {{ $product->price * $product->count}} руб.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    @endforeach
                                    <script>function addCount(elem) {
                                            let type = $(elem).data('type');
                                            let url = type === 'course'
                                                ? '{{route('cart.change.course')}}'
                                                : '{{route('cart.change.product')}}';
                                            let id = $(elem).data('product');
                                            let variant = $(elem).data('variant');
                                            $.post(url, {
                                                id: id,
                                                variant: variant,
                                                count: $(elem).data('count'),
                                                array: true
                                            }).done(function (request) {
                                                extracted(request);
                                                var price = $(elem).data('price');
                                                var number = (price * $(elem).parent().find('input').val());
                                                $('#sum_' + type + id + 'variant' + variant).text(number + ' руб.');
                                                afterChangeCart(request.view);
                                            });
                                        }</script>
                                    </tbody>
                                    <tbody>
                                    </tbody>
                                </table>


                                <input id="column_headers" value="NAME,DISCOUNT,PROPS,DELETE,DELAY,PRICE,QUANTITY,SUM"
                                       type="hidden">
                                <input id="offers_props" value="" type="hidden">
                                <input id="action_var" value="basketAction" type="hidden">
                                <input id="quantity_float" value="N" type="hidden">
                                <input id="count_discount_4_all_quantity" value="N" type="hidden">
                                <input id="price_vat_show_value" value="Y" type="hidden">
                                <input id="hide_coupon" value="N" type="hidden">
                                <input id="use_prepayment" value="N" type="hidden">
                                <input id="auto_calculation" value="Y" type="hidden">


                                <div class="basket-total-price">
                                    <div class="row">
                                        <div style="    padding-top: 20px;">
                                            <div class="bx_ordercart_order_pay_left" id="coupons_block">
                                                <div class="promocode" style="padding: 20px 15px; 0;">
                                                    <input id="coupon" name="COUPON" placeholder="Есть промокод?"
                                                           type="text" value="{{$productCollection->discount->code}}">
                                                    <a class="btn btn--black md-ink-ripple" data-md-ink-ripple="#fff"
                                                       href="javascript:void(0)"
                                                       onclick="$.post('{{route('coupon.store')}}',
                                                               {coupon_code: $('#coupon').val()}
                                                               ).done(function(data) {
                                                               console.log(data.message);
                                                               }).fail(function(request) {
                                                               if (request.status === 422) {
                                                               var $errors = $.parseJSON(request.responseText);
                                                               console.log($errors.errors.coupon_code[0]);
                                                               }
                                                               }).always(function(request) {
                                                               extracted(request);
                                                               });"
                                                       title="Нажмите для применения нового купона">Применить</a>
                                                </div>
                                                <script>function extracted(request) {
                                                        let title = 'Стоимость заказа' + (request.data.discount ? ' <b>со скидкой</b>' : null) + ': ';
                                                        $('#amount').html(title +
                                                            '<span class="fwb" data-amount="" id="allSum_FORMATED">' +
                                                            (request.data.amount - request.data.discount.amount).toLocaleString('ru-RU')
                                                            + '&nbsp;руб.</span>');
                                                    }</script>
                                                {{--<div class="bx_ordercart_coupon"><input disabled="true" readonly="true"
                                                                                        class="bad" name="OLD_COUPON[]"
                                                                                        type="text"><span
                                                            data-coupon="ABC1231" class="bad"></span>
                                                    <div class="bx_ordercart_coupon_notes">не найден</div>
                                                </div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6 cart-text-right" id="amount">
                                            Стоимость заказа @if($productCollection->discount) <b>со скидкой</b>@endif:
                                            <span class="fwb" data-amount="{{$productCollection->amount}}"
                                                  id="allSum_FORMATED">
                                                {{number_format($productCollection->amount - $productCollection->discount->amount, 0, '', ' ') }}
                                                &nbsp;руб.</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6 hidden-xsl"><img
                                                src="/resources/img/payment_cards.jpg"
                                                alt="img"></div>
                                    <a href="javascript:void(0)" onclick="checkOut();"
                                       class="col-sm-6 col-xs-6 col-xsl-12 cart-text-right">
                                        <button class="btn md-ink-ripple" data-md-ink-ripple="#fff"
                                                type="submit"
                                                onclick="window.location = '{{route('delivery.index')}}';"
                                        >ОФОРМИТЬ
                                            ЗАКАЗ
                                        </button>
                                    </a>
                                </div>


                                <div id="basket_items_delayed" class="bx_ordercart_order_table_container"
                                     style="display:none">
                                    <table id="delayed_items">
                                        <thead>
                                        <tr>
                                            <td class="margin"></td>
                                            <td class="item" colspan="2">
                                                Товары
                                            </td>
                                            <td class="custom">
                                                Скидка
                                            </td>
                                            <td class="price">
                                                Цена
                                            </td>
                                            <td class="custom">
                                                Количество
                                            </td>
                                            <td class="custom">
                                                Сумма
                                            </td>
                                            <td class="custom"></td>
                                            <td class="margin"></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>

                                    </table>
                                </div>
                                <div id="basket_items_subscribed" class="bx_ordercart_order_table_container"
                                     style="display:none">
                                    <table>
                                        <thead>
                                        <tr>
                                            <td class="margin"></td>
                                            <td class="item" colspan="2">
                                                Товары
                                            </td>
                                            <td class="custom">
                                                Скидка
                                            </td>
                                            <td class="price">
                                                Цена
                                            </td>
                                            <td class="custom">
                                                Количество
                                            </td>
                                            <td class="custom">
                                                Сумма
                                            </td>
                                            <td class="custom"></td>
                                            <td class="margin"></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>

                                    </table>
                                </div>
                                <div id="basket_items_not_available" class="bx_ordercart_order_table_container"
                                     style="display:none">
                                    <table>

                                        <thead>
                                        <tr>
                                            <td class="margin"></td>
                                            <td class="item" colspan="2">
                                                Товары
                                            </td>
                                            <td class="price">
                                                Цена
                                            </td>
                                            <td class="custom">
                                                Количество
                                            </td>
                                            <td class="custom"></td>
                                            <td class="margin"></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <input name="BasketOrder" value="BasketOrder" type="hidden">
                        <!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
                    </form>
                </div>
            </div>
            <div style="margin-top: 35px;"><!--'start_frame_cache_sz16ux'-->
                <!--'end_frame_cache_sz16ux'-->        </div>
        </div>
    </div>
@endsection