<div class="cart-action__item bx-basket-item-list-item" id="{{$item->type}}{{$item->id}}variant{{$item->variantId}}">
    <div onclick="remove(this)"
         data-id="{{$item->id}}"
         data-variant="{{$item->variantId}}"
         {{--data-quantity="{{$item->count}}"--}}
         data-quantity="99999"
         data-type="{{$item->type}}"
         class="cart-action__circle cart-action__close-btn closebtn">
        <i class="material-icons cart-action__icon single_add_to_cbdel">close</i>
    </div>

    <div class="cart-action__pic">
        <img class="img-responsive"
             src="{{$item->img}}"
             alt="{{$item->title}}"/>
    </div>

    <div class="cart-action__info">
        <div class="cart-action__item-title">
            {{$item->title}}
        </div>
        <div class="cart-action__item-sum">Цена: {{$item->price}} руб. X{{$item->count}}</div>
    </div>

</div>