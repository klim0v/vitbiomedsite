<div id="display" class="cart-container ng-scope" data-ng-controller="CartController as cart">
    <div class="cart crt_cl" id="cart_main">
        <div data-hm-tap="cart.open()" class="cart__icon"></div>
        <div id="cart_div">
            @include('public.shop.cart.small.list')
        </div>
    </div>
</div>
