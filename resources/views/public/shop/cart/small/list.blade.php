<div class="cart__sum" id="cart_sum">{{\count($productCollection->elements)}}</div>
<div data-role="basket-item-list" class="cart-action js-cart-action" id="displon">

    <div id="basket-container" class="basket-container">
        <div id="bx_basketT0kNhm" class="bx-basket bx-opener">

            <div class="cart-action__container">
                <div id="small_basket" class="cart-action__items">
                    @if (count($productCollection->elements))
                        @foreach($productCollection->elements as $item)
                            @include('public.shop.cart.small.item', ['item' => $item])
                        @endforeach
                    @endif
                    <script>
                        function remove(elem) {
                            let token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': token
                                    }
                                });
                            } else {
                                console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
                            }
                            let id = $(elem).data('id');
                            let variantId = $(elem).data('variant');
                            let quantity = $(elem).data('quantity');
                            let type = $(elem).data('type');
                            let route = type === 'course' ? '{{route('cart.change.course')}}' : '{{route('cart.change.product')}}';
                            $.post(route,
                                {
                                    id: id,
                                    variant: variantId,
                                    count: -quantity
                                }
                            ).done(function (data) {
                                $('#1' + type + id + 'variant' + variantId).remove();
                                afterChangeCart(data);
                            });
                        }
                    </script>
                </div>

                <div class="cart-action__title">
                    <span>В корзине: {{\count($productCollection->elements)}} товар</span>
                </div>
                <div class="cart-action__sum">
                    <span>Общая стоимость</span>
                    <span>{{$productCollection->amount}} руб.</span>


                </div>
                <div class="cart-action__buttons">
                    <a href="{{route('cart.index')}}"
                       class="cart-action__button cart-action__button--gutter">Перейти в
                        корзину</a>
                    <a href="#openClickCart" class="cart-action__button">Купить в 1
                        клик</a>
                </div>
            </div>
        </div>

    </div>
</div>