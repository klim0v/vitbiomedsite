<!DOCTYPE html>
<html lang="ru" data-ng-app="app">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Title</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="Keywords" />
    <meta name="description" content="Description" />
    <link href="/old/bitrix/js/main/core/css/core.css?14392234113963" type="text/css"  rel="stylesheet" />
    <link href="/old/bitrix/js/main/core/css/core_popup.css?148665183732860" type="text/css"  rel="stylesheet" />
    <link href="/old/bitrix/components/bitrix/socserv.auth.form/templates/flat/style.css?14845676842847" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/old/bitrix/templates/vit/components/bitrix/menu/menusi/style.css?1482946469581" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/old/bitrix/templates/vit/components/bitrix/sender.subscribe/template1/style.css?14829457374640" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/old/bitrix/templates/vit/components/bitrix/form.result.new/popup/style.css?1439223420666" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/old/bitrix/templates/vit/template_styles.css?1498751288196458" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <script type="text/javascript">if(!window.BX)window.BX={message:function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;}};</script>
    <script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
    <script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'','SERVER_TIME':'1536402116','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'0ed75028c8c81a9ab3b5436d03242344'});</script>

    <script type="text/javascript">
        (function () {
            "use strict";

            var counter = function ()
            {
                var cookie = (function (name) {
                    var parts = ("; " + document.cookie).split("; " + name + "=");
                    if (parts.length == 2) {
                        try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
                        catch (e) {}
                    }
                })("BITRIX_CONVERSION_CONTEXT_s1");

                if (! cookie || cookie.EXPIRE < BX.message("SERVER_TIME"))
                {
                    var request = new XMLHttpRequest();
                    request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
                    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    request.send(
                        "SITE_ID="+encodeURIComponent("s1")+
                        "&sessid="+encodeURIComponent(BX.bitrix_sessid())+
                        "&HTTP_REFERER="+encodeURIComponent(document.referrer)
                    );
                }
            };

            if (window.frameRequestStart === true)
                BX.addCustomEvent("onFrameDataReceived", counter);
            else
                BX.ready(counter);
        })();
    </script>



    <script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "645478648abbf1d479a423fd862edac4"]); _ba.push(["host", "vitbiomed.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>


    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <link href="/old/bitrix/templates/vit/css/swiper.min.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/preloader.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/magnific-popup.css" rel="stylesheet">
    <link href="/old/bitrix/templates/vit/css/new_styles.css" rel="stylesheet">
    <link href="old/static/css/font.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Exo+2:700,300italic,200,900italic,500italic,800,300,600italic,500,800italic,900,100italic,200italic,600,700italic,400,100,400italic&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/old/dist/bundle.css?rev=1ede78ce969cf752c75bdda5642606e0">
    <link rel="stylesheet" href="/old/dist/magnific-popup.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>

</head>
<body data-spy="scroll" data-target="#nav" class="likearts-content">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4&appId=273276476164317";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>



<!-- MODAL WINDOW LK AUTH -->
<div id="auth-modal" class="md-modal uk-modal">
    <div class="uk-modal-content md-content">

        <div class="md-top">
            <div class="md-close" onclick="$('.md-overlay').click();"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7"><polygon points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/></svg></div>
            <div class="md-title">Вход</div>
            <div class="lk-social-media">
                <span>через</span>
                <ul class="lk-social-media__list">
                    <script type="text/javascript">
                        function BxSocServPopup(id)
                        {
                            var content = BX("bx_socserv_form_"+id);
                            if(content)
                            {
                                var popup = BX.PopupWindowManager.create("socServPopup"+id, BX("bx_socserv_icon_"+id), {
                                    autoHide: true,
                                    closeByEsc: true,
                                    angle: {offset: 24},
                                    content: content,
                                    offsetTop: 3
                                });

                                popup.show();

                                var input = BX.findChild(content, {'tag':'input', 'attribute':{'type':'text'}}, true);
                                if(input)
                                {
                                    input.focus();
                                }

                                var button = BX.findChild(content, {'tag':'input', 'attribute':{'type':'submit'}}, true);
                                if(button)
                                {
                                    button.className = 'btn btn-primary';
                                }
                            }
                        }
                    </script>

                    <div class="bx-authform-social">
                        <ul>
                            <li>
                                <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1801245380125777&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fnashi-kontakty%2F%3Fauth_service_id%3DFacebook%26check_key%3Dbbbef01a7bf3d467066f72e8286a60ed%26backurl%3D%252Fnashi-kontakty%252F&amp;scope=email,publish_actions,user_friends&amp;display=popup', 680, 600)" title="Facebook"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://oauth.yandex.ru/authorize?response_type=code&amp;client_id=88cb897161164506923f302388cd2ca5&amp;display=popup&amp;state=site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26mode%3Dopener%26redirect_url%3D%252Fnashi-kontakty%252F', 680, 600)" title="Яндекс"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=5825158&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fnashi-kontakty%2F%3Fauth_service_id%3DVKontakte&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26redirect_url%3D%252Fnashi-kontakty%252F', 660, 425)" title="ВКонтакте"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=674779939050-7ejv3833b2g3vivpkir9ekio10id5d8j.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGooglePlusOAuth%26site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26mode%3Dopener%26redirect_url%3D%252Fnashi-kontakty%252F', 580, 400)" title="Google+"></a>
                            </li>
                        </ul>
                    </div>
                </ul>
                <span>или</span>
            </div>
        </div>
        <div class="md-form">
            <form name="system_auth_form6zOYVN"
                  method="post"
                  action="/nashi-kontakty/?login=yes"
                  class="">
                <input type="hidden" name="USER_REMEMBER" value="Y">
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="AUTH"/>
                <input type="hidden" name="backurl" value="" class="backurl">
                <div class="uk-form-row">
                    <label class="uk-form-label">Эл. почта</label>

                    <div class="uk-form-controls">
                        <input type="text"
                               name="USER_LOGIN"
                               maxlength="50"
                               value="admin"
                               class="uk-form-large uk-width-1-1">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Пароль</label>

                    <div class="uk-form-controls">
                        <div class="uk-form-password uk-width-1-1">
                            <input type="password"
                                   name="USER_PASSWORD"
                                   maxlength="50"
                                   class="uk-form-large uk-width-1-1">
                            <a data-uk-form-password='{lblShow: "<i class=\"uk-icon-eye-slash\"></i>", lblHide: "<i class=\"uk-icon-eye\"></i>"}' class="uk-form-password-toggle" href=""><i class='uk-icon-eye-slash'></i></a>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <button type="submit"
                            name="Login"
                            value="Войти"
                            class="btn" data-md-ink-ripple="#fff" >Войти</button>
                </div>


                <div class="md-b">
                    <a href="/nashi-kontakty/?forgot_password=yes&amp;backurl=%2Fnashi-kontakty%2F" class="fl-l ajax-link" rel="nofollow">Вспомнить пароль</a>
                    <a href="/nashi-kontakty/?register=yes&amp;backurl=%2Fnashi-kontakty%2F" class="fl-r ajax-link" rel="nofollow">Зарегистрироваться</a>
                </div>
            </form>

        </div>				</div>
</div>
<!-- MODAL WINDOW OVERLAY -->
<div class="md-overlay"></div>
<!--//auth-modal-->

<!-- MODAL WINDOW LK reg -->
<div id="auth-modal-reg" class="md-modal uk-modal">
    <div class="uk-modal-content md-content">



        <div class="md-top">
            <div class="md-close" onclick="$('.md-overlay').click();"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.7 77.7"><polygon points="71.4 0 38.9 32.5 6.4 0 0 6.3 32.5 38.8 0 71.3 6.4 77.7 38.9 45.2 71.4 77.7 77.7 71.3 45.2 38.8 77.7 6.3 71.4 0"/></svg></div>
            <div class="md-title">Регистрация</div>
            <div class="lk-social-media">
                <span>через</span>
                <ul class="lk-social-media__list">
                    <script type="text/javascript">
                        function BxSocServPopup(id)
                        {
                            var content = BX("bx_socserv_form_"+id);
                            if(content)
                            {
                                var popup = BX.PopupWindowManager.create("socServPopup"+id, BX("bx_socserv_icon_"+id), {
                                    autoHide: true,
                                    closeByEsc: true,
                                    angle: {offset: 24},
                                    content: content,
                                    offsetTop: 3
                                });

                                popup.show();

                                var input = BX.findChild(content, {'tag':'input', 'attribute':{'type':'text'}}, true);
                                if(input)
                                {
                                    input.focus();
                                }

                                var button = BX.findChild(content, {'tag':'input', 'attribute':{'type':'submit'}}, true);
                                if(button)
                                {
                                    button.className = 'btn btn-primary';
                                }
                            }
                        }
                    </script>

                    <div class="bx-authform-social">
                        <ul>
                            <li>
                                <a id="bx_socserv_icon_Facebook" class="facebook bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1801245380125777&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fnashi-kontakty%2F%3Fauth_service_id%3DFacebook%26check_key%3Dbbbef01a7bf3d467066f72e8286a60ed%26backurl%3D%252Fnashi-kontakty%252F&amp;scope=email,publish_actions,user_friends&amp;display=popup', 680, 600)" title="Facebook"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_YandexOAuth" class="yandex bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://oauth.yandex.ru/authorize?response_type=code&amp;client_id=88cb897161164506923f302388cd2ca5&amp;display=popup&amp;state=site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26mode%3Dopener%26redirect_url%3D%252Fnashi-kontakty%252F', 680, 600)" title="Яндекс"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_VKontakte" class="vkontakte bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=5825158&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fnashi-kontakty%2F%3Fauth_service_id%3DVKontakte&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26redirect_url%3D%252Fnashi-kontakty%252F', 660, 425)" title="ВКонтакте"></a>
                            </li>
                            <li>
                                <a id="bx_socserv_icon_GooglePlusOAuth" class="google-plus bx-authform-social-icon" href="javascript:void(0)" onclick="BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=674779939050-7ejv3833b2g3vivpkir9ekio10id5d8j.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fvitbiomed.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGooglePlusOAuth%26site_id%3Ds1%26backurl%3D%252Fnashi-kontakty%252F%253Fcheck_key%253Dbbbef01a7bf3d467066f72e8286a60ed%26mode%3Dopener%26redirect_url%3D%252Fnashi-kontakty%252F', 580, 400)" title="Google+"></a>
                            </li>
                        </ul>
                    </div>
                </ul>
                <span>или</span>
            </div>
        </div>
        <div class="md-form">
            <form method="POST" action="/nashi-kontakty/" name="regform" class="uk-form uk-form-stacked">
                <input type="hidden" name="TYPE" value="REGISTRATION"/>
                <input type="hidden" name="register_submit_button" value="Y"/>
                <input type="hidden" class="api-mf-antibot" value="" name="ANTIBOT[NAME]">
                <input type="hidden" name="backurl" value="" class="backurl">

                <div class="uk-form-row">
                    <label class="uk-form-label">Имя:<span	class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[NAME]"
                               value="">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Фамилия:<span	class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[LAST_NAME]"
                               value="">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Адрес e-mail:<span	class="asterisk">*</span></label>
                    <div class="uk-form-controls">
                        <input size="30"
                               class="uk-width-1-1 uk-form-large"
                               type="text"
                               name="REGISTER[EMAIL]"
                               value="">
                    </div>
                </div>


                <div class="uk-form-row">
                    <button type="submit"
                            name="register_submit_button"
                            class="btn" data-md-ink-ripple="#fff"
                            value="Регистрация">Регистрация</button>
                </div>
                <div class="uk-form-row">
                    <noindex>
                        <a href="/nashi-kontakty/?login=yes"
                           rel="nofollow"
                           class="ajax-link">Авторизация</a>
                    </noindex>
                </div>
            </form></div>
    </div>
</div>
<!-- MODAL WINDOW OVERLAY -->
<div class="md-overlay"></div>
<!--//reg-modal-->




<div class="header-container">
    <div class="container">
        <header class="header">
            <div class="flex-container">
                <div class="flex-item">
                    <a href="/" class="logo-link">
                        <div class="logo"></div>
                    </a>
                </div>
                <div class="flex-item">
                    <div class="menu-desktop">


                        <ul class="menu-desktop-list">

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru" class="menu-desktop-list__link">Клиника</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru" class="menu-desktop-list__link">Пробиотики</a></li>

                            <li class="menu-desktop-list__item"><a href="http://store.vitbiomed.ru/courses/" class="menu-desktop-list__link">Курсы</a></li>

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/reviews/" class="menu-desktop-list__link">Отзывы</a></li>

                            <li class="menu-desktop-list__item"><a href="http://vitbiomed.ru/blog/" class="menu-desktop-list__link">Блог</a></li>


                            <li class="menu-desktop-list__item">
                                <span class="phone">+7 (499) 164-13-80</span>
                            </li>
                        </ul>
                    </div>
                    <div class="menu-mobile-container">
                        <div class="menu-mobile" data-ng-controller="MobileMenuController as mobileMenu">
                            <div id="menu-mobile-btn" data-hm-tap="mobileMenu.controlMenu($event)" class="menu-mobile__btn">
                                <i class="material-icons menu-mobile__icon">view_headline</i>
                            </div>
                            <div id="menu-mobile-body" class="menu-mobile__body display-hide">
                                <ul class="menu-mobile-list">
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Компания</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://vitbiomed.ru/proizvodstvo/" class="menu-mobile-submenu__link">Производство</a>
                                            <a href="http://vitbiomed.ru/meditsinskiy-tsentr/" class="menu-mobile-submenu__link">Медицинский центр</a>
                                            <a href="http://vitbiomed.ru/reviews/" class="menu-mobile-submenu__link ">О нас говорят</a>
                                            <a href="http://vitbiomed.ru/nashi-kontakty/" class="menu-mobile-submenu__link">Контакты</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Информация</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="#" class="menu-mobile-submenu__link ">Публичная оферта</a>
                                            <a href="http://store.vitbiomed.ru/faq/" class="menu-mobile-submenu__link">Вопросы</a>
                                            <a data-uk-modal="" href="#auth-modal" class="menu-mobile-submenu__link md-trigger" id="auth-link" data-modal="auth-modal">Вход</a>
                                            <a data-uk-modal="" href="#auth-modal-reg" class="menu-mobile-submenu__link md-trigger" id="auth-link" data-modal="auth-modal-reg">Регистрация</a>
                                        </div>
                                    </li>
                                    <li class="menu-mobile-list__item">
                                        <a href="#" class="menu-mobile-list__link menu-mobile-list__link--active">Пробиотики</a>
                                        <div class="menu-mobile-submenu">
                                            <a href="http://store.vitbiomed.ru/dostavka/" class="menu-mobile-submenu__link">Доставка и оплата</a>
                                            <a href="http://store.vitbiomed.ru/" class="menu-mobile-submenu__link">Купить пробиотики</a>
                                            <a href="http://store.vitbiomed.ru/courses/" class="menu-mobile-submenu__link">Купить курс</a>
                                            <a href="http://vitbiomed.ru/partnyeram/" class="menu-mobile-submenu__link">Партнерам</a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="social-block">
                                    <a href="https://www.facebook.com/vitbiomed/"><div class="social-item social-item--facebook"></div></a>
                                    <a href="https://www.instagram.com/vitbiomed/"><div class="social-item social-item--instagram"></div></a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ"><div class="social-item social-item--youtube"></div></a>
                                    <a href="https://plus.google.com/+vitbiomed"><div class="social-item social-item--google"></div></a>
                                    <a href="https://vk.com/vitbiomed "><div class="social-item social-item--vk"></div></a>
                                </div>
                                <div class="phone-block">
                                    <a href="tel:+74991641380"><div class="phone-btn">
                                            <i class="material-icons phone-btn__icon">call</i>
                                        </div></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </header>
    </div>
</div>

<!-- /header -->



<section id="contacts">
    <h1>Контакты</h1>
    <span class="subtitle">
Cвяжитесь с нами любым удобным для вас способом: </span>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="contact-block phone-block">
                    <i class="phone-icon"></i>
                    <a href="tel:+74991641380">+7 (499) 164-13-80</a>
                    <span class="text">запись на приём </span> <a href="tel:+74991648831">+7 (499) 164-88-31</a> <span class="text">бухгалтерия </span>
                </div>
                <div class="contact-block adress-block">
                    <i class="home-icon"></i> <span class="title">Наш адрес:</span> <span class="text">Москва, ул. 5-я Парковая, 46.</span> <a href="#" class="how_to_go">Как добраться?</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="contact-block work-time-block">
                    <i class="time-icon"></i> <span class="title">Режим работы:</span> <span class="text">Пн-чт: 10.00-20.00</span> <span class="text">пт, сб : 10.00-17.00</span> <span class="text">вс: выходной</span>
                </div>
                <div class="contact-block we-in-soc-block">
                    <i class="topic-icon"></i> <span class="title">Мы в соц. сетях:</span>
                    <a href="http://fb.com/vitbiomed" target="_blank">fb.com/vitbiomed</a>  <a href="http://vk.com/vitbiomed" target="_blank">vk.com/vitbiomed </a> <a href="http://plus.google.com" target="_blank">plus.google.com</a>			</div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="contact-block mail-block">
                    <i class="mail-icon"></i> <span class="mobile-block">
				<a href="milto:info@vitbiomed.ru">info@vitbiomed.ru</a> <span class="text">по любым вопросам</span>
				 <a href="milto:sales@vitbiomed.ru">sales@vitbiomed.ru</a> <span class="text">отдел продаж</span> </span> <span class="mobile-block">
				<a href="milto:marketing@vitbiomed.ru">marketing@vitbiomed.ru</a> <span class="text">отдел маркетинга</span>
				<a href="milto:e.kuznetsova@vitbiomed.ru">e.kuznetsova@vitbiomed.ru</a> <span class="text">главный врач</span> </span>
                </div>
            </div>
        </div>
    </div>
</section> <section id="map"> </section>
<h1 class="" style="text-align: center;"><br>
</h1>
<h1 class="" style="text-align: center;">Как добраться?</h1>
<h1 class="" style="text-align: center;"><span style="font-size: 13pt;"><b>м. Первомайская:</b> 14&nbsp;мин. пешком (первый вагон из центра, выход&nbsp;налево)</span></h1>
<span style="font-size: 13pt;"><br>
 </span> <span style="font-size: 11pt;"><br>
 </span> <section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="how-arive-block left">
                    <span class="text"><span style="font-size: 13pt;"><b>м. Измайловская:</b></span></span><span style="font-size: 13pt;"> </span><span class="text"><span style="font-size: 13pt;">первый вагон из центра, </span><span style="font-size: 13pt;">троллейбусы:&nbsp;</span><span style="font-size: 13pt;">№23,&nbsp;№51, </span><span style="font-size: 13pt;">автобус&nbsp;№34,&nbsp;</span><span style="font-size: 13pt;">м/т:&nbsp;№23м,&nbsp;№51м&nbsp;</span><span style="font-size: 13pt;">до остановки&nbsp;“Верхняя Первомайская улица” (~6 мин)</span>&nbsp;<br>
 <span style="font-size: 13pt;"><b>м. Щелковская: </b>последний&nbsp;вагон из центра</span><span style="font-size: 13pt;">,<b>&nbsp;</b>тролл.&nbsp;</span><span style="font-size: 13pt;">№23,&nbsp;</span></span><span style="font-size: 13pt;">м/т&nbsp;№23м (~6 мин)</span>
                </div>
                <span style="font-size: 12pt;"> </span>
            </div>
            <span style="font-size: 12pt;"> </span>
            <div class="col-md-6 col-sm-6">
                <span style="font-size: 12pt;"> </span>
                <div class="how-arive-block right">
                    <span class="text" style="font-size: 13pt;"><b>м. Преображенская площадь:</b></span><span style="font-size: 13pt;"> </span><span class="text"><span style="font-size: 13pt;">первый вагон из центра, автобус&nbsp;№230&nbsp;или м/т №230м </span><span style="font-size: 13pt;">до остановки&nbsp;“5-я Парковая улица” (~14 мин)</span><br>
 <span style="font-size: 13pt;"><b>м. Черкизовская:</b></span> <span style="font-size: 13pt;">автобус №230, м/т 230м (~8 мин)</span><br>
 <br>
 <br>
 </span>
                </div>
            </div>
        </div>
    </div>
</section><br>




<div class="footer-container">
    <div class="container">
        <footer class="footer">
            <div class="footer__content--desktop">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-label">
                            <a href="mailto:info@vitbiomed.ru" class="white-label__link">Задайте вопрос нашим специалистам</a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-sm-12-override">
                        <div class="footer-menu--desktop">
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Компания</div>
                                <div class="footer-menu-links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu-links__item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/" class="footer-menu-links__item">Медицинский центр</a>
                                    <a href="http://vitbiomed.ru/uslugi/" class="footer-menu-links item">Услуги</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu-links__item">О нас говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu-links__item">Контакты</a>
                                    <a href="http://vitbiomed.ru/nezavisimaya-otsenka-kachestva-okazaniya-uslug/" class="footer-menu-links__item">Независимая оценка качества<br> оказания услуг</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Информация</div>
                                <div class="footer-menu-links">
                                    <a href="http://vitbiomed.ru/blog/" class="footer-menu-links__item">Блог</a>
                                    <a href="#" class="footer-menu-links__item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu-links__item">Вопросы</a>
                                    <a href="http://vitbiomed.ru/normativnie-documenti/" class="footer-menu-links__item">Нормативные документы</a>
                                    <a data-uk-modal="" href="#auth-modal" class="footer-menu-links__item md-trigger md-trigger" id="auth-link" data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg" class="footer-menu-links__item md-trigger md-trigger" id="auth-link" data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu__column">
                                <div class="footer-menu__title">Пробиотики</div>
                                <div class="footer-menu-links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu-links__item">Доставка и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu-links__item">Купить пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu-links__item">Купить курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu-links__item">Партнерам</a>
                                </div>
                            </div>
                            <div class="footer-menu__column footer-menu__column--mobile">
                                <div class="footer-menu__title">Мы в соц сетях</div>
                                <div class="footer-menu-links">
                                    <a href="https://www.facebook.com/vitbiomed/" class="footer-menu-links__item footer-social-link footer-social-link--facebook">Facebook</a>
                                    <a href="https://vk.com/vitbiomed " class="footer-menu-links__item footer-social-link footer-social-link--vk">Vkontakte</a>
                                    <a href="https://plus.google.com/+vitbiomed" class="footer-menu-links__item footer-social-link footer-social-link--google">Google+</a>
                                    <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ" class="footer-menu-links__item footer-social-link footer-social-link--youtube">Youtube</a>
                                    <a href="https://www.instagram.com/vitbiomed/" class="footer-menu-links__item footer-social-link footer-social-link--instagram">Instagram</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tablet-visible">
                        <div class="footer-social footer-social--tablet">
                            <a href="https://www.facebook.com/vitbiomed/" class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed" class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ" class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/" class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-offset-1 col-md-3 col-sm-12-override">
                        <a href="#openModal" class="btn-link">
                            <div class="buy-btn">Подписаться</div>
                        </a>
                        <div id="openModal" class="modalbg">
                            <div class="dialog"><div class="modalbh">
                                    <a href="#close" title="Close" class="close"></a>
                                    <div class="text_df"><span class="spanx">Полезная рассылка</span>
                                        <p>Советы по поддержанию здоровья Рекомендации по питанию Обновления блога Видео-консультации наших врачей</p>
                                    </div><div class="text_df">
                                        <div class="bx-subscribe2"  id="sender-subscribe">
                                            <!--'start_frame_cache_sender-subscribe'-->
                                            <script>
                                                BX.ready(function()
                                                {
                                                    BX.bind(BX("bx_subscribe_btn_sljzMT"), 'click', function() {
                                                        setTimeout(mailSender, 250);
                                                        return false;
                                                    });
                                                });

                                                function mailSender()
                                                {
                                                    setTimeout(function() {
                                                        var btn = BX("bx_subscribe_btn_sljzMT");
                                                        if(btn)
                                                        {
                                                            var btn_span = btn.querySelector("span");
                                                            var btn_subscribe_width = btn_span.style.width;
                                                            BX.addClass(btn, "send");
                                                            btn_span.outterHTML = "<span><i class='fa fa-check'></i> ГОТОВО</span>";
                                                            if(btn_subscribe_width)
                                                                btn.querySelector("span").style["min-width"] = btn_subscribe_width+"px";
                                                        }
                                                    }, 400);
                                                }
                                            </script>
                                            <form role="form" method="post" action="/nashi-kontakty/"  onsubmit="BX('bx_subscribe_btn_sljzMT').disabled=true;">
                                                <input type="hidden" name="sessid" id="sessid" value="0ed75028c8c81a9ab3b5436d03242344" />		<input type="hidden" name="sender_subscription" value="add">

                                                <div class="bx-input-group">
                                                    <input class="bx-form-control" type="text" name="SENDER_SUBSCRIBE_NAME" value="" title="" placeholder="Ваше имя">
                                                </div>
                                                <div style="height:5px;"></div>
                                                <div class="bx-input-group">
                                                    <input class="bx-form-control" type="email" name="SENDER_SUBSCRIBE_EMAIL" value="" title="Введите ваш e-mail" placeholder="Введите ваш e-mail">
                                                </div>
                                                <div class="bx_subscribe_submit_container">
                                                    <button class="sendebtn" id="bx_subscribe_btn_sljzMT">ПОДПИСАТЬСЯ</button>
                                                </div>
                                            </form>
                                            <!--'end_frame_cache_sender-subscribe'--></div>												   </div> </div>
                            </div>
                        </div>
                        <div class="contacts">
                            <div class="contacts__item contacts__item--phone">
                                Онлайн-магазин:<br>
                                +7 (962) 938-26-40<br>
                                order@vitbiomed.ru<br>
                            </div>
                            <div class="contacts__item contacts__item--phone">
                                Клиника: <br>
                                +7 (499) 164-13-80<br>
                                info@vitbiomed.ru<br>
                                Москва, ул. 5-я Парковая, д.46
                            </div>								</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">Витбиомед &copy; 2016</div>
                    </div>
                </div>
            </div>
            <div class="footer__content--phone">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="white-label white-label--phone">
                            <a href="#" class="white-label__link">Задайте вопрос нашим специалистам</a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-menu--phone">
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Компания</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://vitbiomed.ru/proizvodstvo/" class="footer-menu--phone__link-item">Производство</a>
                                    <a href="http://vitbiomed.ru/meditsinskiy-tsentr/" class="footer-menu--phone__link-item">Медицинский центр</a>
                                    <a href="http://vitbiomed.ru/reviews/" class="footer-menu--phone__link-item">О нас говорят</a>
                                    <a href="http://vitbiomed.ru/nashi-kontakty/" class="footer-menu--phone__link-item">Контакты</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Информация</div>
                                <div class="footer-menu--phone__links">
                                    <a href="#openModal" class="footer-menu--phone__link-item btn-link">Подписка</a>
                                    <a href="#" class="footer-menu--phone__link-item">Публичная оферта</a>
                                    <a href="http://store.vitbiomed.ru/faq/" class="footer-menu--phone__link-item">Вопросы</a>
                                    <a data-uk-modal="" href="#auth-modal" class="footer-menu--phone__link-item md-trigger" id="auth-link" data-modal="auth-modal">Вход</a>
                                    <a data-uk-modal="" href="#auth-modal-reg" class="footer-menu--phone__link-item md-trigger" id="auth-link" data-modal="auth-modal-reg">Регистрация</a>
                                </div>
                            </div>
                            <div class="footer-menu--phone__item footer-menu--phone__item--active">
                                <div class="footer-menu--phone__title">Пробиотики</div>
                                <div class="footer-menu--phone__links">
                                    <a href="http://store.vitbiomed.ru/dostavka/" class="footer-menu--phone__link-item">Доставка и оплата</a>
                                    <a href="http://store.vitbiomed.ru/" class="footer-menu--phone__link-item">Купить пробиотики</a>
                                    <a href="http://store.vitbiomed.ru/courses/" class="footer-menu--phone__link-item">Купить курс</a>
                                    <a href="http://vitbiomed.ru/partnyeram/" class="footer-menu--phone__link-item">Партнерам</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="footer-social footer-social--phone">
                            <a href="https://www.facebook.com/vitbiomed/" class="footer-social__item footer-social__item--facebook"></a>
                            <a href="https://plus.google.com/+vitbiomed" class="footer-social__item footer-social__item--google"></a>
                            <a href="https://vk.com/vitbiomed " class="footer-social__item footer-social__item--vk"></a>
                            <a href="https://www.youtube.com/channel/UCUHf1MmtISWYGbMhn1O0dRQ" class="footer-social__item footer-social__item--youtube"></a>
                            <a href="https://www.instagram.com/vitbiomed/" class="footer-social__item footer-social__item--instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>


<!-- Modals -->
<div class="modal fade hidden-xs" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade hidden-xs" id="testModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="test-popup">
                    <div class="row">
                        <div class="steps">
                            <div class="step1">
                                <span class="active">шаг 1</span>
                            </div>
                            <div class="step2">
                                <span>шаг 2</span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <h3>
                                ТЕСТ: ЕСТЬ ЛИ У ВАС ПОВОД ОБРАТИТЬСЯ К ВРАЧУ?
                            </h3>
                        </div>
                        <form action="/bitrix/templates/vit/ajax/test.php" id="test_form" method="POST">
                            <div id="step1" class="step-content active">
                                <div class="col-sm-12">

                                    <h5>Ответьте, пожалуйста,  на 8 простых вопросов:</h5>
                                </div>
                                <div class="col-sm-6 left-col">
                                    <div class="test-item">
                                        <p>Жалуетесь ли Вы на кишечник (диспепсия, неустойчивый стул, запоры или поносы, урчание, боли в животе)?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q1" id="test1" type="radio" value="y"><label for="test1"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q1" id="test2" type="radio" value="n"><label for="test2"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Часто ли Вы болеете простудными заболеваниями?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q2" id="test3" type="radio" value="y"><label for="test3"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q2" id="test4" type="radio" value="n"><label for="test4"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Часто летаете самолетом со сменой часовых поясов или работаете в ночную смену?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q3" id="test5" type="radio" value="y"><label for="test5"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q3" id="test6" type="radio" value="n"><label for="test6"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Есть ли у Вас синдром хронической усталости (слабость, быстрая утомляемость, сонливость, ухудшение памяти, упадок сил, не проходящий даже после длительного отдыха)?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q4" id="test7" type="radio" value="y"><label for="test7"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q4" id="test8" type="radio" value="n"><label for="test8"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 right-col">
                                    <div class="test-item">
                                        <p>Есть ли у Вас проблемы с кожей (угревая сыпь, атопический дерматит, псориаз, раннее старение)?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q5" id="test9" type="radio" value="y"><label for="test9"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q5" id="test10" type="radio" value="n"><label for="test10"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Принимали ли Вы в течение последнего года антибиотики и, может быть, неоднократно?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q6" id="test11" type="radio" value="y"><label for="test11"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q6" id="test12" type="radio" value="n"><label for="test12"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Часто ли Вы находитесь в состоянии  стресса?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q7" id="test13" type="radio" value="y"><label for="test13"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q7" id="test14" type="radio" value="n"><label for="test14"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="test-item">
                                        <p>Имеется ли у Вас хроническое воспалительное заболевание любой локализации (например, хронический фарингит/тонзиллит, хронический холецистит, хронический простатит, либо иные воспалительные процессы)?</p>
                                        <div class="row radio-group">
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q8" id="test15" type="radio" value="y"><label for="test15"><i class="check"></i><span>да</span></label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="radio-box"><input name="Q8" id="test16" type="radio" value="n"><label for="test16"><i class="check"></i><span>нет</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button class="next">дальше</button>
                                </div>
                            </div>
                            <div id="step2" class="step-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <h5>Укажите Ваш возраст</h5>
                                            <input type="text" name="AGE" >
                                        </div>
                                        <div class="col-sm-6">
                                            <h5>Укажите Ваш пол</h5>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="radio-box"><input name="MALE" id="g1" type="radio" value="man"><label for="g1"><i class="check"></i><span>М</span></label></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="radio-box"><input name="MALE" id="g2" type="radio" value="woman"><label for="g2"><i class="check"></i><span>Ж</span></label></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h5>Мы проанализируем ваши ответы и вышлем результаты теста на Вашу почту</h5>
                                </div>
                                <div class="col-sm-6">
                                    <p>Ваше имя:</p>
                                    <input type="text" name="YOUR_NAME">
                                </div>
                                <div class="col-sm-6">
                                    <p>Ваш e-mail:</p>
                                    <input type="text" name="EMAIL">
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="get-result">Получить результаты!</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ask modal -->
<div class="modal fade green-modal" id="ask_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="title">
          Задать вопрос
        </span>






                <form name="SIMPLE_FORM_3" action="/nashi-kontakty/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid_2" value="0ed75028c8c81a9ab3b5436d03242344" /><input type="hidden" name="WEB_FORM_ID" value="3" />

                    <input
                            type="text"
                            name="form_text_10"
                            id="form_text_10"
                            required						placeholder="Ваше имя"
                    />




                    <input
                            type="text"
                            name="form_text_11"
                            id="form_text_11"
                            required						placeholder="Ваш e-mail"
                    />




                    <textarea
                            name="form_textarea_12"
                            id="form_textarea_12"
                            placeholder="Вопрос"
                            required						></textarea>

                    <input type="hidden" name="web_form_submit"  value="yes" />
                    <button  type="submit"
                    >Отправить</button>


                </form>
            </div>
        </div>
    </div>
</div>
<div id="preloader">
    <div class="table">
        <div class="cell">
            <div class="windows8">
                <div class="wBall" id="wBall_1">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_2">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_3">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_4">
                    <div class="wInnerBall">
                    </div>
                </div>
                <div class="wBall" id="wBall_5">
                    <div class="wInnerBall">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Test modal -->


<!-- callback-modal modal -->
<div class="modal fade green-modal" id="callback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="title">
              ОБРАТНЫЙ ЗВОНОК
            </span>






                <form name="SIMPLE_FORM_4" action="/nashi-kontakty/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid_3" value="0ed75028c8c81a9ab3b5436d03242344" /><input type="hidden" name="WEB_FORM_ID" value="4" />

                    <input
                            type="text"
                            name="form_text_13"
                            id="form_text_13"
                            required						placeholder="Ваше имя"
                    />




                    <input
                            type="text"
                            name="form_text_14"
                            id="form_text_14"
                            required						placeholder="Ваш телефон"
                    />


                    <input type="hidden" name="web_form_submit"  value="yes" />
                    <button  type="submit"
                    >ПОЗВОНИТЕ МНЕ</button>


                </form>          </div>
        </div>
    </div>
</div>
<!-- fastbuy-modal modal -->
<div class="modal fade green-modal" id="fastbuy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="title">
              Быстрый заказ
            </span>






                <form name="SIMPLE_FORM_2" action="/nashi-kontakty/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid_4" value="0ed75028c8c81a9ab3b5436d03242344" /><input type="hidden" name="WEB_FORM_ID" value="2" />

                    <input
                            type="hidden"
                            name="form_text_5"
                            value=""
                            id="form_text_5"
                            required
                    />



                    <input
                            type="hidden"
                            name="form_text_6"
                            value=""
                            id="form_text_6"

                    />



                    <input
                            type="text"
                            name="form_text_7"
                            id="form_text_7"
                            required						placeholder="Ваше имя "
                    />




                    <input
                            type="text"
                            name="form_text_8"
                            id="form_text_8"
                            required						placeholder="Телефон"
                    />




                    <input
                            type="email"
                            name="form_email_9"
                            id="form_email_9"
                            placeholder="Email "
                    />


                    <input type="hidden" name="web_form_submit"  value="yes" />
                    <button  type="submit"
                    >Заказать</button>


                </form>          </div>
        </div>
    </div>
</div>
<div class="modal fade green-modal" id="app-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="text_title">
          <i class="we-recall-icon"></i>
          Мы вам перезвоним и подтвердим вашу запись!
        </span>
            </div>
        </div>
    </div>
</div>


<div class="modal fade green-modal" id="faq-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="text_title">
          <i class="we-recall-icon"></i>
         	Спасибо за ваш вопрос! Наши специалисты ответят вам в ближайшее время.
        </span>
            </div>
        </div>
    </div>
</div>



<div class="modal fade green-modal" id="callback-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="text_title">
          <i class="we-recall-icon"></i>
          Мы вам обязательно перезвоним!
        </span>
            </div>
        </div>
    </div>
</div>



<div class="modal fade green-modal" id="order-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="close" data-dismiss="modal" aria-label="Close"></span>
                <span class="text_title">
          <i class="we-recall-icon"></i>
          Мы вам перезвоним и уточним все детали заказа!
        </span>
            </div>
        </div>
    </div>
</div>










<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsyG6qAVDl-dRa7NVU1JJcYwE4LV7Ex_o&language=ru"></script>
<script>
    var map;
    function initialize() {g
        var myLatlng = new google.maps.LatLng(55.800413, 37.788275);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatlng,
            scrollwheel: false,
            draggable: false
        });
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    $('.question button.mobile-only').click(function () {
        $('.mobile-slidedown-form').slideToggle();
    });
</script>





<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id:31548593,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->     <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>








<script src="/old/bower_components/jquery/dist/jquery.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="/old/bitrix/templates/vit/js/bootstrap.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.form-mask.js"></script>

<script src="/old/bitrix/templates/vit/js/2swiper.jquery.min.js"></script>
<script src="/old/bitrix/templates/vit/js/retina.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.stellar.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.magnific-popup.min.js"></script>
<script src="/old/bitrix/templates/vit/js/jquery.dotdotdot.min.js"></script>
<script src="/old/bitrix/templates/vit/js/main.js"></script>

<script src="/old/resources/js/common.js"></script>

<script src="/old/bower_components/angular/angular.min.js"></script>
<script src="/old/bower_components/lodash/dist/lodash.min.js"></script>
<script src="/old/bower_components/hammerjs/hammer.min.js"></script>
<script src="/old/bower_components/AngularHammer/angular.hammer.min.js"></script>
<script src="/old/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/old/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/old/bower_components/angular-material/angular-material.min.js"></script>
<script src="/old/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="/old/dist/bundle.js"></script>
<script src="/old/bitrix/templates/vit/js/auth.js"></script>
<script src="/old/bitrix/templates/vit/uikit/uikit.min.js"></script>




<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31548593 = new Ya.Metrika({
                    id:31548593,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31548593" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter-->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65387669-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>