<section class="b-secondary-menu">
    <div class="container">
        <div class="b-secondary-menu__inner">
            <a class="b-secondary-menu__item @if(Route::currentRouteName() == 'clinics') b-secondary-menu__item--active @endif" href="{{ route('clinics') }}"><span>О клинике</span></a>
            <a class="b-secondary-menu__item @if(Route::currentRouteName() == 'treatment.methods') b-secondary-menu__item--active @endif" href="{{ route('treatment.methods') }}"><span>Методы лечения</span></a>
            {{--<a class="b-secondary-menu__item @if(Route::currentRouteName() == 'clinics') b-secondary-menu__item--active @endif" href="#"><span>Методы лечения</span></a>--}}
            <a class="b-secondary-menu__item @if(Route::currentRouteName() == 'feedbacks') b-secondary-menu__item--active @endif" href="{{ route('feedbacks') }}"><span>Отзывы</span></a>
            <a class="b-secondary-menu__item @if(Route::currentRouteName() == 'services') b-secondary-menu__item--active @endif" href="{{ route('services') }}"><span>Услуги</span></a>
            <a class="b-secondary-menu__item @if(Route::currentRouteName() == 'doctors') b-secondary-menu__item--active @endif" href="{{ route('doctors') }}"><span>Врачи</span></a>
        </div>
    </div>
</section>