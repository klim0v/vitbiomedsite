<div class="b-brief-descs__title">
    <span class="js-brief-descs__title">Почему пробиотики</span>
</div>
<div class="b-brief-descs__text b-standart-text js-brief-descs__content">
    <p>
        Пробиотики помогают в лечении заболеваний разного профиля – от нарушений пищеварения и
        аллергии до коррекции иммунодефицитных состояний и нормализации артериального давления.
        Это связано с тем, что правильный баланс микрофлоры необходим для поддержания здоровья
        многих органов и систем. В организме человека обитает 3-5 кг бактерий. Комплекс этих
        микроорганизмов участвует в формировании нормальной микрофлоры и называется микробиота.
    </p>
    <p>
        В последние годы ученые стали рассматривать микробиоту как дополнительный «внешний»
        орган, который участвует во многих жизненно необходимых процессах. Если состав
        микрофлоры нарушается, это приводит к сбоям в функционировании тех или иных органов
        и систем. Правильно подобранный курс пробиотиков устраняет нарушения баланса и потому
        оказывается полезным при лечении разных заболеваний. Методика, применяемая в центре, не
        исключает назначения традиционных схем. Пациент получает комплексное лечение, в том
        числе стандартную терапию и процедуры, показанные при его заболевании.
    </p>
</div>

<div class="b-brief-descs__title">
    <span class="js-brief-descs__title">Почему так долго</span>
</div>
<div class="b-brief-descs__text b-standart-text js-brief-descs__content">
    <p>
        Полный курс лечения пробиотиками длится 2-3 месяца и включает в себя три этапа. На
        первом этапе полезные бактерии вступают в борьбу с патогенами, населяющими кишечник, –
        происходит <b>очищение организма</b>. На этапе <b>активации</b> начинает накапливаться и
        активизироваться собственная микробиота. На третьем этапе <b>восстанавливается</b>
        нарушенный баланс микрофлоры. Полезные бактерии создают на слизистой оболочке кишечника
        устойчивую биопленку, которая активно функционирует и защищает человека от заболеваний.
        Для того чтобы микрофлора полностью сменилась и начала работать, требуется 2-3 месяца.
    </p>
</div>

<div class="b-brief-descs__title">
    <span class="js-brief-descs__title">Почему так много</span>
</div>
<div class="b-brief-descs__text b-standart-text js-brief-descs__content">
    <p>
        Метод лечения пробиотиками в центре «Витбиомед» построен на правильном сочетании разных
        препаратов, которое обеспечивает <b>непрерывную работу</b> бактерий в организме. Бактерии,
        составляющие микробиоту, весят 3-5 кг, и чтобы действительно существенно повлиять на их
        баланс, требуется принимать много препаратов, содержащих бактерии, причем в разных
        лекарственных формах. В таблетках и капсулах бактерии находятся в состоянии анабиоза.
        Они просыпаются и начинают работать только спустя 5-6 часов после попадания в кишечник.
        В жидких препаратах бактерии живые, их активность начинается уже в полости рта.
    </p>
    <p>
        Бактерии – живые организмы, поэтому предсказать, как они будут взаимодействовать друг с
        другом в составе одного препарата, невозможно. Одни штаммы могут вытеснять другие, и
        состав препарата изменится. Умные пробиотики «Витбиомед» являются монокомпонентными, то
        есть каждый препарат содержит только один вид бактерий. Это гарантирует, что необходимое
        количество микроорганизмов сохранится в каждой дозе препарата на протяжении всего срока
        годности.
    </p>
</div>

<div class="b-brief-descs__title">
    <span class="js-brief-descs__title">Почему не так вкусно, как магазинные йогурты</span>
</div>
<div class="b-brief-descs__text b-standart-text js-brief-descs__content">
    <p>
        Пробиотики, которые производит и применяет «Витбиомед», не содержат консервантов,
        ароматизаторов, подсластителей и красителей. Отсутствие вкусовых добавок повышает
        эффективность лечения, так как химические реакции, которые запускаются при их добавлении
        в продукт, снижают активность бактерий.
    </p>
</div>

<div class="b-brief-descs__title">
    <span class="js-brief-descs__title">Почему так дорого</span>
</div>
<div class="b-brief-descs__text b-standart-text js-brief-descs__content">
    <p>
        Курс лечения пробиотиками достаточно длительный и включает большое количество препаратов,
        из этого и складывается его конечная стоимость. «Витбиомед» предлагает гибкую систему
        лояльности для пациентов и скидки при покупке полного курса.
    </p>
</div>