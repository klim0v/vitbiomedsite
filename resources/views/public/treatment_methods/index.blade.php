@include('public.header3')


<section class="l-big-title l-big-title--xs-lalf-padding-bottom">
    <div class="container">
        <div class="b-big-title b-big-title__center">
            <span>Лечение пробиотиками: метод, проверенный временем</span>
        </div>
        <div class="l-big-title__text l-big-title__text--center hidden-sm-down">
            <span>Медицинский центр «Витбиомед» специализируется на лечении пробиотиками уже 15 лет. Эффективность метода подтверждают практические результаты: при помощи авторской методики, применяемой врачами центра, восстановили здоровье тысячи пациентов.</span>
        </div>
    </div>
</section>

<section class="l-two-column">

    <div class="container">

        <div class="l-two-column__wrap">

            <div class="l-two-column__sidebar">

                <ul class="b-vertical-menu">

                    <li class="b-vertical-menu__item">
                        <a href="{{ route('treatment.methods') }}" class="b-vertical-menu__link">Методы лечения</a>
                    </li>
                    <li class="b-vertical-menu__item">
                        <a href="{{ route('treatment.methods', ['id' => 'lishnij-ves']) }}" class="b-vertical-menu__link">Лишний вес</a>
                    </li>
                    <li class="b-vertical-menu__item">
                        <a href="{{ route('treatment.methods', ['id' => 'problemnaya-kozha']) }}" class="b-vertical-menu__link">Проблемная кожа</a>
                    </li>
                </ul>

            </div>

            <div class="l-two-column__content">

                <div class="b-brief-descs js-brief-descs js-brief-descs--scrolltoview">
                    <div class="b-brief-descs__item js-brief-descs__item">
                                @if($list == null)
                                    @include('public.treatment_methods.lists.main')
                                @endif

                                @if($list == 'lishnij-ves')
                                    @include('public.treatment_methods.lists.excess_weight')
                                @endif

                                @if($list == 'problemnaya-kozha')
                                    @include('public.treatment_methods.lists.problem_skin')
                                @endif
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>

@include('public.footer')