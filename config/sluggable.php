<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 23.10.17
 * Time: 23:39
 */

return [
    'source'          => null,
    'maxLength'       => null,
    'method'          => null,
    'separator'       => '-',
    'unique'          => true,
    'uniqueSuffix'    => null,
    'includeTrashed'  => false,
    'reserved'        => null,
    'onUpdate'        => false,
];