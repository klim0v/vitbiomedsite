<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Doctor
 *
 * @property int $id
 * @property string $full_name
 * @property string $speciality
 * @property string $work_experience
 * @property string $description
 * @property string $experience
 * @property string|null $video
 * @property string|null $cover
 * @property string|null $thumbnail
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Feedback[] $feedback
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DoctorSchedule[] $schedule
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereSpeciality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doctor whereWorkExperience($value)
 * @mixin \Eloquent
 */
class Doctor extends Model
{
    public function schedule()
    {
        return $this->hasMany(\App\DoctorSchedule::class);
    }

    public function feedback()
    {
        return $this->hasMany(\App\Feedback::class);
    }
}
