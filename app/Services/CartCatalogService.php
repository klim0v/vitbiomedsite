<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 29.08.18
 * Time: 10:19
 */

namespace App\Services;

use App\Model\Coupon;
use App\Model\Shop\Cart\DiscountModel;
use App\Model\Shop\Cart\Element;
use App\Model\Shop\Cart\ProductCollection;
use App\Model\Shop\Course\Course;
use App\Model\Shop\Product\Variation;
use Cache;

class CartCatalogService
{
    /**
     * @return ProductCollection
     */
    public function getCartCollection(): ProductCollection
    {
        if (!$productCollection = Cache::get('cart')) {
        
            $productCollection = new ProductCollection();
            /** @var array $cacheProduct */
            $cacheProduct = session('cart.products', []);
            if (!empty($cacheProduct)) {
                $variationIds = array_merge(...array_map(function ($item) {
                    return array_map(function ($key) {
                        return $key;
                    }, array_keys($item));
                }, $cacheProduct));
                $resultVariants = Variation::with('product')
                    ->whereIn('id', $variationIds)
                    ->get();
                foreach ($resultVariants as $resultVariant) {
                    $element = new Element();
                    $element->type = 'product';
                    $element->id = $resultVariant->product->id;
                    $element->variantId = $resultVariant->id;
                    $element->variantValue = $resultVariant->volume;
                    $element->price = $resultVariant->price;
                    $element->title = $resultVariant->product->title;
                    $element->img = $resultVariant->product->thumb;
                    $element->slug = $resultVariant->product->slug;
                    $element->count = $cacheProduct[$resultVariant->product_id][$resultVariant->id];
                    $productCollection->elements[] = $element;
                    $productCollection->total += $element->count;
                    $productCollection->amount += $element->price * $element->count;
                }
            }
        
            /** @var array $cacheCourse */
            $cacheCourse = session('cart.courses', []);
            if (!empty($cacheCourse)) {
                $resultCourses = Course::whereIn('id', array_keys($cacheCourse))
                    ->with('ageCategories')
                    ->get();
                foreach ($resultCourses as $resultCourse) {
                    foreach ($cacheCourse[$resultCourse->id] as $key => $item) {
                        $element = new Element();
                        $element->type = 'course';
                        $element->id = $resultCourse->id;
                        $element->variantId = $key;
                        $element->variantValue = $resultCourse->ageCategories->where('id', $key)->pluck('name')->first();
                        $element->title = $resultCourse->title;
                        $element->price = $resultCourse->price;
                        $element->img = $resultCourse->thumb;
                        $element->slug = $resultCourse->slug;
                        $element->count = $item;
                        $productCollection->elements[] = $element;
                        $productCollection->total += $element->count;
                        $productCollection->amount += $element->price * $element->count;
                    }
                }
            }
        
            $productCollection->discount = new DiscountModel();
            /** @var Coupon $coupon */
            if ($coupon = session('coupon')) {
                $productCollection->discount->code = $coupon->code;
                $productCollection->discount->amount = $coupon->discount($productCollection->amount);
            }
            Cache::add('cart', $productCollection, 0.02);
        }
        return $productCollection;
    }
    
}