<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AppRenameResourcesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:rename {rootPath? : Path to the root directory relative to the \'public\' folder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rootPath = public_path($this->argument('rootPath'));
        $this->recursiveRename($rootPath);
    }

    /**
     * @param string $rootPath
     */
    protected function recursiveRename(string $rootPath): void
    {
        $scannedDirectory = array_diff(scandir($rootPath, SCANDIR_SORT_NONE), ['..', '.']);
        foreach ($scannedDirectory as $item) {
            $filename = $rootPath . DIRECTORY_SEPARATOR . $item;
            if (!is_dir($filename)) {
                $strPos = strpos($filename, '?');
                if ($strPos !== false) {
                    $subStr = substr($filename, 0, $strPos);
                    rename($filename, $subStr);
                }
                continue;
            }

            $this->recursiveRename($filename);
        }
    }
}
