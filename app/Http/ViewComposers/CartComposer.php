<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.10.17
 * Time: 20:00
 */

namespace App\Http\ViewComposers;

use App\Services\CartCatalogService;
use Illuminate\View\View;

class CartComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view): void
    {
    
        $productCollection = (new CartCatalogService)->getCartCollection();
        
        $view->with('productCollection', $productCollection);
    }
    
}