<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.10.17
 * Time: 20:00
 */
namespace App\Http\ViewComposers;

use App\Publication;
use App\PublicationType;
use Illuminate\View\View;

class RubricMenuComposer
{

    public function compose(View $view)
    {
        $rubrics = PublicationType::where('is_hidden', '!=', 1)->get();
        $favorite = Publication::whereHas('publicationType', function ($query) {
            $query->where('is_hidden', '!=', 1);
        })
            ->where('is_hidden', '!=', 1)
            ->where('is_favorite', 1)
            ->with('publicationType')
            ->get();

        $view->with('rubrics', $rubrics);
        $view->with('favorite', $favorite);
    }

}