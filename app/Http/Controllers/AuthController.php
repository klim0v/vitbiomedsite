<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{
    public function displayForm()
    {
        return view('admin.login');
    }

    public function entryAttempt(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email|exists:users,email',
            'password'  => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('admin.dashboard');
        }
    
        return redirect()->back()->withErrors((new MessageBag())->add('auth', 'Wrong login or password'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login.form');
    }
}
