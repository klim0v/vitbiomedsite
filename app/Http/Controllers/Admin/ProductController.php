<?php

namespace App\Http\Controllers\Admin;

use App\Model\Shop\Product\Product;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function strips()
    {
        $products = Product::paginate();

        return view('admin.products.strips')
            ->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int|null $productId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $productId = null)
    {
        $product = $productId ? Product::find($productId) : new Product();

        return view('admin.products.edit')
            ->with('product', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

    }
}
