<?php

namespace App\Http\Controllers\Admin;

use App\Doctor;
use App\DoctorSchedule;
use App\Http\Controllers\System\ImageController;
use App\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class DoctorController extends Controller
{
    public function catalog()
    {
        $doctors = Doctor::paginate(10);

        return view('admin.doctors')
            ->with('doctors', $doctors);
    }

    public function edit($id = null)
    {
        $doctor = null;

        if (!empty($id)) {
            $doctor = Doctor::with('schedule')->findOrFail($id);
        }

        return view('admin.doctor')
            ->with('doctor', $doctor);
    }

    public function save(Request $request, $id = null)
    {
        $this->validate($request, [
            'full_name'       => 'required|min:2',
            'speciality'      => 'required|min:2',
            'work_experience' => 'required|min:2',
            'description'     => 'required|min:2',
            'experience'      => 'required|min:2',
            // TODO сделать правило для url
            'cover'           => 'image|nullable',
            'thumbnail'       => 'image|nullable'
        ]);

        if (!empty($id)) {
            $doctor = Doctor::findOrFail($id);
        } else {
            $doctor = new Doctor();
        }

        $doctor->full_name       = $request->input('full_name');
        $doctor->speciality      = $request->input('speciality');
        $doctor->work_experience = $request->input('work_experience');
        $doctor->description     = $request->input('description');
        $doctor->experience      = $request->input('experience');
        $doctor->video           = $request->input('video');

        $image_manipulator = new ImageController();
        // TODO сделать его через игъекцию

        if ($request->hasFile('cover') && $request->file('cover')->isValid()) {
            $doctor->cover = $image_manipulator->resizeAndSave($request->file('cover'), 670, 450);
        }

        if ($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()) {
            $doctor->thumbnail = $image_manipulator->resizeAndSave($request->file('thumbnail'), 178, 178);
        }

        //TODO сделать самовыпил старых ихображений при сохранении

        $doctor->save();

        if (!empty($id)) {
            DoctorSchedule::where('doctor_id', $id)->delete();
        }

        $schedule_insert = [];

        foreach ($request->input('working_time', []) as $time) {
            if (!empty($time)) {
                $schedule_insert[] = [
                    'doctor_id' => $doctor->id,
                    'row'       => $time
                ];
            }
        }

        if (!empty($schedule_insert)) {
            DoctorSchedule::insert($schedule_insert);
        }

        return redirect()->route('admin.doctors');
    }

    public function delete($id)
    {
        $doctor = Doctor::findOrFail($id);
        // TODO анигилировать изображения оставшиеся от врача
        $doctor->delete();
        return redirect()->back();
    }

    public function deleteCoverImage($id)
    {
        $doctor = Doctor::findOrFail($id);
        Storage::disk('public')->delete($doctor->cover);
        $doctor->cover = null;
        $doctor->save();
        return response()->json([
            'status' => 'success'
        ]);
    }

    public function deleteThumbImage($id)
    {
        $doctor = Doctor::findOrFail($id);
        Storage::disk('public')->delete($doctor->thumbnail);
        $doctor->thumbnail = null;
        $doctor->save();
        return response()->json([
            'status' => 'success'
        ]);
    }
}
