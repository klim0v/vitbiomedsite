<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PublicationType;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PublicationTypeController extends Controller
{

    public function strips()
    {
        $publicationTypes = PublicationType::paginate(10);

        return view('admin.publications.types.strips')
            ->with('publicationTypes', $publicationTypes);
    }

    /**
     * @param null $id
     * @return $this
     */
    public function edit($id = null)
    {
        $publicationType = null;

        if (!empty($id)) {
            $publicationType = PublicationType::findOrFail($id);
        }

        return view('admin.publications.types.edit')
            ->with('publicationType', $publicationType);
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, $id = null)
    {
        if (!empty($id)) {
            $publicationType = PublicationType::findOrFail($id);
        } else {
            $publicationType = new PublicationType();
            $publicationType->is_hidden = 0;
        }



        $this->validate($request, [
            'title'             => 'required',
            'heading'           => 'nullable',
            'slug'              => 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|nullable|unique:publication_types,slug,'.$publicationType->id,
            'text'              => 'nullable',
        ]);

        $publicationType->title = $request->input('title');
        $publicationType->slug =  $request->input('slug');
        $publicationType->text = $request->input('text');

        foreach ($request->only(['heading', 'meta_title', 'meta_description']) as $key => $value) {
            $publicationType->{$key} = $value ?? $publicationType->title;
        }

        $publicationType->save();

        return redirect()->route('admin.publication.types');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        PublicationType::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function hide($id)
    {
        $publicationType = PublicationType::findOrFail($id);
        $publicationType->is_hidden = !($publicationType->is_hidden);
        $publicationType->save();
        return redirect()->back();
    }
}
