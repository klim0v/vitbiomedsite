<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function catalog()
    {
        $orders = Order::with('doctor','service')->orderBy('id', 'desc')->paginate(25);

        return view('admin.dashboard')
            ->with('orders', $orders);
    }

    public function processe($id)
    {
        $order = Order::where('id', $id)->first();

        if (!empty($order)) {

            $order->processed = true;
            $order->save();

            $result = [
                'status' => 'success'
            ];
        } else {
            $result = [
                'status' => 'failure'
            ];
        }

        return response()->json($result);
    }

}
