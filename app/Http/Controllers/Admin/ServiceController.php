<?php

namespace App\Http\Controllers\Admin;

use App\Service;
use App\ServiceCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function catalog()
    {
        $services = Service::paginate(10);

        return view('admin.services')
            ->with('services', $services);
    }

    public function edit(Request $request, $id = null)
    {
        $service = null;

        if (!empty($id)) {
            $service = Service::findOrFail($id);
        }

        $categories = ServiceCategory::get();

        return view('admin.service')
            ->with('service', $service)
            ->with('categories', $categories);
    }

    public function save(Request $request, $id = null)
    {
        $this->validate($request, [
            'name'        => 'required|min:2',
            'category_id' => 'required_without:new_category|exists:service_categories,id|nullable',
            'new_category'=> 'required_without:category_id|unique:service_categories,name|nullable',
            'price'       => 'required|numeric'
        ]);

        if (!empty($id)) {
            $service = Service::findOrFail($id);
        } else {
            $service = new Service();
        }


        $service->name = $request->input('name');
        $service->category_id = $request->input('category_id');
        $service->price = $request->input('price');

        if (!empty($request->input('new_category'))) {
            $new_category = new ServiceCategory();
            $new_category->name = $request->input('new_category');
            $new_category->save();
            $service->category_id = $new_category->id;
        }

        $service->save();

        return redirect()->route('admin.services');
    }

    public function delete($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
        return redirect()->back();
    }
}
