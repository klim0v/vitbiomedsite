<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\System\ImageController;
use App\Publication;
use App\PublicationType;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PublicationController extends Controller
{
    public function strips(Request $request)
    {
        $publications = Publication::with('publicationType')
            ->when($request->has('publication_type_id'), function ($query) use ($request) {
                $query->where('publication_type_id', $request->get('publication_type_id'));
            })
            ->paginate(10);
        return view('admin.publications.strips')
            ->with('publications', $publications);
    }

    /**
     * @param null $id
     * @return $this
     */
    public function edit($id = null)
    {
        $publication = null;

        if (!empty($id)) {
            $publication = Publication::findOrFail($id);
        }
        $publicationTypes = PublicationType::all();
        return view('admin.publications.edit')
            ->with('publication', $publication)
            ->with('publicationTypes', $publicationTypes);
    }

    /**
     * @param Request $request
     * @param ImageController $image_manipulator
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, ImageController $image_manipulator, $id = null)
    {
        if (!empty($id)) {
            $publication = Publication::findOrFail($id);
        } else {
            $publication = new Publication();
            $publication->is_hidden = 0;
        }

        $this->validate($request, [
            'title'                 => 'required',
            'heading'               => 'nullable',
            'slug'                  => 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|nullable|unique:publications,slug,'.$publication->id,
            'annotation'            => 'nullable',
            'text'                  => 'nullable',
            'is_favorite'           => 'boolean|nullable',
            'image'                 => 'image|nullable',
            'publication_type_id'   => 'required|exists:publication_types,id',
        ]);

        $publication->title = $request->input('title');
        $publication->slug =  $request->input('slug');
        $publication->annotation = $request->input('annotation');
        $publication->text = $request->input('text');
        $publication->publication_type_id = $request->input('publication_type_id');
        $publication->is_favorite = $request->input('is_favorite') ? 1 : 0;
        $publication->is_hidden = $request->input('is_hidden') ? 1 : 0;

        foreach ($request->only(['heading', 'meta_title', 'meta_description']) as $key => $value) {
            $publication->{$key} = $value ?? $publication->title;
        }

        $publication->fill($request->only($publication->getFillable()));

        if ($request->hasFile('og_image') && $request->file('og_image')->isValid()) {
            $publication->og_image = $image_manipulator->resizeAndSave($request->file('og_image'), 400, 300);
        }

        if( ($request->hasFile('image') && $request->file('image')->isValid()) || $request->input('delete_image') ) {
            if ($publication->image && $publication->thumbnail) {
                Storage::disk('public')->delete($publication->image);
                Storage::disk('public')->delete($publication->thumbnail);
            }

            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $publication->image = $image_manipulator->resizeAndSave($request->file('image'), 870, 450);
                $publication->thumbnail = $image_manipulator->resizeAndSave($request->file('image'), 370, 247);
            } else {
                $publication->image = null;
                $publication->thumbnail = null;
            }
        }

        $publication->image_author = $request->input('image_author');

        $publication->save();

        return redirect()->route('admin.publications');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $publication = Publication::findOrFail($id);
        Storage::disk('public')->delete($publication->image);
        Storage::disk('public')->delete($publication->thumbnail);
        $publication->delete();
        return redirect()->back();
    }

    public function hide($id)
    {
        $publication = Publication::findOrFail($id);
        $publication->is_hidden = !($publication->is_hidden);
        $publication->save();
        return redirect()->back();
    }
}
