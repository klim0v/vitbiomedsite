<?php

namespace App\Http\Controllers\Admin;

use App\Doctor;
use App\Feedback;
use App\Http\Controllers\System\ImageController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function catalog()
    {
        $feedbacks = Feedback::paginate(10);

        return view('admin.feedbacks')
            ->with('feedbacks', $feedbacks);
    }

    public function edit($id = null)
    {
        $feedback = null;

        if (!empty($id)) {
            $feedback = Feedback::findOrFail($id);
        }

        $doctors = Doctor::get();

        return view('admin.feedback')
            ->with('feedback', $feedback)
            ->with('doctors', $doctors);
    }

    public function save(Request $request, $id = null)
    {
        $this->validate($request, [
            'full_name'   => 'required|min:2',
            'doctor_id'   => 'exists:doctors,id|nullable',
            'rating'      => 'required|numeric|min:1|max:5',
            'thumbnail'   => 'image|nullable',
            'text'        => 'required|min:2',
            'facebook'    => 'url|nullable',
            'is_hidden'   => 'boolean|nullable'
        ]);

        if (!empty($id)) {
            $feedback = Feedback::findOrFail($id);
        } else {
            $feedback = new Feedback();
        }

        $feedback->is_hidden = $request->input('is_hidden') ? 1 : 0;
        $feedback->full_name = $request->input('full_name');
        $feedback->doctor_id = $request->input('doctor_id');
        $feedback->rating    = $request->input('rating');
        $feedback->text      = $request->input('text');
        $feedback->facebook  = $request->input('facebook');

        $image_manipulator = new ImageController();
        // TODO сделать его через игъекцию

        if ($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()) {
            $feedback->thumbnail = $image_manipulator->resizeAndSave($request->file('thumbnail'), 178, 178);
        }

        $feedback->save();

        return redirect()->route('admin.feedbacks');
    }

    public function delete($id)
    {
        $feedback = Feedback::findOrFail($id);
        // TODO анигилировать изображения оставшиеся от врача
        $feedback->delete();
        return redirect()->back();
    }
}
