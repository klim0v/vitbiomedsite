<?php

namespace App\Http\Controllers\Content\Shop;

use App\Model\Shop\Course\Category;
use App\Model\Shop\Course\Course;
use App\Model\Slider;
use Illuminate\Routing\Controller;

class CourseController extends Controller
{

    public function strips()
    {
        $courseCategories = Category::with('courses.productVariations.product.productComponents')->get();
        $sliders = Slider::all();

        return view('public.shop.courses.strips')
            ->with('courseCategories', $courseCategories)
            ->with('sliders', $sliders);
    }

    public function index(string $slug)
    {
        $course = Course::whereSlug($slug)->with('productVariations.product')->firstOrFail();

        return view('public.shop.courses.index')->with('course', $course);
    }

}
