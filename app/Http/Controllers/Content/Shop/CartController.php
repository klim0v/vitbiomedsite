<?php

namespace App\Http\Controllers\Content\Shop;

use App\Http\Requests\Shop\Cart\CourseChangePost;
use App\Http\Requests\Shop\Cart\ProductChangePost;
use App\Model\Shop\Cart\Cart;
use App\Services\CartCatalogService;
use Illuminate\Routing\Controller;

class CartController extends Controller
{
    
    /**
     * @var Cart
     */
    private $cart;
    
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('public.shop.cart.page');
    }
    
    /**
     * @param ProductChangePost $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function changeProduct(ProductChangePost $request)
    {
        $this->cart->changeProduct($request->id, $request->variant, $request->count);
    
        if ($request->array) {
            return [
                'view' => view('public.shop.cart.small.list')->render(),
                'data' => (new CartCatalogService)->getCartCollection(),
            ];
        }
        return view('public.shop.cart.small.list');
    }
    
    /**
     * @param CourseChangePost $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function changeCourse(CourseChangePost $request)
    {
        $this->cart->changeCourse($request->id, $request->variant, $request->count);
    
        return view('public.shop.cart.small.list');
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clear()
    {
        session(['cart' => []]);
    
        return view('public.shop.cart.small.list');
    }
    
    
    
}
