<?php

namespace App\Http\Controllers\Content\Shop;

use App\Model\Shop\Product\Category;
use App\Model\Shop\Product\Product;
use App\Model\Slider;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{

    public function strips()
    {
        $productCategories = Category::with('products.productVariations', 'products.productComponents')->get();
        $sliders = Slider::all();

        return view('public.shop.products.strips')
            ->with('productCategories', $productCategories)
            ->with('sliders', $sliders);
    }

    public function index(string $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();

        return view('public.shop.products.index')->with('product', $product);
    }

}
