<?php

namespace App\Http\Controllers\Content\Shop;

use App\Http\Requests\Shop\CouponStorePost;
use App\Model\Coupon;
use App\Services\CartCatalogService;
use Illuminate\Routing\Controller;

class CouponController extends Controller
{
    /**
     * @param CouponStorePost $request
     * @return string
     */
    public function store(CouponStorePost $request): array
    {
        $coupon = Coupon::findByCode($request->coupon_code);
        
        session(['coupon' => $coupon]);
    
        return [
            'result'  => true,
            'message' => 'Купон принят!',
            'data'    => (new CartCatalogService)->getCartCollection(),
        ];
    }
    
    /**
     *
     */
    public function destroy(): array
    {
        session()->forget('coupon');
    
        return [
            'result'  => true,
            'message' => 'Купон принят!',
            'data'    => (new CartCatalogService)->getCartCollection(),
        ];
    }
}
