<?php

namespace App\Http\Controllers\Content\Shop;

use Auth;
use Illuminate\Routing\Controller;

class BillController extends Controller
{
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delivery()
    {
        $cart = session('cart', []);
        if (!array_filter($cart)) {
            return back();
        }
        $user = Auth::user();
        return view('public.shop.bill.delivery')
            ->with('user', $user);
    }
    
    public function save()
    {
    
    }
    
}
