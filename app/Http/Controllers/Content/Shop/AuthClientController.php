<?php

namespace App\Http\Controllers\Content\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthClientController extends Controller
{
    
    public function entryAttempt(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);
        
        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt($credentials)) {
            return [
                'result' => true,
                'user'   => Auth::id(),
            ];
        }
        
        return [
            'result' => false,
            'user'   => null,
        ];
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect()->route('product.strip');
    }
}
