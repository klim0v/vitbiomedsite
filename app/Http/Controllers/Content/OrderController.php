<?php

namespace App\Http\Controllers\Content;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function make(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'full_name' => 'required|min:2',
            'phone'     => 'required|regex:/^\+7[0-9]{10}$/',
            'email'     => 'required|email'
        ]);

        if ($validation->fails()) {
            $result = [
                'status' => 'failure',
                'errors' => $validation->errors()
            ];
        } else {
            $order = new Order();
            $order->full_name  = $request->input('full_name');
            $order->phone      = $request->input('phone');
            $order->email      = $request->input('email');
            $order->doctor_id  = $request->input('doctor_id');
            $order->service_id = $request->input('service_id');
            $order->processed  = 0;
            $order->save();

            $result = [
                'status' => 'success'
            ];
        }

        return response()->json($result);
    }
}
