<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Publication;
use App\PublicationType;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function stripsAll(Request $request)
    {
        $ids = null;
        $query = null;
         if ($request->has('search')){
             $conn = \DB::connection('sphinx');
             //У сфинкса проблемы с двонфми кавычками, их надо дважды экранировать
             $query = addslashes(strip_tags($request->get('search')));
             //Делаем выборку с установкой разного веса полям
             $ids = collect($conn->select(
                 \DB::raw("SELECT id FROM blog_index WHERE MATCH (:query) OPTION  max_matches=50,field_weights=(title=10, heading=10, text=5, annotation=5)"),
                 array('query' => $query))
             )->pluck('id');
//             dd($ids);
             $query = stripslashes($query);
         }

        $publications = Publication::notHidden()
            ->whereHas('publicationType', function ($query) {
                $query->notHidden();
            })
            ->when(isset($ids), function ($query) use ($request, $ids) {
                $query->whereIn('id', $ids)
                    ->orderBy(\DB::raw('FIELD(`id`, '.implode(',', $ids->toArray()).')'));
            })
            ->with('publicationType')
            ->orderBy('created_at', 'DESC')
            ->paginate(4);

        if ($request->isXmlHttpRequest()) {
            return view('public.blog.publication_strips')
                ->with('publications', $publications)
                ->with('query', $query);
        }

        return view('public.blog.index')
            ->with('publications', $publications)
            ->with('query', $query);
    }

    public function strips(Request $request, $type_slug)
    {
        $rubric = PublicationType::notHidden()
            ->where('slug', $type_slug)
            ->firstOrFail();

        $publications = $rubric->publications()
            ->notHidden()
            ->with('publicationType')
            ->orderBy('created_at', 'DESC')
            ->paginate(4);

        if ($request->isXmlHttpRequest()) {
            return view('public.blog.publication_strips')->with('publications', $publications);
        }

        return view('public.blog.rubric_index')->with('rubric', $rubric)->with('publications', $publications);
    }

    public function index($type_slug, $publication_slug)
    {
        $tmp = Publication::notHidden()
            ->whereHas('publicationType', function ($query) use ($type_slug) {
                $query->where('slug', $type_slug)
                    ->notHidden();
            });

        $publication = $tmp
            ->where('slug', $publication_slug)
            ->with('publicationType')
            ->firstOrFail();

        $otherPublications_id = $tmp
            ->where('id', '!=', $publication->id)
            ->limit(3)
            ->inRandomOrder()
            ->pluck('id');


        $otherPublications = Publication::with('publicationType')->findMany($otherPublications_id);
        return view('public.blog.publication_index')
            ->with('publication', $publication)
            ->with('otherPublications', $otherPublications);
    }
}
