<?php

namespace App\Http\Controllers\Content;

use App\ServiceCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function catalog()
    {
        $categories = ServiceCategory::has('service')->get();

        return view('public.service')
            ->with('categories', $categories);
    }
}
