<?php

namespace App\Http\Controllers\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TreatmentMethodController extends Controller
{
    public function item($id = null)
    {
        if (!empty($id) && !in_array($id, ['lishnij-ves', 'problemnaya-kozha'])) {
            abort(404);
        }

        return view('public.treatment_methods.index')->with('list', $id);
    }
}
