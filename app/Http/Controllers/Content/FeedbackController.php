<?php

namespace App\Http\Controllers\Content;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    public function catalog()
    {
        $feedbacks = Feedback::get();

        return view('public.feedbacks')
            ->with('feedbacks', $feedbacks);
    }

    public function save(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'last_name'   => 'required|min:2',
            'first_name'  => 'required|min:2',
            'rating'      => 'required|numeric|min:1|max:5',
            'text'        => 'required|min:2',
            'facebook'    => 'url|nullable'
        ]);

        if (!$validation->fails()) {
            $feedback = new Feedback;

            $feedback->full_name = implode(' ', array_filter([
                $request->input('last_name'),
                $request->input('first_name')
            ]));
            $feedback->rating    = $request->input('rating');
            $feedback->text      = $request->input('text');
            $feedback->facebook  = $request->input('facebook');
            $feedback->is_hidden = 1;

            $result = [
                'result' => 'success'
            ];
        } else {
            $result = [
                'status' => 'failure',
                'errors' => $validation->errors()
            ];
        }

        return response()->json($result);
    }
}
