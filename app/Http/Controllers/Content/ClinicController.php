<?php

namespace App\Http\Controllers\Content;

use App\Doctor;
use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClinicController extends Controller
{
    public function item()
    {
        $doctors   = Doctor::with('feedback')->limit(2)->inRandomOrder()->get();
        // TODO переделать выборку отзывов для врачей в обычный подсчет количества отзывов
        $feedbacks = Feedback::limit(3)->inRandomOrder()->get();

        return view('public.clinic')
            ->with('doctors', $doctors)
            ->with('feedbacks', $feedbacks);
    }
}
