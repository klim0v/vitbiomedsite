<?php

namespace App\Http\Controllers\Content;

use App\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorController extends Controller
{
    public function catalog ()
    {
        $doctors = Doctor::with('schedule')->get();

        return view('public.doctors')
            ->with('doctors', $doctors);
    }

    public function item($id)
    {
        $doctor  = Doctor::with('schedule', 'feedback')->findOrFail($id);
        $doctors = Doctor::with('feedback')->where('id', '!=', $id)->limit(2)->inRandomOrder()->get();
        // TODO переделать выборку отзывов для врачей в обычный подсчет количества отзывов

        $embed_id = null;

        if (!empty($doctor->video)) {
            parse_str(parse_url($doctor->video)['query'], $youtube_parametrs);

            if (!empty($youtube_parametrs['v'])) {
                $embed_id = $youtube_parametrs['v'];
            }
        }

        return view('public.doctor')
            ->with('doctor', $doctor)
            ->with('doctors', $doctors)
            ->with('embed_id', $embed_id);
    }
}
