<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    public function resizeAndSave($image, $width, $height, $aspectRatio = true, $upsize = true)
    {

        $filename   = uniqid() . '.' . $image->getClientOriginalExtension();
        $path       = storage_path('app/public/' . $filename);
        Image::configure(['driver' => 'imagick']);
        Image::make($image->getRealPath())
            ->resize($width, $height, function($constraint) use($aspectRatio, $upsize) {
                if ($aspectRatio)
                    $constraint->aspectRatio();
                if ($upsize)
                    $constraint->upsize();
            })
            ->save($path, 100);

        return $filename;
    }
}
