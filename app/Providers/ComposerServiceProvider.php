<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'public.blog.rubric_menu', \App\Http\ViewComposers\RubricMenuComposer::class
        );
        View::composer(
            ['public.shop.cart.small.list', 'public.shop.cart.page'], \App\Http\ViewComposers\CartComposer::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
