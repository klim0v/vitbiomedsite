<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ServiceCategory
 *
 * @property int $id
 * @property string $name
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ServiceCategory extends Model
{
    public $timestamps = false;

    public function service()
    {
        return $this->hasMany(\App\Service::class, 'category_id');
    }
}
