<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 01.06.18
 * Time: 23:06
 */

namespace App\Model\Shop\Cart;

use Illuminate\Session\Store;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Cart
{

    /**
     * @var Store
     */
    private $store;

    /**
     * Cart constructor.
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * @param int $itemId
     * @param int $variantId
     * @param int $count
     */
    public function changeProduct(int $itemId, int $variantId, int $count): void
    {
        $old = $this->store->get('cart.products', []);
        $products = $this->changeItem($itemId, $variantId, $count, $old);
        $this->store->put('cart.products', $products);
    }

    /**
     * @param int $itemId
     * @param int $variantId
     * @param int $count
     */
    public function changeCourse(int $itemId, int $variantId, int $count = 1): void
    {
        $old = $this->store->get('cart.courses', []);
        $courses = $this->changeItem($itemId, $variantId, $count, $old);
        $this->store->put('cart.courses', $courses);
    }

    /**
     * @param int $itemId
     * @param int $variantId
     * @param int $count
     * @param array $items
     * @return mixed
     */
    protected function changeItem(int $itemId, int $variantId, int $count, array $items)
    {
        $oldCount = $this->getItemCount($itemId, $variantId, $items);

        if ($oldCount === null ) {
            if ($count > 0) {
                $items[$itemId] = [];
            } else {
                throw new BadRequestHttpException('нельзя удалить товар который не лежит в корзине');
            }
        }

        $newCount = $oldCount + $count;
        if ($newCount > 0) {
            $items[$itemId][$variantId] = $newCount;
        } elseif ($newCount <= 0) {
            unset($items[$itemId][$variantId]);
            if (empty($items[$itemId])) {
                unset($items[$itemId]);
            }
        }

        return $items;
    }

    /**
     * @param int $itemId
     * @param int $variantId
     * @param $items
     * @return int|null
     */
    protected function getItemCount(int $itemId, int $variantId, $items): ?int
    {
        if ($items && \is_array($items) && array_key_exists($itemId, $items)) {
            $variants = $items[$itemId];
            if ($variants && \is_array($variants) && array_key_exists($variantId, $variants)) {
                return $variants[$variantId];
            }
            return 0;
        }
        return null;
    }
}
