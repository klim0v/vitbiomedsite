<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 20.08.18
 * Time: 21:45
 */

namespace App\Model\Shop\Cart;

class DiscountModel
{
    /**
     * @var string
     */
    public $code;
    
    /**
     * @var float
     */
    public $amount;
}