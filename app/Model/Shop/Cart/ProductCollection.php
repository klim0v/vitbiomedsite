<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 13.08.18
 * Time: 19:00
 */

namespace App\Model\Shop\Cart;


class ProductCollection
{
    /**
     * @var Element[]
     */
    public $elements = [];
    
    /**
     * @var int
     */
    public $total = 0;
    
    /**
     * @var int
     */
    public $amount = 0;
    
    /**
     * @var DiscountModel
     */
    public $discount;
}