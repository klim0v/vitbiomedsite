<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 13.08.18
 * Time: 19:00
 */

namespace App\Model\Shop\Cart;


class Element
{
    /**
     * @var int
     */
    public $id;
    
    /**
     * @var int
     */
    public $variantId;
    
    /**
     * @var string
     */
    public $variantValue;
    
    /**
     * @var string
     */
    public $title;
    
    /**
     * @var string
     */
    public $img;
    
    /**
     * @var integer
     */
    public $price;
    
    /**
     * @var int
     */
    public $count = 0;
    
    /**
     * @var string
     */
    public $type;
    
    /**
     * @var string
     */
    public $slug;
    
}