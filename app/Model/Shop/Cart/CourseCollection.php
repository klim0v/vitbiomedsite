<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 13.08.18
 * Time: 19:00
 */

namespace App\Model\Shop\Cart;


class CourseCollection
{
    /**
     * @var array
     */
    public $elements = [];
    
    /**
     * @var Total
     */
    public $total;
}