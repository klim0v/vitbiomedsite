<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 13.08.18
 * Time: 19:00
 */

namespace App\Model\Shop\Cart;


class Total
{
    /**
     * @var int
     */
    public $amount = 0;
    
    /**
     * @var int
     */
    public $count = 0;
}