<?php

namespace App\Model\Shop;

use App\Model\Shop\Course\Course;
use App\Model\Shop\Product\Variation;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Model\Shop\Bill
 * @property int $id
 * @property int $user_id
 * @property int $total
 * @property string $promo_code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill wherePromoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereUserId($value)
 * @mixin \Eloquent
 * @property int $type_delivery
 * @property int $type_payment
 * @property string|null $comment
 * @property string|null $note
 * @property string|null $cancel_reason
 * @property string $customer_phone
 * @property string $customer_name
 * @property string $delivery_cost
 * @property string|null $delivery_index
 * @property string|null $delivery_address
 * @property string|null $delivery_date
 * @property string|null $delivery_time
 * @property int $current_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereCancelReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereCurrentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereDeliveryCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereDeliveryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereDeliveryIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereTypeDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Bill whereTypePayment($value)
 */
class Bill extends Model
{
    protected $fillable = [
        'user_id',
        'total',
        'promo_code',
    ];

    protected $withCount = [
        'productVariables',
        'courses',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsToMany
     */
    public function productVariables(): BelongsToMany
    {
        return $this->morphedByMany(Variation::class, 'item')
            ->withPivot('variant')
            ->withPivot('count');
    }

    /**
     * @return BelongsToMany
     */
    public function courses(): BelongsToMany
    {
        return $this->morphedByMany(Course::class, 'item')
            ->withPivot('variant')
            ->withPivot('count');
    }
}
