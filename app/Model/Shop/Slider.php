<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Slider
 *
 * @property int $id
 * @property string $title
 * @property string|null $subtitle
 * @property string $description
 * @property string $image
 * @property int $type_id
 * @property string $link
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Slider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slider extends Model
{
    public const TYPE_COURSE = 1;

    protected $attributes = [
        'type_id' => Slider::TYPE_COURSE
    ];

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'image',
        'link',
    ];


}
