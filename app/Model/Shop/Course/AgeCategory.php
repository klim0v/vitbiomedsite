<?php

namespace App\Model\Shop\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Shop\Course\Category
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Course\Course[] $courses
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Category whereName($value)
 * @mixin \Eloquent
 * @property string $key
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\AgeCategory whereKey($value)
 */
class AgeCategory extends Model
{
    public $timestamps = false;
    
    protected $table = 'age_categories';

    protected $fillable = [
        'key',
        'name',
    ];

    /**
     * @return HasMany
     */
    public function courses(): HasMany
    {
        return $this->hasMany(Course::class);
    }
}
