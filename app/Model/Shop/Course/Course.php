<?php

namespace App\Model\Shop\Course;

use App\Model\Shop\Bill;
use App\Model\Shop\Product\Variation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Model\Shop\Course\Course
 * @property int $id
 * @property string $name
 * @property string|null $keyword
 * @property string $subtitle
 * @property string|null $image
 * @property string|null $thumb
 * @property string $annotation
 * @property string $description
 * @property string $composition
 * @property string|null $for_whom
 * @property string|null $features
 * @property string|null $why_probiotics
 * @property string $duration
 * @property int $price
 * @property int $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Bill[] $bills
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Course\AgeCategory[] $ageCategories
 * @property-read \App\Model\Shop\Course\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Product\Variation[] $productVariations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereFeatures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereForWhom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereWhyProbiotics($value)
 * @mixin \Eloquent
 * @property string $title
 * @property string $heading
 * @property string $meta_title
 * @property string $meta_description
 * @property string $slug
 * @property string|null $og_title
 * @property string|null $og_type
 * @property string|null $og_description
 * @property string|null $og_image
 * @property string|null $og_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereOgType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereOgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Course\Course whereTitle($value)
 * @property-read \Illuminate\Support\Collection $components
 */
class Course extends Model
{
    protected $fillable = [
        'name',
        'keyword',
        'subtitle',
        'annotation',
        'description',
        'composition',
        'for_whom',
        'features',
        'why_probiotics',
        'image',
        'thumb',
        'duration',
        'price',
        'category_id'
    ];

    /**
     * @return BelongsToMany
     */
    public function productVariations(): BelongsToMany
    {
        return $this->belongsToMany(Variation::class, 'course_product_variation', 'course_id', 'product_variation_id')
            ->withPivot('count');
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsToMany
     */
    public function ageCategories(): BelongsToMany
    {
        return $this->belongsToMany(AgeCategory::class);
    }

    public function bills()
    {
        return $this->morphToMany(Bill::class, 'item');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getComponentsAttribute(): \Illuminate\Support\Collection
    {
        $collection = collect();
        foreach ($this->productVariations as $productVariation) {
            foreach ($productVariation->product->productComponents as $productComponent) {
                $collection->prepend($productComponent);
            }
        }
        return $collection->unique('id');
    }
}
