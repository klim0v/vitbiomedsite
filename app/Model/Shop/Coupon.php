<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Coupon
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property int $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Coupon whereValue($value)
 * @mixin \Eloquent
 */
class Coupon extends Model
{
    protected $fillable = [
        'code',
        'type',
        'value',
    ];
    
    /**
     * @param $code
     * @return Coupon|\Illuminate\Database\Eloquent\Builder|Model|null|object
     */
    public static function findByCode($code)
    {
        return self::whereCode($code)->first();
    }
    
    /**
     * @param $total
     * @return float|int
     */
    public function discount($total)
    {
        if ($this->type === 'fixed') {
            return $this->value;
        }

        if ($this->type === 'percent') {
            return round(($this->value / 100) * $total);
        }

        return 0;
    }
}
