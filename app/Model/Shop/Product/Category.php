<?php

namespace App\Model\Shop\Product;

use App\Model\Shop\Bill;
use App\Model\Shop\Course\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Shop\Product\Category
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Product\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Category whereName($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    protected $table = 'product_categories';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'product_category_id');
    }

}
