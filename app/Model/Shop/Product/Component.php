<?php

namespace App\Model\Shop\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Model\Shop\Product\Component
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Product\Product[] $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Component whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Component whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Component whereName($value)
 * @mixin \Eloquent
 */
class Component extends Model
{
    public $timestamps = false;

    protected $table = 'product_components';

    protected $fillable = [
        'name',
        'icon',
    ];

    /**
     * @return BelongsToMany
     */
    public function product(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
