<?php

namespace App\Model\Shop\Product;

use App\Model\Shop\Bill;
use App\Model\Shop\Course\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Model\Shop\Product\Variation
 *
 * @property int $id
 * @property int $volume
 * @property int $price
 * @property int $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Bill[] $bills
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Course\Course[] $courses
 * @property-read \App\Model\Shop\Product\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Variation whereVolume($value)
 * @mixin \Eloquent
 */
class Variation extends Model
{
    protected $table = 'product_variations';

    protected $fillable = [
        'volume',
        'price',
        'product_id',
    ];

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function bills()
    {
        return $this->morphToMany(Bill::class, 'item');
    }

    /**
     * @return BelongsToMany
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'course_product_variation');
    }
}
