<?php

namespace App\Model\Shop\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Shop\Product\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $keyword
 * @property string $subtitle
 * @property string|null $image
 * @property string|null $thumb
 * @property string $annotation
 * @property string $description
 * @property string $composition
 * @property string|null $effects_on_the_body
 * @property string|null $indications_for_use
 * @property string|null $how_to_use
 * @property string|null $storage_conditions
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Product\Component[] $productComponents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Shop\Product\Variation[] $productVariations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereEffectsOnTheBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereHowToUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereIndicationsForUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereStorageConditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $product_category_id
 * @property string $title
 * @property string $heading
 * @property string $meta_title
 * @property string $meta_description
 * @property string $slug
 * @property string|null $og_title
 * @property string|null $og_type
 * @property string|null $og_description
 * @property string|null $og_image
 * @property string|null $og_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereOgType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereOgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereProductCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereTitle($value)
 * @property string $word
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Shop\Product\Product whereWord($value)
 */
class Product extends Model
{
    protected $fillable = [
        'name',
        'title',
        'subtitle',
        'keyword',
        'annotation',
        'description',
        'composition',
        'effects_on_the_body',
        'indications_for_use',
        'how_to_use',
        'storage_conditions',
        'image',
        'thumb',
        'product_category_id'
    ];

    /**
     * @return HasMany
     */
    public function productVariations(): HasMany
    {
        return $this->hasMany(Variation::class)->orderBy('volume');
    }

    /**
     * @return BelongsToMany
     */
    public function productComponents(): BelongsToMany
    {
        return $this->belongsToMany(Component::class, 'product_product_component',  'product_id','product_component_id');
    }
}
