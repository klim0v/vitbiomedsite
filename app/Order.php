<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order
 *
 * @property int $id
 * @property string $full_name
 * @property string $phone
 * @property string $email
 * @property int|null $doctor_id
 * @property int|null $service_id
 * @property int|null $processed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Doctor $doctor
 * @property-read \App\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    public function doctor()
    {
        return $this->hasOne(\App\Doctor::class, 'id', 'doctor_id');
    }

    public function service()
    {
        return $this->hasOne(\App\Service::class, 'id', 'service_id');
    }
}
