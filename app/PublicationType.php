<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicationType
 *
 * @property int $id
 * @property string $title
 * @property string $heading
 * @property string $meta_title
 * @property string $meta_description
 * @property string|null $text
 * @property string $slug
 * @property int $is_hidden
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Publication[] $publications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType notHidden()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublicationType extends Model
{
    use Sluggable;

    public function scopeNotHidden($query)
    {
        return $query->where('is_hidden', '!=', 1);
    }

    public function publications()
    {
        return $this->hasMany(\App\Publication::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
