<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DoctorSchedule
 *
 * @property int $doctor_id
 * @property string $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DoctorSchedule whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DoctorSchedule whereRow($value)
 * @mixin \Eloquent
 */
class DoctorSchedule extends Model
{
    public $timestamps = false;
}
