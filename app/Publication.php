<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Publication
 *
 * @property int $id
 * @property string $title
 * @property string $heading
 * @property string $meta_title
 * @property string $meta_description
 * @property string $slug
 * @property string|null $annotation
 * @property string|null $text
 * @property string|null $image
 * @property string|null $thumbnail
 * @property string|null $image_author
 * @property int $is_favorite
 * @property int $is_hidden
 * @property int $publication_type_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $og_title
 * @property string|null $og_type
 * @property string|null $og_description
 * @property string|null $og_image
 * @property string|null $og_url
 * @property-read mixed $created_at_formatted
 * @property-read \App\PublicationType $publicationType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication notHidden()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereImageAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereIsFavorite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereOgType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereOgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePublicationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Publication extends Model
{

    protected $fillable = [
        'og_title',
        'og_type',
        'og_description',
        'og_url',
    ];

    use Sluggable;

    public function getCreatedAtFormattedAttribute()
    {
        $monthes = array(
            1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
            5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
            9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
        );

        return "{$this->created_at->format('j')} {$monthes[$this->created_at->format('n')]} {$this->created_at->format('Y')}";
    }

    public function scopeNotHidden($query)
    {
        return $query->where('is_hidden', '!=', 1);
    }

    public function publicationType()
    {
        return $this->belongsTo(\App\PublicationType::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
