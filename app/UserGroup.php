<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserGroup
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGroup whereName($value)
 * @mixin \Eloquent
 */
class UserGroup extends Model
{
    public $timestamps = false;
}
