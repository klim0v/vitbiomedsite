<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Feedback
 *
 * @property int $id
 * @property string $full_name
 * @property int|null $doctor_id
 * @property int $rating
 * @property string|null $facebook
 * @property string|null $thumbnail
 * @property string $text
 * @property int $is_hidden
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Feedback extends Model
{
    protected $table = 'feedbacks';
}
