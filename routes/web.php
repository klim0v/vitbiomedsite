<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain(config('my-config-file.app_store_url'))->group(function () {
    Route::get('/courses', 'Content\Shop\CourseController@strips')->name('course.strip');
    Route::get('/courses/{slug}', 'Content\Shop\CourseController@index')->name('course.index');
    
    Route::get('/', 'Content\Shop\ProductController@strips')->name('product.strip');
    Route::get('/{slug}', 'Content\Shop\ProductController@index')->name('product.index');
    
    Route::get('/personal', 'Content\Shop\PersonalController@index')->name('personal.index');
    Route::get('/personal/cart', 'Content\Shop\CartController@index')->name('cart.index');
    Route::any('/personal/delivery', 'Content\Shop\BillController@delivery')->name('delivery.index');
    Route::post('/personal/bill/save', 'Content\Shop\BillController@save')->name('bill.save');
    
    Route::middleware('ajax')->group(function () {
        Route::post('/cart/change/product', 'Content\Shop\CartController@changeProduct')->name('cart.change.product');
        Route::post('/cart/change/course', 'Content\Shop\CartController@changeCourse')->name('cart.change.course');
        Route::post('/cart/clear', 'Content\Shop\CartController@clear')->name('cart.clear');
        Route::post('/personal/cart/promo', 'Content\Shop\CouponController@store')->name('coupon.store');
        Route::post('login', 'Content\Shop\AuthClientController@entryAttempt')->name('login');
        Route::post('logout', 'Content\Shop\AuthClientController@logout')->name('logout');
    });

});

Route::prefix('admin')->group(function () {
    Route::get('login',  'AuthController@displayForm')->name('admin.login.form');
    Route::post('login', 'AuthController@entryAttempt')->name('admin.login');
    Route::get('logout', 'AuthController@logout')->name('admin.logout');

    Route::middleware('administratorsAndModeratorsOnly')->group(function () {
        Route:: get('dashboard',              'Admin\OrderController@catalog')->name('admin.dashboard');
        Route::post('order/processe/{id}',    'Admin\OrderController@processe')->name('admin.orders.processe');
        Route:: get('doctors',                'Admin\DoctorController@catalog')->name('admin.doctors');
        Route:: get('doctors/edit/{id?}',     'Admin\DoctorController@edit')->name('admin.doctors.edit');
        Route::post('doctors/save/{id?}',     'Admin\DoctorController@save')->name('admin.doctors.save');
        Route:: get('doctors/delete/{id?}',   'Admin\DoctorController@delete')->name('admin.doctors.delete');
        Route:: get('doctors/cover/delete/{id?}',   'Admin\DoctorController@deleteCoverImage')->name('admin.doctors.cover.delete');
        Route:: get('doctors/thumb/delete/{id?}',   'Admin\DoctorController@deleteThumbImage')->name('admin.doctors.thumb.delete');
        Route:: get('services',               'Admin\ServiceController@catalog')->name('admin.services');
        Route:: get('services/edit/{id?}',    'Admin\ServiceController@edit')->name('admin.services.edit');
        Route::post('services/save/{id?}',    'Admin\ServiceController@save')->name('admin.services.save');
        Route:: get('services/delete/{id?}',  'Admin\ServiceController@delete')->name('admin.services.delete');
        Route:: get('feedbacks',              'Admin\FeedbackController@catalog')->name('admin.feedbacks');
        Route:: get('feedbacks/edit/{id?}',   'Admin\FeedbackController@edit')->name('admin.feedbacks.edit');
        Route::post('feedbacks/save/{id?}',   'Admin\FeedbackController@save')->name('admin.feedbacks.save');
        Route:: get('feedbacks/delete/{id?}', 'Admin\FeedbackController@delete')->name('admin.feedbacks.delete');

        Route::group(['prefix' => 'products', 'as' => 'product.', 'namespace' => 'Admin'], function () {
            Route::get('/', 'ProductController@strips')->name('strips');
            Route::get('/edit/{id?}', 'ProductController@edit')->name('edit');
            Route::post('/save', 'ProductController@save')->name('save');
            Route::post('/delete', 'ProductController@delete')->name('delete');
        });
        Route::prefix('publications')->group(function () {
            Route:: get('', 'Admin\PublicationController@strips')->name('admin.publications');
            Route:: get('/edit/{id?}', 'Admin\PublicationController@edit')->name('admin.publications.edit');
            Route::post('/save/{id?}', 'Admin\PublicationController@save')->name('admin.publications.save');
            Route:: get('/delete/{id?}', 'Admin\PublicationController@delete')->name('admin.publications.delete');
            Route:: get('/hide/{id?}', 'Admin\PublicationController@hide')->name('admin.publications.hide');
        });
        Route::prefix('publication-types')->group(function () {
            Route:: get('', 'Admin\PublicationTypeController@strips')->name('admin.publication.types');
            Route:: get('/edit/{id?}', 'Admin\PublicationTypeController@edit')->name('admin.publication.types.edit');
            Route::post('/save/{id?}', 'Admin\PublicationTypeController@save')->name('admin.publication.types.save');
            Route:: get('/delete/{id?}', 'Admin\PublicationTypeController@delete')->name('admin.publication.types.delete');
            Route:: get('/hide/{id?}', 'Admin\PublicationTypeController@hide')->name('admin.publication.types.hide');
        });
    });
});


// Публичные роуты

Route::get('/', function () {
    return view('public.index');
})->name('index');

Route::get('/clinics',     'Content\ClinicController@item')->name('clinics');
Route::get('/services',    'Content\ServiceController@catalog')->name('services');
Route::get('/feedbacks',   'Content\FeedbackController@catalog')->name('feedbacks');
Route::get('/doctors',     'Content\DoctorController@catalog')->name('doctors');
Route::get('/doctor/{id}', 'Content\DoctorController@item')->name('doctor');
Route::get('/treatment-methods/{id?}', 'Content\TreatmentMethodController@item')->name('treatment.methods');
Route::get('/contacts', function () {
    return view('public.contacts.index');
})->name('contacts');

Route::post('/order/make', 'Content\OrderController@make')->name('order.make');

Route::prefix('blog')->group(function () {
    Route::get('{type}/{publication}', 'Content\BlogController@index')->name('publication.index');
    Route::get('{type}', 'Content\BlogController@strips')->name('publication.type.index');
    Route::get('', 'Content\BlogController@stripsAll')->name('publication.all.index');

});
