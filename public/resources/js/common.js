// CUSTOM JS

$(document).ready(function(){

	"use strict";

    $('.li-no-click').on('click', function(){
        return false;
    })

    // Toogle Select
    $('.js-toggler-l').on('click', function(){

        $(this).addClass('toggler--is-active');
        $(this).parent().find('.js-switcher').removeClass('active').prop("checked", false);
        $(this).parent().find('.js-toggler-r').removeClass('toggler--is-active');
    });

    $('.js-toggler-r').on('click', function(){
        $(this).addClass('toggler--is-active');
        $(this).parent().find('.js-switcher').addClass('active').prop("checked", true);
        $(this).parent().find('.js-toggler-l').removeClass('toggler--is-active');
    });

    $(".js-switcher").on('click', function(){
        var obj_l = $(this).parent().parent().find('.js-toggler-l'),
            obj_r = $(this).parent().parent().find('.js-toggler-r');

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            obj_l.addClass('toggler--is-active');
            obj_r.removeClass('toggler--is-active');
			obj_l.find('li').trigger('click');
        } 
        else {
            $(this).addClass('active');
            obj_r.addClass('toggler--is-active');
            obj_l.removeClass('toggler--is-active');
			obj_r.find('li').trigger('click');
        }
    });


    $('.product__checker').on('click', function(e){
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parent().find('.product__wrap-1').addClass('active');
            $(this).parent().find('.product__wrap-2').removeClass('active');
        }
        else {
            $(this).addClass('active');
            $(this).parent().find('.product__wrap-1').removeClass('active');
            $(this).parent().find('.product__wrap-2').addClass('active');
        }
    });


	// TABS
    $('.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this).addClass('active').siblings().removeClass('active')
      .closest('.js-tabs').find('.tab__content').hide().eq($(this).index()).fadeIn(600);
      if($(this).parent().hasClass('maps-js')) {
        initMap();
      }
    });

	// RADIO BUTTON
    $('.radio').on('click', function(){
        if ( !$(this).hasClass("active") ){
        	var radio_name = $(this).find('.radio__input').attr('name');
            	$('.radio__input[name="'+ radio_name +'"]').parent().removeClass('active').find('.radio__input').prop("checked", false);
           		$(this).addClass('active').find('.radio__input').prop("checked", true);
            return false;
        }
    });

    // RADIO BUTTON - LABEL
    $('.label-radio').on('click', function(){
		if ( !$(this).find('.radio').hasClass("active") ){
        	var radio_name = '.radio__input[name="' + $(this).find('.radio__input').attr('name') + '"]';
            	$(radio_name).parent().removeClass('active').find('.radio__input').prop("checked", false);
           		$(this).find('.radio').addClass('active').find('.radio__input').prop("checked", true);
        }
    });

    // ACCORDION
    $('.accordion__link').on('click', function(){
        var accordionItem = $(this).parent();
        if(accordionItem.hasClass('active')) {
            accordionItem.removeClass('active').find('.accordion__content').slideUp(300);
        }
        else {
            // accordionItem.siblings().removeClass('active').find('.accordion__content').slideUp(300);
            accordionItem.addClass('active').find('.accordion__content').slideDown(300);
        }
    });

    // AMOUNT
    $('.amount__button--minus').click(function () {
        var input = $(this).next(),
            count = parseInt(input.val()) - 1;
        count = count < 1 ? 1 : count;
        input.attr('value', count);
    });
    $('.amount__button--plus').click(function () {
        var input = $(this).prev(),
            count = parseInt(input.val()) + 1
        input.attr('value', count);
    });

    $('.amount__button').on('mousedown', function(e){
        e.preventDefault();
    });

    $('.amount__button').on('selectstart', function(e){
        e.preventDefault();
    });


});


/* ==================================================================================
    CUSTOM SELECT
   ================================================================================== */
;(function() {
    $('.select').find('.select__title').on('click', function(e){
        var title  = $(this), 
            list   = title.next('.select__list');

        if( title.hasClass('active') ){
            title.removeClass('active');
            list.removeClass('active');
            return false;
        } 
        else {
            $('.select__title').removeClass('active');
            $('.select__list').removeClass('active');
            title.addClass('active');
            list.addClass('active');
            return false;
        }
    });

    $(document).click(function(e){
        $('.select__title').removeClass('active');
        $('.select__list').removeClass('active');
    });

    $('.select__item').on('click', function(e){
        var item  = $(this), 
            title = item.parent().prev('.select__title'),
            input = item.parent().parent().find('.select__input'),
            select = title.parent();         

        if(select.attr('data-select') != undefined){
            title.removeClass().addClass('select__title').addClass(item.data('class'));
        }; 

        $(this).addClass('active').siblings().removeClass('active');

        title.text(item.text());
        input.val(item.text());
        item.parent().removeClass('active');  
    });

    $('.select__title').on('mousedown', function(e){
        e.preventDefault();
    });

    $('.select__title').on('selectstart', function(e){
        e.preventDefault();
    });
})();





/* ==================================================================================
    MODAL WINDOW
   ================================================================================== */
$(document).ready(function(){
    var trigger = $('.md-trigger'),
        close = $('.md-close'),
        overlay = $('.md-overlay'),
        modal = $('.md-modal');

        trigger.on('click', function(e){
            e.preventDefault();
            var modal = $('[id="'+ $(this).data('modal') +'"]'),
                modalContent = modal.find('.md-content'),
                overlay = modal.next('.md-overlay');

            modalContent.scrollTop(0);
            modal.addClass('md-show');
            overlay.addClass('md-show');
        });

        overlay.on('click', function(){
            removeClass();
        });

        close.on('click', function(){
            removeClass();
        });

        function removeClass(){
            modal.removeClass('md-show');
            overlay.removeClass('md-show');
        }
});



