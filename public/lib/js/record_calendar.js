/* js-gen-form-mini-record */
	
	js_gen_form_mini_rec = {
		
			//url для формы записи
		url_record: "/php/entry_for_service.php",
		
			//дополнительные параметры для формы записи
		record_added_params: {},
		
			//url для отмены записи
		url_cancel_record: "/php/cancel_record.php",
		
			//дополнительные параметры для отмены записи
		cancel_record_added_params: {},
		
		send_form : function(el){
			
			form_data = {				
				id_doctor 	 : el.find(".b-mini-record__input-id-doctor").val(),
				id_service	 : el.find(".b-mini-record__input-check-service").val(),
				date_day		 : el.find(".b-mini-record__input-check-day").val(),
				date_month	 : parseInt(el.find(".b-mini-record__input-check-month").val())+1,
				date_year		 : el.find(".b-mini-record__input-check-year").val(),
				date_hour		 : el.find(".b-mini-record__input-check-hour").val(),
				date_minutes : el.find(".b-mini-record__input-check-minutes").val(),
				user_name    : el.find(".b-mini-record__input-name").val(),
				user_family  : el.find(".b-mini-record__input-family").val(),
				user_phone   : el.find(".b-mini-record__input-phone").val(),
				user_email   : el.find(".b-mini-record__input-email").val()
			}
			
			$.extend(form_data, this.record_added_params);
			
			$.ajax({
				url : this.url_record,
				type : "POST",
				dataType: 'json',
				data: form_data,
				context:el,
				success : function(resp){
					if(resp.error == false){
						js_gen_form_mini_rec.set_cancel_id_record(resp.record_id,this);
						js_gen_form_mini_rec.open_step(4,this);
					}else{
						alert(resp.error_message);
					}
				}
			});
			
		},
		
		set_cancel_id_record : function(record_id,el){
			
			el.find(".b-mini-record__id_record").val(record_id);
			
		},
		
		date : {
			d : (new Date()),
			monthes : [
				['Январь',	'Января'],	['Февраль',	'Февраля'],	['Март',			'Марта'],
				['Апрель',	'Апреля'],	['Май',			'Мая'],			['Июнь',			'Июня'],
				['Июль',		'Июля'],		['Август',	'Августа'],	['Сентябрь',	'Сентября'],
				['Октябрь',	'Октября'],	['Ноябрь',	'Ноября'],	['Декабрь',		'Декабря']
			],
			
			daysInMonthes : {
				L : [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
				R : [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
			},
			
			getYear : function(d = this.d){
				return d.getFullYear();
			},
			
			getMonth : function(m = this.d.getMonth()){
				return this.monthes[m][0];
			},
			
			getNMonth : function(){
				return this.d.getMonth();
			},
			
			getMonthDecline : function(m = this.d.getMonth()){
				return this.monthes[m][1];
			},
			
			getDay : function(d,m,y){
				/*  
						Получить день недели
						Пн-Вс : 0-6
				*/
				
				date = new Date(y,m,d);
				n = date.getDay();

				if (n == 0) { n = 7; }
				n--;
				
				return n;
			},
			
			getDaysInMonth(m,y){//получить количество дней в месяце (0-11)
				
				if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0){
					leap = 'L';
				}else{
					leap = 'R';
				}

				return this.daysInMonthes[leap][m];
				
			}
			
		},
		
		get_step0 : function(setting_service){
			
			dropdown_menu = js_gen_rec_doc_mini.gen_services_dropdown(setting_service);
			
			html = '';
			
			html += '<div class="b-mini-calendar__step b-mini-calendar__step--0" attr-step="0">                             ';
			html += '	<div class="b-mini-calendar__service-section">                                           ';
			html += 		'<div class="l-mini-calendar-select-menu js-select-change-display">';
			html +=				dropdown_menu;
			html += 			'<span class="l-mini-calendar-select-menu__value js-select-change-display__display">';
			html += 				setting_service[0].price;
			html += 			'</span>';
			html += 		'</div>';
			html += '		                                                                                      ';
			html += '		<div class="b-mini-calendar__section-btn is-active" attr-open-step="1"></div>                                      ';
			html += '		                                                                                      ';
			html += '	</div>                                                                                  ';
			html += '                                                                                         ';
			html += '</div>                                                                                   ';
			
			return html;
			
		},
		
		get_step1 : function(){
			
			year = this.date.getYear();
			month = this.date.getMonth();
			
			
			html = '';
			html+='<div class="b-mini-calendar__step b-mini-calendar__step--1" attr-step="1">                                                ';
			html+='	<div class="b-mini-calendar__double-section">                                                              ';
			html+='	                                                                                                           ';
			html+='		<div class="b-mini-calendar__section-1">                                                                 ';
			html+='		                                                                                                         ';
			html+='			<div class="b-mini-calendar__section-1-control">                                                       ';
			html+='				<div class="b-mini-calendar__title-step">1. Выберите дату</div>                                      ';
			html+='				<div class="b-mini-calendar__choose-date">                                                           ';
			html+='					<span class="b-mini-calendar__choose-date-year">'+year+'</span>/                                       ';
			html+='					<span class="b-mini-calendar__choose-date-month b-mini-calendar__choose-date-month--left-edge">    ';
			html+='						<div class="b-mini-calendar__choose-date-momth-left"></div>                                      ';
			html+='						<div class="b-mini-calendar__choose-date-momth-value">'+month+'</div>                                 ';
			html+='						<div class="b-mini-calendar__choose-date-momth-right"></div>                                     ';
			html+='					</span>                                                                                            ';
			html+='				</div>                                                                                               ';
			html+='			</div>                                                                                                 ';
			html+='			                                                                                                       ';
			html+='			<div class="b-mini-calendar__section-1-display">                                                       ';
			html+='				<div class="b-mini-calendar__display-month">                                                         ';
			html+='					                                                                                                   ';
			html+='				</div>                                                                                               ';
			html+='			</div>                                                                                                 ';
			html+='			                                                                                                       ';
			html+='		</div>                                                                                                   ';
			html+='		                                                                                                         ';
			html+='		<div class="b-mini-calendar__section-2">                                                                 ';
			html+='			<div class="b-mini-calendar__choose-day">                                                              ';
			html+='				<div class="b-mini-calendar__choose-day-week">                                                       ';
			html+='					<span>ПН</span><span>ВТ</span><span>СР</span>                                                      ';
			html+='					<span>ЧТ</span><span>ПТ</span><span>СБ</span>                                                      ';
			html+='					<span>ВС</span>                                                                                    ';
			html+='				</div>                                                                                               ';
			html+='				<div class="b-mini-calendar__choose-day-days">                                                       ';
			html+='				</div>                                                                                               ';
			html+='			</div>                                                                                                 ';
			html+='		</div>                                                                                                   ';
			html+='		                                                                                                         ';
			html+='		<div class="b-mini-calendar__section-btn" attr-open-step="2"></div>                                                         ';
			html+='		                                                                                                         ';
			html+='	</div>                                                                                                     ';
			html+='</div>                                                                                                      ';
			
			return html;
		},
		
		get_step2 : function(){
			
			html = '';

			html += '<div class="b-mini-calendar__step b-mini-calendar__step--2"  attr-step="2">  ';
			html += '	<div class="b-mini-calendar__double-section">                          ';
			html += '	                                                                       ';
			html += '		<div class="b-mini-calendar__section-1">                             ';
			html += '		                                                                     ';
			html += '			<div class="b-mini-calendar__section-1-control">                   ';
			html += '				<div class="b-mini-calendar__title-step">2. Выберите время</div> ';
			html += '				<div class="b-mini-calendar__change-date">                       ';
			html += '					                                                               ';
			html += '				</div>                                                           ';
			html += '			</div>                                                             ';
			html += '			                                                                   ';
			html += '			<div class="b-mini-calendar__section-1-display">                   ';
			html += '				<div class="b-mini-calendar__display-time">                      ';
			html += '					                                                          ';
			html += '				</div>                                                           ';
			html += '			</div>                                                             ';
			html += '			                                                                   ';
			html += '		</div>                                                               ';
			html += '		                                                                     ';
			html += '		<div class="b-mini-calendar__section-2">                             ';
			html += '			<div class="b-mini-calendar__choose-time">                         ';
			
			hour = 10;
			minutes = '00';
			
			while(hour <= 20){
				n = Math.round(Math.random());
				c = '';
				if(n == 1){
					c = ' class="is-free-space"';
				}
				
				html+= '<span'+c+'><span>'+hour+"."+minutes+'</span></span>';
				
				if(minutes == '00'){
					minutes = '30';
					if(hour == 20){
						hour++;
					}
				}else if(hour < 20){
					minutes = '00';
					hour++;
				}
			}
		
			html += '			</div>                                                             ';
			html += '		</div>                                                               ';
			html += '		                                                                     ';
			html += '		<div class="b-mini-calendar__section-btn" attr-open-step="3"></div>                     ';
			html += '		                                                                     ';
			html += '	</div>                                                                 ';
			html += '                                                                        ';
			html += '</div>                                                                  ';
			
			return html;
			
		},
		
		get_step3 : function(){
			
			html = '';
			
			html += '<div class="b-mini-calendar__step b-mini-calendar__step--3" attr-step="3">                             ';
			html += '	<div class="b-mini-calendar__double-section">                                           ';
			html += '	                                                                                        ';
			html += '		<div class="b-mini-calendar__section-1">                                              ';
			html += '		                                                                                      ';
			html += '			<div class="b-mini-calendar__section-1-control">                                    ';
			html += '				<div class="b-mini-calendar__title-step">3. Контакты</div>                        ';
			html += '				<div class="b-mini-calendar__change-full-date">                                   ';
			html += '					<span class="b-mini-calendar__change-full-date-month"></span>                   ';
			html += '					в                                                                               ';
			html += '					<span class="b-mini-calendar__change-full-date-time"></span>                    ';
			html += '				</div>                                                                            ';
			html += '			</div>                                                                              ';
			html += '			                                                                                    ';
			html += '			<div class="b-mini-calendar__section-1-display"></div>                              ';
			html += '			                                                                                    ';
			html += '		</div>                                                                                ';
			html += '		                                                                                      ';
			html += '		<div class="b-mini-calendar__section-2">                                              ';
			html += '			<div class="b-mini-calendar__form">                                                 ';
			html += '				<label class="b-mini-calendar__form-field b-mini-calendar__form-field--required"> ';
			html += '					<input placeholder="Имя" class="b-mini-record__input-name">                                                       ';
			html += '				</label>                                                                          ';
			html += '				<label class="b-mini-calendar__form-field b-mini-calendar__form-field--required"> ';
			html += '					<input placeholder="Фамилия" class="b-mini-record__input-family">                                                   ';
			html += '				</label>                                                                          ';
			html += '				<label class="b-mini-calendar__form-field b-mini-calendar__form-field--required"> ';
			html += '					<input placeholder="Телефон" class="b-mini-record__input-phone">                                                   ';
			html += '				</label>                                                                          ';
			html += '				<label class="b-mini-calendar__form-field">                                       ';
			html += '					<input placeholder="Email" class="b-mini-record__input-email">                                                     ';
			html += '				</label>                                                                          ';
			html += '			</div>                                                                              ';
			html += '		</div>                                                                                ';
			html += '		                                                                                      ';
			html += '		<div class="b-mini-calendar__section-btn" attr-open-step="4"></div>                                      ';
			html += '		                                                                                      ';
			html += '	</div>                                                                                  ';
			html += '                                                                                         ';
			html += '</div>                                                                                   ';
			
			return html;
			
		},
		
		get_step4 : function(){
			
			html = '';
			
			html += '<div class="b-mini-calendar__step b-mini-calendar__step--4" attr-step="4">                                           ';
			html += '	<div class="b-mini-calendar__success-step">                                                           ';
			html += '	                                                                                                      ';
			html += '		<div class="b-mini-calendar__success-icon-ok"></div>                                                ';
			html += '		<div class="b-mini-calendar__success-info-text-before">                                             ';
			html += '			Спасибо, вы успешно записаны.                                                                     ';
			html += '		</div>                                                                                              ';
			html += '		<div class="b-mini-calendar__success-info-text">                                                    ';
			html += '			Мы ждем вас <span class="b-mini-calendar__success-info-text-before-value">24 марта в 16.00</span>.';
			html += '		</div>                                                                                              ';
			html += '		<div class="b-mini-calendar__success-info-text-after">                                              ';
			html += '			Пожалуйста, приходите заранее. :)                                                                 ';
			html += '		</div>                                                                                              ';
			html += '		                                                                                                    ';
			html += '		<span class="b-mini-calendar__success-cancel">Отменить запись</span>                                ';
			html += '		                                                                                                    ';
			html += '	</div>                                                                                                ';
			html += '                                                                                                       ';
			html += '</div>                                                                                                 ';
			
			return html;
			
		},
		
		get_html_frame : function(settings){
			
			html = '';
			
			html += '<form class="b-mini-record">';
			html += '	<input class="b-mini-record__input-id-doctor" type="hidden" value="'+settings.id_doctor+'">';
			html += '	<input class="b-mini-record__input-n-month" type="hidden" value="'+this.date.getNMonth()+'">';
			html += '	<input class="b-mini-record__input-n-day" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-n-year" type="hidden" value="'+this.date.getYear()+'">';
			html += '	<input class="b-mini-record__input-check-month" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-check-day" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-check-year" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-check-hour" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-check-minutes" type="hidden" value="">';
			html += '	<input class="b-mini-record__input-check-service" type="hidden" value="">';
			html += '	<input class="b-mini-record__id_record" type="hidden" value="">';
			
			html += '<div class="b-mini-record__reception" style="display:none;">' + JSON.stringify(settings.reception) +'</div>';
			
			if(settings.service_type == 'inner_input'){
				html += this.get_step0(settings.setting_services);
			}
			
			html += this.get_step1();
			html += this.get_step2();
			html += this.get_step3();
			html += this.get_step4();
			
			html += '</form>';
			
			return html;
		},
		
		get_obj_day_calendar : function(d,reception,check_date){//получить объект дня календаря с заданной датой d
			
			now_d = d.getDate();
			now_month = d.getMonth() + 1;
			now_year = d.getFullYear();
						
			obj_day = {};
			obj_day.n = d.getDate();
			
			obj_day.date_d = d.getDate();
			obj_day.date_m = d.getMonth();
			obj_day.date_y = d.getFullYear();
			
			//если день активный
			if(
				now_d ==  check_date.d &&
				now_month == check_date.m+1 &&
				now_year == check_date.y
			){
				obj_day.is_active = true;
			}else{
				obj_day.is_active = false;
			}
			
			//если в этот день есть прием
			obj_day.is_free_space = false;
			for(i in reception){
				item = reception[i];
				
				if(
					now_d == item[0] &&
					now_month == item[1] &&
					now_year == item[2]
				){
					obj_day.is_free_space = true;
					break;
				}
				
			}
			
			return obj_day;
			
		},
		
		get_now_date : function(el){//получить текущую дату у календаря el
			
			n_month = el.find(".b-mini-record__input-n-month").val();
			n_month = parseInt(n_month);
			
			n_year = el.find(".b-mini-record__input-n-year").val();
			n_year = parseInt(n_year);
			
			n_day = el.find(".b-mini-record__input-n-day").val();
			n_day = parseInt(n_day);
			
			n_date = {
				d:n_day,
				m:n_month,
				y:n_year
			}
			
			return n_date;
			
		},
		
		set_now_date : function(n_date,el){//получить текущую дату у календаря el
			
			el.find(".b-mini-record__input-n-month").val(n_date.m);
			
			el.find(".b-mini-record__input-n-year").val(n_date.y);

			el.find(".b-mini-record__input-n-day").val(n_date.d);
			
			return true;
			
		},
		
		get_check_date : function(el){ //получить выбранную дату
		
			check_date ={}
			
			check_date.d = el.find(".b-mini-record__input-check-day").val();
			check_date.d = parseInt(check_date.d);
			
			check_date.m = el.find(".b-mini-record__input-check-month").val();
			check_date.m = parseInt(check_date.m);
			
			check_date.y = el.find(".b-mini-record__input-check-year").val();
			check_date.y = parseInt(check_date.y);
			
			return check_date;
			
			
		},
		
		set_check_date : function(check_date,el){

			el.find(".b-mini-record__input-check-month").val(check_date.m);
			el.find(".b-mini-record__input-check-year").val(check_date.y);
			el.find(".b-mini-record__input-check-day").val(check_date.d);
			
			return true;
		},
		
		get_check_time : function(el){//получить выбрнное время
			hour = parseInt(el.find(".b-mini-record__input-check-hour").val());
			minutes = parseInt(el.find(".b-mini-record__input-check-minutes").val());
			return [hour,minutes];
		},
		
		set_check_time : function(hour,minutes,el){//установить выбранное время
			el.find(".b-mini-record__input-check-hour").val(hour);
			el.find(".b-mini-record__input-check-minutes").val(minutes);
			return true;
		},
		
		reset_check_time : function(el){
			
			el.find(".b-mini-record__input-check-hour").val('');
			el.find(".b-mini-record__input-check-minutes").val('');
			
			return true;
		},
		
		reset_all : function(el){
			
			el.find(".b-mini-record__input-check-day").val('');
			el.find(".b-mini-record__input-check-month").val('');
			el.find(".b-mini-record__input-check-year").val('');
			el.find(".b-mini-record__input-family").val('');
			el.find(".b-mini-record__input-phone").val('');
			el.find(".b-mini-record__input-email").val('');
			el.find(".b-mini-record__input-name").val('');
			el.find(".b-mini-record__input-check-hour").val('');
			el.find(".b-mini-record__input-check-minutes").val('');
			
			el.find(".b-mini-calendar__section-btn.is-active").removeClass("is-active");
			el.find(".b-mini-calendar__step--0 .b-mini-calendar__section-btn").addClass("is-active");
			
		},
		
		get_reception : function(el){//получить расписание врача элемента el
			return JSON.parse(el.find(".b-mini-record__reception").html());
		},
		
		update_month : function(el){//обновление дисплея календаря
			
			reception = this.get_reception(el);
			n_date = this.get_now_date(el);//дата  на дисплее
			check_date = this.get_check_date(el);//выбранная дата
			
			d_1_day_month = this.date.getDay(1, n_date.m, n_date.y);
			dayInMonth = this.date.getDaysInMonth(n_date.m, n_date.y);
			d_end_day_month = this.date.getDay(dayInMonth, n_date.m, n_date.y);
			
			ARR_days = []; //массив всех дней
			
			//получение дней из предыдущего месяца
			date_counter = new Date(n_date.y, n_date.m, 1);
			for(j=0;j<d_1_day_month;j++){
				date_counter.setDate( date_counter.getDate() - 1 );
				now_day = this.get_obj_day_calendar(date_counter,reception,check_date);
				ARR_days.push(now_day);
			}
			ARR_days.reverse();
			
			//получение дней текущего месяца
			date_counter = new Date(n_date.y, n_date.m, 1);
			for(j=1;j<=dayInMonth;j++){
				now_day = this.get_obj_day_calendar(date_counter,reception,check_date);
				ARR_days.push(now_day);
				date_counter.setDate( date_counter.getDate() + 1 );
			}
			
			//получение дней следующего месяца
			date_counter = new Date(n_date.y, n_date.m, dayInMonth);
			for(j=0;j<6-d_end_day_month;j++){
				date_counter.setDate( date_counter.getDate() + 1 );
				now_day = this.get_obj_day_calendar(date_counter,reception,check_date);
				ARR_days.push(now_day);
			}
			
			//довести до 6 строк
			if(ARR_days.length < 36){
				for(j=1;j<=7;j++){
					date_counter.setDate( date_counter.getDate() + 1 );
					now_day = this.get_obj_day_calendar(date_counter,reception,check_date);
					ARR_days.push(now_day);
				}
			}
						
			//вывести дни в календарь и обновить обработчик событий
			html = '';
			for(i = 0;i<ARR_days.length;i++){
				item = ARR_days[i];
				
				if(item.is_active){
					is_active = "is-active";
				}else{
					is_active = "";
				}
				
				if(item.is_free_space){
					is_free_space = "is-free-space";
				}else{
					is_free_space = "";
				}
				
				attr_date = '';
				attr_date += ' attr-d="'+item.date_d+'" ';
				attr_date += ' attr-m="'+item.date_m+'" ';
				attr_date += ' attr-y="'+item.date_y+'" ';
				
				html+='<span class=" '+is_active+' '+is_free_space+' "  '+attr_date+'><span>'+item.n+'</span></span>'
			}
			el.find(".b-mini-calendar__choose-day-days").html(html);
			
			
			//обновить месяц и год на дисплее
			el.find(".b-mini-calendar__choose-date-year").text(n_date.y);
			el.find(".b-mini-calendar__choose-date-momth-value").text(this.date.getMonth(n_date.m));
			
			//обновить направление стрелок. 
			if(n_date.m == this.date.getNMonth() && n_date.y == this.date.getYear()){
				el.find(".b-mini-calendar__choose-date-month")
					.addClass("b-mini-calendar__choose-date-month--left-edge");
			}else{
				el.find(".b-mini-calendar__choose-date-month")
					.removeClass("b-mini-calendar__choose-date-month--left-edge");
			}
			
			//Обновить активность месяца
			if(check_date.m == n_date.m && check_date.y == n_date.y){
				el.find(".b-mini-calendar__choose-date-month")
					.addClass("is-active");
			}else{
				el.find(".b-mini-calendar__choose-date-month")
					.removeClass("is-active");
			}
			
			//отображаем выбранное число(если оно есть)
			if(!isNaN(check_date.d)){
				el.find(".b-mini-calendar__display-month").text(check_date.d);
			}else{
				el.find(".b-mini-calendar__display-month").text('');
			}
			
			if(!isNaN(check_date.d) && !isNaN(check_date.m) && !isNaN(check_date.y)){
				this.locker_next_btn(1,true,el);
			}else{
				this.locker_next_btn(1,false,el);
			}
			
			//обновить обработчики событий
			this.set_handlers_month(el)
			
		},
		
		move_calendar : function(direct,el){//движение календаря
			
			n_date = this.get_now_date(el);
			
			if(direct == 'right'){
				n_date.m++;
			}else{
				n_date.m--;
			}
			
			if(n_date.m == 12){
				n_date.m = 0;
				n_date.y++;
			}else if(n_date.m == -1){
				n_date.m = 11;
				n_date.y--;
			}
			
			this.set_now_date(n_date,el);
			this.update_month(el);
			
		},
		
		locker_next_btn : function(step,type,el){//блокировка/разблокировка перехода на следующий шаг
			//true - разблокировать
			if(type==true){
				el.find(".b-mini-calendar__step--"+step+" .b-mini-calendar__section-btn").addClass("is-active");				
			}else{
				el.find(".b-mini-calendar__step--"+step+" .b-mini-calendar__section-btn").removeClass("is-active");				
			}
			
		},
		
		display_time__control_date__update : function(el){//обновить на дисплее "время" выбранную дату
			
			check_date = this.get_check_date(el);
			
			date = check_date.d + " " + this.date.getMonthDecline(check_date.m) + " " + check_date.y;
			
			el.find(".b-mini-calendar__change-date").text(date);
			
			
		},
		
		get_free_times : function(el){//получить свободные времена для выбранной даты
			
			reception = this.get_reception(el);
			check_date = this.get_check_date(el);
			
			free_times = [];
			
			for(i in reception){
				item = reception[i];
				if(
						item[0] == check_date.d &&
						item[1] == check_date.m+1 &&
						item[2] == check_date.y
				){
						free_times.push([item[3],item[4]]);
				}
				
			}
			
			return free_times;
			
		},
		
		find_free_times : function(hour,minutes,free_times){
			
			for(j in free_times){
				item = free_times[j];
				if(item[0] == hour && item[1] == minutes){
					return true;
				}
			}
			
			return false;
			
		},
		
		display_time__update_free_time : function(el){//обновить на дисплее "время" доступное время для записи
			
			free_times = this.get_free_times(el);
			check_time = this.get_check_time(el);
			hour = 10;
			minutes = '00';
			minutes_val = 0;
			
			html = '';
			while(hour <= 20){
				t = hour+"."+minutes;
				
				is_free_space = '';
				is_active = '';
				
				if(this.find_free_times(hour,minutes_val,free_times)){
					is_free_space = ' is-free-space ';
				}
				if(check_time[0] == hour && check_time[1] == minutes_val){
					is_active = ' is-active ';
				}
				
				attr = '';
				attr += ' attr-hour="'+hour+'" ';
				attr += ' attr-minutes="'+minutes_val+'" ';
				
				html+= '<span class="'+is_free_space+' '+is_active+'" '+attr+'><span>'+t+'</span></span>';
				
				if(minutes == '00'){
					minutes = '30';
					minutes_val = 30;
					if(hour == 20){
						hour++;
					}
				}else if(hour < 20){
					minutes = '00';
					minutes_val = 0;
					hour++;
				}
			}
			
			el.find(".b-mini-calendar__choose-time").html(html);
			
		},
		
		display_time__show_check_time : function(el){//показвать выбранное время на дисплее и открыть/закрыть кнопку
			
			t = this.get_check_time(el);
			
			if(!isNaN(t[0]) && !isNaN(t[1])){
				if(t[1] == 0){t[1] = '00';}
				el.find(".b-mini-calendar__display-time").text(t[0] + '.' + t[1]);
				this.locker_next_btn(2,true,el);
				
			}else{
				this.locker_next_btn(2,false,el);
				el.find(".b-mini-calendar__display-time").text('');
			}
			
		},
		
		display_form__update_check_date : function(el){//обновить на 3 шаге выбранную дату и время
			
			d = this.get_check_date(el);
			t = this.get_check_time(el);
			
			if(
					!isNaN(d.d) && !isNaN(d.m) && !isNaN(d.y) && 
					!isNaN(t[0]) && !isNaN(t[1])
			){
				text1 = d.d + ' ' + this.date.getMonthDecline(d.m)
				el.find(".b-mini-calendar__change-full-date-month").text(text1);
				if(t[1] == 0){t[1] = '00';}
				text2 = t[0] + '.' + t[1];
				el.find(".b-mini-calendar__change-full-date-time").text(text2);
				text = text1 + ' в ' + text2;
				el.find(".b-mini-calendar__success-info-text-before-value").text(text);
				
			}
			
		},
		
		update_display_time : function(el){//обновить дисплей времени
			
			this.display_time__control_date__update(el)
			this.display_time__update_free_time(el);
			this.display_time__show_check_time(el);
			this.display_form__update_check_date(el);
			this.set_handlers_time(el);
		},
		
		update_check_date : function(check_date,el){//обновить выбранную дату
			
			this.set_check_date(check_date,el);
			this.update_month(el);
			this.reset_check_time(el);
			this.update_display_time(el);
			
		},
		
		set_handlers_month : function(el){//установить обработчики событий для первого шага
			
			//движение календаря вправо
			el.find(".b-mini-calendar__choose-date-momth-right").off("click");
			el.find(".b-mini-calendar__choose-date-momth-right").on("click",function(){
				el = $(this.closest(".b-mini-record"));
				direct = 'right';
				js_gen_form_mini_rec.move_calendar(direct,el);
			});
			
			//движение календярая влево
			el.find(".b-mini-calendar__choose-date-momth-left").off("click");
			el.find(".b-mini-calendar__choose-date-momth-left").on("click",function(){
				el = $(this.closest(".b-mini-record"));
				direct = 'left';
				js_gen_form_mini_rec.move_calendar(direct,el);
			});
			
			//установка текущего дня
			el.find(".b-mini-calendar__choose-day-days .is-free-space span").off("click");
			el.find(".b-mini-calendar__choose-day-days .is-free-space span").on("click",function(){
				_t = $(this).parent();
				el = $(this.closest(".b-mini-record"));
				check_date = {}
				check_date.d = parseInt(_t.attr("attr-d"));
				check_date.m = parseInt(_t.attr("attr-m"));
				check_date.y = parseInt(_t.attr("attr-y"));
			
				js_gen_form_mini_rec.update_check_date(check_date,el);
				
			});
			
			
		},
		
		set_handlers_time : function(el){//обработчики события для 2 шага
			
			//выбираем дату
			el.find(".b-mini-calendar__choose-time .is-free-space span").off("click");
			el.find(".b-mini-calendar__choose-time .is-free-space span").on("click",function(){
				_t = $(this).parent();
				el = $(this.closest(".b-mini-record"));
				hour = parseInt(_t.attr("attr-hour"));
				minutes = parseInt(_t.attr("attr-minutes"));
				
				js_gen_form_mini_rec.set_check_time(hour,minutes,el);
				js_gen_form_mini_rec.update_display_time(el);
				
			});
			
			
			
		},
		
		open_step : function(n,el){
			
			el.find(".b-mini-calendar__step").removeClass("is-active");
			el.find(".b-mini-calendar__step[attr-step='"+n+"']").addClass("is-active");
			
		},
		
		set_handlers_stepper : function(el){//обработчики событий на перемещение по календаарю
			
			//клик на крайню стрелку справа
			el.find(".b-mini-calendar__section-btn").click(function(){
				if(!$(this).hasClass("is-active")){
					return false;
				}
				n = parseInt($(this).attr("attr-open-step"));
				
				if(n == 4){
					js_gen_form_mini_rec.send_form(el);
					return false;
				}
				
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.open_step(n, el);
			});
			
			//клик на дату на 2 шаге
			el.find(".b-mini-calendar__change-date").click(function(){
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.open_step(1, el);
			});
			
			//клик на дату на 3 шаге
			el.find(".b-mini-calendar__change-full-date-month").click(function(){
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.open_step(1, el);
			});
			
			//клик на время на 3 шаге
			el.find(".b-mini-calendar__change-full-date-time").click(function(){
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.open_step(2, el);
			});
			
		},
		
		form_input_handler : function(el){//отследить заполнение обязательных полей формы
			
			el.find(".b-mini-calendar__form").attr("attr-required","true");
			el.find(".b-mini-calendar__form-field--required input").each(function(){
				if($.trim($(this).val()) == ''){
					$(this).closest(".b-mini-calendar__form").attr("attr-required","false");
				}
			});
			
			form_req = ( el.find(".b-mini-calendar__form").attr("attr-required") == "true" ) ? true : false;
			console.log(form_req);
			if(form_req){
				this.locker_next_btn(3,true,el);
			}else{
				this.locker_next_btn(3,false,el);
			}
			
		},
		
		set_handlers_form : function(el){//установить обработчик событий на форму
			
			el.find(".b-mini-calendar__form-field--required input").keyup(function(){
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.form_input_handler(el);
			});
			
		},
		
		set_handler_cancel_record : function(el){
			
			el.find(".b-mini-calendar__success-cancel").click(function(){
				el = $(this).closest(".b-mini-record");
				js_gen_form_mini_rec.cancel_record(el);
			});
			
		},	
		
		cancel_record : function(el){
						
			form_data = {
				id_record : el.find(".b-mini-record__id_record").val()
			}
			
			$.extend(form_data, this.cancel_record_added_params);
			
			$.ajax({
				url : this.url_cancel_record,
				type : "POST",
				dataType: 'json',
				data: form_data,
				context:el,
				success : function(resp){
					if(resp.error == false){
						
						js_gen_form_mini_rec.set_cancel_id_record('',this);
						js_gen_form_mini_rec.reset_all(this);
						js_gen_form_mini_rec.update_month(this);
						
						if(this.find(".b-mini-calendar__step--0").length > 0){
							js_gen_form_mini_rec.open_step(0,this);
						}else{
							js_gen_form_mini_rec.open_step(1,this);
						}
						
						
					}else{
						alert(resp.error_message);
					}
				}
			});
			
		},
		
		recovery : function(el,settings){
			
			//основные функции:
			/*
				update_check_date - пользователь выбрал дату, обновляем на дисплее и в форме
				update_month - обновить отображение календаря(первый шаг, выбор даты)
				update_display_time - обновить дисплей выбора времени
			*/
			el.find(".b-mini-calendar__step:nth-of-type(2)")
				.addClass("is-active");//открыть первый шаг
			this.update_month(el);//обновить дисплец календарь
			this.set_handlers_month(el);//обработчики событий на календарь
			this.set_handlers_stepper(el);//на перемещение
			this.set_handlers_form(el);//на форму
			this.set_handler_cancel_record(el);
			
			if(settings.service_type == 'inner_input'){
				b_select_menu.init_one_el(el.find(".b-select-menu"));
			}
			
			js_select_change_display.init();
		},
		
		recovery_services : function(el,settings){//оживить услуги
			
			my_serv = el.find(".b-mini-record__input-check-service");
			
			//если услуги определяются во внешем input
			if(settings.service_type == 'outer_input'){
				
				my_serv.val(settings.service_outer_input.val());
				
				settings.service_outer_input.on(
						"change",
						{
								change_input:my_serv
						},
						function(e){
								e.data.change_input.val($(this).val());
						}
				);
			
			//значение услуг фиксировано
			}else if(settings.service_type == 'fixed'){
				
				my_serv.val(settings.service_value);
			
			//если услуги создаются в календаре
			}else if(settings.service_type == 'inner_input'){
				
				my_serv.val(el.find(".b-select-menu__input").val());
				
				el.find(".b-select-menu__input").on(
						"change",
						{
								change_input:my_serv
						},
						function(e){
								e.data.change_input.val($(this).val());
						}
				);
				
			}
			
		},
		
		create : function(el,settings){
			
			html = this.get_html_frame(settings);
			el.html(html);
			this.recovery_services(el,settings)
			this.recovery(el,settings);
			
		}
		
	}
	