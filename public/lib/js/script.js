$(function(){
	jQuery.fx.interval = 13;
	
	/* js-select-change-display */
	
	js_select_change_display = {
		
		init_one_el : function(el){
			el.on("click",function(){
				_t = $(this).closest(".js-select-change-display");
				_val = $(this).attr("el-change-value");
				
				_t.find(".js-select-change-display__display").text(_val);
				
			}),
			el.filter(".js-select-change-display__active").click();
			
		},
		
		init : function(){
			
			this.init_one_el($(".js-select-change-display__handler"));
			
		}
		
	}
	js_select_change_display.init();
	
	/* b-select-menu */
	
	b_select_menu = {
		
		init_one_el : function(el){
			
			//$(".b-select-menu__display").off("click");
			$(".b-select-menu__display").on("click",function(){
				_t = $(this).closest(".b-select-menu");
			
				_t.addClass("is-active");
			});
			
			//$(".b-select-menu__dropdown-list span").off("click");
			$(".b-select-menu__dropdown-list span").on("click",function(){
				
				_t = $(this).closest(".b-select-menu");
				_attr_value = $(this).attr("attr-value");
				_t.removeClass("is-active");
				_t.find(".b-select-menu__input").val(_attr_value);
				
				_t.find(".b-select-menu__input").change();
				return false;
			});
			
			//$(".b-select-menu__input").off("change");
			$(".b-select-menu__input").on("change",function(){
				
				_t = $(this).closest(".b-select-menu");
				_attr_value = $(this).val();
				_attr_value_display = _t.find("[attr-value='"+_attr_value+"']").text();
				$(".b-select-menu__info").text(_attr_value_display);
			});
			
			$(".b-select-menu__input").each(function(){
				$(this).change();
			});
			
		},
		
		init : function(){
			$(".b-select-menu").each(function(){
				b_select_menu.init_one_el($(this));
			});
		}
		
	}
	b_select_menu.init();
	
	/* js-generate-record-doctor-mini */
	//генерация мини доктора - слева выбираются услуги, справа выбирается дата  и зполняется форма
	js_gen_rec_doc_mini = {
		
		gen_services_dropdown : function(services){
			
			html  = '';
			
			html += '<div class="b-select-menu">';
			html += '	<input type="hidden" value="'+services[0].id+'" class="b-select-menu__input">';
			html += '	<div class="b-select-menu__display">';
			html += '		<div class="b-select-menu__info">'+services[0].price+'</div>';
			html += '		<div class="b-select-menu__opener"></div>';
			html += '	</div>';
			html += '	<div class="b-select-menu__dropdown-list">';
			
			firstly = true;
			for(i in services){
					item = services[i];
					
					if(firstly == false){
						added_class = '';
					}else{
						firstly = false;
						added_class = ' js-select-change-display__active';
					}
					
					html += '<div>';
					html += '	<span class="js-select-change-display__handler'+added_class+'"';
					html += '				attr-value="'+item.id+'"';
					html += '				el-change-value="'+item.price+'"';
					html += '	>';
					html += '		'+item.name+'';
					html += '	</span>';
					html += '</div>';
				
			}
			
			html += '	</div>';
			html += '</div>';
			
			return html;
		},
		
		gen_left_part : function(services,services_dropdown){
			
			html  = '';
			html += '<div class="b-doctor-mini__record-left js-select-change-display">';
			html += '	<div class="b-doctor-mini__service-wrap">';
			html += '		<div class="b-doctor-mini__service-title">Услуга</div>';
			html += '		<div class="b-doctor-mini__service-val">';
			html += 			services_dropdown;
			html += '		</div>';
			html += '	</div>';
			html += '	<div class="b-doctor-mini__price">';
			html += '		<span class="b-doctor-mini__price-value js-select-change-display__display">';
			html += '			'+services[0].price+'';
									'</span>';
			html += '	</div>';
			html += '</div>';
			
			return html;
			
		},
		
		gen_right_part : function(){
			
			html = '';
			
			html += '<div class="b-doctor-mini__record-right">';
			html += '	<div class="l-doctor-mini-record">';
			html += '		<form class="b-mini-record">';
			html += '		</form>';
			html += '	</div>';
			html += '</div>';
			
			return html;
			
		},
		
		generate_element : function(t,settings){
			
			services_dropdown = this.gen_services_dropdown(settings.services);
			left_part = this.gen_left_part(settings.services,services_dropdown)
			
			
			right_part = this.gen_right_part();
			
			full_part = left_part + right_part;
			
			t.html(full_part);
			
			b_select_menu.init_one_el(t.find(".b-select-menu"));
			
			record_form_settings = {
				id_doctor : settings.id_doctor,
				reception : settings.reception,
				service_type : 'outer_input',
				service_outer_input : t.find(".b-select-menu__input"),
				service : t.find(".b-select-menu__input")
			}
			
			js_gen_form_mini_rec.create(t.find('.b-mini-record'),record_form_settings);
			
			
		},
		
		init : function(){
			
			$(".js-generate-record-doctor-mini").each(function(){
				
				t = $(this);
				//settings = $.trim($(this).attr("attr-gen-rec-doc-mini-sets"));
				//settings = eval('('+settings+')');
				settings = JSON.parse($(this).attr("attr-gen-rec-doc-mini-sets"));

				js_gen_rec_doc_mini.generate_element(t,settings);
				
			});
			
		}
		
	}
	
	js_gen_rec_doc_mini.init();
	
	/* js-mobile-record-doctor */
	
	//генерация всплывающей записи для телефонов
	js_mobile_record_doctor = {
		
		el_fill_html : function(el){
			
			html = '';
			
			html += '<div class="b-mobile_record__title">';
			html += '	Запись на прием';
			html += '</div>';
			html += '<div class="b-mobile_record__wrap_form">';
			html += '	<form class="b-mini-record b-mini-record--mobile"></form>';
			html += '</div>';
			
			el.html(html)
			
		},
		
		add_form : function(el,settings_elem){
			console.log(settings_elem);
			if("services" in settings_elem){			
				record_form_settings = {
					id_doctor : settings_elem.id_doctor,
					reception : settings_elem.reception,
					service_type : 'inner_input',
					setting_services : settings_elem.services
				}
			}else{
				
				record_form_settings = {
					id_doctor : settings_elem.id_doctor,
					reception : settings_elem.reception,
					service_type : 'fixed',
					service_value : settings_elem.service.id
				}
				
			}
			
			js_gen_form_mini_rec.create(el.find('.b-mini-record'),record_form_settings);
			
		},
		
		generate_element : function(settings_elem){
			//создать бокс
			id = 'el'+Math.round(Math.random()*10000);
			$("body").append('<div id="'+id+'" class="b-mobile_record mfp-hide"></div>')
			el = $("#"+id);
			
			//создать ссылку для открытия
			id2 = 'el'+Math.round(Math.random()*10000);
			$("body").append('<a href="#'+id+'" id="'+id2+'" style="display:none"></a>');
			el_a = $("#"+id2);
			
			//заполнить бокс
			this.el_fill_html(el);
			this.add_form(el,settings_elem);
						
			//открыть через ссылку
			el_a.magnificPopup({
				type: 'inline',
				mainClass: 'mfp-fade l-mini-record',
				removalDelay: 200,
				fixedContentPos:true
			}).magnificPopup("open");
			
		},
		
		init : function(){
			
			if($(window).width() > 790) return;
			
			this.init_click();
			//this.init_each();
			
		},
		
		init_click : function(){
			$(".js-mobile-record-doctor").click(function(){
				//if(i != 1) return;
				settings_elem = JSON.parse($(this).attr("attr-record-settings"));
				js_mobile_record_doctor.generate_element(settings_elem);
			});
		},
		init_each : function(){
			$(".js-mobile-record-doctor").each(function(i){
				if(i != 1) return;
				settings_elem = JSON.parse($(this).attr("attr-record-settings"));
				js_mobile_record_doctor.generate_element(settings_elem);
			});
		}
		
	}
	js_mobile_record_doctor.init();
	
	/* SECTION HEADER MENU */
	
	$(".b-header__wrap-menu__icon").click(function(){
		_t = $(this);
		_p = $(this).parent();
		
		if(_p.hasClass("active")){
			_p.removeClass("active");
		}else{
			_p.addClass("active");
		}
	});
	
	$(".b-hide-menu__list > li > span").click(function(){
		_menu = $(this).closest(".b-hide-menu__list");
		_t = $(this);
		_t_menu = _t.parent().find('ul');
		_submenu_active = _menu.find("ul.active");
		
		if(!_t_menu.hasClass("active")){
			_submenu_active.slideUp().removeClass("active");
			_t_menu.slideDown().addClass("active");			
		}else{
			_submenu_active.slideUp().removeClass("active");
		}
	});
	
	
	/* SECTION MAIN SLIDER */
	
	if($("section").is(".b-main-slider")){
			$(".b-main-slider__wrap-slider").slick({
				vertical:false,
				verticalSwiping:true,
				adaptiveHeight:true,
				arrows:false,
				fade:true,
				dots:true,
				responsive:[
					{
						breakpoint:791,
						settings:{
							adaptiveHeight:true,
							vertical:false,
							verticalSwiping:false
						}
					}
				]
			});
	}
	
	/* SECTION FOOTER */
	
	
	$(".b-footer__main-links > div > span").click(function(){
		_t = $(this);
		_t_menu = _t.closest("div").find("ul");
		_t_menu_active = _t_menu.closest("div").find("ul.active");
		_t_span_active = _t_menu.closest("div").find("span.active");
		
		if(_t_menu.hasClass("active")){
			
			_t.removeClass("active");
			_t.parent().removeClass("active");
			_t_menu.removeClass("active").slideUp();
			
		}else{
			_t_span_active.removeClass("active");
			_t_span_active.parent().removeClass("active");
			_t_menu_active.removeClass("active");
			
			_t.parent().addClass("active")
			_t.addClass("active")
			_t_menu.addClass("active").slideDown();
			
		}
		
	});
	
	/* SECTION TOP SLIDER */
	
	$(".b-top-slider__wrap-slider").slick();
	
	/* SECTION APPEAL TO US TEXT */
	
	if($("section").is(".b-appeal-to-us-text")){
		ww = $(window).width();
		if(ww <= 790){
			arr = [];
			
			for(j=2;j<=3;j++){
				l = $(".b-appeal-to-us-text__inner > div:nth-of-type("+j+") > div a").length;
				for(i=0;i<l;i++){
					el = $(".b-appeal-to-us-text__inner >  div:nth-of-type("+j+") > div a").eq(i);
					arr.push(el.clone().wrap('<div>').parent().html());
				}
			}
			
			arr = arr.join("");
			
			$(".b-appeal-to-us-text__inner > div:nth-of-type(1) > div").append(arr);
			
		}else if(ww <=1209){
			arr = [];
			
			for(j=3;j<=3;j++){
				l = $(".b-appeal-to-us-text__inner > div:nth-of-type("+j+") > div a").length;
				for(i=0;i<l;i++){
					el = $(".b-appeal-to-us-text__inner >  div:nth-of-type("+j+") > div a").eq(i);
					arr.push(el.clone().wrap('<div>').parent().html());
				}
			}
			
			arr1 = [];
			arr2 = [];
			
			for(i=0;i<Math.ceil(arr.length/2);i++){
				arr1.push(arr[i]);
			}
			for(i=Math.ceil(arr.length/2);i<arr.length;i++){
				arr2.push(arr[i]);
			}
			
			$(".b-appeal-to-us-text__inner > div:nth-of-type(1) > div").append(arr1.join(""));
			$(".b-appeal-to-us-text__inner > div:nth-of-type(2) > div").append(arr2.join(""));
			
		}
		
	}
	
	
	/* OTHER */
	
	
	/* B-DROPDOWN-LIST */
	
	b_dropdown_list = {
		set_events : function(){
			$(".js-dropownlist-open").click(function(){
				_item = $(this).closest(".b-dropdown-list");
				if(_item.hasClass("b-dropdown-list--open")){
					_item.removeClass("b-dropdown-list--open");
					_item.find(".b-dropdown-list__content").stop();
					_item.find(".b-dropdown-list__content").slideUp(300);
				}else{
					_item.addClass("b-dropdown-list--open");
					_item.find(".b-dropdown-list__content").stop();
					_item.find(".b-dropdown-list__content").slideDown(300);
				}
			});
		},
		init : function(){
			$(".b-dropdown-list--open .b-dropdown-list__content").css({display:"block"});
			
			this.set_events();
		}
	}
	b_dropdown_list.init();
	
	/* B-LIST-VIDEO */
	
	if($(window).width() > 790){		
		$(".b-list-video").slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots:false,
			arrows:true
		});
	}
	
	/* L-FEEDBACK-RESPONSE */
	
	l_feedback_response = {
		trigger : function(el,animation = true){
			
			if(animation == true){
				animation = 300;
			}else{
				animation = 0;
			}
			
			if(el.hasClass("l-feedback-list__item--active")){
				
				el.removeClass("l-feedback-list__item--active");
				el.find(".l-feedback-list__responses").slideUp(animation);
				n = el.find(".l-feedback-list__link-response").attr("attr-n-responses")
				txt = 'Смотреть ответы('+n+')';
				el.find(".l-feedback-list__link-response").text(txt);
				
			}else{
				
				el.addClass("l-feedback-list__item--active");
				el.find(".l-feedback-list__responses").slideDown(animation);
				txt = 'Свернуть';
				el.find(".l-feedback-list__link-response").text(txt);
				
			}
			
		},
		open : function(el,animation = true){
			
			if(el.hasClass("l-feedback-list__item--active")){
				el.removeClass("l-feedback-list__item--active");
			}
			this.trigger(el,animation);
			
		},
		init : function(){
			
			$(".l-feedback-list__item--active").each(function(){
				l_feedback_response.open($(this),false);
			});
			
			$(".l-feedback-list__link-response").click(function(){
				el = $(this).closest(".l-feedback-list__item");
				l_feedback_response.trigger(el);
			});
			
		}
	}
	l_feedback_response.init();
	
	/* B-LEAVE-RATING */
	
	b_leave_rating = {
		set_def_val : function(){
			
			$(".b-leave-rating").each(function(){
				
				n = $(this).find(".b-leave-rating__input").val();
				n = parseInt(n);
				
				if(n<1 || n>5){
					n = 2;
					$(this).find(".b-leave-rating__input").val(n)
				}
				
				n = 6-n;
				$(this).find(".b-leave-rating__box-item--active").removeClass("b-leave-rating__box-item--active");
				$(this).find(".b-leave-rating__box-item:nth-of-type("+n+")").addClass("b-leave-rating__box-item--active");
				
			});
			
		},
		set_val : function(el,n){
			
			el.find(".b-leave-rating__box-item--active").removeClass("b-leave-rating__box-item--active");
			el.find(".b-leave-rating__box-item:nth-of-type("+(6-n)+")").addClass("b-leave-rating__box-item--active");
			
			el.find(".b-leave-rating__input").val(n)
			
		},
		set_events : function(){
			
			$(".b-leave-rating__box-item").click(function(){
				
				el = $(this).closest(".b-leave-rating");
				
				n = $(this).index();
				n +=1;
				n = 6-n;
				
				b_leave_rating.set_val(el,n);
				
			});
			
		},
		init : function(){
			
			this.set_def_val();
			this.set_events();
			
		}
	}
	b_leave_rating.init();
	
	
	/* YouTube Pop-Up */
	
	
  $('.popup-youtube').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 200
  });
   
	/* b-doctor-mini */
	
	$(".b-doctor-mini__trigger").click(function(){
		$(this).closest(".b-doctor-mini").addClass("is-record");
	});
	
	/* js-price-med-service-record */
	
	var price_med_service_rec = {
		
		//основные классы
		clss : {
			wrap : '.js-price-med-service-rec',
			opener_btn : '.js-price-med-service-rec__open-btn',
			opener_rec : '.js-price-med-service-rec__btn-record',
			item : '.js-price-med-service-rec__item',
			wrap_rec : '.b-dropdown-list__inner-record',
			wrap_serv : '.b-dropdown-list__inner-services'
		},
		//получить обертку исходя из текущего элемента
		get_wrap : function(el){
			return el.closest(this.clss.wrap);
		},
		//получить обертку item
		get_item : function(el){
			return el.closest(this.clss.item);
		},
		//получить все элементы внутри обертки и обертку
		get_wrap_els : function(el){
			obj = {}
			obj.wrap = this.get_wrap(el);
			
			for(key in this.clss){
				if(key == 'wrap') continue;
				obj[key] = obj.wrap.find(this.clss[key]);
			}
			
			return obj;
		},
		//получить объект jquert из e.currentTarget
		get_el : function(e){
			return $(e.currentTarget);
		},
		
		//открыть кнопку "записаться"
		open_rec_btn : function(objs,e){

			el = this.get_el(e);
			item = this.get_item(el);
			
			media_el = el.closest(this.clss.item).find(this.clss.opener_rec).parent();
			
			//текущий открыт?
			if(!media_el.hasClass("is-active")){
				
				//закрыть все
				objs.opener_rec.each(function(){
					item_each = $(this).parent();
					if(item_each.hasClass("is-active")){
						item_each.removeClass("is-active").addClass("is-active--last");
					}
				});
				
				media_el.addClass("is-active");
				
			}else{
				media_el.removeClass("is-active");
			}
						
		},
		
		//получить html для бокса
		get_html_inner_record : function(objs,settings){
			
			html = '';
			
			html += '<div class="b-dropdown-list__head b-dropdown-list__head--green">';
			html += '		<div class="b-dropdown-list__head-title">';
			html += '			Амбулаторный приём врача терапевта/педиатра';
			html += '		</div>';
			html += '		<div class="b-dropdown-list__wrap-icon">';
			html += '			<div class="b-dropdown-list__icon-close"></div>';
			html += '		</div>';
			html += '	</div>';
			html += '	';
			html += '	<div class="b-dropdown-list__content-record">';
			html += '		<div class="b-doctor-mini__record-wrap">';
			html += '			<div class="b-doctor-mini__record-left js-select-change-display">';
			html += '					<div class="b-doctor-mini__service-wrap">';
			html += '							<div class="b-doctor-mini__service-title">Вы  выбрали:</div>';
			html += '							<div class="b-doctor-mini__service-val">';
			html += '								<div class="b-doctor-mini__service-val-inner">';
			html += '									'+settings.service.name+'';
			html += '								</div>';
			html += '							</div>';
			html += '					</div>';
			html += '					<div class="b-doctor-mini__price">';
			html += '							<span class="b-doctor-mini__price-value js-select-change-display__display">'+settings.service.price+'</span>';
			html += '					</div>';
			html += '			</div>';
			html += '			<div class="b-doctor-mini__record-right">';
			html += '					<div class="l-doctor-mini-record">';
			html += '							<form class="b-mini-record">';
			html += '							</form>';
			html += '					</div>';
			html += '			</div>';
			html += '		</div>';
			html += '	</div>';
			html += '</div>';
			
			return html;
			
		},
		
		//открыть окно записи для десктопа
		open_window_rec : function(objs,e){
			
			//получить настройки
			settings = JSON.parse(objs.wrap.attr("attr-doctor-info"));
			settings_service = JSON.parse(objs.opener_rec.attr("attr-info-serv"));
			settings.service = settings_service.service;
			
			
			//создать html
			html = this.get_html_inner_record(objs,settings);
			objs.wrap_rec.html(html);
			
			//создание календаря
			
			record_form_settings = {
				id_doctor : settings.id_doctor,
				reception : settings.reception,
				service_type : 'fixed',
				service_value : settings.service.id
			}
			
			js_gen_form_mini_rec.create(objs.wrap.find('.b-mini-record'),record_form_settings);
			
			//корректировка высоты
			h_service =  objs.wrap.height();
			h_record = objs.wrap_rec.height();

			objs.wrap_serv.attr("attr-h-service",h_service);
			
			
			if($("body").scrollTop() == objs.wrap.offset().top - 20){
				delay_open = 1;
			}else{
				delay_open = 300;
			}
			
			objs.wrap_serv.css({height:h_service+"px"});
			objs.wrap_serv.delay(delay_open).animate({height:h_record+"px"},1);
			
			//делаем видимым inner-record
			objs.wrap_rec.delay(delay_open).queue(function(next){
				$(this).addClass("is-active");
				next();
			});
			
			//скролим к началу блока
			$("body").animate({scrollTop: objs.wrap.offset().top -20 + "px"},300);

			//обработчик закрытия
			
			objs.wrap.find(".b-dropdown-list__icon-close")
				.on("click",$.proxy(price_med_service_rec,"close_window_rec",objs));
			
		},
		
		//закрыть окно записи для десктопа
		close_window_rec : function(objs,e){
			
			//убрать inner-record
			objs.wrap_rec.removeClass("is-active");
			objs.wrap_rec.delay(200).queue(function(next){
				$(this).html("");
				next();
			});
			
			//вернуть в исходное состояние inner-services
			objs.wrap_serv.css({
				height: objs.wrap_serv.attr("attr-h-service") + "px"
			});
			
			objs.wrap_serv.delay(250).queue(function(next){
				$(this).css("height","");
				next();
			});
			
		},
		
		init_el : function(el){
			
			objs = this.get_wrap_els(el);
			
			if($(window).width() > 790){
				//событие открытия кнопки "записаться"
				objs.opener_btn.click( $.proxy(price_med_service_rec,"open_rec_btn",objs) );	

				//открытие окна записи для десктопа
				objs.opener_rec.click( $.proxy(price_med_service_rec,"open_window_rec",objs) );
			}
			
			
			
		},
		
		init : function(){
			
			$(this.clss.wrap).each(function(){
				
				price_med_service_rec.init_el($(this));
				
			});
			
		}
		
	}
	
	price_med_service_rec.init();
	
	/* js-brief-descs */
	if($(window).width() <= 790){
		$(".js-brief-descs__title").click(function(){
			item = $(this).closest(".js-brief-descs__item");
			wrap = $(this).closest(".js-brief-descs");
			if(!item.hasClass("is-active")){
				wrap.find('.js-brief-descs__item.is-active .js-brief-descs__content')
					.slideUp(200)
					//.css({display:'none'})
					.closest('.js-brief-descs__item').removeClass("is-active");
				item.addClass("is-active").find(".js-brief-descs__content")
					.slideDown(200,function(){
						if(
							$(this).closest('.js-brief-descs').hasClass('js-brief-descs--scrolltoview')
						){
							$('html,body')
								.animate({
									scrollTop: $(this).closest('.js-brief-descs__item').offset().top - 20
								});
						}
					});
					//.css({display:'block'})
				
			}
		})
	}
	
});