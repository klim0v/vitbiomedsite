// CUSTOM JS

$(document).ready(function(){



	"use strict";


    $('.li-no-click').on('click', function(){
        return false;
    })

    // Toogle Select
    $('.js-toggler-l').on('click', function(){

        $(this).addClass('toggler--is-active');
        $(this).parent().find('.js-switcher').removeClass('active').prop("checked", false);
        $(this).parent().find('.js-toggler-r').removeClass('toggler--is-active');
    });

    $('.js-toggler-r').on('click', function(){
        $(this).addClass('toggler--is-active');
        $(this).parent().find('.js-switcher').addClass('active').prop("checked", true);
        $(this).parent().find('.js-toggler-l').removeClass('toggler--is-active');
    });

    $(".js-switcher").on('click', function(){
        var obj_l = $(this).parent().parent().find('.js-toggler-l'),
            obj_r = $(this).parent().parent().find('.js-toggler-r');

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            obj_l.addClass('toggler--is-active');
            obj_r.removeClass('toggler--is-active');
			obj_l.find('li').trigger('click');
        } 
        else {
            $(this).addClass('active');
            obj_r.addClass('toggler--is-active');
            obj_l.removeClass('toggler--is-active');
			obj_r.find('li').trigger('click');
        }
    });


    $('.product__checker').on('click', function(e){
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parent().find('.product__wrap-1').addClass('active');
            $(this).parent().find('.product__wrap-2').removeClass('active');
        }
        else {
            $(this).addClass('active');
            $(this).parent().find('.product__wrap-1').removeClass('active');
            $(this).parent().find('.product__wrap-2').addClass('active');
        }
    });


	// TABS
    $('.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this).addClass('active').siblings().removeClass('active')
      .closest('.js-tabs').find('.tab__content').hide().eq($(this).index()).fadeIn(600);
      if($(this).parent().hasClass('maps-js')) {
        //initMap();
      }
    });

	// RADIO BUTTON
    $('.radio').on('click', function(){
        if ( !$(this).hasClass("active") ){
        	var radio_name = $(this).find('.radio__input').attr('name');
            	$('.radio__input[name="'+ radio_name +'"]').parent().removeClass('active').find('.radio__input').prop("checked", false);
           		$(this).addClass('active').find('.radio__input').prop("checked", true);
            return false;
        }
    });

    // RADIO BUTTON - LABEL
    $('.label-radio').on('click', function(){
		if ( !$(this).find('.radio').hasClass("active") ){
        	var radio_name = '.radio__input[name="' + $(this).find('.radio__input').attr('name') + '"]';
            	$(radio_name).parent().removeClass('active').find('.radio__input').prop("checked", false);
           		$(this).find('.radio').addClass('active').find('.radio__input').prop("checked", true);
        }
    });

    $('.label-radiosi').on('click', function(){
		if ( !$(this).find('.radio').hasClass("active") ){
        	var radio_name = '.radio__input[name="' + $(this).find('.radio__input').attr('name') + '"]';
            	$(radio_name).parent().removeClass('active').find('.radio__input').prop("checked", false);
           		$(this).find('.radio').addClass('active').find('.radio__input').prop("checked", true);
        }
        else {
			$(this).find('.radio').removeClass('active').find('.radio__input').prop("checked", true);
        }
    });

    // ACCORDION
    $('.accordion__link').on('click', function(){
        var accordionItem = $(this).parent();
        if(accordionItem.hasClass('active')) {
            accordionItem.removeClass('active').find('.accordion__content').slideUp(300);
        }
        else {
            // accordionItem.siblings().removeClass('active').find('.accordion__content').slideUp(300);
            accordionItem.addClass('active').find('.accordion__content').slideDown(300);
        }
    });

    // AMOUNT
    $('.amount__button--minus').click(function () {
        var input = $(this).next(),
            count = parseInt(input.val()) - 1;
        count = count < 1 ? 1 : count;
        input.attr('value', count);
    });
    $('.amount__button--plus').click(function () {
        var input = $(this).prev(),
            count = parseInt(input.val()) + 1
        input.attr('value', count);
    });

    $('.amount__button').on('mousedown', function(e){
        e.preventDefault();
    });

    $('.amount__button').on('selectstart', function(e){
        e.preventDefault();
    });




    $('.cl234icajax').click(function(){
            $.ajax({        
				url: "/bitrix/templates/vitbnew/ajax/ajaxbas2.php", 
				success: function(data) {
					$('.clicaj').html(data);

        			}
    		});
    });

	
	$('body').on('click', '.js-maps-item', function(){
						$(this).addClass('active').siblings().removeClass('active');
						$('.pickup-address').val($(this).text());
	});

    $('.single_add_to_cb').on('click', function(){
	    var counter = Number($(".cart__sum").text()) + 1;
	    $(".cart__sum").text(counter);
    });



    $('.cart-action__icon').on('click', function(){
       if (  $(this).parent().find('.cart-action__container').hasClass("active_bs") ){
        	$(this).parent().find('.cart-action__container').removeClass('active_bs').prop("checked", true);
        }
        else {
        	$(this).parent().find('.cart-action__container').addClass('active_bs').prop("checked", true);
        }

    });



   $('.cart__icon').on('click', function()
   {

        var accordionItem = $(this).parent();
       if (  $(this).parent().find('.js-cart-action').hasClass("active_bs") ){
        	$(this).parent().find('.js-cart-action').removeClass('active_bs').prop("checked", true);
        }
        else {
        	$(this).parent().find('.js-cart-action').addClass('active_bs').prop("checked", true);
        }

    });


});







/* ==================================================================================
    CUSTOM SELECT
   ================================================================================== */
;(function() {
    $('.select').find('.select__title').on('click', function(e){
        var title  = $(this), 
            list   = title.next('.select__list');

        if( title.hasClass('active') ){
            title.removeClass('active');
            list.removeClass('active');
            return false;
        } 
        else {
            $('.select__title').removeClass('active');
            $('.select__list').removeClass('active');
            title.addClass('active');
            list.addClass('active');
            return false;
        }
    });

    $(document).click(function(e){
        $('.select__title').removeClass('active');
        $('.select__list').removeClass('active');
    });

    $('.select__item').on('click', function(e){
        var item  = $(this), 
            title = item.parent().prev('.select__title'),
            input = item.parent().parent().find('.select__input'),
            select = title.parent();         

        if(select.attr('data-select') != undefined){
            title.removeClass().addClass('select__title').addClass(item.data('class'));
        }; 

        $(this).addClass('active').siblings().removeClass('active');

        title.text(item.text());
        input.val(item.text());
        item.parent().removeClass('active');  
    });

    $('.select__title').on('mousedown', function(e){
        e.preventDefault();
    });

    $('.select__title').on('selectstart', function(e){
        e.preventDefault();
    });
})();


/* ==================================================================================
    MODAL WINDOW
   ================================================================================== */
;(function() {
    var trigger = $('.md-trigger'),
        close = $('.md-close'),
        overlay = $('.md-overlay'),
        modal = $('.md-modal');

        trigger.on('click', function(e){
            e.preventDefault();
            var modal = $('[id="'+ $(this).data('modal') +'"]'),
                modalContent = modal.find('.md-content'),
                overlay = modal.next('.md-overlay');

            modalContent.scrollTop(0);
            modal.addClass('md-show');
            overlay.addClass('md-show');
        });

        overlay.on('click', function(){
            removeClass();
        });

        close.on('click', function(){
            removeClass();
        });

        function removeClass(){
            modal.removeClass('md-show');
            overlay.removeClass('md-show');
        }
})();




// IK-CALENDAR INIT
$(document).ready(function(){

    var date = new Date(), // Текущая дата
        weekday = 1,
        today = date.getDate(),
        before_day = date.getDay() - 1,
        month = date.getMonth(),
        year = date.getFullYear()
        month_last = new Date(year, month + 1, 0).getDate(); // Последний день месяца

    var month_array = [
        "Января",
        "Февраля",
        "Марта",
        "Фпреля",
        "Мая",
        "Июня",
        "Июля",
        "Августа",
        "Сентября",
        "Октября",
        "Ноября",
        "Декабря"
    ];

    var max = 400, // Максимальное кол-во дней
        j = 0,
        list = '';

    list = "<div class='ik-calendar__item'>";
    for(var i = today - before_day; i <= month_last; i++) {

        // День недели
        weekday = new Date(year, month, i).getDay();

        // Если суббота или воскресенье
        if(today == i) {
            list = list + "<span class='today active' data-date='" + i +' '+ month_array[month] +' '+ year + "'>" + i + "</span>";
        }
        else if (weekday == 6 || weekday == 0) {
            list = list + "<span class='weekend' data-date='" + i +' '+ month_array[month] +' '+ year + "'>" + i + "</span>";
        }
        else if (j < before_day) {
            list = list + "<span class='disable' data-date='" + i +' '+ month_array[month] +' '+ year + "'>" + i + "</span>";
        }
        else {
            list = list + "<span data-date='" + i +' '+ month_array[month] +' '+ year + "'>" + i + "</span>";
        }
        
        // Если воскресенье
        if (new Date(year, month, i).getDay() == 0) { 
            list = list + "</div>";
            list = list + "<div class='ik-calendar__item'>";
        }

        if (i == month_last) {
            i = 0;
            month++;
            if(month == 12) {
                year++;
                month = 0;
            }
            month_last = new Date(year, month + 1, 0).getDate();
        }

        j++;
        if(j == max) break; 
    }
    list = list + "</div>";

    $('.ik-calendar__slider').append(list);

    $('.ik-calendar__slider').owlCarousel({
        singleItem: true,
        autoPlay: false,
        navigation: true,
        rewindNav: false,
        pagination: false,
        navigationText: 
                ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="M45.93,0L49,3.19,21.09,32,49,60.81,45.93,64,15,32.08Z"/></svg>', 
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="M49,32.08L18.07,64,15,60.81,42.91,32,15,3.19,18.07,0Z"/></svg>'],
        slideSpeed: 700,
        stopOnHover : true,
        mouseDrag: false,
    });

    // Выбор даты доставки
    $('.ik-calendar__item span:not(.disable)').on('click', function(){
        if(!$(this).hasClass('active')) {
            $('.ik-calendar__item span').removeClass('active');
            $(this).addClass('active');
            $('#ik-date').val($(this).attr('data-date'));
            $('.ik-date-title').text($(this).attr('data-date'));
        }
        else {
            $(this).removeClass('active');
        }
    });

    $('.js-drop-date').on('click', function(){
        if(!$(this).hasClass('active')) {
            $('.select-date__top span').removeClass('active');
            $('.select-date__ik-time').removeClass('active');

            $(this).addClass('active');
            $('.select-date__ik-date').slideDown(200);
        }
        else {
            $(this).removeClass('active');
            $('.select-date__ik-date').slideUp(200);
        }
    });

    $('.js-drop-time').on('click', function(){
        if(!$(this).hasClass('active')) {
            $('.select-date__top span').removeClass('active');
            $('.select-date__ik-date').slideUp(200);

            $(this).addClass('active');
            $('.select-date__ik-time').addClass('active');
        }
        else {
            $(this).removeClass('active');
            $('.select-date__ik-time').removeClass('active');
        }
    });

    $('.ik-time__item').on('click', function(){
        $('#ik-time').val($(this).text());
        $('.ik-time-title').text($(this).text());

        $('.ik-time__item').removeClass('active');
        $(this).addClass('active');
        $('.js-drop-time').removeClass('active');
        $('.select-date__ik-time').removeClass('active');
    });
    
});



// LK
$(document).ready(function(){

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#user-ava').attr('src', e.target.result);
                $('.js-save-user-ava').css('display', 'block');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#user-ava-input").change(function(){
        readURL(this);
    });
    $('.js-ava-img').on('click', function(){
        $("#user-ava-input").trigger('click');
    });


    $('.all-orders_open-info').on('click', function(){
        if(!$(this).closest('.all-orders__item').hasClass('active')) {
            $('.all-orders__item').removeClass('active');
            $('.all-orders__info').slideUp(200);

            $(this).closest('.all-orders__item').addClass('active');
            $(this).closest('.all-orders__item').find('.all-orders__info').slideDown(200);
        }
        else {
            $(this).closest('.all-orders__item').removeClass('active');
            $(this).closest('.all-orders__item').find('.all-orders__info').slideUp(200);
        }
    });

    $('.all-orders__info-more').on('click', function(){
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).closest('.all-orders__info').find('.all-orders__info-content').slideDown(200);
        }
        else {
            $(this).removeClass('active');
            $(this).closest('.all-orders__info').find('.all-orders__info-content').slideUp(200);
        }
    });

    $('.all-orders_open-info').on('mousedown', function(e){
        e.preventDefault();
    });

    $('.all-orders_open-info').on('selectstart', function(e){
        e.preventDefault();
    });

    $('.lk-edit').on('click', function(){
        $('.lk-edit-input').fadeOut(100);
        $('.replace-password__contents').fadeOut(100);
        $(this).closest('.user-info__field-content').find('.lk-edit-input').fadeIn(100);
    });

    $('.js-input-save').on('click', function(e){
        e.preventDefault();
        $('.lk-edit-input').fadeOut(100);
    });

    $('.js-replace-password').on('click', function(e){
        e.preventDefault();
        $('.lk-edit-input').fadeOut(100);
        $(this).closest('.replace-password-wrap').find('.replace-password__contents').fadeIn(100);
    });

    $('.js-input-save').on('click', function(e){
        e.preventDefault();
        $('.replace-password__contents').fadeOut(100);
    });
});