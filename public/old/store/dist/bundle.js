;
(function () {
	'use strict';

	angular.module('app', [
		'hmTouchEvents',
		'ngMaterial',
		'ngDialog'
	]);

})();
;
(function() {
	'use strict';
	
	angular
		.module('app')
		.config(prodMode);

	prodMode.$inject = ['$compileProvider'];

	function prodMode($compileProvider) {
		//$compileProvider.debugInfoEnabled(false);
	}
})();
;
(function() {
'use strict';

	angular
		.module('app')
		.factory('lkFunctions', lkFunctions);

	lkFunctions.$inject = [];
	
	function lkFunctions() {
		var service = {
			makeArrayOf: makeArrayOf,
			getCurIndexObjectInArray: getCurIndexObjectInArray
		};
		
		return service;

		////////////////
		function makeArrayOf(value, length) {
			var array = [];
			
			while (length--) {
				array[length] = value;
			}
		  return array;
		}
		
		function getCurIndexObjectInArray(arrayOfObjects, key, value) {
			for (var i = 0, len = arrayOfObjects.length; i < len; i++) {
				if (arrayOfObjects[i][key] === value) {
					return i;
				}
			}
			return -1;
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.factory('lkCart', lkCart);

	lkCart.$inject = ['$rootScope', 'lkFunctions', 'lkSelectProduct'];

	function lkCart($rootScope, lkFunctions, lkSelectProduct) {
		var service = {
			visibility: false,
			getAmount: getAmount,
			addToCart: addToCart,
			getGeneralSum: getGeneralSum,
			removeItem: removeItem,
			changeVisibility: changeVisibility,
			store: []
		};

		return service;

		////////////////
		function addToCart(options) {	
			var indexProductsStore = lkFunctions.getCurIndexObjectInArray(lkSelectProduct.store, 'id', options.id);
			var indexCartStore = lkFunctions.getCurIndexObjectInArray(service.store, 'id', options.id);
			
			if (indexProductsStore === -1) {
				lkSelectProduct.store.push({
						id: options.id,
						amount: options.amount,
						volume: options.volume,
						title: options.title,
						price: options.price,
						image: options.image,
						snippet: options.snippet
					});
				indexProductsStore = lkFunctions.getCurIndexObjectInArray(lkSelectProduct.store, 'id', options.id);	
			} 

			if (indexProductsStore !== -1) {
				if (indexCartStore === -1) {
					service.store.push(lkSelectProduct.store[indexProductsStore]);
				}
				if (indexCartStore !== -1) {
					service.store[indexCartStore] = lkSelectProduct.store[indexProductsStore];
				}
			}
		}
		
		function getAmount() {
			if (service.store) {
				return service.store.length;
			}
			return 0;
		}
		
		function getGeneralSum() {
			var sum = 0;
			if (service.store) {
				for (var i = 0, len = service.store.length; i < len; i++) {
					sum += parseFloat(service.store[i].amount * service.store[i].price);
				}
			}
			return sum;
		}
		
		function removeItem(id) {
			var indexProductsStore = lkFunctions.getCurIndexObjectInArray(lkSelectProduct.store, 'id', parseInt(id));
			var indexCartStore = lkFunctions.getCurIndexObjectInArray(service.store, 'id', parseInt(id));

			lkSelectProduct.store.splice(indexProductsStore, 1);
			service.store.splice(indexCartStore, 1);
		}

		function changeVisibility(value) {
			service.visibility = value;

			$rootScope.$broadcast('cart:visibility', {
				visibility: service.visibility
			});
		}
	}
})();
;
(function() {
'use strict';

	angular
		.module('app')
		.factory('lkInsets', lkInsets);

	lkInsets.$inject = ['lkFunctions'];
	
	function lkInsets(lkFunctions) {
		var service = {
			init: init,
			control: control
		};
		
		var click = [];
		var flag = true;
		
		return service;

		////////////////
		function init() { 
			click = lkFunctions.makeArrayOf(0, document.querySelectorAll('[data-inset-trigger]').length);
		}
		
		function control(event) {		
			var trigger = angular.element(event.target);
			var id = parseInt(trigger.attr('data-inset-trigger'));
			var target = angular.element(document.querySelector('[data-inset-target="' + id + '"]'));
			
			if (flag && trigger.attr('data-inset-show')) {
				click[id]++;
				flag = false;		
			}
			
			click[id]++;
			
			if (click[id] % 2) {
				target.show(200);
			} else {
				target.hide(200);
			}
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.factory('lkSelectProduct', lkSelectProduct);

	lkSelectProduct.$inject = ['lkFunctions'];

	function lkSelectProduct(lkFunctions) {
		var service = {
			setAmount: setAmount,
			setVolume: setVolume,
			getAmount: getAmount,
			store: []
		};

		var defaultData = {
			volume: 60,
			amount: 1
		}

		return service;

		////////////////
		function setAmount(options) {
			var index = lkFunctions.getCurIndexObjectInArray(service.store, 'id', options.id);
			if (index === -1) {		
				service.store.push({
					id: options.id,
					amount: _counter(options.count, defaultData.amount),
					volume: defaultData.volume,
					title: options.title,
					price: options.price,
					image: options.image,
					snippet: options.snippet
				});
			} else {	
				service.store[index].amount = _counter(options.count, service.store[index].amount);
			}
			
			_logger(options.id);
		}

		function setVolume(options) {
			var index = lkFunctions.getCurIndexObjectInArray(service.store, 'id', options.id);
			if (index === -1) {		
				service.store.push({
					id: options.id,
					amount: defaultData.amount,
					volume: options.volume,
					title: options.title,
					price: options.price,
					image: options.image,
					snippet: options.snippet
				});
			} else {
				service.store[index].volume = options.volume;	
			}
			
			_logger(options.id);
		}

		function getAmount(id) {
			var index = lkFunctions.getCurIndexObjectInArray(service.store, 'id', id);
			if (index === -1) {
				return -1;
			} else {
				return service.store[index].amount;
			}
		}

		/////////////////
		function _logger(id) {
			var index = lkFunctions.getCurIndexObjectInArray(service.store, 'id', id);
			if (index === -1) {
				return console.log('object not found!');
			}
			console.log('product: ', service.store[index]);
		}

		function _counter(count, amount) {
			if (count < 0 && amount > 1) {
				amount += count;
			}
			if (count > 0) {
				amount += count;
			}
			return amount
		}
	}
})();
;
(function() {
'use strict';

	angular
		.module('app')
		.factory('lkSlider', lkSlider);

	lkSlider.$inject = ['$interval'];
	
	function lkSlider($interval) {
		var service = {
			init: init,
			changeContent: changeContent
		};
		
		var timer = {
			name: [],
			nodeName: []
		};
		
		return service;

		////////////////
		function changeContent(count, nodeName) {
			var children =  angular.element(document.querySelector(nodeName)).children();
			var delay = delayInit(nodeName);
			
			for(var i = 0; i < timer.name.length; i++) {
				if(timer.nodeName[i] === nodeName) {
					$interval.cancel(timer.name[i]);
					timer.name[i] = startInterval(nodeName);
				}		
			}
			
			for (var i = 0; i < children.length; i++) {
				if (!angular.element(children[i]).hasClass('display-hide')) {
					angular.element(children[i]).addClass('display-hide');
					
					if (count > 0) {
						if (children[i + 1]) {
							if (angular.element(children[i + 1]).hasClass('slide-left')) {
								angular.element(children[i + 1])
									.removeClass('display-hide')
									.removeClass('slide-left')
									.addClass('slide-right');
							} else {
								angular.element(children[i + 1])
									.removeClass('display-hide')
									.addClass('slide-right');
							}
							break;
						} else {
							if (angular.element(children[0]).hasClass('slide-left')) {
								angular.element(children[0])
									.removeClass('display-hide')
									.removeClass('slide-left')
									.addClass('slide-right');
							} else {
								angular.element(children[0])
									.removeClass('display-hide')
									.addClass('slide-right');
							}
							break;
						}
					} else {
						if (children[i - 1]) {
							if (angular.element(children[i - 1]).hasClass('slide-right')) {
								angular.element(children[i - 1])
									.removeClass('display-hide')
									.removeClass('slide-right')
									.addClass('slide-left');
							} else {
								angular.element(children[i - 1])
									.removeClass('display-hide')
									.addClass('slide-left');
							}
							break;
						} else {
							if (angular.element(children[children.length - 1]).hasClass('slide-right')) {
								angular.element(children[children.length - 1])
									.removeClass('display-hide')
									.removeClass('slide-right')
									.addClass('slide-left');
							} else {
								angular.element(children[children.length - 1])
									.removeClass('display-hide')
									.addClass('slide-left');
							}
							break;
						}
					}
				}
			}
		}
		
		function hideAllUnlessFirst(nodeName) {
			var children = angular.element(document.querySelector(nodeName)).children();
			
			for (var i = 1; i < children.length; i++) {
				if (!angular.element(children[i]).hasClass('display-hide')) {
					angular.element(children[i]).addClass('display-hide');
				}
			}
		}
		
		function delayInit(nodeName) {
			return parseInt(angular.element(document.querySelector(nodeName)).attr('data-slider-delay')) || 8000;
		}
		
		function startInterval(nodeName) {
			var delay = delayInit(nodeName);
			
			return $interval(function() {
						changeContent(1, nodeName);
					}, delay);
		}
		
		function overflowXHidden() {
			angular.element('body').css('overflow-x', 'hidden');
		}
		
		function init(nodeName) {
			hideAllUnlessFirst(nodeName);
			overflowXHidden();
			timer.name.push(startInterval(nodeName));
			timer.nodeName.push(nodeName);	
		}
	}
})();
;
(function() {
'use strict';

	angular
		.module('app')
		.factory('lkTabs', lkTabs);

	lkTabs.$inject = ['$rootScope'];
	
	function lkTabs($rootScope) {
		var service = {
			switchTab: switchTab,
			activeTab: {
				id: null,
				text: null
			}
		};
		
		return service;

		////////////////
		function switchTab(event) { 
			var parentId = angular.element(event.target).closest('[data-tab-panel]').attr('data-tab-panel');	
			var element = angular.element(event.target);
			var triggerId = element.attr('data-tab-trigger');
			var target = angular.element(document.querySelector('[data-tab-panel="' + parentId + '"] [data-tab-target="' + triggerId + '"]'));
			var tabs = document.querySelectorAll('[data-tab-panel="' + parentId + '"] [data-tab-target]');
			var buttons = document.querySelectorAll('[data-tab-panel="' + parentId + '"] [data-tab-trigger]');
			var targetBtn = angular.element(document.querySelector('[data-tab-panel="' + parentId + '"] [data-tab-trigger="'+ triggerId +'"]'));
			
			service.activeTab.id = angular.element(document.querySelector('[data-tab-panel="' + parentId + '"] [data-tab-trigger="'+ triggerId +'"]')).attr('data-tab-trigger');
			service.activeTab.text = angular.element(document.querySelector('[data-tab-panel="' + parentId + '"] [data-tab-trigger="'+ triggerId +'"]')).text();
			service.activeTab.text = service.activeTab.text.replace(/(^\s+|\s+$)/g,'');

			$rootScope.$broadcast('tabs:changeActive', {	
				id: service.activeTab.id,
				text: service.activeTab.text
			});


			for (var i = 0; i < buttons.length; i++) {
				if (angular.element(buttons[i]).hasClass('js-tab-panel__btn--active')) {
					angular.element(buttons[i]).removeClass('js-tab-panel__btn--active');
				}
			}
			
			for (var i = 0; i < tabs.length; i++) {
				if (angular.element(tabs[i]).hasClass('js-tab-panel__item--active')) {
					angular.element(tabs[i]).removeClass('js-tab-panel__item--active');
				}
			}
			
			target.addClass('js-tab-panel__item--active');
			targetBtn.addClass('js-tab-panel__btn--active');	
		}
	}
})();
(function() {
'use strict';

	angular
		.module('app')
		.factory('CoursesPageDataService', CoursesPageDataService);

	CoursesPageDataService.$inject = [];

	function CoursesPageDataService() {
		var data = [
			{
				id: 0,
				type: 'Профилактические курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 10000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			},
			{
				id: 1,
				type: 'Профилактические курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 10000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			},
			{
				id: 2,
				type: 'Профилактические курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 10000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			},
			{
				id: 3,
				type: 'Профилактические курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 10000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			},
			{
				id: 4,
				type: 'Лечебные курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 12000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			},
			{
				id: 5,
				type: 'Лечебные курсы',
				title: 'Курс «Укрепление иммунитета»',
				link: '#',
				image: './resources/img/products-page/cards/1.png',
				snippet: 'Жидкая форма',
				price: 12000,
				subtitle: 'Курс на 1 месяц. Рекомендуемая длительность приема – 1 месяц.',
				description: 'Принимайте пробиотики 2 раза в год – в начале весны и в начале осени – в эти периоды флора даже полностью здорового человека ослабевает. Пробиотики поддерживают здоровой баланс микрофлоры и противостоят развитию многих заболеваний.',
				composition: [
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Л, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс А, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс Б, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Биобаланс В, жидкий',
						amount: '4 шт'
					},
					{
						item: 'Нормоспорин',
						amount: '20 шт'
					},
				]
			}
		]

		var service = {
			data: data
		};
		
		return service;

		////////////////
		
	}
})();
;
(function () {
  'use strict';

	angular
		.module('app')
		.controller('IosToggleController', IosToggleController);

	IosToggleController.$inject = ['$scope'];

	function IosToggleController($scope) {
		var vm = this;

		activate();
		////////////////
		function activate() { 
		}

		var vm = this;
		var click = 0;

		vm.toggle = toggle;

		//functions
		function toggle(event) {
			var target = null;
			var body = null;
			var toggle = false;
			click++;

			if (angular.element(event.target).hasClass('js-ios-toggle__circle')) {
				target = angular.element(event.target);
				body = target.closest('.js-ios-toggle');
			} else {
				target = angular.element(event.target).find('.js-ios-toggle__circle');
				body = angular.element(event.target);
			}

			if (click % 2) {
				if (target.hasClass('js-ios-toggle__circle--slide-left')) {
					target.removeClass('js-ios-toggle__circle--slide-left');
				}
				if (!target.hasClass('js-ios-toggle__circle--slide-right')) {
					target.addClass('js-ios-toggle__circle--slide-right');
				}
			} else {
				if (target.hasClass('js-ios-toggle__circle--slide-right')) {
					target.removeClass('js-ios-toggle__circle--slide-right');
				}
				if (!target.hasClass('js-ios-toggle__circle--slide-left')) {
					target.addClass('js-ios-toggle__circle--slide-left');
				}
			}

			body.toggleClass('js-ios-toggle--inactive');

			if (body.hasClass('js-ios-toggle--inactive')) {
				toggle = true;
			}
			
			$scope.$emit('toggle:toggle', {
				id: vm.id,
				toggle: toggle
			});
		}
	}

  angular.module('app')
		.component('iosToggle', {
			bindings: {
				id: '@'
			},
			templateUrl: 'app/components/ios-toggle/ios-toggle.component.html',
			controller: 'IosToggleController as $ctrl'
		});

})();

;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('CartController', CartController);

	CartController.$inject = ['$scope', '$timeout', 'lkCart'];

	function CartController($scope, $timeout, lkCart) {
		var vm = this;
		var cart = null;

		vm.visibility = lkCart.visibility;
		vm.open = open;
		vm.amount = lkCart.getAmount;
		vm.getGeneralSum = lkCart.getGeneralSum;
		vm.products = lkCart.store;
		vm.removeItem = lkCart.removeItem;

		activate();

		////////////////

		function activate() {
			$scope.$on('cart:visibility', function (event, data) {
				vm.visibility = data.visibility;

				$timeout(function () {
					cart = angular.element(document.querySelector('.js-cart-action'));
					cart.addClass('js-cart-action--animated');
									
					$timeout(function (){
						lkCart.visibility = false;
						vm.visibility = lkCart.visibility;
						cart.removeClass('js-cart-action--animated');				
					}, 5000);

				}, 0);
			});
		}

		function open() {
			lkCart.visibility = !lkCart.visibility;
			vm.visibility = lkCart.visibility;
		}
	}
})();


;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('MobileMenuController', MobileMenuController);

	MobileMenuController.$inject = [];

	function MobileMenuController() {
		var vm = this;
		var click = 0;
		var menu = angular.element(document.querySelector('#menu-mobile-body'));
		var btn = angular.element(document.querySelector('#menu-mobile-btn'));
		
		vm.controlMenu = controlMenu;
		
		function controlMenu(event) {
			menu.toggleClass('display-hide');
			btn.toggleClass('menu-mobile__clicked');	
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('ProductCardController', ProductCardController);

	ProductCardController.$inject = ['$scope', 'lkSelectProduct', 'lkCart', 'CoursesPageDataService'];

	function ProductCardController($scope, lkSelectProduct, lkCart, CoursesPageDataService) {
		var vm = this;

		vm.showDetail = showDetail;
		vm.setAmount = setAmount;
		vm.setVolume = setVolume;
		vm.getAmount = getAmount;
		vm.addToCart = addToCart;
		vm.toggle = [];
		vm.cards = [];

		activate();

		////////////////
		function activate() {
			subscribeOnChangeToggle();
			vm.cards = CoursesPageDataService.data;
			console.log(CoursesPageDataService.data);
		 }

		function setAmount(event, count) {
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));

			var metadata = _getMetadata(id);

			var options = {
				id: id,
				count: count,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setAmount(options);
		}

		function setVolume(event, count) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var volume = parseInt(target.attr('data-volume-product'));

			var metadata = _getMetadata(id);

			var options = {
				id: id,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setVolume(options);
			addActiveClass(event, options.id);
		}

		function getAmount(id) {
			if (lkSelectProduct.getAmount(id) === -1) {
				return 1;
			}

			return lkSelectProduct.getAmount(id);
		}

		function addActiveClass(event, id) {
			var selectsAll = document.querySelectorAll('[data-card-id="' + id + '"] .js-select-product__item');
			var target = angular.element(event.target);

			for (var i = 0, len = selectsAll.length; i < len; i++) {
				if (angular.element(selectsAll[i]).hasClass('js-select-product__item--active')) {
					angular.element(selectsAll[i]).removeClass('js-select-product__item--active');
				}
			}

			if (!target.hasClass('js-select-product__item--active')) {
				target.addClass('js-select-product__item--active');
			}
		}

		function showDetail(event) {
			var cardId = angular.element(event.target.parentNode.parentNode.parentNode).attr('data-card-id');
			var typeBtn = angular.element(event.target).attr('data-card-button-type');
			var contentMain = angular.element(document.querySelector('[data-card-id="' + cardId + '"] [data-card-content-type="main"]'));
			var contentOther = angular.element(document.querySelector('[data-card-id="' + cardId + '"] [data-card-content-type="other"]'));

			if (typeBtn === 'info-open') {
				if (contentMain.hasClass('js-product-card__content--active')) {
					contentMain.removeClass('js-product-card__content--active');
				}
				if (!contentOther.hasClass('js-product-card__content--active')) {
					contentOther.addClass('js-product-card__content--active');
				}
			}

			if (typeBtn === 'info-close') {
				if (contentOther.hasClass('js-product-card__content--active')) {
					contentOther.removeClass('js-product-card__content--active');
				}
				if (!contentMain.hasClass('js-product-card__content--active')) {
					contentMain.addClass('js-product-card__content--active');
				}
			}
		}

		function addToCart(event) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var amount = 1;
			var volume = parseInt(
				angular.element(
					document.querySelector('[data-card-id="' + id + '"] [data-volume-product]')
				)
					.attr('data-volume-product')
			);

			var metadata = _getMetadata(id);

			var options = {
				id: id,
				amount: amount,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}
			lkCart.addToCart(options);

			lkCart.changeVisibility(true);
		}

		function subscribeOnChangeToggle() {
			$scope.$on('toggle:toggle', function(event, data) {
				vm.toggle[data.id] = data.toggle;

				console.log('id:', data.id);
				console.log('toggle:', data.toggle);
			});
		}

		
		function _getMetadata(id) {
			var title = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-title]')
			).attr('data-metadata-title');

			var price = parseFloat(
				angular.element(
					document.querySelector('[data-card-id="' + id + '"] [data-metadata-price]')
				).attr('data-metadata-price')
			);

			var image = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-image]')
			).attr('data-metadata-image');

			var snippet = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-snippet]')
			).attr('data-metadata-snippet');
			
			var metadata = {
				title: title,
				price: price,
				image: image,
				snippet: snippet	
			}
			
			return metadata;
		}

	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('SelectController', SelectController);

	SelectController.$inject = ['$scope'];

	function SelectController($scope) {
		var vm = this;

		vm.open = open;
		vm.change = change;
		vm.selectName = null;


		activate();

		////////////////

		function activate() {
			addActiveClass(0);
			subscribeOnChangeActiveTabs();
		}

		function open(event) {
			var parent = angular.element(event.target).closest('.js-vit-select');
			parent.find('.js-vit-select__items').toggleClass('js-vit-select__items--active');
		}

		function change(event) {
			var parent = angular.element(event.target).closest('.js-vit-select');
			parent.find('.js-vit-select__items').toggleClass('js-vit-select__items--active');
		}

		function addActiveClass(id) {
			var items = document.querySelectorAll('.vit-select__items [data-tab-trigger]');

			for (var i = 0, len = items.length; i < len; i++) {
				if (angular.element(items[i]).hasClass('js-vit-select__item--active')) {
					angular.element(items[i]).removeClass('js-vit-select__item--active');
				}
			}

			angular.element(document.querySelector('.vit-select__items [data-tab-trigger="' + id + '"]')).toggleClass('js-vit-select__item--active');
		}

		function subscribeOnChangeActiveTabs() {
			$scope.$on('tabs:changeActive', function (event, data) {
				vm.selectName = data.text;
				addActiveClass(data.id);
			});
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('SliderFooterController', SliderFooterController);

	SliderFooterController.$inject = ['$interval', 'lkSlider'];

	function SliderFooterController($interval, lkSlider) {
		var vm = this;
		var selector = '#vit-slider-content--footer';

		vm.changeContent = lkSlider.changeContent;
		
		activate();
		
		function activate() {
			lkSlider.init(selector);
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('SliderMainController', SliderMainController);

	SliderMainController.$inject = ['$interval', 'lkSlider'];

	function SliderMainController($interval, lkSlider) {
		var vm = this;
		var selector = '#vit-slider-content--main';
		
		vm.changeContent = lkSlider.changeContent;
		
		activate();
		
		function activate() {
			lkSlider.init(selector);
		}
	
	}
})();
;
(function() {
'use strict';

	angular
		.module('app')
		.controller('TabsController', TabsController);

	TabsController.$inject = ['lkTabs'];

	function TabsController(lkTabs) {
		var vm = this;
		
		vm.switchTab = switchTab;

		activate();

		////////////////

		function activate() { }

		function switchTab(event, styleBtn) {

			lkTabs.switchTab(event, styleBtn);
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('ProductItemPageController', ProductItemPageController);

	ProductItemPageController.$inject = [
		'ngDialog', 
		'lkFunctions', 
		'lkSelectProduct', 
		'lkInsets', 
		'lkCart'
	];

	function ProductItemPageController(
		ngDialog, 
		lkFunctions, 
		lkSelectProduct, 
		lkInsets, 
		lkCart) {
		var vm = this;

		vm.setAmount = setAmount;
		vm.setVolume = setVolume;
		vm.getAmount = getAmount;
		vm.addToCart = addToCart;

		vm.controlInset = lkInsets.control;
		vm.clickToOpen = clickToOpen;

		activate();

		function activate() {
			lkInsets.init();
		}

		function setAmount(event, count) {
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			
			var metadata = _getMetadata(id);
				
			var options = {
				id: id,
				count: count,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setAmount(options);
		}

		function setVolume(event, count) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var volume = parseInt(target.attr('data-volume-product'));
			
			var metadata = _getMetadata(id);
			
			var options = {
				id: id,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setVolume(options);
			addActiveClass(event, options.id, options.volume);
		}

		function getAmount(id) {
			if (lkSelectProduct.getAmount(id) === -1) {
				return 1;
			}

			return lkSelectProduct.getAmount(id);
		}

		function addActiveClass(event, id, volume) {
			var selectsAll = document.querySelectorAll('[data-card-id="' + id + '"] .js-select-product__item');
			var targets = document.querySelectorAll('[data-card-id="' + id + '"] [data-volume-product="' + volume + '"]');

			for (var i = 0, len = selectsAll.length; i < len; i++) {
				if (angular.element(selectsAll[i]).hasClass('js-select-product__item--active')) {
					angular.element(selectsAll[i]).removeClass('js-select-product__item--active');
				}
			}
			
			for (var i = 0, len = targets.length; i < len; i++) {
				if (!angular.element(targets[i]).hasClass('js-select-product__item--active')) {
					angular.element(targets[i]).addClass('js-select-product__item--active');
				}
			}
		}
		
		function addToCart(event) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var amount = 1;
			var volume = parseInt(
				angular.element(
					document.querySelector('[data-card-id="' + id +'"] [data-volume-product]')
				)
				.attr('data-volume-product')
			);
			
			var metadata = _getMetadata(id);

			var options = {
				id: id,
				amount: amount,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}
			lkCart.addToCart(options);

			lkCart.changeVisibility(true);
		}

		function clickToOpen(templateId) {
			ngDialog.open({
				template: templateId,
				className: 'ngdialog-theme-default'
			});
		}
		
		////////////////////////////	
		function _getMetadata(id) {
			var title = angular.element(
				document.querySelector('[data-metadata-id="' + id + '"] [data-metadata-title]')
			).attr('data-metadata-title');

			var price = angular.element(
				document.querySelector('[data-metadata-id="' + id + '"] [data-metadata-price]')
			).attr('data-metadata-price');

			var image = angular.element(
				document.querySelector('[data-metadata-id="' + id + '"] [data-metadata-image]')
			).attr('data-metadata-image');

			var snippet = angular.element(
				document.querySelector('[data-metadata-id="' + id + '"] [data-metadata-snippet]')
			).attr('data-metadata-snippet');
			
			var metadata = {
				title: title,
				price: price,
				image: image,
				snippet: snippet	
			}
			
			return metadata;
		}
	}
})();
;
(function () {
	'use strict';

	angular
		.module('app')
		.controller('CourseItemPageController', CourseItemPageController);

	CourseItemPageController.$inject = [
		'ngDialog', 
		'lkFunctions', 
		'lkSelectProduct', 
		'lkInsets', 
		'lkCart'
	];

	function CourseItemPageController(
		ngDialog, 
		lkFunctions, 
		lkSelectProduct, 
		lkInsets, 
		lkCart) {
		var vm = this;

		vm.setAmount = setAmount;
		vm.setVolume = setVolume;
		vm.getAmount = getAmount;
		vm.addToCart = addToCart;

		vm.controlInset = lkInsets.control;
		vm.clickToOpen = clickToOpen;

		activate();

		function activate() {
			lkInsets.init();
		}

		function setAmount(event, count) {
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			
			var metadata = _getMetadata(id);
				
			var options = {
				id: id,
				count: count,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setAmount(options);
		}

		function setVolume(event, count) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var volume = parseInt(target.attr('data-volume-product'));
			
			var metadata = _getMetadata(id);
			
			var options = {
				id: id,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}

			lkSelectProduct.setVolume(options);
			addActiveClass(event, options.id, options.volume);
		}

		function getAmount(id) {
			if (lkSelectProduct.getAmount(id) === -1) {
				return 1;
			}

			return lkSelectProduct.getAmount(id);
		}

		function addActiveClass(event, id, volume) {
			var selectsAll = document.querySelectorAll('[data-card-id="' + id + '"] .js-select-product__item');
			var targets = document.querySelectorAll('[data-card-id="' + id + '"] [data-volume-product="' + volume + '"]');

			for (var i = 0, len = selectsAll.length; i < len; i++) {
				if (angular.element(selectsAll[i]).hasClass('js-select-product__item--active')) {
					angular.element(selectsAll[i]).removeClass('js-select-product__item--active');
				}
			}
			
			for (var i = 0, len = targets.length; i < len; i++) {
				if (!angular.element(targets[i]).hasClass('js-select-product__item--active')) {
					angular.element(targets[i]).addClass('js-select-product__item--active');
				}
			}
		}
		
		function addToCart(event) {
			var target = angular.element(event.target);
			var parent = angular.element(event.target).closest('[data-card-id]');
			var id = parseInt(parent.attr('data-card-id'));
			var amount = 1;
			var volume = parseInt(
				angular.element(
					document.querySelector('[data-card-id="' + id +'"] [data-volume-product]')
				)
				.attr('data-volume-product')
			);
			
			var metadata = _getMetadata(id);

			var options = {
				id: id,
				amount: amount,
				volume: volume,
				title: metadata.title,
				price: metadata.price,
				image: metadata.image,
				snippet: metadata.snippet
			}
			lkCart.addToCart(options);

			lkCart.changeVisibility(true);
		}

		function clickToOpen(templateId) {
			ngDialog.open({
				template: templateId,
				className: 'ngdialog-theme-default'
			});
		}
		
		////////////////////////////	
		function _getMetadata(id) {
			var title = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-title]')
			).attr('data-metadata-title');

			var price = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-price]')
			).attr('data-metadata-price');

			var image = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-image]')
			).attr('data-metadata-image');

			var snippet = angular.element(
				document.querySelector('[data-card-id="' + id + '"] [data-metadata-snippet]')
			).attr('data-metadata-snippet');
			
			var metadata = {
				title: title,
				price: price,
				image: image,
				snippet: snippet	
			}
			
			return metadata;
		}
	}
})();