$(document).ready(function(){
  $(".blog-menu ul li a").not(".partners_menu ul li a").click(function(){
    $("html, body").animate({
      scrollTop:$("section.partners > .container").offset().top
    }, 600);
  });


	// show-hide section8 rows on button click
	// $('#section8 .show-more').click(function(){
	// 	$(this).closest('.tab-pane').find('.goods-row-hidden').toggle();
	// 	if ($(this).hasClass('active')) {
	// 		$(this).removeClass('active');
	// 		$(this).html('ПОКАЗАТЬ ЕЩЁ');
	// 		// return false;
	// 	}
	// 	$(this).addClass('active');
	// 	if ($(this).hasClass('active')) {
	// 		$(this).html('скрыть');
	// 	}
	// });
	// corousel
	$('.carousel').carousel({
		interval: 5000
	});
	// scrol to section 
    $('nav ul > li > a, .mobile-button a').bind('click', function(event) {
        var anchor = $(this);
        var qaz = $(anchor.attr('href')).offset().top;
        $('html, body').stop().animate({
            scrollTop: qaz 
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
    // remove focus aftec click
    $('nav ul.nav > li > a, #section8 .good-item a, .count .minus, .count .plus, .carousel-control, .medcentr #medsec3 .next-slide').focus(function() {
        this.blur();
    });
    // go top button visibiliti
	$( window ).scroll(function() {
		if ($( window ).scrollTop() > '500'){
			$('#go-top').css("visibility", "visible" )
		}
		if ($( window ).scrollTop() < '1000'){
			$('#go-top').css("visibility", "hidden" )
		}
  });
  $( window ).scroll(function() {
    var ww = $(window).width();
    if ($('nav').hasClass('affix')) {
      $('#section1').css('margin', '51px 0 0 0');
    }
    else {
      $('#section1').css('margin', '0 0 0 0');
    }
  });
  // go top button
	$('#go-top').click(function() { 
		$('html,body').animate({scrollTop: $('body').offset().top}, 800);
		return false;
	});
	// open mobile menu 
	$('.mobile-header .menu .menu-icon-place .menu-ico').click(function() { 
		if ($(this).parent().hasClass('active')) {
			$(this).parent().removeClass('active');
		}
		else {
			$(this).parent().addClass('active');
		}
		$('.mobile-header .menu .dropdown').fadeToggle(400);
		return false;
	});
    // counter
    $('.count .minus').click(function() { 
    	var current = $(this).parent().find('input').attr('value');
    	if (current == 1) {
    		return false;
    	}
    	current = parseInt(current);
    	current = current - 1;
    	$(this).parent().find('input').attr('value', current);
    	return false;
    });
    $('.count .plus').click(function() { 
    	var current = $(this).parent().find('input').attr('value');
    	current = parseInt(current);
    	current = current + 1;
    	$(this).parent().find('input').attr('value', current);
    	return false;
    });
    // test popup
     $('.test-popup .next').click(function() { 
        var errors = 0;
        $('#step1 .test-item').each(function(i,elem) {
            if($(this).find("input:checked").size() == 0) {
                $(this).find('p').addClass('error');
                errors++;

            }
            else {
                $(this).find('p').removeClass('error');
            }
        });
        if (errors == 0) {
         	$('.test-popup .steps .step1 span').removeClass('active');
         	$('.test-popup .steps .step2 span').addClass('active');
         	$('.test-popup .step-content#step1').removeClass('active');
         	$('.test-popup .step-content#step2').addClass('active');
        }
        else {
            alert("Ответьте, пожалуйста, на все вопросы.")
        }
     	return false;
     });
     // test page
    $('.test-page .next').click(function() { 
        var errors = 0;
        $('#step1 .test-item').each(function(i,elem) {
            if($(this).find("input:checked").size() == 0) {
                $(this).find('p').addClass('error');
                errors++;

            }
            else {
                $(this).find('p').removeClass('error');
            }
        });
        if (errors == 0) {
            $('.test-page .steps .step1 span').removeClass('active');
            $('.test-page .steps .step2 span').addClass('active');
            $('.test-page .step-content#step1').removeClass('active');
            $('.test-page .step-content#step2').addClass('active');
            $('html,body').animate({scrollTop: $('body').offset().top}, 800);
        }
        else {
            alert("Ответьте, пожалуйста, на все вопросы.")
        }
        return false;
     });
      $('#section1 a.button').click(function() { 
     	$('.test-popup .steps .step2 span').removeClass('active');
     	$('.test-popup .steps .step1 span').addClass('active');
     	$('.test-popup .step-content#step2').removeClass('active');
     	$('.test-popup .step-content#step1').addClass('active');
     });
      // change slideshow caption
      function changeCapt() {
	      var photoCaption = $('#med-slideshow .carousel-inner > .item.active > img').attr('data-caption');
	      $('.medcentr #medsec1 .caption p').html(photoCaption);
      }
      changeCapt();
      $('#med-slideshow').on('slid.bs.carousel', function () {
			changeCapt();
	 });
      // chande review numbers
      function changeRevNumb() {
        var numbTotal = $('#med-reviews .carousel-inner > .item').length;
        $('.medcentr #medsec3 .next-slide .total-slides').html(numbTotal);
        var numbCurrent = $('#med-reviews .carousel-inner > .item.active').attr('data-number');
        $('.medcentr #medsec3 .next-slide .curent-slide').html(numbCurrent);
      }
      changeRevNumb();
      $('#med-reviews').on('slid.bs.carousel', function () {
        changeRevNumb();
      });


 
      ////prog code
      $(".product_modal").click(function(){
        var url = $(this).attr('data-url');
        var product_id = $(this).attr('data-product_id');
        var name = $(this).attr('data-name');
        $.ajax({
          url: '/bitrix/templates/vit/ajax/product_cart.php',
          type: 'POST',
          //dataType : "json",
          async   : true,  
           data: {
              product_id:product_id
           },
           
          beforeSend: function(){
            $('#preloader').fadeIn('fast');
          },
          success: function(data){
           
            $("#myModal .modal-body").html(data);
            $("#popp_button_" + product_id).click(function(){
             var quantity = $(this).parent().find('input[name="quantity"]').val();
             var product_name = $(this).parent().find('h3').text();

              $("#fastbuy-modal #form_text_5").attr('value', product_name);
              $("#fastbuy-modal #form_text_6").attr('value', quantity);
              $('#fastbuy-modal').modal('open');
            });

            $("#plus_"+ product_id +", #minus_"+ product_id ).click(function(){
            
              var class_name = $(this).attr('class');
              if(class_name == 'plus'){
                var val = parseInt($(this).prev().val());
                $(this).prev().val(val + 1);
              }else{
                var val = parseInt($(this).next().val());
                if(val == 1)
                  return false;
                $(this).next().val(val - 1);
              }
              return false;
            });


            //change url
            
            window.history.pushState({"pageTitle":name},"", url);
            $('#preloader').fadeOut('fast');
          },
          error: function(){
            $('#preloader').fadeOut('fast');
          }
        }); 

        return true;
      });
  

  

      $("#app_form form, #order_form form, #callback-modal form, #blog_add_comment, #test_form, #fastbuy-modal form, #ask_modal form").submit(function(){
          var form = $(this);

          var text = 'Спасибо!<br/> Ваша заявка принята принята.';
            if($(this).attr('id') == 'blog_add_comment')
              text = 'Ваш комментарий добавлен и ожидает модерации.';
            if($(this).attr('id') == 'test_form')
              text = 'ожидайте результатов.';

          $('#callback-modal, #ask_modal, #testModal, #fastbuy-modal, #myModal').modal('hide');
         
          $.ajax({
          url: form.attr('action'),
          type: form.attr('method'),
          //dataType : "json",
          async   : true,  
           data: new FormData(this),
            processData: false,
            contentType: false,
          beforeSend: function(){
            $('#preloader').fadeIn('fast');
          },
          success: function(data){
          
         
            $('#sucsess-modal').modal('show');

            $('#preloader').fadeOut('fast');
          },
          error: function(){
              if(form.closest('.modal').attr('id') == 'callback-modal'){
                  $('#callback-success').modal('show');
              }else{
                if(form.closest('.modal').attr('id') == 'ask_modal'){
                  $('#faq-success').modal('show');
                }else{
                   if(form.closest('.modal').attr('id') == 'fastbuy-modal'){
                      $('#order-success').modal('show');
                    }else{
                         $('#app-success').modal('show');
                    }
                }
              }
               
                
             
                form.find('input[type="text"], textarea').val('');
            $('#preloader').fadeOut('fast');
          }
        }); 

        return false;
      });



    $(".count .minus, .count .plus").click(function(){
      var class_name = $(this).attr('class');
      if(class_name == 'plus'){
        var val = parseInt($(this).prev().val());
        $(this).prev().val(val + 1);
      }else{
        var val = parseInt($(this).next().val());
        if(val == 1)
          return false;
        $(this).next().val(val - 1);
      }
      return false;
    });

    // $(".show-all").click(function(){
    //   var next_page = parseInt($(this).attr('id').split('_').pop());
    //   var button = $(this);
    //   if(window.location.href.indexOf('?') != -1)
    //     var url =  window.location.href + '&PAGEN_2=' + next_page;
    //   else
    //     var url =  window.location.href + '?PAGEN_2=' + next_page;
    //
    //    $.ajax({
    //       url:url,
    //       type: 'GET',
    //       //dataType : "json",
    //       async   : true,
    //        data: {},
    //         processData: false,
    //         contentType: false,
    //       beforeSend: function(){
    //         $('#preloader').fadeIn('fast');
    //       },
    //       success: function(data){
    //
    //         var html = canvart_strstr(data, '<!--blog_articles_here-->');
    //         html = canvart_strstr(html, '<!--blog_articles_here_end-->', true);
    //         html = canvart_strstr(html, '<div');
    //         console.log(html);
    //         button.prev().append(html);
    //         var count = (html.match(/article-prew/g) || []).length;
    //         if(count < 4)
    //           button.remove();
    //         button.attr('id','show_all_' +  next_page + 1);
    //          $('#preloader').fadeOut('fast');
    //       },
    //       error: function(){
    //
    //         $('#preloader').fadeOut('fast');
    //       }
    //     });
    //     return false;
    // });
});
// form mask
jQuery(function($){
   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
   $(".inputtext[name='form_text_3']").mask("+9 (999) 999-9999");
   $("#ssn").mask("999-99-9999");
});
// set navbar affix offset
  var ww = $(window).width()
  var topOffset = 157
  if (ww <= 990 && ww > 768) {
      var topOffset = 190
  }
  $('#nav').affix({
    offset: {
      top: topOffset
    }
  });

function canvart_strstr( haystack, needle, bool ) { // Find first occurrence of a string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.n
    var pos = 0;
    pos = haystack.indexOf( needle );
    if( pos == -1 ){
        return false;
    } else{
        if( bool ){
            return haystack.substr( 0, pos );
        } else{
            return haystack.slice( pos );
        }
    }
}