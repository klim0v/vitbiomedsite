## Docker installation

1. Install _docker_ and _docker-compose_ to your system
2. Add `127.0.0.1 vitbiomed.loc` to your `/etc/hosts` file
3. Copy `.env.docker` to `.env` in the project root
4. Generate a Docker bundle from the Compose file `docker-compose build`
5. Create and start containers with demon mode `docker-compose up -d`
6. Install composer dependencies `docker-compose exec -u user web composer install`
7. Set the application key `docker-compose exec -u user web php artisan key:generate`
8. Run the database migrations with `docker-compose exec -u user web php artisan migrate`
9. Create symlink for storage `cd public && ln -s ../storage/app/public storage`
10. Seed the database with records `docker-compose exec -u user web php artisan db:seed`
11. That's all - your application is accessible on <http://vitbiomedplus.loc>
12. Use command `docker-compose exec -u user web php artisan command` for **non-root** user
