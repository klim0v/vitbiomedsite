<?php

namespace Tests\Feature;

use App\Model\Shop\Product\Product;
use App\Model\Shop\Product\Variation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CartTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware;

    private $itemId;

    /** @inheritdoc */
    protected function setUp()
    {
        parent::setUp();
        $this->itemId = $this->createProductItem();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddProductTest(): void
    {
        $productVariationId = $this->itemId;

        $count = 2;
        $this
            ->withSession(['cart' => []])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $count,
        ]);

        $cartProducts = session('cart.products', []);

//        dd($cartProducts);

        $this->assertTrue(array_key_exists($productVariationId, $cartProducts));
        $this->assertEquals($count, $cartProducts[$productVariationId]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIncProductCountTest(): void
    {
        /** @var Product $product */
        $productVariationId = $this->itemId;
        $oldCount = 3;
        $count = 5;
        $this
            ->withSession(['cart.products' => [ $productVariationId => $oldCount, ]])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $count,
        ]);

        $cartProducts = session('cart.products', []);


        $this->assertTrue(array_key_exists($productVariationId, $cartProducts));
        $this->assertEquals($oldCount + $count, $cartProducts[$productVariationId]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDecProductTest(): void
    {
        /** @var Product $product */
        $productVariationId = $this->itemId;
        $oldCount = 3;
        $dec = -1;
        $this
            ->withSession(['cart.products' => [ $productVariationId => $oldCount, ]])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $dec,
        ]);

        $cartProducts = session('cart.products', []);

        $this->assertTrue(array_key_exists($productVariationId, $cartProducts));
        $this->assertEquals($oldCount + $dec, $cartProducts[$productVariationId]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteProductTest(): void
    {
        /** @var Product $product */
        $productVariationId = $this->itemId;
        $oldCount = 3;
        $dec = -$oldCount;
        $this
            ->withSession(['cart.products' => [ $productVariationId => $oldCount, ]])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $dec,
        ]);

        $cartProducts = session('cart.products', []);

        $this->assertFalse(array_key_exists($productVariationId, $cartProducts));
        $this->assertEmpty($cartProducts);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteMoreProductTest(): void
    {
        /** @var Product $product */
        $productVariationId = $this->itemId;
        $oldCount = 3;
        $dec = -99;
        $this
            ->withSession(['cart.products' => [ $productVariationId => $oldCount, ]])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $dec,
        ]);

        $cartProducts = session('cart.products', []);

        $this->assertFalse(array_key_exists($productVariationId, $cartProducts));
        $this->assertEmpty($cartProducts);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteNoneProductTest(): void
    {
        /** @var Product $product */
        $productVariationId = $this->itemId;
        $oldCount = 3;
        $dec = -$oldCount;
        $this
            ->withSession([])
            ->post(route('cart.change.product'), [
                'id' => $productVariationId,
                'count' => $dec,
        ]);

        $cartProducts = session('cart.products', []);

        $this->assertFalse(array_key_exists($productVariationId, $cartProducts));
        $this->assertEmpty($cartProducts);
    }

    /**
     * @return int
     */
    protected function createProductItem(): int
    {
        /** @var Product $product */
        $product = factory(Product::class)->create();
        /** @var Collection $productVariations */
        $productVariations = factory(Variation::class, random_int(1, 3))
            ->create(['product_id' => $product->id]);
        return $productVariations->first()->id;
    }
}
