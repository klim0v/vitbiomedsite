<?php

use Tests\TestCase;


/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.11.17
 * Time: 0:22
 */
class SphinxTest extends TestCase
{

    public  function testConnect()
    {
        $conn = \DB::connection('sphinx');
        $items = $conn->select("SELECT * FROM blog_index WHERE MATCH ('новинка')   ");
        $this->assertInternalType('array',$items);
        $this->assertGreaterThan(0,count($items));

    }

}